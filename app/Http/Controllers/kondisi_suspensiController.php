<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createkondisi_suspensiRequest;
use App\Http\Requests\Updatekondisi_suspensiRequest;
use App\Repositories\kondisi_suspensiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class kondisi_suspensiController extends AppBaseController
{
    /** @var  kondisi_suspensiRepository */
    private $kondisiSuspensiRepository;

    public function __construct(kondisi_suspensiRepository $kondisiSuspensiRepo)
    {
        $this->middleware('auth');
        $this->kondisiSuspensiRepository = $kondisiSuspensiRepo;
    }

    /**
     * Display a listing of the kondisi_suspensi.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $kondisiSuspensis = $this->kondisiSuspensiRepository->paginate(10);

        return view('kondisi_suspensis.index')
            ->with('kondisiSuspensis', $kondisiSuspensis);
    }

    /**
     * Show the form for creating a new kondisi_suspensi.
     *
     * @return Response
     */
    public function create()
    {
        return view('kondisi_suspensis.create');
    }

    /**
     * Store a newly created kondisi_suspensi in storage.
     *
     * @param Createkondisi_suspensiRequest $request
     *
     * @return Response
     */
    public function store(Createkondisi_suspensiRequest $request)
    {
        $input = $request->all();

        $kondisiSuspensi = $this->kondisiSuspensiRepository->create($input);

        Flash::success('Kondisi Suspensi saved successfully.');

        return redirect(route('kondisiSuspensis.index'));
    }

    /**
     * Display the specified kondisi_suspensi.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $kondisiSuspensi = $this->kondisiSuspensiRepository->find($id);

        if (empty($kondisiSuspensi)) {
            Flash::error('Kondisi Suspensi not found');

            return redirect(route('kondisiSuspensis.index'));
        }

        return view('kondisi_suspensis.show')->with('kondisiSuspensi', $kondisiSuspensi);
    }

    /**
     * Show the form for editing the specified kondisi_suspensi.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $kondisiSuspensi = $this->kondisiSuspensiRepository->find($id);

        if (empty($kondisiSuspensi)) {
            Flash::error('Kondisi Suspensi not found');

            return redirect(route('kondisiSuspensis.index'));
        }

        return view('kondisi_suspensis.edit')->with('kondisiSuspensi', $kondisiSuspensi);
    }

    /**
     * Update the specified kondisi_suspensi in storage.
     *
     * @param int $id
     * @param Updatekondisi_suspensiRequest $request
     *
     * @return Response
     */
    public function update($id, Updatekondisi_suspensiRequest $request)
    {
        $kondisiSuspensi = $this->kondisiSuspensiRepository->find($id);

        if (empty($kondisiSuspensi)) {
            Flash::error('Kondisi Suspensi not found');

            return redirect(route('kondisiSuspensis.index'));
        }

        $kondisiSuspensi = $this->kondisiSuspensiRepository->update($request->all(), $id);

        Flash::success('Kondisi Suspensi updated successfully.');

        return redirect(route('kondisiSuspensis.index'));
    }

    /**
     * Remove the specified kondisi_suspensi from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $kondisiSuspensi = $this->kondisiSuspensiRepository->find($id);

        if (empty($kondisiSuspensi)) {
            Flash::error('Kondisi Suspensi not found');

            return redirect(route('kondisiSuspensis.index'));
        }

        $this->kondisiSuspensiRepository->delete($id);

        Flash::success('Kondisi Suspensi deleted successfully.');

        return redirect(route('kondisiSuspensis.index'));
    }
}
