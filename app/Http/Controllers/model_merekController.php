<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createmodel_merekRequest;
use App\Http\Requests\Updatemodel_merekRequest;
use App\Repositories\model_merekRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\model_merek;
use App\Models\merek;

class model_merekController extends AppBaseController
{
    /** @var  model_merekRepository */
    private $modelMerekRepository;

    public function __construct(model_merekRepository $modelMerekRepo)
    {
        $this->middleware('auth');
        $this->modelMerekRepository = $modelMerekRepo;
    }

    /**
     * Display a listing of the model_merek.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $modelMereks = model_merek::with('get_merek')->paginate(10);
        // $modelMereks = $this->modelMerekRepository->paginate(10);

        return view('model_mereks.index')
            ->with('modelMereks', $modelMereks);
    }

    /**
     * Show the form for creating a new model_merek.
     *
     * @return Response
     */
    public function create()
    {
        $mereks = merek::all();
        return view('model_mereks.create',compact('mereks'));
    }

    /**
     * Store a newly created model_merek in storage.
     *
     * @param Createmodel_merekRequest $request
     *
     * @return Response
     */
    public function store(Createmodel_merekRequest $request)
    {
        $input = $request->all();

        $modelMerek = $this->modelMerekRepository->create($input);

        Flash::success('Model Merek saved successfully.');

        return redirect(route('modelMereks.index'));
    }

    /**
     * Display the specified model_merek.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $modelMerek = $this->modelMerekRepository->find($id);

        if (empty($modelMerek)) {
            Flash::error('Model Merek not found');

            return redirect(route('modelMereks.index'));
        }

        return view('model_mereks.show')->with('modelMerek', $modelMerek);
    }

    /**
     * Show the form for editing the specified model_merek.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $modelMerek = $this->modelMerekRepository->find($id);
        $mereks = merek::all();

        if (empty($modelMerek)) {
            Flash::error('Model Merek not found');

            return redirect(route('modelMereks.index'));
        }

        return view('model_mereks.edit', compact('modelMerek','mereks'));
    }

    /**
     * Update the specified model_merek in storage.
     *
     * @param int $id
     * @param Updatemodel_merekRequest $request
     *
     * @return Response
     */
    public function update($id, Updatemodel_merekRequest $request)
    {
        $modelMerek = $this->modelMerekRepository->find($id);

        if (empty($modelMerek)) {
            Flash::error('Model Merek not found');

            return redirect(route('modelMereks.index'));
        }

        $modelMerek = $this->modelMerekRepository->update($request->all(), $id);

        Flash::success('Model Merek updated successfully.');

        return redirect(route('modelMereks.index'));
    }

    /**
     * Remove the specified model_merek from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $modelMerek = $this->modelMerekRepository->find($id);

        if (empty($modelMerek)) {
            Flash::error('Model Merek not found');

            return redirect(route('modelMereks.index'));
        }

        $this->modelMerekRepository->delete($id);

        Flash::success('Model Merek deleted successfully.');

        return redirect(route('modelMereks.index'));
    }
}
