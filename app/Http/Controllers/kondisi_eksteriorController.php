<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createkondisi_eksteriorRequest;
use App\Http\Requests\Updatekondisi_eksteriorRequest;
use App\Repositories\kondisi_eksteriorRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class kondisi_eksteriorController extends AppBaseController
{
    /** @var  kondisi_eksteriorRepository */
    private $kondisiEksteriorRepository;

    public function __construct(kondisi_eksteriorRepository $kondisiEksteriorRepo)
    {
        $this->middleware('auth');
        $this->kondisiEksteriorRepository = $kondisiEksteriorRepo;
    }

    /**
     * Display a listing of the kondisi_eksterior.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $kondisiEksteriors = $this->kondisiEksteriorRepository->paginate(10);

        return view('kondisi_eksteriors.index')
            ->with('kondisiEksteriors', $kondisiEksteriors);
    }

    /**
     * Show the form for creating a new kondisi_eksterior.
     *
     * @return Response
     */
    public function create()
    {
        return view('kondisi_eksteriors.create');
    }

    /**
     * Store a newly created kondisi_eksterior in storage.
     *
     * @param Createkondisi_eksteriorRequest $request
     *
     * @return Response
     */
    public function store(Createkondisi_eksteriorRequest $request)
    {
        $input = $request->all();

        $kondisiEksterior = $this->kondisiEksteriorRepository->create($input);

        Flash::success('Kondisi Eksterior saved successfully.');

        return redirect(route('kondisiEksteriors.index'));
    }

    /**
     * Display the specified kondisi_eksterior.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $kondisiEksterior = $this->kondisiEksteriorRepository->find($id);

        if (empty($kondisiEksterior)) {
            Flash::error('Kondisi Eksterior not found');

            return redirect(route('kondisiEksteriors.index'));
        }

        return view('kondisi_eksteriors.show')->with('kondisiEksterior', $kondisiEksterior);
    }

    /**
     * Show the form for editing the specified kondisi_eksterior.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $kondisiEksterior = $this->kondisiEksteriorRepository->find($id);

        if (empty($kondisiEksterior)) {
            Flash::error('Kondisi Eksterior not found');

            return redirect(route('kondisiEksteriors.index'));
        }

        return view('kondisi_eksteriors.edit')->with('kondisiEksterior', $kondisiEksterior);
    }

    /**
     * Update the specified kondisi_eksterior in storage.
     *
     * @param int $id
     * @param Updatekondisi_eksteriorRequest $request
     *
     * @return Response
     */
    public function update($id, Updatekondisi_eksteriorRequest $request)
    {
        $kondisiEksterior = $this->kondisiEksteriorRepository->find($id);

        if (empty($kondisiEksterior)) {
            Flash::error('Kondisi Eksterior not found');

            return redirect(route('kondisiEksteriors.index'));
        }

        $kondisiEksterior = $this->kondisiEksteriorRepository->update($request->all(), $id);

        Flash::success('Kondisi Eksterior updated successfully.');

        return redirect(route('kondisiEksteriors.index'));
    }

    /**
     * Remove the specified kondisi_eksterior from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $kondisiEksterior = $this->kondisiEksteriorRepository->find($id);

        if (empty($kondisiEksterior)) {
            Flash::error('Kondisi Eksterior not found');

            return redirect(route('kondisiEksteriors.index'));
        }

        $this->kondisiEksteriorRepository->delete($id);

        Flash::success('Kondisi Eksterior deleted successfully.');

        return redirect(route('kondisiEksteriors.index'));
    }
}
