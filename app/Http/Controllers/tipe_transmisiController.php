<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createtipe_transmisiRequest;
use App\Http\Requests\Updatetipe_transmisiRequest;
use App\Repositories\tipe_transmisiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class tipe_transmisiController extends AppBaseController
{
    /** @var  tipe_transmisiRepository */
    private $tipeTransmisiRepository;

    public function __construct(tipe_transmisiRepository $tipeTransmisiRepo)
    {
        $this->middleware('auth');
        $this->tipeTransmisiRepository = $tipeTransmisiRepo;
    }

    /**
     * Display a listing of the tipe_transmisi.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $tipeTransmisis = $this->tipeTransmisiRepository->paginate(10);

        return view('tipe_transmisis.index')
            ->with('tipeTransmisis', $tipeTransmisis);
    }

    /**
     * Show the form for creating a new tipe_transmisi.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipe_transmisis.create');
    }

    /**
     * Store a newly created tipe_transmisi in storage.
     *
     * @param Createtipe_transmisiRequest $request
     *
     * @return Response
     */
    public function store(Createtipe_transmisiRequest $request)
    {
        $input = $request->all();

        $tipeTransmisi = $this->tipeTransmisiRepository->create($input);

        Flash::success('Tipe Transmisi saved successfully.');

        return redirect(route('tipeTransmisis.index'));
    }

    /**
     * Display the specified tipe_transmisi.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipeTransmisi = $this->tipeTransmisiRepository->find($id);

        if (empty($tipeTransmisi)) {
            Flash::error('Tipe Transmisi not found');

            return redirect(route('tipeTransmisis.index'));
        }

        return view('tipe_transmisis.show')->with('tipeTransmisi', $tipeTransmisi);
    }

    /**
     * Show the form for editing the specified tipe_transmisi.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipeTransmisi = $this->tipeTransmisiRepository->find($id);

        if (empty($tipeTransmisi)) {
            Flash::error('Tipe Transmisi not found');

            return redirect(route('tipeTransmisis.index'));
        }

        return view('tipe_transmisis.edit')->with('tipeTransmisi', $tipeTransmisi);
    }

    /**
     * Update the specified tipe_transmisi in storage.
     *
     * @param int $id
     * @param Updatetipe_transmisiRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetipe_transmisiRequest $request)
    {
        $tipeTransmisi = $this->tipeTransmisiRepository->find($id);

        if (empty($tipeTransmisi)) {
            Flash::error('Tipe Transmisi not found');

            return redirect(route('tipeTransmisis.index'));
        }

        $tipeTransmisi = $this->tipeTransmisiRepository->update($request->all(), $id);

        Flash::success('Tipe Transmisi updated successfully.');

        return redirect(route('tipeTransmisis.index'));
    }

    /**
     * Remove the specified tipe_transmisi from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipeTransmisi = $this->tipeTransmisiRepository->find($id);

        if (empty($tipeTransmisi)) {
            Flash::error('Tipe Transmisi not found');

            return redirect(route('tipeTransmisis.index'));
        }

        $this->tipeTransmisiRepository->delete($id);

        Flash::success('Tipe Transmisi deleted successfully.');

        return redirect(route('tipeTransmisis.index'));
    }
}
