<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createkondisi_kemudiRequest;
use App\Http\Requests\Updatekondisi_kemudiRequest;
use App\Repositories\kondisi_kemudiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class kondisi_kemudiController extends AppBaseController
{
    /** @var  kondisi_kemudiRepository */
    private $kondisiKemudiRepository;

    public function __construct(kondisi_kemudiRepository $kondisiKemudiRepo)
    {
        $this->middleware('auth');
        $this->kondisiKemudiRepository = $kondisiKemudiRepo;
    }

    /**
     * Display a listing of the kondisi_kemudi.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $kondisiKemudis = $this->kondisiKemudiRepository->paginate(10);

        return view('kondisi_kemudis.index')
            ->with('kondisiKemudis', $kondisiKemudis);
    }

    /**
     * Show the form for creating a new kondisi_kemudi.
     *
     * @return Response
     */
    public function create()
    {
        return view('kondisi_kemudis.create');
    }

    /**
     * Store a newly created kondisi_kemudi in storage.
     *
     * @param Createkondisi_kemudiRequest $request
     *
     * @return Response
     */
    public function store(Createkondisi_kemudiRequest $request)
    {
        $input = $request->all();

        $kondisiKemudi = $this->kondisiKemudiRepository->create($input);

        Flash::success('Kondisi Kemudi saved successfully.');

        return redirect(route('kondisiKemudis.index'));
    }

    /**
     * Display the specified kondisi_kemudi.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $kondisiKemudi = $this->kondisiKemudiRepository->find($id);

        if (empty($kondisiKemudi)) {
            Flash::error('Kondisi Kemudi not found');

            return redirect(route('kondisiKemudis.index'));
        }

        return view('kondisi_kemudis.show')->with('kondisiKemudi', $kondisiKemudi);
    }

    /**
     * Show the form for editing the specified kondisi_kemudi.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $kondisiKemudi = $this->kondisiKemudiRepository->find($id);

        if (empty($kondisiKemudi)) {
            Flash::error('Kondisi Kemudi not found');

            return redirect(route('kondisiKemudis.index'));
        }

        return view('kondisi_kemudis.edit')->with('kondisiKemudi', $kondisiKemudi);
    }

    /**
     * Update the specified kondisi_kemudi in storage.
     *
     * @param int $id
     * @param Updatekondisi_kemudiRequest $request
     *
     * @return Response
     */
    public function update($id, Updatekondisi_kemudiRequest $request)
    {
        $kondisiKemudi = $this->kondisiKemudiRepository->find($id);

        if (empty($kondisiKemudi)) {
            Flash::error('Kondisi Kemudi not found');

            return redirect(route('kondisiKemudis.index'));
        }

        $kondisiKemudi = $this->kondisiKemudiRepository->update($request->all(), $id);

        Flash::success('Kondisi Kemudi updated successfully.');

        return redirect(route('kondisiKemudis.index'));
    }

    /**
     * Remove the specified kondisi_kemudi from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $kondisiKemudi = $this->kondisiKemudiRepository->find($id);

        if (empty($kondisiKemudi)) {
            Flash::error('Kondisi Kemudi not found');

            return redirect(route('kondisiKemudis.index'));
        }

        $this->kondisiKemudiRepository->delete($id);

        Flash::success('Kondisi Kemudi deleted successfully.');

        return redirect(route('kondisiKemudis.index'));
    }
}
