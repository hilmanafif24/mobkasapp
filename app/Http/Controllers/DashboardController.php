<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\mobil_bekas;
use App\Models\perhitungan;
use App\User;

class DashboardController extends Controller
{
    public function __construct()
	{
	    $this->middleware('auth');
	}

	public function index()
    {
    	$jml_mobkas 	= mobil_bekas::count();
    	$jml_hitung	 	= perhitungan::count();
    	$jml_user		= user::count();
        return view('dashboard', compact('jml_mobkas','jml_hitung','jml_user'));
    }
}
