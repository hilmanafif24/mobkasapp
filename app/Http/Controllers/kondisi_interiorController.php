<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createkondisi_interiorRequest;
use App\Http\Requests\Updatekondisi_interiorRequest;
use App\Repositories\kondisi_interiorRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class kondisi_interiorController extends AppBaseController
{
    /** @var  kondisi_interiorRepository */
    private $kondisiInteriorRepository;

    public function __construct(kondisi_interiorRepository $kondisiInteriorRepo)
    {
        $this->middleware('auth');
        $this->kondisiInteriorRepository = $kondisiInteriorRepo;
    }

    /**
     * Display a listing of the kondisi_interior.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $kondisiInteriors = $this->kondisiInteriorRepository->paginate(10);

        return view('kondisi_interiors.index')
            ->with('kondisiInteriors', $kondisiInteriors);
    }

    /**
     * Show the form for creating a new kondisi_interior.
     *
     * @return Response
     */
    public function create()
    {
        return view('kondisi_interiors.create');
    }

    /**
     * Store a newly created kondisi_interior in storage.
     *
     * @param Createkondisi_interiorRequest $request
     *
     * @return Response
     */
    public function store(Createkondisi_interiorRequest $request)
    {
        $input = $request->all();

        $kondisiInterior = $this->kondisiInteriorRepository->create($input);

        Flash::success('Kondisi Interior saved successfully.');

        return redirect(route('kondisiInteriors.index'));
    }

    /**
     * Display the specified kondisi_interior.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $kondisiInterior = $this->kondisiInteriorRepository->find($id);

        if (empty($kondisiInterior)) {
            Flash::error('Kondisi Interior not found');

            return redirect(route('kondisiInteriors.index'));
        }

        return view('kondisi_interiors.show')->with('kondisiInterior', $kondisiInterior);
    }

    /**
     * Show the form for editing the specified kondisi_interior.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $kondisiInterior = $this->kondisiInteriorRepository->find($id);

        if (empty($kondisiInterior)) {
            Flash::error('Kondisi Interior not found');

            return redirect(route('kondisiInteriors.index'));
        }

        return view('kondisi_interiors.edit')->with('kondisiInterior', $kondisiInterior);
    }

    /**
     * Update the specified kondisi_interior in storage.
     *
     * @param int $id
     * @param Updatekondisi_interiorRequest $request
     *
     * @return Response
     */
    public function update($id, Updatekondisi_interiorRequest $request)
    {
        $kondisiInterior = $this->kondisiInteriorRepository->find($id);

        if (empty($kondisiInterior)) {
            Flash::error('Kondisi Interior not found');

            return redirect(route('kondisiInteriors.index'));
        }

        $kondisiInterior = $this->kondisiInteriorRepository->update($request->all(), $id);

        Flash::success('Kondisi Interior updated successfully.');

        return redirect(route('kondisiInteriors.index'));
    }

    /**
     * Remove the specified kondisi_interior from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $kondisiInterior = $this->kondisiInteriorRepository->find($id);

        if (empty($kondisiInterior)) {
            Flash::error('Kondisi Interior not found');

            return redirect(route('kondisiInteriors.index'));
        }

        $this->kondisiInteriorRepository->delete($id);

        Flash::success('Kondisi Interior deleted successfully.');

        return redirect(route('kondisiInteriors.index'));
    }
}
