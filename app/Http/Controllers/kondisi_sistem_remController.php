<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createkondisi_sistem_remRequest;
use App\Http\Requests\Updatekondisi_sistem_remRequest;
use App\Repositories\kondisi_sistem_remRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class kondisi_sistem_remController extends AppBaseController
{
    /** @var  kondisi_sistem_remRepository */
    private $kondisiSistemRemRepository;

    public function __construct(kondisi_sistem_remRepository $kondisiSistemRemRepo)
    {
        $this->middleware('auth');
        $this->kondisiSistemRemRepository = $kondisiSistemRemRepo;
    }

    /**
     * Display a listing of the kondisi_sistem_rem.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $kondisiSistemRems = $this->kondisiSistemRemRepository->paginate(10);

        return view('kondisi_sistem_rems.index')
            ->with('kondisiSistemRems', $kondisiSistemRems);
    }

    /**
     * Show the form for creating a new kondisi_sistem_rem.
     *
     * @return Response
     */
    public function create()
    {
        return view('kondisi_sistem_rems.create');
    }

    /**
     * Store a newly created kondisi_sistem_rem in storage.
     *
     * @param Createkondisi_sistem_remRequest $request
     *
     * @return Response
     */
    public function store(Createkondisi_sistem_remRequest $request)
    {
        $input = $request->all();

        $kondisiSistemRem = $this->kondisiSistemRemRepository->create($input);

        Flash::success('Kondisi Sistem Rem saved successfully.');

        return redirect(route('kondisiSistemRems.index'));
    }

    /**
     * Display the specified kondisi_sistem_rem.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $kondisiSistemRem = $this->kondisiSistemRemRepository->find($id);

        if (empty($kondisiSistemRem)) {
            Flash::error('Kondisi Sistem Rem not found');

            return redirect(route('kondisiSistemRems.index'));
        }

        return view('kondisi_sistem_rems.show')->with('kondisiSistemRem', $kondisiSistemRem);
    }

    /**
     * Show the form for editing the specified kondisi_sistem_rem.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $kondisiSistemRem = $this->kondisiSistemRemRepository->find($id);

        if (empty($kondisiSistemRem)) {
            Flash::error('Kondisi Sistem Rem not found');

            return redirect(route('kondisiSistemRems.index'));
        }

        return view('kondisi_sistem_rems.edit')->with('kondisiSistemRem', $kondisiSistemRem);
    }

    /**
     * Update the specified kondisi_sistem_rem in storage.
     *
     * @param int $id
     * @param Updatekondisi_sistem_remRequest $request
     *
     * @return Response
     */
    public function update($id, Updatekondisi_sistem_remRequest $request)
    {
        $kondisiSistemRem = $this->kondisiSistemRemRepository->find($id);

        if (empty($kondisiSistemRem)) {
            Flash::error('Kondisi Sistem Rem not found');

            return redirect(route('kondisiSistemRems.index'));
        }

        $kondisiSistemRem = $this->kondisiSistemRemRepository->update($request->all(), $id);

        Flash::success('Kondisi Sistem Rem updated successfully.');

        return redirect(route('kondisiSistemRems.index'));
    }

    /**
     * Remove the specified kondisi_sistem_rem from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $kondisiSistemRem = $this->kondisiSistemRemRepository->find($id);

        if (empty($kondisiSistemRem)) {
            Flash::error('Kondisi Sistem Rem not found');

            return redirect(route('kondisiSistemRems.index'));
        }

        $this->kondisiSistemRemRepository->delete($id);

        Flash::success('Kondisi Sistem Rem deleted successfully.');

        return redirect(route('kondisiSistemRems.index'));
    }
}
