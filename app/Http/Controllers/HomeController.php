<?php

namespace App\Http\Controllers;

// use App\Http\Requests\CreateestimasiRequest;
// use App\Http\Controllers\AppBaseController;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Estimasi;

use App\Models\merek;
use App\Models\model_merek;
use App\Models\tipe_model;
use App\Models\warna;
use App\Models\tipe_transmisi;
use App\Models\kondisi_mesin;
use App\Models\kondisi_sistem_rem;
use App\Models\kondisi_kemudi;
use App\Models\kondisi_suspensi;
use App\Models\kondisi_eksterior;
use App\Models\kondisi_interior;
use App\Models\kondisi_dokumen;
use App\Models\perhitungan;
use DB;

// use Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $mereks = merek::all();
        // $models = model_merek::all();
        // $tipes = tipe_model::all();
        $mereks     = DB::table('mereks')->pluck('nama_merek','id')->all();
        $warnas     = warna::all();
        $transmisis = tipe_transmisi::all();
        $mesins     = kondisi_mesin::all();
        $rems       = kondisi_sistem_rem::all();
        $kemudis    = kondisi_kemudi::all();
        $suspensis  = kondisi_suspensi::all();
        $eksteriors = kondisi_eksterior::all();
        $interiors  = kondisi_interior::all();
        $dokumens   = kondisi_dokumen::all();
        return view('frontend.home', compact('mereks','warnas','transmisis','mesins',
                                            'rems','kemudis','suspensis','eksteriors',
                                            'interiors', 'dokumens') );
    }

    public function selectAjax(Request $request)
    {
        if($request->ajax())
        {
            $models = DB::table('model_mereks')->where('merek_id',$request->merek_id)->pluck('nama_model','id')->all();
            $data = view('frontend.ajax-select',compact('models'))->render();
            return response()->json(['options'=>$data]);
        }
    }

    public function selectAjax_2(Request $request)
    {
        if($request->ajax())
        {
            $tipes = DB::table('tipe_models')->where('model_merek_id',$request->model_merek_id)->pluck('nama_tipe','id')->all();
            $data = view('frontend.ajax-select_2',compact('tipes'))->render();
            return response()->json(['options'=>$data]);
        }
    }

    public function pilihmesin(Request $request)
    {
        // if($request->ajax())
        // {
        //     $data = 'tes';
        //     return view('frontend.hasil_mesin',compact('data'));
        // }
        return view('frontend.hasil_mesin');
    }

    public function proses_estimasi(Request $request)
    {
        $input = new Estimasi();
        $input->merek_id        = $request->merek_id;
        $input->model_merek_id  = $request->model_merek_id;
        $input->tipe_model_id   = $request->tipe_model_id;
        $input->warna_id        = $request->warna_id;
        $input->tahun           = $request->tahun;
        $input->transmisi_id    = $request->transmisi_id;
        $input->mesin_id        = $request->mesin_id;
        $input->sis_rem_id      = $request->sis_rem_id;
        $input->kemudi_id       = $request->kemudi_id;
        $input->suspensi_id     = $request->suspensi_id;
        $input->eksterior_id    = $request->eksterior_id;
        $input->interior_id     = $request->interior_id;
        $input->dokumen_id      = $request->dokumen_id;
        // Estimasi::create($request->all());
        // $input = $request->all();

        //menghitung estimasi harga dari tabel perhitungan
        $rlb = perhitungan::orderBy('id','desc')->first();
        $b1 = $rlb->b1 * ($input->merek_id);
        $b2 = $rlb->b2 * ($input->model_merek_id);
        $b3 = $rlb->b3 * ($input->tipe_model_id);
        $b4 = $rlb->b4 * ($input->warna_id);
        $b5 = $rlb->b5 * ($input->tahun);
        $b6 = $rlb->b6 * ($input->transmisi_id);
        $b7 = $rlb->b7 * ($input->mesin_id);
        $b8 = $rlb->b8 * ($input->sis_rem_id);
        $b9 = $rlb->b9 * ($input->kemudi_id);
        $b10 = $rlb->b10 * ($input->suspensi_id);
        $b11 = $rlb->b11 * ($input->eksterior_id);
        $b12 = $rlb->b12 * ($input->interior_id);
        $b13 = $rlb->b13 * ($input->dokumen_id);

        $input->harga = $rlb->b0 + $b1 + $b2 + $b3 + $b4 + $b5 + $b6 + $b7 + $b8 + $b9 + $b10 + $b11 + $b12 + $b13;
        $input->save();
        // Estimasi::create($input);
        return redirect('result');
    }

    public function result()
    {
        // $hargas = Estimasi::all()->last()->pluck('harga');
        // $hargas = Estimasi::all();
        // $hargas = number_format($allharga['harga'])$allharga->harga; ,0,",","."
        $allharga = Estimasi::orderBy('id','desc')->with('get_merek')->first();
        $hargas = $allharga->harga;
        // $hargas = number_format($allharga['harga'],2,",",".");
        $tahun = $allharga->tahun;
        $mereks = $allharga->get_merek->nama_merek;
        $models = $allharga->get_model->nama_model;
        $tipes   = $allharga->get_tipe->nama_tipe; 
        // $mobilBekas = mobil_bekas::with('get_merek')
        return view('frontend.result', compact('hargas','mereks','models','tipes','tahun'));
    }

}
