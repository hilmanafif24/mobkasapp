<?php

namespace App\Http\Controllers;

use App\Repositories\estimasiRepository;
use Illuminate\Http\Request;
use App\Models\Estimasi;

class laporanController extends Controller
{
    
    private $estimasiRepository;

    public function __construct(estimasiRepository $estimasiRepo)
	{
	    $this->middleware('auth');
	    $this->estimasiRepository = $estimasiRepo;
	}
	public function index()
	{
		// $estimasis = Estimasi::all()->paginate(10);
		$estimasis = Estimasi::orderBy('id','DESC')->paginate(10);
		return view('laporans.estimasi', compact('estimasis'));
	}
}
