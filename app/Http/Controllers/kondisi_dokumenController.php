<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createkondisi_dokumenRequest;
use App\Http\Requests\Updatekondisi_dokumenRequest;
use App\Repositories\kondisi_dokumenRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class kondisi_dokumenController extends AppBaseController
{
    /** @var  kondisi_dokumenRepository */
    private $kondisiDokumenRepository;

    public function __construct(kondisi_dokumenRepository $kondisiDokumenRepo)
    {
        $this->middleware('auth');
        $this->kondisiDokumenRepository = $kondisiDokumenRepo;
    }

    /**
     * Display a listing of the kondisi_dokumen.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $kondisiDokumens = $this->kondisiDokumenRepository->paginate(10);

        return view('kondisi_dokumens.index')
            ->with('kondisiDokumens', $kondisiDokumens);
    }

    /**
     * Show the form for creating a new kondisi_dokumen.
     *
     * @return Response
     */
    public function create()
    {
        return view('kondisi_dokumens.create');
    }

    /**
     * Store a newly created kondisi_dokumen in storage.
     *
     * @param Createkondisi_dokumenRequest $request
     *
     * @return Response
     */
    public function store(Createkondisi_dokumenRequest $request)
    {
        $input = $request->all();

        $kondisiDokumen = $this->kondisiDokumenRepository->create($input);

        Flash::success('Kondisi Dokumen saved successfully.');

        return redirect(route('kondisiDokumens.index'));
    }

    /**
     * Display the specified kondisi_dokumen.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $kondisiDokumen = $this->kondisiDokumenRepository->find($id);

        if (empty($kondisiDokumen)) {
            Flash::error('Kondisi Dokumen not found');

            return redirect(route('kondisiDokumens.index'));
        }

        return view('kondisi_dokumens.show')->with('kondisiDokumen', $kondisiDokumen);
    }

    /**
     * Show the form for editing the specified kondisi_dokumen.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $kondisiDokumen = $this->kondisiDokumenRepository->find($id);

        if (empty($kondisiDokumen)) {
            Flash::error('Kondisi Dokumen not found');

            return redirect(route('kondisiDokumens.index'));
        }

        return view('kondisi_dokumens.edit')->with('kondisiDokumen', $kondisiDokumen);
    }

    /**
     * Update the specified kondisi_dokumen in storage.
     *
     * @param int $id
     * @param Updatekondisi_dokumenRequest $request
     *
     * @return Response
     */
    public function update($id, Updatekondisi_dokumenRequest $request)
    {
        $kondisiDokumen = $this->kondisiDokumenRepository->find($id);

        if (empty($kondisiDokumen)) {
            Flash::error('Kondisi Dokumen not found');

            return redirect(route('kondisiDokumens.index'));
        }

        $kondisiDokumen = $this->kondisiDokumenRepository->update($request->all(), $id);

        Flash::success('Kondisi Dokumen updated successfully.');

        return redirect(route('kondisiDokumens.index'));
    }

    /**
     * Remove the specified kondisi_dokumen from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $kondisiDokumen = $this->kondisiDokumenRepository->find($id);

        if (empty($kondisiDokumen)) {
            Flash::error('Kondisi Dokumen not found');

            return redirect(route('kondisiDokumens.index'));
        }

        $this->kondisiDokumenRepository->delete($id);

        Flash::success('Kondisi Dokumen deleted successfully.');

        return redirect(route('kondisiDokumens.index'));
    }
}
