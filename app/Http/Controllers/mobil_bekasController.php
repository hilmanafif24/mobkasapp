<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createmobil_bekasRequest;
use App\Http\Requests\Updatemobil_bekasRequest;
use App\Repositories\mobil_bekasRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\mobil_bekas;
use App\Models\merek;
use App\Models\model_merek;
use App\Models\tipe_model;
use App\Models\warna;
use App\Models\tipe_transmisi;
use App\Models\kondisi_mesin;
use App\Models\kondisi_sistem_rem;
use App\Models\kondisi_kemudi;
use App\Models\kondisi_suspensi;
use App\Models\kondisi_eksterior;
use App\Models\kondisi_interior;
use App\Models\kondisi_dokumen;
use DB;

class mobil_bekasController extends AppBaseController
{
    /** @var  mobil_bekasRepository */
    private $mobilBekasRepository;

    public function __construct(mobil_bekasRepository $mobilBekasRepo)
    {
        $this->middleware('auth');
        $this->mobilBekasRepository = $mobilBekasRepo;
    }

    /**
     * Display a listing of the mobil_bekas.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        
        $mobilBekas = mobil_bekas::with('get_merek')->orderBy('id','DESC')->paginate(10);
        // $mobilBekas = $this->mobilBekasRepository->paginate(10);
        // $mobilBekas = DB::table('mobil_bekas')
        //     ->join('mereks','mobil_bekas.merek_id','=','mereks.id')
        //     ->select('mobil_bekas.*','mereks.nama_merek')
        //     ->get();
        return view('mobil_bekas.index',compact('mobilBekas'));
    }

    /**
     * Show the form for creating a new mobil_bekas.
     *
     * @return Response
     */
    public function create()
    {
        $mereks     = merek::pluck('nama_merek','id')->all();
        $warnas     = warna::pluck('nama_warna','id');
        $transmisis = tipe_transmisi::pluck('nama_transmisi','id');
        $mesins     = kondisi_mesin::pluck('nama_kondisi','id');
        $rems       = kondisi_sistem_rem::pluck('nama_kondisi','id');
        $kemudis    = kondisi_kemudi::pluck('nama_kondisi','id');
        $suspensis  = kondisi_suspensi::pluck('nama_kondisi','id');
        $eksteriors = kondisi_eksterior::pluck('nama_kondisi','id');
        $interiors  = kondisi_interior::pluck('nama_kondisi','id');
        $dokumens   = kondisi_dokumen::pluck('nama_kondisi','id');
        return view('mobil_bekas.create', compact('mereks','warnas','transmisis','mesins','rems',
                                                  'kemudis','suspensis','eksteriors',
                                                  'interiors','dokumens'
                                                ));
    }

    public function selectAjax(Request $request)
    {
        if($request->ajax())
        {
            $models = DB::table('model_mereks')->where('merek_id',$request->merek_id)->pluck('nama_model','id')->all();
            $data = view('mobil_bekas.ajax-select',compact('models'))->render();
            return response()->json(['options'=>$data]);
        }
    }

    public function selectAjax_2(Request $request)
    {
        if($request->ajax())
        {
            $tipes = DB::table('tipe_models')->where('model_merek_id',$request->model_merek_id)->pluck('nama_tipe','id')->all();
            $data = view('mobil_bekas.ajax-select_2',compact('tipes'))->render();
            return response()->json(['options'=>$data]);
        }
    }

    /**
     * Store a newly created mobil_bekas in storage.
     *
     * @param Createmobil_bekasRequest $request
     *
     * @return Response
     */
    public function store(Createmobil_bekasRequest $request)
    {
        $input = $request->all();

        $mobilBekas = $this->mobilBekasRepository->create($input);

        Flash::success('Mobil Bekas saved successfully.');

        return redirect(route('mobilBekas.index'));
    }

    /**
     * Display the specified mobil_bekas.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mobilBekas = $this->mobilBekasRepository->find($id);

        if (empty($mobilBekas)) {
            Flash::error('Mobil Bekas not found');

            return redirect(route('mobilBekas.index'));
        }

        return view('mobil_bekas.show', compact('mobilBekas'));
    }

    /**
     * Show the form for editing the specified mobil_bekas.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mobilBekas = $this->mobilBekasRepository->find($id);
        $mereks     = merek::pluck('nama_merek','id')->all();
        $models     = model_merek::pluck('nama_model','id')->all();
        $tipes      = tipe_model::pluck('nama_tipe','id')->all();
        $warnas     = warna::pluck('nama_warna','id');
        $mesins     = kondisi_mesin::pluck('nama_kondisi','id');
        $rems       = kondisi_sistem_rem::pluck('nama_kondisi','id');
        $kemudis    = kondisi_kemudi::pluck('nama_kondisi','id');
        $suspensis  = kondisi_suspensi::pluck('nama_kondisi','id');
        $eksteriors = kondisi_eksterior::pluck('nama_kondisi','id');
        $interiors  = kondisi_interior::pluck('nama_kondisi','id');
        $dokumens   = kondisi_dokumen::pluck('nama_kondisi','id');
        $transmisis = tipe_transmisi::pluck('nama_transmisi','id');

        if (empty($mobilBekas)) {
            Flash::error('Mobil Bekas not found');

            return redirect(route('mobilBekas.index'));
        }

        return view('mobil_bekas.edit', compact('mobilBekas','mereks','models','tipes',
                                                'warnas','mesins','rems',
                                                'kemudis','suspensis','eksteriors',
                                                'interiors','dokumens','transmisis'));
    }

    /**
     * Update the specified mobil_bekas in storage.
     *
     * @param int $id
     * @param Updatemobil_bekasRequest $request
     *
     * @return Response
     */
    public function update($id, Updatemobil_bekasRequest $request)
    {
        $mobilBekas = $this->mobilBekasRepository->find($id);

        if (empty($mobilBekas)) {
            Flash::error('Mobil Bekas not found');

            return redirect(route('mobilBekas.index'));
        }

        $mobilBekas = $this->mobilBekasRepository->update($request->all(), $id);

        Flash::success('Mobil Bekas updated successfully.');

        return redirect(route('mobilBekas.index'));
    }

    /**
     * Remove the specified mobil_bekas from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mobilBekas = $this->mobilBekasRepository->find($id);

        if (empty($mobilBekas)) {
            Flash::error('Mobil Bekas not found');

            return redirect(route('mobilBekas.index'));
        }

        $this->mobilBekasRepository->delete($id);

        Flash::success('Mobil Bekas deleted successfully.');

        return redirect(route('mobilBekas.index'));
    }
}
