<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createtipe_modelRequest;
use App\Http\Requests\Updatetipe_modelRequest;
use App\Repositories\tipe_modelRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\tipe_model;
use App\Models\model_merek;
use Flash;
use Response;

class tipe_modelController extends AppBaseController
{
    /** @var  tipe_modelRepository */
    private $tipeModelRepository;

    public function __construct(tipe_modelRepository $tipeModelRepo)
    {
        $this->middleware('auth');
        $this->tipeModelRepository = $tipeModelRepo;
    }

    /**
     * Display a listing of the tipe_model.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $tipeModels = tipe_model::with('get_model')->paginate(10);
        // $tipeModels = $this->tipeModelRepository->paginate(10);

        return view('tipe_models.index')
            ->with('tipeModels', $tipeModels);
    }

    /**
     * Show the form for creating a new tipe_model.
     *
     * @return Response
     */
    public function create()
    {   
        // $models = model_merek::pluck('nama_model','id');
        $models = model_merek::all();
        // $models = model_merek::pluck('nama_model','id');
        return view('tipe_models.create', compact('models'));
    }

    /**
     * Store a newly created tipe_model in storage.
     *
     * @param Createtipe_modelRequest $request
     *
     * @return Response
     */
    public function store(Createtipe_modelRequest $request)
    {
        $input = $request->all();

        $tipeModel = $this->tipeModelRepository->create($input);

        Flash::success('Tipe Model saved successfully.');

        return redirect(route('tipeModels.index'));
    }

    /**
     * Display the specified tipe_model.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipeModel = $this->tipeModelRepository->find($id);

        if (empty($tipeModel)) {
            Flash::error('Tipe Model not found');

            return redirect(route('tipeModels.index'));
        }

        return view('tipe_models.show')->with('tipeModel', $tipeModel);
    }

    /**
     * Show the form for editing the specified tipe_model.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipeModel = $this->tipeModelRepository->find($id);
        $models = model_merek::all();
        // $models = model_merek::pluck('nama_model','id');

        if (empty($tipeModel)) {
            Flash::error('Tipe Model not found');

            return redirect(route('tipeModels.index'));
        }

        return view('tipe_models.edit', compact('tipeModel','models'));
    }

    /**
     * Update the specified tipe_model in storage.
     *
     * @param int $id
     * @param Updatetipe_modelRequest $request
     *
     * @return Response
     */
    public function update($id, Updatetipe_modelRequest $request)
    {
        $tipeModel = $this->tipeModelRepository->find($id);

        if (empty($tipeModel)) {
            Flash::error('Tipe Model not found');

            return redirect(route('tipeModels.index'));
        }

        $tipeModel = $this->tipeModelRepository->update($request->all(), $id);

        Flash::success('Tipe Model updated successfully.');

        return redirect(route('tipeModels.index'));
    }

    /**
     * Remove the specified tipe_model from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipeModel = $this->tipeModelRepository->find($id);

        if (empty($tipeModel)) {
            Flash::error('Tipe Model not found');

            return redirect(route('tipeModels.index'));
        }

        $this->tipeModelRepository->delete($id);

        Flash::success('Tipe Model deleted successfully.');

        return redirect(route('tipeModels.index'));
    }
}
