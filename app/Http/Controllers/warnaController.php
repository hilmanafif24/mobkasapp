<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatewarnaRequest;
use App\Http\Requests\UpdatewarnaRequest;
use App\Repositories\warnaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class warnaController extends AppBaseController
{
    /** @var  warnaRepository */
    private $warnaRepository;

    public function __construct(warnaRepository $warnaRepo)
    {
        $this->middleware('auth');
        $this->warnaRepository = $warnaRepo;
    }

    /**
     * Display a listing of the warna.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $warnas = $this->warnaRepository->paginate(10);

        return view('warnas.index')
            ->with('warnas', $warnas);
    }

    /**
     * Show the form for creating a new warna.
     *
     * @return Response
     */
    public function create()
    {
        return view('warnas.create');
    }

    /**
     * Store a newly created warna in storage.
     *
     * @param CreatewarnaRequest $request
     *
     * @return Response
     */
    public function store(CreatewarnaRequest $request)
    {
        $input = $request->all();

        $warna = $this->warnaRepository->create($input);

        Flash::success('Warna saved successfully.');

        return redirect(route('warnas.index'));
    }

    /**
     * Display the specified warna.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $warna = $this->warnaRepository->find($id);

        if (empty($warna)) {
            Flash::error('Warna not found');

            return redirect(route('warnas.index'));
        }

        return view('warnas.show')->with('warna', $warna);
    }

    /**
     * Show the form for editing the specified warna.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $warna = $this->warnaRepository->find($id);

        if (empty($warna)) {
            Flash::error('Warna not found');

            return redirect(route('warnas.index'));
        }

        return view('warnas.edit')->with('warna', $warna);
    }

    /**
     * Update the specified warna in storage.
     *
     * @param int $id
     * @param UpdatewarnaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatewarnaRequest $request)
    {
        $warna = $this->warnaRepository->find($id);

        if (empty($warna)) {
            Flash::error('Warna not found');

            return redirect(route('warnas.index'));
        }

        $warna = $this->warnaRepository->update($request->all(), $id);

        Flash::success('Warna updated successfully.');

        return redirect(route('warnas.index'));
    }

    /**
     * Remove the specified warna from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $warna = $this->warnaRepository->find($id);

        if (empty($warna)) {
            Flash::error('Warna not found');

            return redirect(route('warnas.index'));
        }

        $this->warnaRepository->delete($id);

        Flash::success('Warna deleted successfully.');

        return redirect(route('warnas.index'));
    }

    public function coba(){
        return view('warnas.coba');
    }
}
