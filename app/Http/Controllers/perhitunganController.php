<?php

namespace App\Http\Controllers;

// use App\Http\Requests\CreateperhitunganRequest;
// use App\Http\Requests\UpdateperhitunganRequest;
use App\Repositories\mobil_bekasRepository;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\mobil_bekas;
use App\Models\perhitungan;

class perhitunganController extends AppBaseController
{
    /** @var  perhitunganRepository */
    private $mobilBekasRepository;

    public function __construct(mobil_bekasRepository $mobilBekasRepo)
    {
        $this->middleware('auth');
        $this->mobilBekasRepository = $mobilBekasRepo;
    }

    /**
     * Display a listing of the perhitungan.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $mobkas = $this->mobilBekasRepository->paginate(10);
        $mobkas = mobil_bekas::orderBy('id','ASC')->get();
        $data = mobil_bekas::count();
        return view('perhitungans.index', 
            compact('mobkas','data'));
    }

    /**
     * Show the form for creating a new perhitungan.
     *
     * @return Response
     */
    public function create()
    {
        return view('perhitungans.create');
    }

    /**
     * Store a newly created perhitungan in storage.
     *
     * @param CreateperhitunganRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        //memvalidasi data
        $this->validate($request, [
            'b0' => 'required|numeric',
            'b1' => 'required|numeric',
            'b2' => 'required|numeric',
            'b3' => 'required|numeric',
            'b4' => 'required|numeric',
            'b5' => 'required|numeric',
            'b6' => 'required|numeric',
            'b7' => 'required|numeric',
            'b8' => 'required|numeric',
            'b9' => 'required|numeric',
            'b10' => 'required|numeric',
            'b11' => 'required|numeric',
            'b12' => 'required|numeric',
            'b13' => 'required|numeric'
        ]);

        try {
            //simpan data ke dalam table
            $perhitungan = perhitungan::firstOrCreate([
                'b0' => $request->b0,
                'b1' => $request->b1,
                'b2' => $request->b2,
                'b3' => $request->b3,
                'b4' => $request->b4,
                'b5' => $request->b5,
                'b6' => $request->b6,
                'b7' => $request->b7,
                'b8' => $request->b8,
                'b9' => $request->b9,
                'b10' => $request->b10,
                'b11' => $request->b11,
                'b12' => $request->b12,
                'b13' => $request->b13
            ]);

            //jika sukses maka akan redirect ke halaman perhitungan
            Flash::success('Perhitungan saved successfully.');
            return redirect(route('perhitungans.index'));              
        } catch (Exception $e) {
            //jika terjadi kesalahan maka akan redirect ke halaman sebelumnya
            return redirect()->back()
                    ->with(['error' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified perhitungan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        // $perhitungan = $this->perhitunganRepository->find($id);

        // if (empty($perhitungan)) {
        //     Flash::error('Perhitungan not found');

        //     return redirect(route('perhitungans.index'));
        // }

        // return view('perhitungans.show')->with('perhitungan', $perhitungan);
    }

    /**
     * Show the form for editing the specified perhitungan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // $perhitungan = $this->perhitunganRepository->find($id);

        // if (empty($perhitungan)) {
        //     Flash::error('Perhitungan not found');

        //     return redirect(route('perhitungans.index'));
        // }

        // return view('perhitungans.edit')->with('perhitungan', $perhitungan);
    }

    /**
     * Update the specified perhitungan in storage.
     *
     * @param int $id
     * @param UpdateperhitunganRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateperhitunganRequest $request)
    {
        // $perhitungan = $this->perhitunganRepository->find($id);

        // if (empty($perhitungan)) {
        //     Flash::error('Perhitungan not found');

        //     return redirect(route('perhitungans.index'));
        // }

        // $perhitungan = $this->perhitunganRepository->update($request->all(), $id);

        // Flash::success('Perhitungan updated successfully.');

        // return redirect(route('perhitungans.index'));
    }

    /**
     * Remove the specified perhitungan from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        // $perhitungan = $this->perhitunganRepository->find($id);

        // if (empty($perhitungan)) {
        //     Flash::error('Perhitungan not found');

        //     return redirect(route('perhitungans.index'));
        // }

        // $this->perhitunganRepository->delete($id);

        // Flash::success('Perhitungan deleted successfully.');

        // return redirect(route('perhitungans.index'));
    }
}
