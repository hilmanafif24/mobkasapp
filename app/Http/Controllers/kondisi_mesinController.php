<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createkondisi_mesinRequest;
use App\Http\Requests\Updatekondisi_mesinRequest;
use App\Repositories\kondisi_mesinRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class kondisi_mesinController extends AppBaseController
{
    /** @var  kondisi_mesinRepository */
    private $kondisiMesinRepository;

    public function __construct(kondisi_mesinRepository $kondisiMesinRepo)
    {
        $this->middleware('auth');
        $this->kondisiMesinRepository = $kondisiMesinRepo;
    }

    /**
     * Display a listing of the kondisi_mesin.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $kondisiMesins = $this->kondisiMesinRepository->paginate(10);

        return view('kondisi_mesins.index')
            ->with('kondisiMesins', $kondisiMesins);
    }

    /**
     * Show the form for creating a new kondisi_mesin.
     *
     * @return Response
     */
    public function create()
    {
        return view('kondisi_mesins.create');
    }

    /**
     * Store a newly created kondisi_mesin in storage.
     *
     * @param Createkondisi_mesinRequest $request
     *
     * @return Response
     */
    public function store(Createkondisi_mesinRequest $request)
    {
        $input = $request->all();

        $kondisiMesin = $this->kondisiMesinRepository->create($input);

        Flash::success('Kondisi Mesin saved successfully.');

        return redirect(route('kondisiMesins.index'));
    }

    /**
     * Display the specified kondisi_mesin.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $kondisiMesin = $this->kondisiMesinRepository->find($id);

        if (empty($kondisiMesin)) {
            Flash::error('Kondisi Mesin not found');

            return redirect(route('kondisiMesins.index'));
        }

        return view('kondisi_mesins.show')->with('kondisiMesin', $kondisiMesin);
    }

    /**
     * Show the form for editing the specified kondisi_mesin.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $kondisiMesin = $this->kondisiMesinRepository->find($id);

        if (empty($kondisiMesin)) {
            Flash::error('Kondisi Mesin not found');

            return redirect(route('kondisiMesins.index'));
        }

        return view('kondisi_mesins.edit')->with('kondisiMesin', $kondisiMesin);
    }

    /**
     * Update the specified kondisi_mesin in storage.
     *
     * @param int $id
     * @param Updatekondisi_mesinRequest $request
     *
     * @return Response
     */
    public function update($id, Updatekondisi_mesinRequest $request)
    {
        $kondisiMesin = $this->kondisiMesinRepository->find($id);

        if (empty($kondisiMesin)) {
            Flash::error('Kondisi Mesin not found');

            return redirect(route('kondisiMesins.index'));
        }

        $kondisiMesin = $this->kondisiMesinRepository->update($request->all(), $id);

        Flash::success('Kondisi Mesin updated successfully.');

        return redirect(route('kondisiMesins.index'));
    }

    /**
     * Remove the specified kondisi_mesin from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $kondisiMesin = $this->kondisiMesinRepository->find($id);

        if (empty($kondisiMesin)) {
            Flash::error('Kondisi Mesin not found');

            return redirect(route('kondisiMesins.index'));
        }

        $this->kondisiMesinRepository->delete($id);

        Flash::success('Kondisi Mesin deleted successfully.');

        return redirect(route('kondisiMesins.index'));
    }
}
