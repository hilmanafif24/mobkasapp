<?php

namespace App\Repositories;

use App\Models\merek;
use App\Repositories\BaseRepository;

/**
 * Class merekRepository
 * @package App\Repositories
 * @version September 26, 2019, 9:20 am UTC
*/

class merekRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_merek'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return merek::class;
    }
}
