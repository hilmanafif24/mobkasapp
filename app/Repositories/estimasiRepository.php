<?php

namespace App\Repositories;

use App\Models\Estimasi;
use App\Repositories\BaseRepository;

/**
 * Class warnaRepository
 * @package App\Repositories
 * @version September 26, 2019, 12:56 pm UTC
*/

class estimasiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'merek_id',
        'model_merek_id',      
        'tipe_model_id',
        'warna_id',
        'tahun',
        'transmisi_id',
        'mesin_id',
        'sis_rem_id',
        'kemudi_id',
        'suspensi_id',
        'eksterior_id',
        'interior_id',
        'dokumen_id',
        'harga'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Estimasi::class;
    }
}
