<?php

namespace App\Repositories;

use App\Models\tipe_transmisi;
use App\Repositories\BaseRepository;

/**
 * Class tipe_transmisiRepository
 * @package App\Repositories
 * @version November 5, 2019, 12:40 pm UTC
*/

class tipe_transmisiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_transmisi',
        'kode_transmisi'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tipe_transmisi::class;
    }
}
