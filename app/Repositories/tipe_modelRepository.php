<?php

namespace App\Repositories;

use App\Models\tipe_model;
use App\Repositories\BaseRepository;

/**
 * Class tipe_modelRepository
 * @package App\Repositories
 * @version September 26, 2019, 12:45 pm UTC
*/

class tipe_modelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_tipe',
        'model_merek_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tipe_model::class;
    }
}
