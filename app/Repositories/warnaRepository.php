<?php

namespace App\Repositories;

use App\Models\warna;
use App\Repositories\BaseRepository;

/**
 * Class warnaRepository
 * @package App\Repositories
 * @version September 26, 2019, 12:56 pm UTC
*/

class warnaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_warna'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return warna::class;
    }
}
