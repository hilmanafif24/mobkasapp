<?php

namespace App\Repositories;

use App\Models\mobil_bekas;
use App\Repositories\BaseRepository;

/**
 * Class mobil_bekasRepository
 * @package App\Repositories
 * @version September 26, 2019, 1:50 pm UTC
*/

class mobil_bekasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'merek_id',
        'model_merek_id',
        'tipe_model_id',
        'warna_id',
        'mesin_id',
        'sis_rem_id',
        'kemudi_id',
        'suspensi_id',
        'eksterior_id',
        'interior_id',
        'dokumen_id',
        'tahun',
        'transmisi_id',
        'harga'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return mobil_bekas::class;
    }
}
