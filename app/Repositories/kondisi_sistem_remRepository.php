<?php

namespace App\Repositories;

use App\Models\kondisi_sistem_rem;
use App\Repositories\BaseRepository;

/**
 * Class kondisi_sistem_remRepository
 * @package App\Repositories
 * @version September 26, 2019, 1:11 pm UTC
*/

class kondisi_sistem_remRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_kondisi',
        'nilai'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return kondisi_sistem_rem::class;
    }
}
