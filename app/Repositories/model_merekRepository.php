<?php

namespace App\Repositories;

use App\Models\model_merek;
use App\Repositories\BaseRepository;

/**
 * Class model_merekRepository
 * @package App\Repositories
 * @version November 4, 2019, 9:25 am UTC
*/

class model_merekRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_model',
        'merek_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return model_merek::class;
    }
}
