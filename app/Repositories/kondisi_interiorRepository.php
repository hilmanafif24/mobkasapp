<?php

namespace App\Repositories;

use App\Models\kondisi_interior;
use App\Repositories\BaseRepository;

/**
 * Class kondisi_interiorRepository
 * @package App\Repositories
 * @version September 26, 2019, 1:22 pm UTC
*/

class kondisi_interiorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_kondisi',
        'nilai'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return kondisi_interior::class;
    }
}
