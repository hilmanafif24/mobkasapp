<?php

namespace App\Repositories;

use App\Models\kondisi_mesin;
use App\Repositories\BaseRepository;

/**
 * Class kondisi_mesinRepository
 * @package App\Repositories
 * @version September 26, 2019, 1:03 pm UTC
*/

class kondisi_mesinRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_kondisi',
        'nilai'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return kondisi_mesin::class;
    }
}
