<?php

namespace App\Repositories;

use App\Models\perhitungan;
use App\Repositories\BaseRepository;

/**
 * Class perhitunganRepository
 * @package App\Repositories
 * @version October 28, 2019, 2:43 pm UTC
*/

class perhitunganRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'b0',
        'b1',
        'b2',
        'b3',
        'b4',
        'b5',
        'b6',
        'b7',
        'b8',
        'b9',
        'b10',
        'b11',
        'b12',
        'b13'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return perhitungan::class;
    }
}
