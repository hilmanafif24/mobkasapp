<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\merek;

/**
 * Class model_merek
 * @package App\Models
 * @version November 4, 2019, 9:25 am UTC
 *
 * @property string nama_model
 * @property integer merek_id
 */
class model_merek extends Model
{
    use SoftDeletes;

    public $table = 'model_mereks';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama_model',
        'merek_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_model' => 'string',
        'merek_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama_model' => 'required',
        'merek_id' => 'required'
    ];

    public function get_merek()
    {
        return $this->belongsTo('App\\Models\\merek','merek_id','id');
    }

    public function get_tipe()
    {
        return $this->hasMany('App\\Models\\tipe','model_merek_id','id');
    }

    public function get_mobil()
    {
        return $this->hasMany('App\\Models\\mobil_bekas','model_merek_id','id');
    }

    public function get_estimasi()
    {
        return $this->hasMany('App\\Models\\Estimasi','model_merek_id','id');
    }

    
}
