<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\merek;

/**
 * Class mobil_bekas
 * @package App\Models
 * @version September 26, 2019, 1:50 pm UTC
 *
 * @property integer id_merek
 * @property integer id_model_m
 * @property integer id_tipe
 * @property integer id_warna
 * @property integer id_mesin
 * @property integer id_sis_rem
 * @property integer id_kemudi
 * @property integer id_suspensi
 * @property integer id_eksterior
 * @property integer id_interior
 * @property integer id_dokumen
 * @property string tahun
 * @property string transmisi_id
 * @property number harga
 */
class mobil_bekas extends Model
{
    use SoftDeletes;

    public $table = 'mobil_bekas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'merek_id',
        'model_merek_id',
        'tipe_model_id',
        'warna_id',
        'mesin_id',
        'sis_rem_id',
        'kemudi_id',
        'suspensi_id',
        'eksterior_id',
        'interior_id',
        'dokumen_id',
        'tahun',
        'transmisi_id',
        'harga'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                => 'integer',
        'merek_id'          => 'integer',
        'model_merek_id'    => 'integer',
        'tipe_model_id'     => 'integer',
        'warna_id'          => 'integer',
        'mesin_id'          => 'integer',
        'sis_rem_id'        => 'integer',
        'kemudi_id'         => 'integer',
        'suspensi_id'       => 'integer',
        'eksterior_id'      => 'integer',
        'interior_id'       => 'integer',
        'dokumen_id'        => 'integer',
        'tahun'             => 'string',
        'transmisi_id'      => 'integer',
        'harga'             => 'decimal:3'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'merek_id' => 'required',
        'model_merek_id' => 'required',
        'tipe_model_id' => 'required',
        'warna_id' => 'required',
        'mesin_id' => 'required',
        'sis_rem_id' => 'required',
        'kemudi_id' => 'required',
        'suspensi_id' => 'required',
        'eksterior_id' => 'required',
        'interior_id' => 'required',
        'dokumen_id' => 'required',
        'tahun' => 'required',
        'transmisi_id' => 'required',
        'harga' => 'required'
    ];

    public function get_merek()
    {
        return $this->belongsTo('App\\Models\\merek','merek_id','id');
    }

    public function get_model()
    {
        return $this->belongsTo('App\\Models\\model_merek','model_merek_id','id');
    }

    public function get_tipe()
    {
        return $this->belongsTo('App\\Models\\tipe_model','tipe_model_id','id');
    }

    public function get_warna()
    {
        return $this->belongsTo('App\\Models\\warna','warna_id','id');
    }

    public function get_transmisi()
    {
        return $this->belongsTo('App\\Models\\tipe_transmisi','transmisi_id','id');
    }
}
