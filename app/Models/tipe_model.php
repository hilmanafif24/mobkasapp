<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\model_merek;

/**
 * Class tipe_model
 * @package App\Models
 * @version September 26, 2019, 12:45 pm UTC
 *
 * @property string nama_tipe_m
 * @property integer id_model_m
 */
class tipe_model extends Model
{
    use SoftDeletes;

    public $table = 'tipe_models';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama_tipe',
        'model_merek_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_tipe' => 'string',
        'model_merek_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama_tipe' => 'required',
        'model_merek_id' => 'required'
    ];

    public function get_model()
    {
        return $this->belongsTo('App\\Models\\model_merek','model_merek_id','id');
    }
    
    public function get_mobil_bekas()
    {
        return $this->hasMany('App\\Models\\mobil_bekas','tipe_model_id','id');
    }

    public function get_estimasi()
    {
        return $this->hasMany('App\\Models\\Estimasi','tipe_model_id','id');
    }

}
