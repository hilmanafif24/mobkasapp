<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class kondisi_sistem_rem
 * @package App\Models
 * @version September 26, 2019, 1:11 pm UTC
 *
 * @property string nama_kondisi
 * @property string keterangan
 */
class kondisi_sistem_rem extends Model
{
    use SoftDeletes;

    public $table = 'kondisi_sistem_rems';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama_kondisi',
        'keterangan'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_kondisi' => 'string',
        'keterangan' => 'text'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama_kondisi' => 'required'
    ];

    
}
