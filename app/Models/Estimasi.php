<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estimasi extends Model
{
    use SoftDeletes;

    public $table = 'hasil_estimasis';
    public $primaryKey = "id";

    public $fillable = [
    	'merek_id',
    	'model_merek_id',      
        'tipe_model_id',
        'warna_id',
        'tahun',
        'transmisi_id',
        'mesin_id',
        'sis_rem_id',
        'kemudi_id',
        'suspensi_id',
        'eksterior_id',
        'interior_id',
        'dokumen_id',
        'harga'
    ]; 

    protected $casts = [
        'id'                => 'integer',
        'merek_id'          => 'integer',
        'model_merek_id'    => 'integer',
        'tipe_model_id'     => 'integer',
        'warna_id'          => 'integer',
        'tahun'             => 'string',
        'transmisi_id'      => 'integer',
        'mesin_id'          => 'integer',
        'sis_rem_id'        => 'integer',
        'kemudi_id'         => 'integer',
        'suspensi_id'       => 'integer',
        'eksterior_id'      => 'integer',
        'interior_id'       => 'integer',
        'dokumen_id'        => 'integer',
        'harga'             => 'decimal:3'
    ];

    public static $rules = [
    	'id' => 'required',
        'merek_id' => 'required',
        'model_merek_id' => 'required',
        'tipe_model_id' => 'required',
        'warna_id' => 'required',
        'tahun' => 'required',
        'transmisi_id' => 'required',
        'mesin_id' => 'required',
        'sis_rem_id' => 'required',
        'kemudi_id' => 'required',
        'suspensi_id' => 'required',
        'eksterior_id' => 'required',
        'interior_id' => 'required',
        'dokumen_id' => 'required',
        'harga' => 'required'
    ];

    public function get_merek()
    {
        return $this->belongsTo('App\\Models\\merek','merek_id','id');
    }

    public function get_model()
    {
        return $this->belongsTo('App\\Models\\model_merek','model_merek_id','id');
    }

    public function get_tipe()
    {
        return $this->belongsTo('App\\Models\\tipe_model','tipe_model_id','id');
    }
}
