<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class tipe_transmisi
 * @package App\Models
 * @version November 5, 2019, 12:40 pm UTC
 *
 * @property string nama_transmisi
 * @property string kode_transmisi
 */
class tipe_transmisi extends Model
{
    use SoftDeletes;

    public $table = 'tipe_transmisis';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama_transmisi',
        'kode_transmisi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_transmisi' => 'string',
        'kode_transmisi' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama_transmisi' => 'required',
        'kode_transmisi' => 'required'
    ];

    public function get_mobil_bekas()
    {
        return $this->hasMany('App\\Models\\mobil_bekas','transmisi_id','id');
    }
    
}
