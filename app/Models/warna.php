<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class warna
 * @package App\Models
 * @version September 26, 2019, 12:56 pm UTC
 *
 * @property string nama_warna
 */
class warna extends Model
{
    use SoftDeletes;

    public $table = 'warnas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama_warna'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_warna' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama_warna' => 'required'
    ];

    public function get_mobil_bekas()
    {
        return $this->hasMany('App\\Models\\mobil_bekas','warna_id','id');
    }
}
