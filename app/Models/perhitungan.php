<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class perhitungan
 * @package App\Models
 * @version October 28, 2019, 2:43 pm UTC
 *
 * @property number b0
 * @property number b1
 * @property number b2
 * @property number b3
 * @property number b4
 * @property number b5
 * @property number b6
 * @property number b7
 * @property number b8
 * @property number b9
 * @property number b10
 * @property number b11
 * @property number b12
 * @property number b13
 */
class perhitungan extends Model
{
    use SoftDeletes;

    public $table = 'perhitungans';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'b0',
        'b1',
        'b2',
        'b3',
        'b4',
        'b5',
        'b6',
        'b7',
        'b8',
        'b9',
        'b10',
        'b11',
        'b12',
        'b13'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'b0' => 'decimal:3',
        'b1' => 'decimal:3',
        'b2' => 'decimal:3',
        'b3' => 'decimal:3',
        'b4' => 'decimal:3',
        'b5' => 'decimal:3',
        'b6' => 'decimal:3',
        'b7' => 'decimal:3',
        'b8' => 'decimal:3',
        'b9' => 'decimal:3',
        'b10' => 'decimal:3',
        'b11' => 'decimal:3',
        'b12' => 'decimal:3',
        'b13' => 'decimal:3'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'b0' => 'required',
        'b1' => 'required',
        'b2' => 'required',
        'b3' => 'required',
        'b4' => 'required',
        'b5' => 'required',
        'b6' => 'required',
        'b7' => 'required',
        'b8' => 'required',
        'b9' => 'required',
        'b10' => 'required',
        'b11' => 'required',
        'b12' => 'required',
        'b13' => 'required'
    ];

    
}
