<?php

namespace App\Models;
use DB;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\model_merek;
use App\Models\mobil_bekas;

/**
 * Class merek
 * @package App\Models
 * @version September 26, 2019, 9:20 am UTC
 *
 * @property string nama_merek
 */
class merek extends Model
{
    use SoftDeletes;

    public $table = 'mereks';
    public $primaryKey = "id";
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama_merek',
        'asal'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_merek' => 'string',
        'asal' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama_merek' => 'required',
        'asal'      => 'required'
    ];

    public function get_model()
    {
        return $this->hasMany('App\\Models\\model_mobkas','merek_id','id');
        // return $this->hasMany(model_merek::class);
    }

    public function get_mobil_bekas()
    {
        return $this->hasMany('App\\Models\\mobil_bekas','merek_id','id');
    }

    public function get_estimasi()
    {
        return $this->hasMany('App\\Models\\Estimasi','merek_id','id');
    }

}
