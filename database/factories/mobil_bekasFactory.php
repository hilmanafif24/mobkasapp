<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\mobil_bekas;
use Faker\Generator as Faker;

$factory->define(mobil_bekas::class, function (Faker $faker) {

    return [
        'id_merek' => $faker->randomDigitNotNull,
        'id_model_m' => $faker->randomDigitNotNull,
        'id_tipe' => $faker->randomDigitNotNull,
        'id_warna' => $faker->randomDigitNotNull,
        'id_mesin' => $faker->randomDigitNotNull,
        'id_sis_rem' => $faker->randomDigitNotNull,
        'id_kemudi' => $faker->randomDigitNotNull,
        'id_suspensi' => $faker->randomDigitNotNull,
        'id_eksterior' => $faker->randomDigitNotNull,
        'id_interior' => $faker->randomDigitNotNull,
        'id_dokumen' => $faker->randomDigitNotNull,
        'tahun' => $faker->randomElement(['option1', 'option2']),
        'tipe_transimisi' => $faker->randomElement(['opstion1', 'option2']),
        'harga' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
