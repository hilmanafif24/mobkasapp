<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\tipe_transmisi;
use Faker\Generator as Faker;

$factory->define(tipe_transmisi::class, function (Faker $faker) {

    return [
        'nama_transmisi' => $faker->word,
        'kode_transmisi' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
