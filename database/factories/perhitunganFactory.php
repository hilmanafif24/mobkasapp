<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\perhitungan;
use Faker\Generator as Faker;

$factory->define(perhitungan::class, function (Faker $faker) {

    return [
        'b0' => $faker->word,
        'b1' => $faker->word,
        'b2' => $faker->word,
        'b3' => $faker->word,
        'b4' => $faker->word,
        'b5' => $faker->word,
        'b6' => $faker->word,
        'b7' => $faker->word,
        'b8' => $faker->word,
        'b9' => $faker->word,
        'b10' => $faker->word,
        'b11' => $faker->word,
        'b12' => $faker->word,
        'b13' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
