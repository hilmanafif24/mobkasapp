<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\tipe_model;
use Faker\Generator as Faker;

$factory->define(tipe_model::class, function (Faker $faker) {

    return [
        'nama_tipe_m' => $faker->word,
        'id_model_m' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
