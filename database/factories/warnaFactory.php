<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\warna;
use Faker\Generator as Faker;

$factory->define(warna::class, function (Faker $faker) {

    return [
        'nama_warna' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
