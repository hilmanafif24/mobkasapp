<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\kondisi_interior;
use Faker\Generator as Faker;

$factory->define(kondisi_interior::class, function (Faker $faker) {

    return [
        'nama_kondisi' => $faker->word,
        'nilai' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
