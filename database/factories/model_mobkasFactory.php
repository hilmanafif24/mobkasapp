<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\model_mobkas;
use Faker\Generator as Faker;

$factory->define(model_mobkas::class, function (Faker $faker) {

    return [
        'nama_model' => $faker->word,
        'id_merek' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
