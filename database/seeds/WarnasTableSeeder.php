<?php

use Illuminate\Database\Seeder;
use App\Models\warna;

class WarnasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        warna::create([
        	'nama_warna' => 'Hitam'
        ]);

        warna::create([
        	'nama_warna' => 'Putih'
        ]);

        warna::create([
        	'nama_warna' => 'Silver'
        ]);
    }
}
