<?php

use Illuminate\Database\Seeder;
use App\Models\kondisi_kemudi;

class KemudisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        kondisi_kemudi::create([
        	'nama_kondisi' => 'Sangat Bagus',
        	'nilai' => '90'
        ]);

        kondisi_kemudi::create([
        	'nama_kondisi' => 'Bagus',
        	'nilai' => '80'
        ]);

        kondisi_kemudi::create([
        	'nama_kondisi' => 'Cukup',
        	'nilai' => '70'
        ]);
    }
}
