<?php

use Illuminate\Database\Seeder;
use App\Models\kondisi_eksterior;

class EksteriorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        kondisi_eksterior::create([
        	'nama_kondisi' => 'Sangat Bagus',
        	'nilai' => '90'
        ]);

        kondisi_eksterior::create([
        	'nama_kondisi' => 'Bagus',
        	'nilai' => '80'
        ]);

        kondisi_eksterior::create([
        	'nama_kondisi' => 'Cukup',
        	'nilai' => '70'
        ]);
    }
}
