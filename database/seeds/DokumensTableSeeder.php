<?php

use Illuminate\Database\Seeder;
use App\Models\kondisi_dokumen;

class DokumensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        kondisi_dokumen::create([
        	'nama_kondisi' => 'Lengkap',
        	'nilai' => '76-90'
        ]);

        kondisi_dokumen::create([
        	'nama_kondisi' => 'Cukup',
        	'nilai' => '60-75'
        ]);

        kondisi_dokumen::create([
        	'nama_kondisi' => 'Tidak_Lengkap',
        	'nilai' => '0-50'
        ]);
    }
}
