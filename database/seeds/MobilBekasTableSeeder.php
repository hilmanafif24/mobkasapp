<?php

use Illuminate\Database\Seeder;
use App\Models\mobil_bekas;

class MobilBekasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        mobil_bekas::create([
        'id_merek' => '1',
        'id_model_m' => '2',
        'id_tipe' => '2',
        'id_warna' => '1',
        'id_mesin' => '2',
        'id_sis_rem' => '1',
        'id_kemudi' => '3',
        'id_suspensi' => '1',
        'id_eksterior' => '2',
        'id_interior' => '3',
        'id_dokumen' => '1',
        'tahun' => '2015',
        'tipe_transimisi' => 'Manual',
        'harga' => '200000000'
        ]);

        mobil_bekas::create([
        'id_merek' => '2',
        'id_model_m' => '1',
        'id_tipe' => '2',
        'id_warna' => '1',
        'id_mesin' => '2',
        'id_sis_rem' => '1',
        'id_kemudi' => '3',
        'id_suspensi' => '1',
        'id_eksterior' => '2',
        'id_interior' => '3',
        'id_dokumen' => '1',
        'tahun' => '2015',
        'tipe_transimisi' => 'Manual',
        'harga' => '230000000'
        ]);

        mobil_bekas::create([
        'id_merek' => '3',
        'id_model_m' => '2',
        'id_tipe' => '2',
        'id_warna' => '1',
        'id_mesin' => '2',
        'id_sis_rem' => '1',
        'id_kemudi' => '3',
        'id_suspensi' => '1',
        'id_eksterior' => '2',
        'id_interior' => '3',
        'id_dokumen' => '1',
        'tahun' => '2018',
        'tipe_transimisi' => 'Manual',
        'harga' => '270000000'
        ]);
    }
}
