<?php

use Illuminate\Database\Seeder;
use App\Models\kondisi_mesin;

class MesinsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        kondisi_mesin::create([
        	'nama_kondisi' => 'Sangat Bagus',
        	'nilai' => '95'
        ]);

        kondisi_mesin::create([
        	'nama_kondisi' => 'Bagus',
        	'nilai' => '80'
        ]);

        kondisi_mesin::create([
        	'nama_kondisi' => 'Cukup',
        	'nilai' => '70'
        ]);
    }
}
