<?php

use Illuminate\Database\Seeder;
use App\Models\kondisi_sistem_rem;

class SistemRemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        kondisi_sistem_rem::create([
        	'nama_kondisi' => 'Sangat Bagus',
        	'nilai' => '90'
        ]);

        kondisi_sistem_rem::create([
        	'nama_kondisi' => 'Bagus',
        	'nilai' => '80'
        ]);

        kondisi_sistem_rem::create([
        	'nama_kondisi' => 'Cukup',
        	'nilai' => '70'
        ]);
    }
}
