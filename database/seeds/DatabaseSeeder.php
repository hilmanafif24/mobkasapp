<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        // $this->call(UsersTableSeeder::class);
        $this->call(DokumensTableSeeder::class);
        $this->call(EksteriorsTableSeeder::class);
        $this->call(InteriorsTableSeeder::class);
        $this->call(KemudisTableSeeder::class);
        $this->call(MereksTableSeeder::class);
        $this->call(MesinsTableSeeder::class);
        $this->call(MobilBekasTableSeeder::class);
        $this->call(SistemRemsTableSeeder::class);
        $this->call(SuspensisTableSeeder::class);
        $this->call(TipeModelsTableSeeder::class);
        $this->call(WarnasTableSeeder::class);
    }
}
