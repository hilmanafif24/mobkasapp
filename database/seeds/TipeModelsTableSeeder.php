<?php

use Illuminate\Database\Seeder;
use App\Models\tipe_model;

class TipeModelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        tipe_model::create([
        	'nama_tipe_m' => 'X',
        	'id_model_m' => '1'
        ]);

        tipe_model::create([
        	'nama_tipe_m' => 'M',
        	'id_model_m' => '1'
        ]);

        tipe_model::create([
        	'nama_tipe_m' => 'E',
        	'id_model_m' => '2'
        ]);
    }
}
