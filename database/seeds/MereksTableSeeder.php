<?php

use Illuminate\Database\Seeder;
use App\Models\merek;

class MereksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        merek::create([
        	'nama_merek' => 'Toyota'
        ]);

        merek::create([
        	'nama_merek' => 'Honda'
        ]);

        merek::create([
        	'nama_merek' => 'Daihatsu'
        ]);

        merek::create([
        	'nama_merek' => 'Nissan'
        ]);

    }
}
