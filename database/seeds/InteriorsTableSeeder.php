<?php

use Illuminate\Database\Seeder;
use App\Models\kondisi_interior;

class InteriorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        kondisi_interior::create([
        	'nama_kondisi' => 'Sangat Bagus',
        	'nilai' => '90'
        ]);

        kondisi_interior::create([
        	'nama_kondisi' => 'Bagus',
        	'nilai' => '80'
        ]);

        kondisi_interior::create([
        	'nama_kondisi' => 'Cukup',
        	'nilai' => '70'
        ]);
    }
}
