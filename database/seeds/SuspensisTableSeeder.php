<?php

use Illuminate\Database\Seeder;
use App\Models\kondisi_suspensi;

class SuspensisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        kondisi_suspensi::create([
        	'nama_kondisi' => 'Sangat Bagus',
        	'nilai' => '90'
        ]);

        kondisi_suspensi::create([
        	'nama_kondisi' => 'Bagus',
        	'nilai' => '80'
        ]);

        kondisi_suspensi::create([
        	'nama_kondisi' => 'Cukup',
        	'nilai' => '70'
        ]);
    }
}
