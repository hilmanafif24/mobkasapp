<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePerhitungansTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perhitungans', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('b0', 15, 3);
            $table->decimal('b1', 15, 3);
            $table->decimal('b2', 15, 3);
            $table->decimal('b3', 15, 3);
            $table->decimal('b4', 15, 3);
            $table->decimal('b5', 15, 3);
            $table->decimal('b6', 15, 3);
            $table->decimal('b7', 15, 3);
            $table->decimal('b8', 15, 3);
            $table->decimal('b9', 15, 3);
            $table->decimal('b10', 15, 3);
            $table->decimal('b11', 15, 3);
            $table->decimal('b12', 15, 3);
            $table->decimal('b13', 15, 3);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('perhitungans');
    }
}
