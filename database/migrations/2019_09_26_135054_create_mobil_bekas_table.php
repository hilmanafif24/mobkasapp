<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMobilBekasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobil_bekas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merek_id');
            $table->integer('model_mobkas_id');
            $table->integer('tipe_model_id');
            $table->integer('warna_id');
            $table->integer('kondisi_mesin_id');
            $table->integer('kondisi_sistem_rem_id');
            $table->integer('kondisi_kemudi_id');
            $table->integer('kondisi_suspensi_id');
            $table->integer('kondisi_eksterior_id');
            $table->integer('kondisi_interior_id');
            $table->integer('kondisi_dokumen_id');
            $table->enum('tahun', ['2019','2018','2017','2016','2015','2014']);
            $table->integer('transmisi_id');
            $table->decimal('harga',15,3);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mobil_bekas');
    }
}
