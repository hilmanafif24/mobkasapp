<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameMerekIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('model_mobkas', function (Blueprint $table) {
            $table->renameColumn('id_merek','merek_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('model_mobkas', function (Blueprint $table) {
            $table->renameColumn('merek_id','id_merek');
        });
    }
}
