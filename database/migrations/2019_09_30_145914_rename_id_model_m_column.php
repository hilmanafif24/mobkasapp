<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameIdModelMColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tipe_models', function (Blueprint $table) {
            $table->renameColumn('id_model_m','model_mobkas_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tipe_models', function (Blueprint $table) {
            $table->renameColumn('model_mobkas_id','id_model_m');
        });
    }
}
