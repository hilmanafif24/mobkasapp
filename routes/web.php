<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index');

Auth::routes();

// Route::get('/home', 'HomeController@index');

Route::resource('home', 'HomeController');
Route::get('result', 'HomeController@result');
Route::post('home', 'HomeController@proses_estimasi')->name('home.proses_estimasi');
Route::post('select-ajax', ['as'=>'select-ajax','uses'=>'HomeController@selectAjax']);
Route::post('select-ajax_2', ['as'=>'select-ajax_2','uses'=>'HomeController@selectAjax_2']);
Route::post('select-mesin', ['as'=>'select-mesin','uses'=>'HomeController@pilihmesin']);
// Route::post('select-mesin','HomeController@pilihmesin')->name('home.select-mesin');

Route::resource('dashboard', 'DashboardController');

Route::resource('mereks', 'merekController');

Route::resource('modelMereks', 'model_merekController');

Route::resource('tipeModels', 'tipe_modelController');

Route::resource('warnas', 'warnaController');

Route::resource('kondisiMesins', 'kondisi_mesinController');

Route::resource('kondisiSistemRems', 'kondisi_sistem_remController');

Route::resource('kondisiKemudis', 'kondisi_kemudiController');

Route::resource('kondisiSuspensis', 'kondisi_suspensiController');

Route::resource('kondisiEksteriors', 'kondisi_eksteriorController');

Route::resource('kondisiInteriors', 'kondisi_interiorController');

Route::resource('kondisiDokumens', 'kondisi_dokumenController');

Route::resource('mobilBekas', 'mobil_bekasController');

Route::resource('perhitungans', 'perhitunganController');

Route::resource('tipeTransmisis', 'tipe_transmisiController');

Route::resource('laporans', 'laporanController');