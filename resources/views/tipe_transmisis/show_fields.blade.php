<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $tipeTransmisi->id !!}</p>
</div>

<!-- Nama Transmisi Field -->
<div class="form-group">
    {!! Form::label('nama_transmisi', 'Nama Transmisi:') !!}
    <p>{!! $tipeTransmisi->nama_transmisi !!}</p>
</div>

<!-- Kode Transmisi Field -->
<div class="form-group">
    {!! Form::label('kode_transmisi', 'Kode Transmisi:') !!}
    <p>{!! $tipeTransmisi->kode_transmisi !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $tipeTransmisi->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $tipeTransmisi->updated_at !!}</p>
</div>

