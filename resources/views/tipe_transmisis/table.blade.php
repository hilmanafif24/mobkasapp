<div class="table-responsive-sm">
    <table class="table table-striped" id="tipeTransmisis-table">
        <thead>
            <th>Nama Transmisi</th>
        <th>Kode Transmisi</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($tipeTransmisis as $tipeTransmisi)
            <tr>
                <td>{!! $tipeTransmisi->nama_transmisi !!}</td>
            <td>{!! $tipeTransmisi->kode_transmisi !!}</td>
                <td>
                    {!! Form::open(['route' => ['tipeTransmisis.destroy', $tipeTransmisi->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('tipeTransmisis.show', [$tipeTransmisi->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('tipeTransmisis.edit', [$tipeTransmisi->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>