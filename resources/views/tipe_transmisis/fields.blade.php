<!-- Nama Transmisi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_transmisi', 'Nama Transmisi:') !!}
    {!! Form::text('nama_transmisi', null, ['class' => 'form-control']) !!}
</div>

<!-- Kode Transmisi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('kode_transmisi', 'Kode Transmisi:') !!}
    {!! Form::text('kode_transmisi', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('tipeTransmisis.index') !!}" class="btn btn-default">Cancel</a>
</div>
