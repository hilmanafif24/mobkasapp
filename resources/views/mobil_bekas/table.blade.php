<div class="table-responsive-sm">
    <table class="table table-striped" id="mobilBekas-table">
        <thead>
            <th>No</th>
            <th>Merek</th>
            <th>Model</th>
            <th>Tipe</th>
            <th>Warna</th>
            <!-- <th>Id Mesin</th>
            <th>Id Sis Rem</th>
            <th>Id Kemudi</th>
            <th>Id Suspensi</th>
            <th>Id Eksterior</th>
            <th>Id Interior</th>
            <th>Id Dokumen</th> -->
            <th>Tahun</th>
            <th>Transmisi</th>
            <th>Harga</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($mobilBekas as $no => $data)
            <tr>
            <td>{{ ++$no + ($mobilBekas->currentPage()-1) * $mobilBekas->perPage() }}</td>
            <td>{!! $data->get_merek->nama_merek !!}</td>
            <td>{!! $data->get_model->nama_model !!}</td>
            <td>{!! $data->get_tipe->nama_tipe !!}</td>
            <td>{!! $data->get_warna->nama_warna !!}</td>
            <!-- <td>{!! $data->id_mesin !!}</td>
            <td>{!! $data->id_sis_rem !!}</td>
            <td>{!! $data->id_kemudi !!}</td>
            <td>{!! $data->id_suspensi !!}</td>
            <td>{!! $data->id_eksterior !!}</td>
            <td>{!! $data->id_interior !!}</td>
            <td>{!! $data->id_dokumen !!}</td> -->
            <td>{!! $data->tahun !!}</td>
            <td>{!! $data->get_transmisi->nama_transmisi !!}</td>
            <td>Rp. {!! number_format($data->harga*1000000, 0, ",",".") !!}</td>
                <td>
                    {!! Form::open(['route' => ['mobilBekas.destroy', $data->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('mobilBekas.show', [$data->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('mobilBekas.edit', [$data->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>