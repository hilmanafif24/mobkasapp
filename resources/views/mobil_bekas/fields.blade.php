<div class="row">
    <div class="col-sm-6">
        <!-- Id Merek Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('merek_id', 'Merek:') !!}
            {!! Form::select('merek_id',[''=>'--- Pilih Merek ---']+$mereks,null,['class'=>'js-example-basic-single js-states form-control','style'=>'width: 100%'])!!}
            <!-- {!! Form::text('merek_id', null, ['class' => 'form-control']) !!} -->
        </div>

        <!-- Id Model M Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('model_merek_id', 'Model:') !!}
            {!! Form::select('model_merek_id',[''=>'--- Pilih Model ---']+$models,null,['class'=>'js-example-basic-single js-states form-control','style'=>'width: 100%']) !!}
            <!-- {!! Form::text('model_merek_id', null, ['class' => 'form-control']) !!} -->
        </div>

        <!-- Id Tipe Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('tipe_model_id', 'Tipe:') !!}
            {!! Form::select('tipe_model_id',[''=>'--- Pilih Tipe ---']+$tipes,null,['class'=>'js-example-basic-single js-states form-control','style'=>'width: 100%']) !!}
            <!--{!! Form::text('tipe_model_id', null, ['class' => 'form-control']) !!} -->
        </div>

        <!-- Id Warna Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('warna_id', 'Warna:') !!}
            {!! Form::select('warna_id', $warnas, null, ['class' => 'form-control']) !!}
        </div>

        <!-- Id Mesin Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('mesin_id', 'Kondisi Mesin:') !!}
            {!! Form::select('mesin_id', $mesins, null, ['class' => 'form-control']) !!}
        </div>

        <!-- Id Sis Rem Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('sis_rem_id', 'Kondisi Sistem Rem:') !!}
            {!! Form::select('sis_rem_id', $rems, null, ['class' => 'form-control']) !!}
        </div>

        <!-- Id Kemudi Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('kemudi_id', 'Kondisi Kemudi:') !!}
            {!! Form::select('kemudi_id', $kemudis, null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <!-- Id Suspensi Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('suspensi_id', 'Kondisi Suspensi:') !!}
            {!! Form::select('suspensi_id', $suspensis, null, ['class' => 'form-control']) !!}
        </div>

        <!-- Id Eksterior Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('eksterior_id', 'Kondisi Eksterior:') !!}
            {!! Form::select('eksterior_id', $eksteriors, null, ['class' => 'form-control']) !!}
        </div>

        <!-- Id Interior Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('interior_id', 'Kondisi Interior:') !!}
            {!! Form::select('interior_id', $interiors, null, ['class' => 'form-control']) !!}
        </div>

        <!-- Id Dokumen Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('dokumen_id', 'Kondisi Dokumen:') !!}
            {!! Form::select('dokumen_id', $dokumens, null, ['class' => 'form-control']) !!}
        </div>

        <!-- Tahun Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('tahun', 'Tahun:') !!}
            {!! Form::select('tahun', ['2019' => '2019', '2018' => '2018',
                                       '2017' => '2017', '2016' => '2016',
                                       '2015' => '2015', '2014' => '2014'], 
                                null, ['class' => 'form-control']) !!}
        </div>

        <!-- Tipe Transimisi Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('transmisi_id', 'Tipe Transmisi:') !!}
            {!! Form::select('transmisi_id', $transmisis, null, ['class' => 'form-control']) !!}
        </div>

        <!-- Harga Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('harga', 'Harga (Juta):') !!}
            {!! Form::text('harga', null, ['class' => 'form-control']) !!}
            <small>Contoh penulisan: 100.5 maka dibaca Rp.100.500.000 </small>
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('mobilBekas.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            console.log("mulai");
        });

        $("select[name='merek_id']").change(function(){
          console.log('menampilkan model')
          var merek_id = $(this).val();
          var token = $("input[name='_token']").val();
          $.ajax({
              url: "<?php echo route('select-ajax') ?>",
              method: 'POST',
              data: {merek_id:merek_id, _token:token},
              success: function(data) {
                $("select[name='model_merek_id']").html('');
                $("select[name='model_merek_id']").html(data.options);
              }
            });
        }); 

        $("select[name='model_merek_id']").change(function(){
          console.log('menampilkan tipe')
          var model_merek_id = $(this).val();
          var token2 = $("input[name='_token']").val();
          $.ajax({
              url: "<?php echo route('select-ajax_2') ?>",
              method: 'POST',
              data: {model_merek_id:model_merek_id, _token:token2},
              success: function(data) {
                $("select[name='tipe_model_id']").html('');
                $("select[name='tipe_model_id']").html(data.options);
              }
            });
        });
    </script>
@endsection