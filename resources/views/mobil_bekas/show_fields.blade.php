<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $mobilBekas->id !!}</p>
</div>

<!-- Id Merek Field -->
<div class="form-group">
    {!! Form::label('merek_id', 'Id Merek:') !!}
    <p>{!! $mobilBekas->merek_id !!}</p>
</div>

<!-- Id Model M Field -->
<div class="form-group">
    {!! Form::label('model_merek_id', 'Id Model Merek:') !!}
    <p>{!! $mobilBekas->model_merek_id !!}</p>
</div>

<!-- Id Tipe Field -->
<div class="form-group">
    {!! Form::label('tipe_model_id', 'Id Tipe Model:') !!}
    <p>{!! $mobilBekas->tipe_model_id !!}</p>
</div>

<!-- Id Warna Field -->
<div class="form-group">
    {!! Form::label('warna_id', 'Id Warna:') !!}
    <p>{!! $mobilBekas->warna_id !!}</p>
</div>

<!-- Tahun Field -->
<div class="form-group">
    {!! Form::label('tahun', 'Tahun:') !!}
    <p>{!! $mobilBekas->tahun !!}</p>
</div>

<!-- Tipe Transimisi Field -->
<div class="form-group">
    {!! Form::label('transimisi_id', 'Tipe Transmisi:') !!}
    <p>{!! $mobilBekas->transmisi_id !!}</p>
</div>

<!-- Id Mesin Field -->
<div class="form-group">
    {!! Form::label('mesin_id', 'Id Mesin:') !!}
    <p>{!! $mobilBekas->mesin_id !!}</p>
</div>

<!-- Id Sis Rem Field -->
<div class="form-group">
    {!! Form::label('sis_rem_id', 'Id Sis Rem:') !!}
    <p>{!! $mobilBekas->sis_rem_id !!}</p>
</div>

<!-- Id Kemudi Field -->
<div class="form-group">
    {!! Form::label('kemudi_id', 'Id Kemudi:') !!}
    <p>{!! $mobilBekas->kemudi_id !!}</p>
</div>

<!-- Id Suspensi Field -->
<div class="form-group">
    {!! Form::label('suspensi_id', 'Id Suspensi:') !!}
    <p>{!! $mobilBekas->suspensi_id !!}</p>
</div>

<!-- Id Eksterior Field -->
<div class="form-group">
    {!! Form::label('eksterior_id', 'Id Eksterior:') !!}
    <p>{!! $mobilBekas->eksterior_id !!}</p>
</div>

<!-- Id Interior Field -->
<div class="form-group">
    {!! Form::label('interior_id', 'Id Interior:') !!}
    <p>{!! $mobilBekas->interior_id !!}</p>
</div>

<!-- Id Dokumen Field -->
<div class="form-group">
    {!! Form::label('dokumen_id', 'Id Dokumen:') !!}
    <p>{!! $mobilBekas->dokumen_id !!}</p>
</div>

<!-- Harga Field -->
<div class="form-group">
    {!! Form::label('harga', 'Harga (Juta):') !!}
    <p>Rp. {!! number_format($mobilBekas->harga*1000000, 0, ",",".") !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $mobilBekas->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $mobilBekas->updated_at !!}</p>
</div>

