@extends('layouts.app')

@section('content')
	<ol class="breadcrumb">
        <li class="breadcrumb-item">Laporan Estimasis</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
        	<div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>
                             estimasis
                            <!-- <a class="pull-right" href=""><i class="fa fa-print fa-lg"></i></a> -->
                        </div>
                        <div class="card-body">
                            @include('laporans.table')
                            <div class="pull-right mr-3">
                                     
        @include('coreui-templates::common.paginate', ['records' => $estimasis])

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection