<div class="table-responsive">
    <table class="table table-striped" id="mobilBekas-table">
        <thead>
        	<th>No</th>
            <th>Merek</th>
            <th>Model</th>
            <th>Tipe</th>
            <th>Warna</th>
            <th>Tahun</th>
            <th>Transmisi</th>
            <th>Mesin</th>
            <th>SisRem</th>
            <th>Kemudi</th>
            <th>Suspensi</th>
            <th>Eksterior</th>
            <th>Interior</th>
            <th>Dokumen</th>
            <th>Harga (Juta)</th>
        </thead>
        <tbody>
        @foreach($estimasis as $no => $row)
            <tr>
            <td>{{ ++$no + ($estimasis->currentPage()-1) * $estimasis->perPage() }}</td>
            <td>{!! $row->merek_id !!}</td>
            <td>{!! $row->model_merek_id !!}</td>
            <td>{!! $row->tipe_model_id !!}</td>
            <td>{!! $row->warna_id !!}</td>
            <td>{!! $row->tahun !!}</td>
            <td>{!! $row->transmisi_id !!}</td>
            <td>{!! $row->mesin_id !!}</td>
            <td>{!! $row->sis_rem_id !!}</td>
            <td>{!! $row->kemudi_id !!}</td>
            <td>{!! $row->suspensi_id !!}</td>
            <td>{!! $row->eksterior_id !!}</td>
            <td>{!! $row->interior_id !!}</td>
            <td>{!! $row->dokumen_id !!}</td>
            <td>Rp.{!! number_format($row->harga*1000000, 2, ",",".") !!}</td>
        @endforeach
        </tbody>
    </table>
</div>