<div class="table-responsive-sm">
    <table class="table table-striped" id="modelMereks-table">
        <thead>
            <th>Nama Model</th>
        <th>Merek</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($modelMereks as $modelMerek)
            <tr>
                <td>{!! $modelMerek->nama_model !!}</td>
            <td>{!! $modelMerek->get_merek->nama_merek !!}</td>
                <td>
                    {!! Form::open(['route' => ['modelMereks.destroy', $modelMerek->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('modelMereks.show', [$modelMerek->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('modelMereks.edit', [$modelMerek->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>