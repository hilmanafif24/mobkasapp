<!-- Nama Model Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_model', 'Nama Model:') !!}
    {!! Form::text('nama_model', null, ['class' => 'form-control']) !!}
</div>

<!-- Merek Id Field -->
<div class="form-group col-sm-6">
    <!-- {!! Form::label('merek_id', 'Merek Id:') !!} -->
    <label for="merekSelect">Merek</label>
    <select class="form-control" id="merekSelect" name="merek_id">
    @foreach($mereks as $data)                              
      <option value="{{ $data->id }}">{{ $data->nama_merek }}</option>
    @endforeach
	</select>
    <!-- {!! Form::text('merek_id', null, ['class' => 'form-control']) !!} -->
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('modelMereks.index') !!}" class="btn btn-default">Cancel</a>
</div>
