<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $modelMerek->id !!}</p>
</div>

<!-- Nama Model Field -->
<div class="form-group">
    {!! Form::label('nama_model', 'Nama Model:') !!}
    <p>{!! $modelMerek->nama_model !!}</p>
</div>

<!-- Merek Id Field -->
<div class="form-group">
    {!! Form::label('merek_id', 'Merek Id:') !!}
    <p>{!! $modelMerek->merek_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $modelMerek->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $modelMerek->updated_at !!}</p>
</div>

