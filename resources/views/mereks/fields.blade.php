<!-- Nama Merek Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_merek', 'Nama Merek:') !!}
    {!! Form::text('nama_merek', null, ['class' => 'form-control']) !!}
</div>

<!-- Asal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('asal', 'Asal:') !!}
    {!! Form::text('asal', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('mereks.index') !!}" class="btn btn-default">Cancel</a>
</div>
