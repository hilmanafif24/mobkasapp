<div class="table-responsive-sm">
    <table class="table table-striped" id="kondisiSuspensis-table">
        <thead>
            <th>Nama Kondisi</th>
        <!-- <th>Nilai</th> -->
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($kondisiSuspensis as $kondisiSuspensi)
            <tr>
                <td>{!! $kondisiSuspensi->nama_kondisi !!}</td>
            <!-- <td>{!! $kondisiSuspensi->nilai !!}</td> -->
                <td>
                    {!! Form::open(['route' => ['kondisiSuspensis.destroy', $kondisiSuspensi->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('kondisiSuspensis.show', [$kondisiSuspensi->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('kondisiSuspensis.edit', [$kondisiSuspensi->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>