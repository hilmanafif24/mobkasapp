<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $kondisiSuspensi->id !!}</p>
</div>

<!-- Nama Kondisi Field -->
<div class="form-group">
    {!! Form::label('nama_kondisi', 'Nama Kondisi:') !!}
    <p>{!! $kondisiSuspensi->nama_kondisi !!}</p>
</div>

<!-- Nilai Field -->
<div class="form-group">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    <p>{!! $kondisiSuspensi->keterangan !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $kondisiSuspensi->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $kondisiSuspensi->updated_at !!}</p>
</div>

