<div class="table-responsive-sm">
    <table class="table table-striped" id="kondisiDokumens-table">
        <thead>
            <th>Nama Kondisi</th>
        <!-- <th>Nilai</th> -->
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($kondisiDokumens as $kondisiDokumen)
            <tr>
                <td>{!! $kondisiDokumen->nama_kondisi !!}</td>
            <!-- <td>{!! $kondisiDokumen->nilai !!}</td> -->
                <td>
                    {!! Form::open(['route' => ['kondisiDokumens.destroy', $kondisiDokumen->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('kondisiDokumens.show', [$kondisiDokumen->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('kondisiDokumens.edit', [$kondisiDokumen->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>