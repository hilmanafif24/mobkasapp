<!-- B0 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b0', 'B0:') !!}
    {!! Form::text('b0', null, ['class' => 'form-control']) !!}
</div>

<!-- B1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b1', 'B1:') !!}
    {!! Form::text('b1', null, ['class' => 'form-control']) !!}
</div>

<!-- B2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b2', 'B2:') !!}
    {!! Form::text('b2', null, ['class' => 'form-control']) !!}
</div>

<!-- B3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b3', 'B3:') !!}
    {!! Form::text('b3', null, ['class' => 'form-control']) !!}
</div>

<!-- B4 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b4', 'B4:') !!}
    {!! Form::text('b4', null, ['class' => 'form-control']) !!}
</div>

<!-- B5 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b5', 'B5:') !!}
    {!! Form::text('b5', null, ['class' => 'form-control']) !!}
</div>

<!-- B6 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b6', 'B6:') !!}
    {!! Form::text('b6', null, ['class' => 'form-control']) !!}
</div>

<!-- B7 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b7', 'B7:') !!}
    {!! Form::text('b7', null, ['class' => 'form-control']) !!}
</div>

<!-- B8 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b8', 'B8:') !!}
    {!! Form::text('b8', null, ['class' => 'form-control']) !!}
</div>

<!-- B9 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b9', 'B9:') !!}
    {!! Form::text('b9', null, ['class' => 'form-control']) !!}
</div>

<!-- B10 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b10', 'B10:') !!}
    {!! Form::text('b10', null, ['class' => 'form-control']) !!}
</div>

<!-- B11 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b11', 'B11:') !!}
    {!! Form::text('b11', null, ['class' => 'form-control']) !!}
</div>

<!-- B13 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('b13', 'B13:') !!}
    {!! Form::text('b13', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('perhitungans.index') !!}" class="btn btn-default">Cancel</a>
</div>
