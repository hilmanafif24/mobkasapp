@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Perhitungans</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             perhitungans
                             <!-- <a class="pull-right" href="{!! route('perhitungans.create') !!}"><i class="fa fa-plus-square fa-lg"></i></a> -->
                         </div>
                         <div class="card-body">
                            @include('perhitungans.table')
                            <div class="pull-right mr-3">

                            </div>
                            <div class="container" style="margin-top: 10px;"> 
                                <div class="form-row" style="margin-bottom: 10px;">
                                    <button type="button" class="btn btn-success btn-lg" id="proses">Proses Hitung</button>
                                    <!-- <a class="btn btn-primary" id="proses">Proses Hitung</a> -->
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s1">Persamaan (1)</label>
                                        <input id="s1" type="text" class="form-control" placeholder="Persamaan (1)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s2">Persamaan (2)</label>
                                        <input id="s2" type="text" class="form-control" placeholder="Persamaan (2)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s3">Persamaan (3)</label>
                                        <input id="s3" type="text" class="form-control" placeholder="Persamaan (3)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s4">Persamaan (4)</label>
                                        <input id="s4" type="text" class="form-control" placeholder="Persamaan (4)">         
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s5">Persamaan (5)</label>
                                        <input id="s5" type="text" class="form-control" placeholder="Persamaan (5)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s6">Persamaan (6)</label>
                                        <input id="s6" type="text" class="form-control" placeholder="Persamaan (6)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s7">Persamaan (7)</label>
                                        <input id="s7" type="text" class="form-control" placeholder="Persamaan (7)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s8">Persamaan (8)</label>
                                        <input id="s8" type="text" class="form-control" placeholder="Persamaan (8)">         
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s9">Persamaan (9)</label>
                                        <input id="s9" type="text" class="form-control" placeholder="Persamaan (9)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s10">Persamaan (10)</label>
                                        <input id="s10" type="text" class="form-control" placeholder="Persamaan (10)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s11">Persamaan (11)</label>
                                        <input id="s11" type="text" class="form-control" placeholder="Persamaan (11)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s12">Persamaan (12)</label>
                                        <input id="s12" type="text" class="form-control" placeholder="Persamaan (12)">         
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s13">Persamaan (13)</label>
                                        <input id="s13" type="text" class="form-control" placeholder="Persamaan (13)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s14">Persamaan (14)</label>
                                        <input id="s14" type="text" class="form-control" placeholder="Persamaan (14)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s15">Persamaan (15)</label>
                                        <input id="s15" type="text" class="form-control" placeholder="Persamaan (15)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s16">Persamaan (16)</label>
                                        <input id="s16" type="text" class="form-control" placeholder="Persamaan (16)">         
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s17">Persamaan (17)</label>
                                        <input id="s17" type="text" class="form-control" placeholder="Persamaan (17)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s18">Persamaan (18)</label>
                                        <input id="s18" type="text" class="form-control" placeholder="Persamaan (18)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s19">Persamaan (19)</label>
                                        <input id="s19" type="text" class="form-control" placeholder="Persamaan (19)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s20">Persamaan (20)</label>
                                        <input id="s20" type="text" class="form-control" placeholder="Persamaan (20)">         
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s21">Persamaan (21)</label>
                                        <input id="s21" type="text" class="form-control" placeholder="Persamaan (21)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s22">Persamaan (22)</label>
                                        <input id="s22" type="text" class="form-control" placeholder="Persamaan (22)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s23">Persamaan (23)</label>
                                        <input id="s23" type="text" class="form-control" placeholder="Persamaan (23)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s24">Persamaan (24)</label>
                                        <input id="s24" type="text" class="form-control" placeholder="Persamaan (24)">         
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s25">Persamaan (25)</label>
                                        <input id="s25" type="text" class="form-control" placeholder="Persamaan (25)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s26">Persamaan (26)</label>
                                        <input id="s26" type="text" class="form-control" placeholder="Persamaan (26)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s27">Persamaan (27)</label>
                                        <input id="s27" type="text" class="form-control" placeholder="Persamaan (27)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s28">Persamaan (28)</label>
                                        <input id="s28" type="text" class="form-control" placeholder="Persamaan (28)">         
                                    </div>
                                </div>
                                <div class="form-row">
                                    
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s29">Persamaan (29)</label>
                                        <input id="s29" type="text" class="form-control" placeholder="Persamaan (29)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s30">Persamaan (30)</label>
                                        <input id="s30" type="text" class="form-control" placeholder="Persamaan (30)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s31">Persamaan (31)</label>
                                        <input id="s31" type="text" class="form-control" placeholder="Persamaan (31)">         
                                    </div>    
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s32">Persamaan (32)</label>
                                        <input id="s32" type="text" class="form-control" placeholder="Persamaan (32)">         
                                    </div>                               
                                </div>
                                <div class="form-row">
                                    
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s33">Persamaan (33)</label>
                                        <input id="s33" type="text" class="form-control" placeholder="Persamaan (33)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s34">Persamaan (34)</label>
                                        <input id="s34" type="text" class="form-control" placeholder="Persamaan (34)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s35">Persamaan (35)</label>
                                        <input id="s35" type="text" class="form-control" placeholder="Persamaan (35)">         
                                    </div> 
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s36">Persamaan (36)</label>
                                        <input id="s36" type="text" class="form-control" placeholder="Persamaan (36)">         
                                    </div>                                  
                                </div>
                                <div class="form-row">                                   
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s37">Persamaan (37)</label>
                                        <input id="s37" type="text" class="form-control" placeholder="Persamaan (37)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s38">Persamaan (38)</label>
                                        <input id="s38" type="text" class="form-control" placeholder="Persamaan (38)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s39">Persamaan (39)</label>
                                        <input id="s39" type="text" class="form-control" placeholder="Persamaan (39)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s40">Persamaan (40)</label>
                                        <input id="s40" type="text" class="form-control" placeholder="Persamaan (40)">         
                                    </div>                                   
                                </div>
                                <div class="form-row">                                   
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s41">Persamaan (41)</label>
                                        <input id="s41" type="text" class="form-control" placeholder="Persamaan (41)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s42">Persamaan (42)</label>
                                        <input id="s42" type="text" class="form-control" placeholder="Persamaan (42)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s43">Persamaan (43)</label>
                                        <input id="s43" type="text" class="form-control" placeholder="Persamaan (43)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s44">Persamaan (44)</label>
                                        <input id="s44" type="text" class="form-control" placeholder="Persamaan (44)">         
                                    </div>                                   
                                </div>
                                <div class="form-row">                                   
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s45">Persamaan (45)</label>
                                        <input id="s45" type="text" class="form-control" placeholder="Persamaan (45)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s46">Persamaan (46)</label>
                                        <input id="s46" type="text" class="form-control" placeholder="Persamaan (46)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s47">Persamaan (47)</label>
                                        <input id="s47" type="text" class="form-control" placeholder="Persamaan (47)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s48">Persamaan (48)</label>
                                        <input id="s48" type="text" class="form-control" placeholder="Persamaan (48)">         
                                    </div>                                   
                                </div>
                                <div class="form-row">                                   
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s49">Persamaan (49)</label>
                                        <input id="s49" type="text" class="form-control" placeholder="Persamaan (49)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s50">Persamaan (50)</label>
                                        <input id="s50" type="text" class="form-control" placeholder="Persamaan (50)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s51">Persamaan (51)</label>
                                        <input id="s51" type="text" class="form-control" placeholder="Persamaan (51)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s52">Persamaan (52)</label>
                                        <input id="s52" type="text" class="form-control" placeholder="Persamaan (52)">         
                                    </div>                                   
                                </div>
                                <div class="form-row">                                   
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s53">Persamaan (53)</label>
                                        <input id="s53" type="text" class="form-control" placeholder="Persamaan (53)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s54">Persamaan (54)</label>
                                        <input id="s54" type="text" class="form-control" placeholder="Persamaan (54)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s55">Persamaan (55)</label>
                                        <input id="s55" type="text" class="form-control" placeholder="Persamaan (55)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s56">Persamaan (56)</label>
                                        <input id="s56" type="text" class="form-control" placeholder="Persamaan (56)">         
                                    </div>                                   
                                </div>
                                <div class="form-row">                                   
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s57">Persamaan (57)</label>
                                        <input id="s57" type="text" class="form-control" placeholder="Persamaan (57)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s58">Persamaan (58)</label>
                                        <input id="s58" type="text" class="form-control" placeholder="Persamaan (58)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s59">Persamaan (59)</label>
                                        <input id="s59" type="text" class="form-control" placeholder="Persamaan (59)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s60">Persamaan (60)</label>
                                        <input id="s60" type="text" class="form-control" placeholder="Persamaan (60)">         
                                    </div>                                   
                                </div>
                                <div class="form-row">                                   
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s61">Persamaan (61)</label>
                                        <input id="s61" type="text" class="form-control" placeholder="Persamaan (61)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s62">Persamaan (62)</label>
                                        <input id="s62" type="text" class="form-control" placeholder="Persamaan (62)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s63">Persamaan (63)</label>
                                        <input id="s63" type="text" class="form-control" placeholder="Persamaan (63)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s64">Persamaan (64)</label>
                                        <input id="s64" type="text" class="form-control" placeholder="Persamaan (64)">         
                                    </div>                                   
                                </div>
                                <div class="form-row">                                   
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s65">Persamaan (65)</label>
                                        <input id="s65" type="text" class="form-control" placeholder="Persamaan (65)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s66">Persamaan (66)</label>
                                        <input id="s66" type="text" class="form-control" placeholder="Persamaan (66)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s67">Persamaan (67)</label>
                                        <input id="s67" type="text" class="form-control" placeholder="Persamaan (67)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s68">Persamaan (68)</label>
                                        <input id="s68" type="text" class="form-control" placeholder="Persamaan (68)">         
                                    </div>                                   
                                </div>
                                <div class="form-row">                                   
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s69">Persamaan (69)</label>
                                        <input id="s69" type="text" class="form-control" placeholder="Persamaan (69)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s70">Persamaan (70)</label>
                                        <input id="s70" type="text" class="form-control" placeholder="Persamaan (70)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s71">Persamaan (71)</label>
                                        <input id="s71" type="text" class="form-control" placeholder="Persamaan (71)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s72">Persamaan (72)</label>
                                        <input id="s72" type="text" class="form-control" placeholder="Persamaan (72)">         
                                    </div>                                   
                                </div>
                                <div class="form-row">                                   
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s73">Persamaan (73)</label>
                                        <input id="s73" type="text" class="form-control" placeholder="Persamaan (73)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s74">Persamaan (74)</label>
                                        <input id="s74" type="text" class="form-control" placeholder="Persamaan (74)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s75">Persamaan (75)</label>
                                        <input id="s75" type="text" class="form-control" placeholder="Persamaan (75)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s76">Persamaan (76)</label>
                                        <input id="s76" type="text" class="form-control" placeholder="Persamaan (76)">         
                                    </div>                                   
                                </div>
                                <div class="form-row">                                   
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s77">Persamaan (77)</label>
                                        <input id="s77" type="text" class="form-control" placeholder="Persamaan (77)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s78">Persamaan (78)</label>
                                        <input id="s78" type="text" class="form-control" placeholder="Persamaan (78)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s79">Persamaan (79)</label>
                                        <input id="s79" type="text" class="form-control" placeholder="Persamaan (79)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s80">Persamaan (80)</label>
                                        <input id="s80" type="text" class="form-control" placeholder="Persamaan (80)">         
                                    </div>                                   
                                </div>
                                <div class="form-row">                                   
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s81">Persamaan (81)</label>
                                        <input id="s81" type="text" class="form-control" placeholder="Persamaan (81)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s82">Persamaan (82)</label>
                                        <input id="s82" type="text" class="form-control" placeholder="Persamaan (82)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s83">Persamaan (83)</label>
                                        <input id="s83" type="text" class="form-control" placeholder="Persamaan (83)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s84">Persamaan (84)</label>
                                        <input id="s84" type="text" class="form-control" placeholder="Persamaan (84)">         
                                    </div>                                   
                                </div>
                                <div class="form-row">                                   
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s85">Persamaan (85)</label>
                                        <input id="s85" type="text" class="form-control" placeholder="Persamaan (85)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s86">Persamaan (86)</label>
                                        <input id="s86" type="text" class="form-control" placeholder="Persamaan (86)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s87">Persamaan (87)</label>
                                        <input id="s87" type="text" class="form-control" placeholder="Persamaan (87)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s88">Persamaan (88)</label>
                                        <input id="s88" type="text" class="form-control" placeholder="Persamaan (88)">         
                                    </div>                                   
                                </div>
                                <div class="form-row">                                   
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s89">Persamaan (89)</label>
                                        <input id="s89" type="text" class="form-control" placeholder="Persamaan (89)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s90">Persamaan (90)</label>
                                        <input id="s90" type="text" class="form-control" placeholder="Persamaan (90)">         
                                    </div>
                                    <div class="form-group col-md-3  mb-3">
                                        <label for="s91">Persamaan (91)</label>
                                        <input id="s91" type="text" class="form-control" placeholder="Persamaan (91)">         
                                    </div>                                  
                                </div>

                                <!-- Hasil Eliminasi Pembagian Ujung SEMENTARA -->
                                <!-- <div class="form-group col-md-6  mb-6">
                                    <label for="s1A">Persamaan (1A)</label>
                                    <input id="s1A" type="text" class="form-control" placeholder="Persamaan (1A)">         
                                </div>
                                <div class="form-group col-md-6  mb-6">
                                    <label for="s2A">Persamaan (2A)</label>
                                    <input id="s2A" type="text" class="form-control" placeholder="Persamaan (2A)">         
                                </div>
                                <div class="form-group col-md-6  mb-6">
                                    <label for="s3A">Persamaan (3A)</label>
                                    <input id="s3A" type="text" class="form-control" placeholder="Persamaan (3A)">         
                                </div>
                                <div class="form-group col-md-6  mb-6">
                                    <label for="s4A">Persamaan (4A)</label>
                                    <input id="s4A" type="text" class="form-control" placeholder="Persamaan (4A)">         
                                </div>
                                <div class="form-group col-md-6  mb-6">
                                    <label for="s5A">Persamaan (5A)</label>
                                    <input id="s5A" type="text" class="form-control" placeholder="Persamaan (5A)">         
                                </div>
                                <div class="form-group col-md-6  mb-6">
                                    <label for="s6A">Persamaan (6A)</label>
                                    <input id="s6A" type="text" class="form-control" placeholder="Persamaan (6A)">         
                                </div>
                                <div class="form-group col-md-6  mb-6">
                                    <label for="s7A">Persamaan (7A)</label>
                                    <input id="s7A" type="text" class="form-control" placeholder="Persamaan (7A)">         
                                </div>
                                <div class="form-group col-md-6  mb-6">
                                    <label for="s8A">Persamaan (8A)</label>
                                    <input id="s8A" type="text" class="form-control" placeholder="Persamaan (8A)">         
                                </div>
                                <div class="form-group col-md-6  mb-6">
                                    <label for="s9A">Persamaan (9A)</label>
                                    <input id="s9A" type="text" class="form-control" placeholder="Persamaan (9A)">         
                                </div>
                                <div class="form-group col-md-6  mb-6">
                                    <label for="s10A">Persamaan (10A)</label>
                                    <input id="s10A" type="text" class="form-control" placeholder="Persamaan (10A)">         
                                </div>
                                <div class="form-group col-md-6  mb-6">
                                    <label for="s11A">Persamaan (11A)</label>
                                    <input id="s11A" type="text" class="form-control" placeholder="Persamaan (11A)">         
                                </div>
                                <div class="form-group col-md-6  mb-6">
                                    <label for="s12A">Persamaan (12A)</label>
                                    <input id="s12A" type="text" class="form-control" placeholder="Persamaan (12A)">         
                                </div>
                                <div class="form-group col-md-6  mb-6">
                                    <label for="s13A">Persamaan (13A)</label>
                                    <input id="s13A" type="text" class="form-control" placeholder="Persamaan (13A)">         
                                </div> -->
                                <!-- BATAS Hasil Eliminasi Pembagian Ujung SEMENTARA -->

                                <!-- HASIL Nilai Koefisien-->
                                <div class="row">
                                    <div class="col-sm-4">
                                        <h5 class="text-success">Hasil Perhitungan</h5>
                                        
                                        <form action="{{ route('perhitungans.store') }}" method="POST">
                                            <div class="form-group">
                                            @csrf
                                            <label for="b0">Nilai B<sub>0</sub></label>
                                            <input id="b0" name="b0" type="text" class="form-control" placeholder="Nilai B0">
                                            @if ($errors->has('b0'))
                                                <div id="gagal_hitung">{{ $errors->first('b0') }}</div>
                                            @endif
                                            
                                            <label for="b1" style="padding-top: 10px;">Nilai B<sub>1</sub></label>
                                            <input id="b1" name="b1" type="text" class="form-control" placeholder="Nilai B1">
                                            @if ($errors->has('b1'))
                                                <div id="gagal_hitung">{{ $errors->first('b1') }}</div>
                                            @endif

                                            <label for="b2" style="padding-top: 10px;">Nilai B<sub>2</sub></label>
                                            <input id="b2" name="b2" type="text" class="form-control" placeholder="Nilai B2">
                                            @if ($errors->has('b2'))
                                                <div id="gagal_hitung">{{ $errors->first('b2') }}</div>
                                            @endif

                                            <label for="b3" style="padding-top: 10px;">Nilai B<sub>3</sub></label>
                                            <input id="b3" name="b3" type="text" class="form-control" placeholder="Nilai B3">
                                            @if ($errors->has('b3'))
                                                <div id="gagal_hitung">{{ $errors->first('b3') }}</div>
                                            @endif

                                            <label for="b4" style="padding-top: 10px;">Nilai B<sub>4</sub></label>
                                            <input id="b4" name="b4" type="text" class="form-control" placeholder="Nilai B4">
                                            @if ($errors->has('b4'))
                                                <div id="gagal_hitung">{{ $errors->first('b4') }}</div>
                                            @endif

                                            <label for="b5" style="padding-top: 10px;">Nilai B<sub>5</sub></label>
                                            <input id="b5" name="b5" type="text" class="form-control" placeholder="Nilai B4">
                                            @if ($errors->has('b5'))
                                                <div id="gagal_hitung">{{ $errors->first('b5') }}</div>
                                            @endif

                                            <label for="b6" style="padding-top: 10px;">Nilai B<sub>6</sub></label>
                                            <input id="b6" name="b6" type="text" class="form-control" placeholder="Nilai B6">
                                            @if ($errors->has('b6'))
                                                <div id="gagal_hitung">{{ $errors->first('b6') }}</div>
                                            @endif

                                            <label for="b7" style="padding-top: 10px;">Nilai B<sub>7</sub></label>
                                            <input id="b7" name="b7" type="text" class="form-control" placeholder="Nilai B7">
                                            @if ($errors->has('b7'))
                                                <div id="gagal_hitung">{{ $errors->first('b7') }}</div>
                                            @endif

                                            <label for="b8" style="padding-top: 10px;">Nilai B<sub>8</sub></label>
                                            <input id="b8" name="b8" type="text" class="form-control" placeholder="Nilai B8">
                                            @if ($errors->has('b8'))
                                                <div id="gagal_hitung">{{ $errors->first('b8') }}</div>
                                            @endif

                                            <label for="b9" style="padding-top: 10px;">Nilai B<sub>9</sub></label>
                                            <input id="b9" name="b9" type="text" class="form-control" placeholder="Nilai B9">
                                            @if ($errors->has('b9'))
                                                <div id="gagal_hitung">{{ $errors->first('b9') }}</div>
                                            @endif

                                            <label for="b10" style="padding-top: 10px;">Nilai B<sub>10</sub></label>
                                            <input id="b10" name="b10" type="text" class="form-control" placeholder="Nilai B10">
                                            @if ($errors->has('b10'))
                                                <div id="gagal_hitung">{{ $errors->first('b10') }}</div>
                                            @endif

                                            <label for="b11" style="padding-top: 10px;">Nilai B<sub>11</sub></label>
                                            <input id="b11" name="b11" type="text" class="form-control" placeholder="Nilai B11">
                                            @if ($errors->has('b11'))
                                                <div id="gagal_hitung">{{ $errors->first('b11') }}</div>
                                            @endif

                                            <label for="b12" style="padding-top: 10px;">Nilai B<sub>12</sub></label>
                                            <input id="b12" name="b12" type="text" class="form-control" placeholder="Nilai B12">
                                            @if ($errors->has('b12'))
                                                <div id="gagal_hitung">{{ $errors->first('b12') }}</div>
                                            @endif

                                            <label for="b13" style="padding-top: 10px;">Nilai B<sub>13</sub></label>
                                            <input id="b13" name="b13" type="text" class="form-control" placeholder="Nilai B13">
                                            </div>
                                            @if ($errors->has('b13'))
                                                <div id="gagal_hitung">{{ $errors->first('b13') }}</div>
                                            @endif
                                        </form>
                                    
                                    </div>
                                    <div class="col-sm-8">
                                        Rumus Persamaan
                                        <table class="table table-striped" style="font-size: 12px;">
                                            <tr>
                                                <th>Persamaan(1)</th>
                                                <td colspan="3">
                                                    &Sigma;YX<sub>1</sub>= B<sub>1</sub> &Sigma;X<sub>1</sub><sup>2</sup> + B<sub>2</sub> &Sigma;X<sub>1</sub>X<sub>2</sub> + B<sub>3</sub> &Sigma;X<sub>1</sub>X<sub>3</sub> + B<sub>4</sub> &Sigma;X<sub>1</sub>X<sub>4</sub> + B<sub>5</sub> &Sigma;X<sub>1</sub>X<sub>5</sub> + B<sub>6</sub> &Sigma;X<sub>1</sub>X<sub>6</sub> + B<sub>7</sub> &Sigma;X<sub>1</sub>X<sub>7</sub> + B<sub>8</sub> &Sigma;X<sub>1</sub>X<sub>8</sub> + B<sub>9</sub> &Sigma;X<sub>1</sub>X<sub>9</sub> + B<sub>10</sub> &Sigma;X<sub>1</sub>X<sub>10</sub> + B<sub>11</sub> &Sigma;X<sub>1</sub>X<sub>11</sub> + B<sub>12</sub> &Sigma;X<sub>1</sub>X<sub>12</sub> + B<sub>13</sub> &Sigma;X<sub>1</sub>X<sub>13</sub>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Persamaan(2)</th>
                                                <td colspan="3">
                                                    &Sigma;YX<sub>2</sub>= B<sub>1</sub> &Sigma;X<sub>1</sub>&Sigma;X<sub>2</sub> + B<sub>2</sub> &Sigma;X<sub>2</sub><sup>2</sup> + B<sub>3</sub> &Sigma;X<sub>2</sub>X<sub>3</sub> + B<sub>4</sub> &Sigma;X<sub>2</sub>X<sub>4</sub> + B<sub>5</sub> &Sigma;X<sub>2</sub>X<sub>5</sub> + B<sub>6</sub> &Sigma;X<sub>2</sub>X<sub>6</sub> + B<sub>7</sub> &Sigma;X<sub>2</sub>X<sub>7</sub> + B<sub>8</sub> &Sigma;X<sub>2</sub>X<sub>8</sub> + B<sub>9</sub> &Sigma;X<sub>2</sub>X<sub>9</sub> + B<sub>10</sub> &Sigma;X<sub>2</sub>X<sub>10</sub> + B<sub>11</sub> &Sigma;X<sub>2</sub>X<sub>11</sub> + B<sub>12</sub> &Sigma;X<sub>2</sub>X<sub>12</sub> + B<sub>13</sub> &Sigma;X<sub>2</sub>X<sub>13</sub>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Persamaan(3)</th>
                                                <td colspan="3">
                                                    &Sigma;YX<sub>3</sub>= B<sub>1</sub> &Sigma;X<sub>1</sub>&Sigma;X<sub>3</sub> + B<sub>2</sub> &Sigma;X<sub>2</sub> &Sigma;X<sub>3</sub> + B<sub>3</sub> &Sigma;X<sub>3</sub><sup>2</sup> + B<sub>4</sub> &Sigma;X<sub>3</sub>X<sub>4</sub> + B<sub>5</sub> &Sigma;X<sub>3</sub>X<sub>5</sub> + B<sub>6</sub> &Sigma;X<sub>3</sub>X<sub>6</sub> + B<sub>7</sub> &Sigma;X<sub>3</sub>X<sub>7</sub> + B<sub>8</sub> &Sigma;X<sub>3</sub>X<sub>8</sub> + B<sub>9</sub> &Sigma;X<sub>3</sub>X<sub>9</sub> + B<sub>10</sub> &Sigma;X<sub>3</sub>X<sub>10</sub> + B<sub>11</sub> &Sigma;X<sub>3</sub>X<sub>11</sub> + B<sub>12</sub> &Sigma;X<sub>3</sub>X<sub>12</sub> + B<sub>13</sub> &Sigma;X<sub>3</sub>X<sub>13</sub>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Persamaan(4)</th>
                                                <td colspan="3">
                                                    &Sigma;YX<sub>4</sub>= B<sub>1</sub> &Sigma;X<sub>1</sub>&Sigma;X<sub>4</sub> + B<sub>2</sub> &Sigma;X<sub>2</sub> &Sigma;X<sub>4</sub> + B<sub>3</sub> &Sigma;X<sub>3</sub> &Sigma;X<sub>4</sub> + B<sub>4</sub> &Sigma;X<sub>4</sub><sup>2</sup> + B<sub>5</sub> &Sigma;X<sub>4</sub>X<sub>5</sub> + B<sub>6</sub> &Sigma;X<sub>4</sub>X<sub>6</sub> + B<sub>7</sub> &Sigma;X<sub>4</sub>X<sub>7</sub> + B<sub>8</sub> &Sigma;X<sub>4</sub>X<sub>8</sub> + B<sub>9</sub> &Sigma;X<sub>4</sub>X<sub>9</sub> + B<sub>10</sub> &Sigma;X<sub>4</sub>X<sub>10</sub> + B<sub>11</sub> &Sigma;X<sub>4</sub>X<sub>11</sub> + B<sub>12</sub> &Sigma;X<sub>4</sub>X<sub>12</sub> + B<sub>13</sub> &Sigma;X<sub>4</sub>X<sub>13</sub>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Persamaan(5)</th>
                                                <td colspan="3">
                                                    &Sigma;YX<sub>5</sub>= B<sub>1</sub> &Sigma;X<sub>1</sub>&Sigma;X<sub>5</sub> + B<sub>2</sub> &Sigma;X<sub>2</sub> &Sigma;X<sub>5</sub> + B<sub>3</sub> &Sigma;X<sub>3</sub> &Sigma;X<sub>5</sub> + B<sub>4</sub> &Sigma;X<sub>4</sub> &Sigma;X<sub>5</sub> + B<sub>5</sub> &Sigma;X<sub>5</sub><sup>2</sup> + B<sub>6</sub> &Sigma;X<sub>5</sub>X<sub>6</sub> + B<sub>7</sub> &Sigma;X<sub>5</sub>X<sub>7</sub> + B<sub>8</sub> &Sigma;X<sub>5</sub>X<sub>8</sub> + B<sub>9</sub> &Sigma;X<sub>5</sub>X<sub>9</sub> + B<sub>10</sub> &Sigma;X<sub>5</sub>X<sub>10</sub> + B<sub>11</sub> &Sigma;X<sub>5</sub>X<sub>11</sub> + B<sub>12</sub> &Sigma;X<sub>5</sub>X<sub>12</sub> + B<sub>13</sub> &Sigma;X<sub>5</sub>X<sub>13</sub>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Persamaan(6)</th>
                                                <td colspan="3">
                                                    &Sigma;YX<sub>6</sub>= B<sub>1</sub> &Sigma;X<sub>1</sub> &Sigma;X<sub>6</sub> + B<sub>2</sub> &Sigma;X<sub>2</sub> &Sigma;X<sub>6</sub> + B<sub>3</sub> &Sigma;X<sub>3</sub> &Sigma;X<sub>6</sub> + B<sub>4</sub> &Sigma;X<sub>4</sub> &Sigma;X<sub>6</sub> + B<sub>5</sub> &Sigma;X<sub>5</sub> &Sigma;X<sub>6</sub> + B<sub>6</sub> &Sigma;X<sub>6</sub><sup>2</sup> + B<sub>7</sub> &Sigma;X<sub>6</sub>X<sub>7</sub> + B<sub>8</sub> &Sigma;X<sub>6</sub>X<sub>8</sub> + B<sub>9</sub> &Sigma;X<sub>6</sub>X<sub>9</sub> + B<sub>10</sub> &Sigma;X<sub>6</sub>X<sub>10</sub> + B<sub>11</sub> &Sigma;X<sub>6</sub>X<sub>11</sub> + B<sub>12</sub> &Sigma;X<sub>6</sub>X<sub>12</sub> + B<sub>13</sub> &Sigma;X<sub>6</sub>X<sub>13</sub>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Persamaan(7)</th>
                                                <td colspan="3">
                                                    &Sigma;YX<sub>7</sub>= B<sub>1</sub> &Sigma;X<sub>1</sub> &Sigma;X<sub>7</sub> + B<sub>2</sub> &Sigma;X<sub>2</sub> &Sigma;X<sub>7</sub> + B<sub>3</sub> &Sigma;X<sub>3</sub> &Sigma;X<sub>7</sub> + B<sub>4</sub> &Sigma;X<sub>4</sub> &Sigma;X<sub>7</sub> + B<sub>5</sub> &Sigma;X<sub>5</sub> &Sigma;X<sub>7</sub> + B<sub>6</sub> &Sigma;X<sub>6</sub> &Sigma;X<sub>7</sub> + B<sub>7</sub> &Sigma;X<sub>7</sub><sup>2</sup> + B<sub>8</sub> &Sigma;X<sub>7</sub>X<sub>8</sub> + B<sub>9</sub> &Sigma;X<sub>7</sub>X<sub>9</sub> + B<sub>10</sub> &Sigma;X<sub>7</sub>X<sub>10</sub> + B<sub>11</sub> &Sigma;X<sub>7</sub>X<sub>11</sub> + B<sub>12</sub> &Sigma;X<sub>7</sub>X<sub>12</sub> + B<sub>13</sub> &Sigma;X<sub>7</sub>X<sub>13</sub>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Persamaan(8)</th>
                                                <td colspan="3">
                                                    &Sigma;YX<sub>8</sub>= B<sub>1</sub> &Sigma;X<sub>1</sub> &Sigma;X<sub>8</sub> + B<sub>2</sub> &Sigma;X<sub>2</sub> &Sigma;X<sub>8</sub> + B<sub>3</sub> &Sigma;X<sub>3</sub> &Sigma;X<sub>8</sub> + B<sub>4</sub> &Sigma;X<sub>4</sub> &Sigma;X<sub>8</sub> + B<sub>5</sub> &Sigma;X<sub>5</sub> &Sigma;X<sub>8</sub> + B<sub>6</sub> &Sigma;X<sub>6</sub> &Sigma;X<sub>8</sub> + B<sub>7</sub> &Sigma;X<sub>7</sub> &Sigma;X<sub>8</sub> + B<sub>8</sub> &Sigma;X<sub>8</sub><sup>2</sup> + B<sub>9</sub> &Sigma;X<sub>8</sub>X<sub>9</sub> + B<sub>10</sub> &Sigma;X<sub>8</sub>X<sub>10</sub> + B<sub>11</sub> &Sigma;X<sub>8</sub>X<sub>11</sub> + B<sub>12</sub> &Sigma;X<sub>8</sub>X<sub>12</sub> + B<sub>13</sub> &Sigma;X<sub>8</sub>X<sub>13</sub>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Persamaan(9)</th>
                                                <td colspan="3">
                                                    &Sigma;YX<sub>9</sub>= B<sub>1</sub> &Sigma;X<sub>1</sub> &Sigma;X<sub>9</sub> + B<sub>2</sub> &Sigma;X<sub>2</sub> &Sigma;X<sub>9</sub> + B<sub>3</sub> &Sigma;X<sub>3</sub> &Sigma;X<sub>9</sub> + B<sub>4</sub> &Sigma;X<sub>4</sub> &Sigma;X<sub>9</sub> + B<sub>5</sub> &Sigma;X<sub>5</sub> &Sigma;X<sub>9</sub> + B<sub>6</sub> &Sigma;X<sub>6</sub> &Sigma;X<sub>9</sub> + B<sub>7</sub> &Sigma;X<sub>7</sub> &Sigma;X<sub>9</sub> + B<sub>8</sub> &Sigma;X<sub>8</sub> &Sigma;X<sub>9</sub> + B<sub>9</sub> &Sigma;X<sub>9</sub><sup>2</sup> + B<sub>10</sub> &Sigma;X<sub>9</sub>X<sub>10</sub> + B<sub>11</sub> &Sigma;X<sub>9</sub>X<sub>11</sub> + B<sub>12</sub> &Sigma;X<sub>9</sub>X<sub>12</sub> + B<sub>13</sub> &Sigma;X<sub>9</sub>X<sub>13</sub>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Persamaan(10)</th>
                                                <td colspan="3">
                                                    &Sigma;YX<sub>10</sub>= B<sub>1</sub> &Sigma;X<sub>1</sub> &Sigma;X<sub>10</sub> + B<sub>2</sub> &Sigma;X<sub>2</sub> &Sigma;X<sub>10</sub> + B<sub>3</sub> &Sigma;X<sub>3</sub> &Sigma;X<sub>10</sub> + B<sub>4</sub> &Sigma;X<sub>4</sub> &Sigma;X<sub>10</sub> + B<sub>5</sub> &Sigma;X<sub>5</sub> &Sigma;X<sub>10</sub> + B<sub>6</sub> &Sigma;X<sub>6</sub> &Sigma;X<sub>10</sub> + B<sub>7</sub> &Sigma;X<sub>7</sub> &Sigma;X<sub>10</sub> + B<sub>8</sub> &Sigma;X<sub>8</sub> &Sigma;X<sub>10</sub> + B<sub>9</sub> &Sigma;X<sub>9</sub> &Sigma;X<sub>10</sub> + B<sub>10</sub> &Sigma;X<sub>10</sub><sup>2</sup> + B<sub>11</sub> &Sigma;X<sub>10</sub>X<sub>11</sub> + B<sub>12</sub> &Sigma;X<sub>10</sub>X<sub>12</sub> + B<sub>13</sub> &Sigma;X<sub>10</sub>X<sub>13</sub>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Persamaan(11)</th>
                                                <td colspan="3">
                                                    &Sigma;YX<sub>11</sub>= B<sub>1</sub> &Sigma;X<sub>1</sub> &Sigma;X<sub>11</sub> + B<sub>2</sub> &Sigma;X<sub>2</sub> &Sigma;X<sub>11</sub> + B<sub>3</sub> &Sigma;X<sub>3</sub> &Sigma;X<sub>11</sub> + B<sub>4</sub> &Sigma;X<sub>4</sub> &Sigma;X<sub>11</sub> + B<sub>5</sub> &Sigma;X<sub>5</sub> &Sigma;X<sub>11</sub> + B<sub>6</sub> &Sigma;X<sub>6</sub> &Sigma;X<sub>11</sub> + B<sub>7</sub> &Sigma;X<sub>7</sub> &Sigma;X<sub>11</sub> + B<sub>8</sub> &Sigma;X<sub>8</sub> &Sigma;X<sub>11</sub> + B<sub>9</sub> &Sigma;X<sub>9</sub> &Sigma;X<sub>11</sub> + B<sub>10</sub> &Sigma;X<sub>10</sub> &Sigma;X<sub>11</sub> + B<sub>11</sub> &Sigma;X<sub>11</sub><sup>2</sup> + B<sub>12</sub> &Sigma;X<sub>11</sub>X<sub>12</sub> + B<sub>13</sub> &Sigma;X<sub>11</sub>X<sub>13</sub>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Persamaan(12)</th>
                                                <td colspan="3">
                                                    &Sigma;YX<sub>12</sub>= B<sub>1</sub> &Sigma;X<sub>1</sub> &Sigma;X<sub>12</sub> + B<sub>2</sub> &Sigma;X<sub>2</sub> &Sigma;X<sub>12</sub> + B<sub>3</sub> &Sigma;X<sub>3</sub> &Sigma;X<sub>12</sub> + B<sub>4</sub> &Sigma;X<sub>4</sub> &Sigma;X<sub>12</sub> + B<sub>5</sub> &Sigma;X<sub>5</sub> &Sigma;X<sub>12</sub> + B<sub>6</sub> &Sigma;X<sub>6</sub> &Sigma;X<sub>12</sub> + B<sub>7</sub> &Sigma;X<sub>7</sub> &Sigma;X<sub>12</sub> + B<sub>8</sub> &Sigma;X<sub>8</sub> &Sigma;X<sub>12</sub> + B<sub>9</sub> &Sigma;X<sub>9</sub> &Sigma;X<sub>12</sub> + B<sub>10</sub> &Sigma;X<sub>10</sub> &Sigma;X<sub>12</sub> + B<sub>11</sub> &Sigma;X<sub>12</sub> &Sigma;X<sub>12</sub> + B<sub>12</sub> &Sigma;X<sub>12</sub><sup>2</sup> + B<sub>13</sub> &Sigma;X<sub>12</sub>X<sub>13</sub>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Persamaan(13)</th>
                                                <td colspan="3">
                                                    &Sigma;YX<sub>13</sub>= B<sub>1</sub> &Sigma;X<sub>1</sub> &Sigma;X<sub>13</sub> + B<sub>2</sub> &Sigma;X<sub>2</sub> &Sigma;X<sub>13</sub> + B<sub>3</sub> &Sigma;X<sub>3</sub> &Sigma;X<sub>13</sub> + B<sub>4</sub> &Sigma;X<sub>4</sub> &Sigma;X<sub>13</sub> + B<sub>5</sub> &Sigma;X<sub>5</sub> &Sigma;X<sub>13</sub> + B<sub>6</sub> &Sigma;X<sub>6</sub> &Sigma;X<sub>13</sub> + B<sub>7</sub> &Sigma;X<sub>7</sub> &Sigma;X<sub>13</sub> + B<sub>8</sub> &Sigma;X<sub>8</sub> &Sigma;X<sub>13</sub> + B<sub>9</sub> &Sigma;X<sub>9</sub> &Sigma;X<sub>13</sub> + B<sub>10</sub> &Sigma;X<sub>10</sub> &Sigma;X<sub>13</sub> + B<sub>11</sub> &Sigma;X<sub>13</sub> &Sigma;X<sub>13</sub> + B<sub>12</sub> &Sigma;X<sub>12</sub> &Sigma;X<sub>13</sub> + B<sub>13</sub> &Sigma;X<sub>13</sub><sup>2</sup>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>Persamaan(14)</th>
                                                <td>Eliminasi persamaan (1A) dengan (2A)</td>
                                                <th>Persamaan(26)</th>
                                                <td>Eliminasi persamaan (14A) dengan (15A)</td>
                                            </tr>

                                            <tr>
                                                <th>Persamaan(37)</th>
                                                <td>Eliminasi persamaan (26A) dengan (27A)</td>
                                                <th>Persamaan(47)</th>
                                                <td>Eliminasi persamaan (37A) dengan (38A)</td>
                                            </tr>

                                            <tr>
                                                <th>Persamaan(56)</th>
                                                <td>Eliminasi persamaan (47A) dengan (48A)</td>
                                                <th>Persamaan(64)</th>
                                                <td>Eliminasi persamaan (56A) dengan (57A)</td>
                                            </tr>

                                            <tr>
                                                <th>Persamaan(71)</th>
                                                <td>Eliminasi persamaan (64A) dengan (65A)</td>
                                                <th>Persamaan(77)</th>
                                                <td>Eliminasi persamaan (71A) dengan (72A)</td>
                                            </tr>

                                            <tr>
                                                <th>Persamaan(82)</th>
                                                <td>Eliminasi persamaan (77A) dengan (78A)</td>
                                                <th>Persamaan(86)</th>
                                                <td>Eliminasi persamaan (82A) dengan (83A)</td>
                                            </tr>

                                            <tr>
                                                <th>Persamaan(89)</th>
                                                <td>Eliminasi persamaan (86A) dengan (87A)</td>
                                                <th>Persamaan(91)</th>
                                                <td>Eliminasi persamaan (89A) dengan (90A)</td>
                                            </tr>                                           
                                        </table>
                                    </div>
                                    
                                    <div class="col-12">
                                        <!-- <div class="table-responsive-sm">
                                        
                                        </div> -->
                                        <div class="form-group">
                                            <label for="s10">Rumus Persamaan Regresi Linear Berganda</label>
                                            <input id="regresi" type="text" class="form-control"placeholder="Persamaan Regresi Linear Berganda">         
                                        </div>
                                        <div class="form-group" style="float: right;">
                                            <button type="button" class="btn btn-primary" id="simpan_rumus">Simpan</button>
                                            <a class="btn btn-primary" href="{{ route('dashboard.index')}}" role="button">Kembali</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection