 <div class="table-responsive">
    <!-- <table class="table table-striped" id="perhitungans-table"> -->
    <table class="table table-striped" id="perhitungans-table">
        <thead style="display:table;width:100%;table-layout:fixed;text-align: center;">
            <tr>
                <th>No</th>
                <th>Y</th>
                <th>X<sub>1</sub></th>
                <th>X<sub>2</sub></th>
                <th>X<sub>3</sub></th>
                <th>X<sub>4</sub></th>
                <th>X<sub>5</sub></th>
                <th>X<sub>6</sub></th>
                <th>X<sub>7</sub></th>
                <th>X<sub>8</sub></th>
                <th>X<sub>9</sub></th>
                <th>X<sub>10</sub></th>
                <th>X<sub>11</sub></th>
                <th>X<sub>12</sub></th>
                <th>X<sub>13</sub></th>
                
                <th>X<sub>1</sub><sup>2</sup></th>
                <th>X<sub>2</sub><sup>2</sup></th>
                <th>X<sub>3</sub><sup>2</sup></th>
                <th>X<sub>4</sub><sup>2</sup></th>
                <th>X<sub>5</sub><sup>2</sup></th>
                <th>X<sub>6</sub><sup>2</sup></th>
                <th>X<sub>7</sub><sup>2</sup></th>
                <th>X<sub>8</sub><sup>2</sup></th>
                <th>X<sub>9</sub><sup>2</sup></th>
                <th>X<sub>10</sub><sup>2</sup></th>
                <th>X<sub>11</sub><sup>2</sup></th>
                <th>X<sub>12</sub><sup>2</sup></th>
                <th>X<sub>13</sub><sup>2</sup></th>

                <th>Y*X<sub>1</sub></th>
                <th>Y*X<sub>2</sub></th>
                <th>Y*X<sub>3</sub></th>
                <th>Y*X<sub>4</sub></th>
                <th>Y*X<sub>5</sub></th>
                <th>Y*X<sub>6</sub></th>
                <th>Y*X<sub>7</sub></th>
                <th>Y*X<sub>8</sub></th>
                <th>Y*X<sub>9</sub></th>
                <th>Y*X<sub>10</sub></th>
                <th>Y*X<sub>11</sub></th>
                <th>Y*X<sub>12</sub></th>
                <th>Y*X<sub>13</sub></th>

                <th>X<sub>1</sub>*X<sub>2</sub></th>
                <th>X<sub>1</sub>*X<sub>3</sub></th>
                <th>X<sub>1</sub>*X<sub>4</sub></th>
                <th>X<sub>1</sub>*X<sub>5</sub></th>
                <th>X<sub>1</sub>*X<sub>6</sub></th>
                <th>X<sub>1</sub>*X<sub>7</sub></th>
                <th>X<sub>1</sub>*X<sub>8</sub></th>
                <th>X<sub>1</sub>*X<sub>9</sub></th>
                <th>X<sub>1</sub>*X<sub>10</sub></th>
                <th>X<sub>1</sub>*X<sub>11</sub></th>
                <th>X<sub>1</sub>*X<sub>12</sub></th>
                <th>X<sub>1</sub>*X<sub>13</sub></th> 

                <th>X<sub>2</sub>*X<sub>3</sub></th>
                <th>X<sub>2</sub>*X<sub>4</sub></th>
                <th>X<sub>2</sub>*X<sub>5</sub></th>
                <th>X<sub>2</sub>*X<sub>6</sub></th>
                <th>X<sub>2</sub>*X<sub>7</sub></th>
                <th>X<sub>2</sub>*X<sub>8</sub></th>
                <th>X<sub>2</sub>*X<sub>9</sub></th>
                <th>X<sub>2</sub>*X<sub>10</sub></th>
                <th>X<sub>2</sub>*X<sub>11</sub></th>
                <th>X<sub>2</sub>*X<sub>12</sub></th>
                <th>X<sub>2</sub>*X<sub>13</sub></th> 

                <th>X<sub>3</sub>*X<sub>4</sub></th>
                <th>X<sub>3</sub>*X<sub>5</sub></th>
                <th>X<sub>3</sub>*X<sub>6</sub></th>
                <th>X<sub>3</sub>*X<sub>7</sub></th>
                <th>X<sub>3</sub>*X<sub>8</sub></th>
                <th>X<sub>3</sub>*X<sub>9</sub></th>
                <th>X<sub>3</sub>*X<sub>10</sub></th>
                <th>X<sub>3</sub>*X<sub>11</sub></th>
                <th>X<sub>3</sub>*X<sub>12</sub></th>
                <th>X<sub>3</sub>*X<sub>13</sub></th> 

                <th>X<sub>4</sub>*X<sub>5</sub></th>
                <th>X<sub>4</sub>*X<sub>6</sub></th>
                <th>X<sub>4</sub>*X<sub>7</sub></th>
                <th>X<sub>4</sub>*X<sub>8</sub></th>
                <th>X<sub>4</sub>*X<sub>9</sub></th>
                <th>X<sub>4</sub>*X<sub>10</sub></th>
                <th>X<sub>4</sub>*X<sub>11</sub></th>
                <th>X<sub>4</sub>*X<sub>12</sub></th>
                <th>X<sub>4</sub>*X<sub>13</sub></th> 

                <th>X<sub>5</sub>*X<sub>6</sub></th>
                <th>X<sub>5</sub>*X<sub>7</sub></th>
                <th>X<sub>5</sub>*X<sub>8</sub></th>
                <th>X<sub>5</sub>*X<sub>9</sub></th>
                <th>X<sub>5</sub>*X<sub>10</sub></th>
                <th>X<sub>5</sub>*X<sub>11</sub></th>
                <th>X<sub>5</sub>*X<sub>12</sub></th>
                <th>X<sub>5</sub>*X<sub>13</sub></th> 

                <th>X<sub>6</sub>*X<sub>7</sub></th>
                <th>X<sub>6</sub>*X<sub>8</sub></th>
                <th>X<sub>6</sub>*X<sub>9</sub></th>
                <th>X<sub>6</sub>*X<sub>10</sub></th>
                <th>X<sub>6</sub>*X<sub>11</sub></th>
                <th>X<sub>6</sub>*X<sub>12</sub></th>
                <th>X<sub>6</sub>*X<sub>13</sub></th>

                <th>X<sub>7</sub>*X<sub>8</sub></th>
                <th>X<sub>7</sub>*X<sub>9</sub></th>
                <th>X<sub>7</sub>*X<sub>10</sub></th>
                <th>X<sub>7</sub>*X<sub>11</sub></th>
                <th>X<sub>7</sub>*X<sub>12</sub></th>
                <th>X<sub>7</sub>*X<sub>13</sub></th>

                <th>X<sub>8</sub>*X<sub>9</sub></th>
                <th>X<sub>8</sub>*X<sub>10</sub></th>
                <th>X<sub>8</sub>*X<sub>11</sub></th>
                <th>X<sub>8</sub>*X<sub>12</sub></th>
                <th>X<sub>8</sub>*X<sub>13</sub></th>

                <th>X<sub>9</sub>*X<sub>10</sub></th>
                <th>X<sub>9</sub>*X<sub>11</sub></th>
                <th>X<sub>9</sub>*X<sub>12</sub></th>
                <th>X<sub>9</sub>*X<sub>13</sub></th>

                <th>X<sub>10</sub>*X<sub>11</sub></th>
                <th>X<sub>10</sub>*X<sub>12</sub></th>
                <th>X<sub>10</sub>*X<sub>13</sub></th>

                <th>X<sub>11</sub>*X<sub>12</sub></th>
                <th>X<sub>11</sub>*X<sub>13</sub></th>

                <th>X<sub>12</sub>*X<sub>13</sub></th>
            </tr>
        </thead>
        <tbody style="display: block;height: 250px;overflow: auto;width:12000px;">
        @php
           $y = $x1 = $x2 = $x3 = $x4 = $x5 = $x6 = $x7 = $x8 = $x9 = $x10 = $x11 = $x12 = $x13 = $y_2 = $x1_2 = $x2_2 = $x3_2 = $x4_2 = $x5_2 = $x6_2 = $x7_2 = $x8_2 = $x9_2 = $x10_2 = $x11_2 = $x12_2 = $x13_2 = $yx1 = $yx2 = $yx3 = $yx4 = $yx5 = $yx6 = $yx7 = $yx8 = $yx9 = $yx10 = $yx11 = $yx12 = $yx13 = $x1x2 = $x1x3 = $x1x4 = $x1x5  = $x1x6 = $x1x7 = $x1x8 = $x1x9 = $x1x10 = $x1x11 = $x1x12 = $x1x13 = $x2x3 = $x2x4 = $x2x5 = $x2x5 = $x2x6 = $x2x7 = $x2x8 = $x2x9 = $x2x10 = $x2x11 = $x2x12 = $x2x13 = $x3x4 = $x3x5 = $x3x6 = $x3x7 = $x3x8 = $x3x9 = $x3x10 = $x3x11  = $x3x12 = $x3x13 = $x4x5 = $x4x6 = $x4x7 = $x4x8 = $x4x9 = $x4x10 = $x4x11  = $x4x12 = $x4x13 = $x5x6 = $x5x7 = $x5x8 = $x5x9 = $x5x10 = $x5x11  = $x5x12 = $x5x13 = $x6x7 = $x6x8 = $x6x9 = $x6x10 = $x6x11 = $x6x12 = $x6x13 = $x7x8 = $x7x9 = $x7x10 = $x7x11  = $x7x12 = $x7x13 = $x8x9 = $x8x10 = $x8x11  = $x8x12 = $x8x13 = $x9x10 = $x9x11  = $x9x12 = $x9x13 = $x10x11  = $x10x12 = $x10x13 = $x11x12 = $x11x13 = $x12x13 = 0;
        @endphp

        @forelse($mobkas as $nomor => $row)

            <tr style="display:table;width:100%;table-layout:fixed;text-align: center;">
                <td>{{ ++$nomor }}</td>
                <td>{!! $row->harga !!}</td>
                <td>{!! $row->merek_id !!}</td>
                <td>{!! $row->model_merek_id !!}</td>
                <td>{!! $row->tipe_model_id !!}</td>
                <td>{!! $row->warna_id !!}</td>
                <td>{!! $row->tahun !!}</td>
                <td>{!! $row->transmisi_id !!}</td>
                <td>{!! $row->mesin_id !!}</td>
                <td>{!! $row->sis_rem_id !!}</td>
                <td>{!! $row->kemudi_id !!}</td>
                <td>{!! $row->suspensi_id !!}</td>
                <td>{!! $row->eksterior_id !!}</td>
                <td>{!! $row->interior_id !!}</td>
                <td>{!! $row->dokumen_id !!}</td>

                <!-- Pengkuadratan -->
                <td>{{ pow($row->merek_id,2) }}</td>
                <td>{{ pow($row->model_merek_id,2)}}</td>
                <td>{{ pow($row->tipe_model_id,2)}}</td>
                <td>{{ pow($row->warna_id,2)}}</td>
                <td>{{ pow($row->tahun,2)}}</td>
                <td>{{ pow($row->transmisi_id,2)}}</td>
                <td>{{ pow($row->mesin_id,2) }}</td>
                <td>{{ pow($row->sis_rem_id,2) }}</td>
                <td>{{ pow($row->kemudi_id,2) }}</td>
                <td>{{ pow($row->suspensi_id,2) }}</td>
                <td>{{ pow($row->eksterior_id,2) }}</td>
                <td>{{ pow($row->interior_id,2) }}</td>
                <td>{{ pow($row->dokumen_id,2) }}</td>

                <!-- Perkalian variabel tak bebas dengan variabel bebas -->
                <td>{{ $row->harga * $row->merek_id}}</td>
                <td>{{ $row->harga * $row->model_merek_id}}</td>
                <td>{{ $row->harga * $row->tipe_model_id}}</td>
                <td>{{ $row->harga * $row->warna_id}}</td>
                <td>{{ $row->harga * $row->tahun}}</td>
                <td>{{ $row->harga * $row->transmisi_id}}</td>
                <td>{{ $row->harga * $row->mesin_id}}</td>
                <td>{{ $row->harga * $row->sis_rem_id}}</td>
                <td>{{ $row->harga * $row->kemudi_id}}</td>
                <td>{{ $row->harga * $row->suspensi_id}}</td>
                <td>{{ $row->harga * $row->eksterior_id}}</td>
                <td>{{ $row->harga * $row->interior_id}}</td>
                <td>{{ $row->harga * $row->dokumen_id}}</td>

                <!-- Perkalian silang X1 -->
                <td>{{ $row->merek_id * $row->model_merek_id}}</td>
                <td>{{ $row->merek_id * $row->tipe_model_id}}</td>
                <td>{{ $row->merek_id * $row->warna_id}}</td>
                <td>{{ $row->merek_id * $row->tahun}}</td>
                <td>{{ $row->merek_id * $row->transmisi_id}}</td>
                <td>{{ $row->merek_id * $row->mesin_id}}</td>
                <td>{{ $row->merek_id * $row->sis_rem_id}}</td>
                <td>{{ $row->merek_id * $row->kemudi_id}}</td>
                <td>{{ $row->merek_id * $row->suspensi_id}}</td>
                <td>{{ $row->merek_id * $row->eksterior_id}}</td>
                <td>{{ $row->merek_id * $row->interior_id}}</td>
                <td>{{ $row->merek_id * $row->dokumen_id}}</td>

                <!-- Perkalian silang X2 -->
                <td>{{ $row->model_merek_id * $row->tipe_model_id}}</td>
                <td>{{ $row->model_merek_id * $row->warna_id}}</td>
                <td>{{ $row->model_merek_id * $row->tahun}}</td>
                <td>{{ $row->model_merek_id * $row->transmisi_id}}</td>
                <td>{{ $row->model_merek_id * $row->mesin_id}}</td>
                <td>{{ $row->model_merek_id * $row->sis_rem_id}}</td>
                <td>{{ $row->model_merek_id * $row->kemudi_id}}</td>
                <td>{{ $row->model_merek_id * $row->suspensi_id}}</td>
                <td>{{ $row->model_merek_id * $row->eksterior_id}}</td>
                <td>{{ $row->model_merek_id * $row->interior_id}}</td>
                <td>{{ $row->model_merek_id * $row->dokumen_id}}</td>

                <!-- Perkalian silang X3 -->
                <td>{{ $row->tipe_model_id * $row->warna_id}}</td>
                <td>{{ $row->tipe_model_id * $row->tahun}}</td>
                <td>{{ $row->tipe_model_id * $row->transmisi_id}}</td>
                <td>{{ $row->tipe_model_id * $row->mesin_id}}</td>
                <td>{{ $row->tipe_model_id * $row->sis_rem_id}}</td>
                <td>{{ $row->tipe_model_id * $row->kemudi_id}}</td>
                <td>{{ $row->tipe_model_id * $row->suspensi_id}}</td>
                <td>{{ $row->tipe_model_id * $row->eksterior_id}}</td>
                <td>{{ $row->tipe_model_id * $row->interior_id}}</td>
                <td>{{ $row->tipe_model_id * $row->dokumen_id}}</td>

                <!-- Perkalian silang X4 -->
                <td>{{ $row->warna_id * $row->tahun}}</td>
                <td>{{ $row->warna_id * $row->transmisi_id}}</td>
                <td>{{ $row->warna_id * $row->mesin_id}}</td>
                <td>{{ $row->warna_id * $row->sis_rem_id}}</td>
                <td>{{ $row->warna_id * $row->kemudi_id}}</td>
                <td>{{ $row->warna_id * $row->suspensi_id}}</td>
                <td>{{ $row->warna_id * $row->eksterior_id}}</td>
                <td>{{ $row->warna_id * $row->interior_id}}</td>
                <td>{{ $row->warna_id * $row->dokumen_id}}</td>

                <!-- Perkalian silang X5 -->
                <td>{{ $row->tahun * $row->transmisi_id}}</td>
                <td>{{ $row->tahun * $row->mesin_id}}</td>
                <td>{{ $row->tahun * $row->sis_rem_id}}</td>
                <td>{{ $row->tahun * $row->kemudi_id}}</td>
                <td>{{ $row->tahun * $row->suspensi_id}}</td>
                <td>{{ $row->tahun * $row->eksterior_id}}</td>
                <td>{{ $row->tahun * $row->interior_id}}</td>
                <td>{{ $row->tahun * $row->dokumen_id}}</td>

                <!-- Perkalian silang X6 -->
                <td>{{ $row->transmisi_id * $row->mesin_id}}</td>
                <td>{{ $row->transmisi_id * $row->sis_rem_id}}</td>
                <td>{{ $row->transmisi_id * $row->kemudi_id}}</td>
                <td>{{ $row->transmisi_id * $row->suspensi_id}}</td>
                <td>{{ $row->transmisi_id * $row->eksterior_id}}</td>
                <td>{{ $row->transmisi_id * $row->interior_id}}</td>
                <td>{{ $row->transmisi_id * $row->dokumen_id}}</td>

                <!-- Perkalian silang X7 -->
                <td>{{ $row->mesin_id * $row->sis_rem_id}}</td>
                <td>{{ $row->mesin_id * $row->kemudi_id}}</td>
                <td>{{ $row->mesin_id * $row->suspensi_id}}</td>
                <td>{{ $row->mesin_id * $row->eksterior_id}}</td>
                <td>{{ $row->mesin_id * $row->interior_id}}</td>
                <td>{{ $row->mesin_id * $row->dokumen_id}}</td>

                <!-- Perkalian silang X8 -->
                <td>{{ $row->sis_rem_id * $row->kemudi_id}}</td>
                <td>{{ $row->sis_rem_id * $row->suspensi_id}}</td>
                <td>{{ $row->sis_rem_id * $row->eksterior_id}}</td>
                <td>{{ $row->sis_rem_id * $row->interior_id}}</td>
                <td>{{ $row->sis_rem_id * $row->dokumen_id}}</td>

                <!-- Perkalian silang X9 -->
                <td>{{ $row->kemudi_id * $row->suspensi_id}}</td>
                <td>{{ $row->kemudi_id * $row->eksterior_id}}</td>
                <td>{{ $row->kemudi_id * $row->interior_id}}</td>
                <td>{{ $row->kemudi_id * $row->dokumen_id}}</td>

                <!-- Perkalian silang X10 -->
                <td>{{ $row->suspensi_id * $row->eksterior_id}}</td>
                <td>{{ $row->suspensi_id * $row->interior_id}}</td>
                <td>{{ $row->suspensi_id * $row->dokumen_id}}</td>

                <!-- Perkalian silang X11 -->
                <td>{{ $row->eksterior_id * $row->interior_id}}</td>
                <td>{{ $row->eksterior_id * $row->dokumen_id}}</td>

                <!-- Perkalian silang X12 -->
                <td>{{ $row->interior_id * $row->dokumen_id}}</td>

            </tr>
       @php
            $y += $row->harga;
            $x1 += $row->merek_id;
            $x2 += $row->model_merek_id;
            $x3 += $row->tipe_model_id;
            $x4 += $row->warna_id;
            $x5 += $row->tahun;
            $x6 += $row->transmisi_id;
            $x7 += $row->mesin_id;
            $x8 += $row->sis_rem_id;
            $x9 += $row->kemudi_id;
            $x10 += $row->suspensi_id;
            $x11 += $row->eksterior_id;
            $x12 += $row->interior_id;
            $x13 += $row->dokumen_id;

            $y_2 += pow($row->harga,2);
            $x1_2 += pow($row->merek_id,2);
            $x2_2 += pow($row->model_merek_id,2);
            $x3_2 += pow($row->tipe_model_id,2);
            $x4_2 += pow($row->warna_id,2);
            $x5_2 += pow($row->tahun,2);
            $x6_2 += pow($row->transmisi_id,2);
            $x7_2 += pow($row->mesin_id,2);
            $x8_2 += pow($row->sis_rem_id,2);
            $x9_2 += pow($row->kemudi_id,2);
            $x10_2 += pow($row->suspensi_id,2);
            $x11_2 += pow($row->eksterior_id,2);
            $x12_2 += pow($row->interior_id,2);
            $x13_2 += pow($row->dokumen_id,2);

            $yx1 += $row->harga * $row->merek_id;
            $yx2 += $row->harga * $row->model_merek_id;
            $yx3 += $row->harga * $row->tipe_model_id;
            $yx4 += $row->harga * $row->warna_id;
            $yx5 += $row->harga * $row->tahun;
            $yx6 += $row->harga * $row->transmisi_id;
            $yx7 += $row->harga * $row->mesin_id;
            $yx8 += $row->harga * $row->sis_rem_id;
            $yx9 += $row->harga * $row->kemudi_id;
            $yx10 += $row->harga * $row->suspensi_id;
            $yx11 += $row->harga * $row->eksterior_id;
            $yx12 += $row->harga * $row->interior_id;
            $yx13 += $row->harga * $row->dokumen_id;

            $x1x2 += $row->merek_id * $row->model_merek_id;
            $x1x3 += $row->merek_id * $row->tipe_model_id;
            $x1x4 += $row->merek_id * $row->warna_id;
            $x1x5 += $row->merek_id * $row->tahun;
            $x1x6 += $row->merek_id * $row->transmisi_id;
            $x1x7 += $row->merek_id * $row->mesin_id;
            $x1x8 += $row->merek_id * $row->sis_rem_id;
            $x1x9 += $row->merek_id * $row->kemudi_id;
            $x1x10 += $row->merek_id * $row->suspensi_id;
            $x1x11 += $row->merek_id * $row->eksterior_id;
            $x1x12 += $row->merek_id * $row->interior_id;
            $x1x13 += $row->merek_id * $row->dokumen_id;

            $x2x3 += $row->model_merek_id * $row->tipe_model_id;
            $x2x4 += $row->model_merek_id * $row->warna_id; 
            $x2x5 += $row->model_merek_id * $row->tahun;
            $x2x6 += $row->model_merek_id * $row->transmisi_id;
            $x2x7 += $row->model_merek_id * $row->mesin_id;
            $x2x8 += $row->model_merek_id * $row->sis_rem_id;
            $x2x9 += $row->model_merek_id * $row->kemudi_id;
            $x2x10 += $row->model_merek_id * $row->suspensi_id;
            $x2x11 += $row->model_merek_id * $row->eksterior_id;
            $x2x12 += $row->model_merek_id * $row->interior_id;
            $x2x13 += $row->model_merek_id * $row->dokumen_id;

            $x3x4 += $row->tipe_model_id * $row->warna_id;
            $x3x5 += $row->tipe_model_id * $row->tahun;
            $x3x6 += $row->tipe_model_id * $row->transmisi_id;
            $x3x7 += $row->tipe_model_id * $row->mesin_id;
            $x3x8 += $row->tipe_model_id * $row->sis_rem_id;
            $x3x9 += $row->tipe_model_id * $row->kemudi_id;
            $x3x10 += $row->tipe_model_id * $row->suspensi_id;
            $x3x11 += $row->tipe_model_id * $row->eksterior_id;
            $x3x12 += $row->tipe_model_id * $row->interior_id;
            $x3x13 += $row->tipe_model_id * $row->dokumen_id;

            $x4x5 += $row->warna_id * $row->tahun;
            $x4x6 += $row->warna_id * $row->transmisi_id;
            $x4x7 += $row->warna_id * $row->mesin_id;
            $x4x8 += $row->warna_id * $row->sis_rem_id;
            $x4x9 += $row->warna_id * $row->kemudi_id;
            $x4x10 += $row->warna_id * $row->suspensi_id;
            $x4x11 += $row->warna_id * $row->eksterior_id;
            $x4x12 += $row->warna_id * $row->interior_id;
            $x4x13 += $row->warna_id * $row->dokumen_id;

            $x5x6 += $row->tahun * $row->transmisi_id;
            $x5x7 += $row->tahun * $row->mesin_id;
            $x5x8 += $row->tahun * $row->sis_rem_id;
            $x5x9 += $row->tahun * $row->kemudi_id;
            $x5x10 += $row->tahun * $row->suspensi_id;
            $x5x11 += $row->tahun * $row->eksterior_id;
            $x5x12 += $row->tahun * $row->interior_id;
            $x5x13 += $row->tahun * $row->dokumen_id;

            $x6x7 += $row->transmisi_id * $row->mesin_id;
            $x6x8 += $row->transmisi_id * $row->sis_rem_id;
            $x6x9 += $row->transmisi_id * $row->kemudi_id;
            $x6x10 += $row->transmisi_id * $row->suspensi_id;
            $x6x11 += $row->transmisi_id * $row->eksterior_id;
            $x6x12 += $row->transmisi_id * $row->interior_id;
            $x6x13 += $row->transmisi_id * $row->dokumen_id;

            $x7x8 += $row->mesin_id * $row->sis_rem_id;
            $x7x9 += $row->mesin_id * $row->kemudi_id;
            $x7x10 += $row->mesin_id * $row->suspensi_id;
            $x7x11 += $row->mesin_id * $row->eksterior_id;
            $x7x12 += $row->mesin_id * $row->interior_id;
            $x7x13 += $row->mesin_id * $row->dokumen_id;

            $x8x9 += $row->sis_rem_id * $row->kemudi_id;
            $x8x10 += $row->sis_rem_id * $row->suspensi_id;
            $x8x11 += $row->sis_rem_id * $row->eksterior_id;
            $x8x12 += $row->sis_rem_id * $row->interior_id;
            $x8x13 += $row->sis_rem_id * $row->dokumen_id;

            $x9x10 += $row->kemudi_id * $row->suspensi_id;
            $x9x11 += $row->kemudi_id * $row->eksterior_id;
            $x9x12 += $row->kemudi_id * $row->interior_id;
            $x9x13 += $row->kemudi_id * $row->dokumen_id;

            $x10x11 += $row->suspensi_id * $row->eksterior_id;
            $x10x12 += $row->suspensi_id * $row->interior_id;
            $x10x13 += $row->suspensi_id * $row->dokumen_id;

            $x11x12 += $row->eksterior_id * $row->interior_id;
            $x11x13 += $row->eksterior_id * $row->dokumen_id;

            $x12x13 += $row->interior_id * $row->dokumen_id;
        @endphp
        @empty
            <tr>
              <td colspan="14" class="center">Data tidak ditemukan</td>
            </tr>
        @endforelse
        </tbody>
        <tfoot style="display:table;table-layout:fixed;text-align: center;">
            <tr>
                <th style="width: 90px;">Jml = {{ $data }}</th>
                <th style="width: 120px;">{{ $y }}</th>
                <th style="width: 80px;">{{ $x1 }}</th>
                <th style="width: 110px;padding-left: 30px;">{{ $x2 }}</th>
                <th style="width: 100px;">{{ $x3 }}</th>
                <th style="width: 100px;">{{ $x4 }}</th>
                <th style="width: 100px;">{{ $x5 }}</th>
                <th style="width: 100px;">{{ $x6 }}</th>
                <th style="width: 100px;">{{ $x7 }}</th>
                <th style="width: 100px;">{{ $x8 }}</th>
                <th style="width: 100px;">{{ $x9 }}</th>
                <th style="width: 100px;">{{ $x10 }}</th>
                <th style="width: 100px;">{{ $x11 }}</th>
                <th style="width: 100px;">{{ $x12 }}</th>
                <th style="width: 100px;">{{ $x13 }}</th>
                <!-- <th colspan="7"></th> -->
                <th style="width: 100px;">{{ $x1_2 }}</th>
                <th style="width: 100px;">{{ $x2_2 }}</th>
                <th style="width: 100px;">{{ $x3_2 }}</th>
                <th style="width: 90px;">{{ $x4_2 }}</th>
                <th style="width: 120px;">{{ $x5_2 }}</th>
                <th style="width: 90px;">{{ $x6_2 }}</th>
                <th style="width: 100px;">{{ $x7_2 }}</th>
                <th style="width: 100px;">{{ $x8_2 }}</th>
                <th style="width: 100px;">{{ $x9_2 }}</th>
                <th style="width: 100px;">{{ $x10_2 }}</th>
                <th style="width: 100px;">{{ $x11_2 }}</th>
                <th style="width: 100px;">{{ $x12_2 }}</th>
                <th style="width: 100px;">{{ $x13_2 }}</th>

                <th>{{ $yx1 }}</th>
                <th>{{ $yx2 }}</th>
                <th>{{ $yx3 }}</th>
                <th style="width: 90px;">{{ $yx4 }}</th>
                <th>{{ $yx5 }}</th>
                <th>{{ $yx6 }}</th>
                <th>{{ $yx7 }}</th>
                <th>{{ $yx8 }}</th>
                <th style="width: 90px;">{{ $yx9 }}</th>
                <th style="width: 100px;">{{ $yx10 }}</th>
                <th style="width: 100px;">{{ $yx11 }}</th>
                <th style="width: 100px;">{{ $yx12 }}</th>
                <th style="width: 100px;">{{ $yx13 }}</th>

                <th style="width: 90px;">{{ $x1x2 }}</th>
                <th style="width: 110px;">{{ $x1x3 }}</th>
                <th style="width: 100px;">{{ $x1x4 }}</th>
                <th style="width: 100px;">{{ $x1x5 }}</th>
                <th style="width: 100px;">{{ $x1x6 }}</th>
                <th style="width: 100px;">{{ $x1x7 }}</th>
                <th style="width: 105px;">{{ $x1x8 }}</th>
                <th style="width: 100px;">{{ $x1x9 }}</th>
                <th style="width: 100px;">{{ $x1x10 }}</th>
                <th style="width: 100px;">{{ $x1x11 }}</th>
                <th style="width: 100px;">{{ $x1x12 }}</th>
                <th style="width: 100px;">{{ $x1x13 }}</th>

                <th style="width: 100px;">{{ $x2x3 }}</th>
                <th style="width: 100px;">{{ $x2x4 }}</th>
                <th style="width: 110px;">{{ $x2x5 }}</th>
                <th style="width: 100px;">{{ $x2x6 }}</th>
                <th style="width: 100px;">{{ $x2x7 }}</th>
                <th style="width: 100px;">{{ $x2x8 }}</th>
                <th style="width: 100px;">{{ $x2x9 }}</th>
                <th style="width: 100px;">{{ $x2x10 }}</th>
                <th style="width: 100px;">{{ $x2x11 }}</th>
                <th style="width: 100px;">{{ $x2x12 }}</th>
                <th style="width: 105px;">{{ $x2x13 }}</th>

                <th style="width: 100px;">{{ $x3x4 }}</th>
                <th style="width: 115px;">{{ $x3x5 }}</th>
                <th style="width: 90px;">{{ $x3x6 }}</th>
                <th style="width: 100px;">{{ $x3x7 }}</th>
                <th style="width: 100px;">{{ $x3x8 }}</th>
                <th style="width: 100px;">{{ $x3x9 }}</th>
                <th style="width: 100px;">{{ $x3x10 }}</th>
                <th style="width: 100px;">{{ $x3x11 }}</th>
                <th style="width: 100px;">{{ $x3x12 }}</th>
                <th style="width: 100px;">{{ $x3x13 }}</th>

                <th style="width: 110px;">{{ $x4x5 }}</th>
                <th style="width: 100px;">{{ $x4x6 }}</th>
                <th style="width: 100px;">{{ $x4x7 }}</th>
                <th style="width: 100px;">{{ $x4x8 }}</th>
                <th style="width: 100px;padding-left: 20px;">{{ $x4x9 }}</th>
                <th style="width: 100px;padding-left: 20px;">{{ $x4x10 }}</th>
                <th style="width: 100px;padding-left: 20px;">{{ $x4x11 }}</th>
                <th style="width: 100px;padding-left: 20px;">{{ $x4x12 }}</th>
                <th style="width: 100px;padding-left: 20px;">{{ $x4x13 }}</th>

                <th style="width: 102px;padding-left: 20px;">{{ $x5x6 }}</th>
                <th style="width: 100px;padding-left: 20px;">{{ $x5x7 }}</th>
                <th style="width: 100px;padding-left: 20px;">{{ $x5x8 }}</th>
                <th style="width: 100px;padding-left: 20px;">{{ $x5x9 }}</th>
                <th style="width: 105px;padding-left: 20px;">{{ $x5x10 }}</th>
                <th style="width: 100px;padding-left: 20px;">{{ $x5x11 }}</th>
                <th style="width: 100px;padding-left: 20px;">{{ $x5x12 }}</th>
                <th style="width: 102px;padding-left: 20px;">{{ $x5x13 }}</th>

                <th style="width: 100px;">{{ $x6x7 }}</th>
                <th style="width: 102px;padding-left: 20px;">{{ $x6x8 }}</th>
                <th style="width: 100px;padding-left: 20px;">{{ $x6x9 }}</th>
                <th style="width: 103px;padding-left: 20px;">{{ $x6x10 }}</th>
                <th style="width: 101px;padding-left: 20px;">{{ $x6x11 }}</th>
                <th style="width: 101px;padding-left: 20px;">{{ $x6x12 }}</th>
                <th style="width: 101px;padding-left: 20px;">{{ $x6x13 }}</th>

                <th style="width: 101px;padding-left: 20px;">{{ $x7x8 }}</th>
                <th style="width: 101px;padding-left: 20px;">{{ $x7x9 }}</th>
                <th style="width: 102px;">{{ $x7x10 }}</th>
                <th style="width: 100px;">{{ $x7x11 }}</th>
                <th style="width: 100px;">{{ $x7x12 }}</th>
                <th style="width: 102px;">{{ $x7x13 }}</th>

                <th style="width: 101px;">{{ $x8x9 }}</th>
                <th style="width: 101px;">{{ $x8x10 }}</th>
                <th style="width: 101px;">{{ $x8x11 }}</th>
                <th style="width: 101px;">{{ $x8x12 }}</th>
                <th style="width: 101px;">{{ $x8x13 }}</th>

                <th style="width: 101px;">{{ $x9x10 }}</th>
                <th style="width: 101px;">{{ $x9x11 }}</th>
                <th style="width: 101px;">{{ $x9x12 }}</th>
                <th style="width: 101px;">{{ $x9x13 }}</th>

                <th style="width: 101px;">{{ $x10x11 }}</th>
                <th style="width: 101px;">{{ $x10x12 }}</th>
                <th style="width: 101px;">{{ $x10x13 }}</th>

                <th style="width: 101px;">{{ $x11x12 }}</th>
                <th style="width: 101px;">{{ $x11x13 }}</th>
                <th style="width: 110px;">{{ $x12x13 }}</th>
            </tr>
        </tfoot>
    </table>
</div>

@section('scripts')
    <script type="text/javascript">

        $(document).ready(function() {
            console.log("mulai")
            $('#simpan_rumus').attr('disabled', 'disabled');
        });

        $('#proses').click(function(){
            console.log("proses")
            $('#simpan_rumus').removeAttr('disabled');
            //declare variable
            const data = {{ $data }},
            y     = "{{ $y }}", 
            x1    = "{{ $x1 }}",
            x2    = "{{ $x2 }}",
            x3    = "{{ $x3 }}",
            x4    = "{{ $x4 }}",
            x5    = "{{ $x5 }}",
            x6    = "{{ $x6 }}",
            x7    = "{{ $x7 }}",
            x8    = "{{ $x8 }}",
            x9    = "{{ $x9 }}",
            x10   = "{{ $x10 }}",
            x11   = "{{ $x11 }}",
            x12   = "{{ $x12 }}",
            x13   = "{{ $x13 }}",
            y_2   = "{{ $y_2 }}",  
            x1_2  = "{{ $x1_2 }}",
            x2_2  = "{{ $x2_2 }}",
            x3_2  = "{{ $x3_2 }}",
            x4_2  = "{{ $x4_2 }}",
            x5_2  = "{{ $x5_2 }}",
            x6_2  = "{{ $x6_2 }}",
            x7_2  = "{{ $x7_2 }}",
            x8_2  = "{{ $x8_2 }}",
            x9_2  = "{{ $x9_2 }}",
            x10_2 = "{{ $x10_2 }}",
            x11_2 = "{{ $x11_2 }}",
            x12_2 = "{{ $x12_2 }}",
            x13_2 = "{{ $x13_2 }}",
            yx1   = "{{ $yx1 }}",
            yx2   = "{{ $yx2 }}",
            yx3   = "{{ $yx3 }}",
            yx4   = "{{ $yx4 }}",
            yx5   = "{{ $yx5 }}",
            yx6   = "{{ $yx6 }}",
            yx7   = "{{ $yx7 }}",
            yx8   = "{{ $yx8 }}",
            yx9   = "{{ $yx9 }}",
            yx10  = "{{ $yx10 }}",
            yx11  = "{{ $yx11 }}",
            yx12  = "{{ $yx12 }}",
            yx13  = "{{ $yx13 }}",
            x1x2  = "{{ $x1x2 }}",
            x1x3  = "{{ $x1x3 }}",
            x1x4  = "{{ $x1x4 }}",
            x1x5  = "{{ $x1x5 }}",
            x1x6  = "{{ $x1x6 }}",
            x1x7  = "{{ $x1x7 }}",
            x1x8  = "{{ $x1x8 }}",
            x1x9  = "{{ $x1x9 }}",
            x1x10 = "{{ $x1x10 }}",
            x1x11 = "{{ $x1x11 }}",
            x1x12 = "{{ $x1x12 }}",
            x1x13 = "{{ $x1x13 }}",
            x2x3 = "{{ $x2x3 }}"
            x2x4 = "{{ $x2x4 }}",
            x2x5 = "{{ $x2x5 }}",
            x2x6 = "{{ $x2x6 }}",
            x2x7  = "{{ $x2x7 }}",
            x2x8  = "{{ $x2x8 }}",
            x2x9  = "{{ $x2x9 }}",
            x2x10 = "{{ $x2x10 }}",
            x2x11 = "{{ $x2x11 }}",
            x2x12 = "{{ $x2x12 }}",
            x2x13 = "{{ $x2x13 }}",
            x3x4 = "{{ $x3x4 }}",
            x3x5 = "{{ $x3x5 }}",
            x3x6 = "{{ $x3x6 }}",
            x3x7  = "{{ $x3x7 }}",
            x3x8  = "{{ $x3x8 }}",
            x3x9  = "{{ $x3x9 }}",
            x3x10 = "{{ $x3x10 }}",
            x3x11 = "{{ $x3x11 }}",
            x3x12 = "{{ $x3x12 }}",
            x3x13 = "{{ $x3x13 }}",
            x4x5 = "{{ $x4x5 }}",
            x4x6 = "{{ $x4x6 }}",
            x4x7  = "{{ $x4x7 }}",
            x4x8  = "{{ $x4x8 }}",
            x4x9  = "{{ $x4x9 }}",
            x4x10 = "{{ $x4x10 }}",
            x4x11 = "{{ $x4x11 }}",
            x4x12 = "{{ $x4x12 }}",
            x4x13 = "{{ $x4x13 }}",
            x5x6 = "{{ $x5x6 }}",
            x5x7  = "{{ $x5x7 }}",
            x5x8  = "{{ $x5x8 }}",
            x5x9  = "{{ $x5x9 }}",
            x5x10 = "{{ $x5x10 }}",
            x5x11 = "{{ $x5x11 }}",
            x5x12 = "{{ $x5x12 }}",
            x5x13 = "{{ $x5x13 }}",
            x6x7  = "{{ $x6x7 }}",
            x6x8  = "{{ $x6x8 }}",
            x6x9  = "{{ $x6x9 }}",
            x6x10 = "{{ $x6x10 }}",
            x6x11 = "{{ $x6x11 }}",
            x6x12 = "{{ $x6x12 }}",
            x6x13 = "{{ $x6x13 }}",
            x7x8  = "{{ $x7x8 }}",
            x7x9  = "{{ $x7x9 }}",
            x7x10 = "{{ $x7x10 }}",
            x7x11 = "{{ $x7x11 }}",
            x7x12 = "{{ $x7x12 }}",
            x7x13 = "{{ $x7x13 }}",
            x8x9  = "{{ $x8x9 }}",
            x8x10 = "{{ $x8x10 }}",
            x8x11 = "{{ $x8x11 }}",
            x8x12 = "{{ $x8x12 }}",
            x8x13 = "{{ $x8x13 }}",
            x9x10 = "{{ $x9x10 }}",
            x9x11 = "{{ $x9x11 }}",
            x9x12 = "{{ $x9x12 }}",
            x9x13 = "{{ $x9x13 }}",
            x10x11 = "{{ $x10x11 }}",
            x10x12 = "{{ $x10x12 }}",
            x10x13 = "{{ $x10x13 }}",
            x11x12 = "{{ $x11x12 }}",
            x11x13 = "{{ $x11x13 }}",          
            x12x13 = "{{ $x11x13 }}";

            Y       = Number(y/data).toFixed(3);
            X1      = Number(x1/data).toFixed(3);
            X2      = Number(x2/data).toFixed(3);
            X3      = Number(x3/data).toFixed(3);
            X4      = Number(x4/data).toFixed(3);
            X5      = Number(x5/data).toFixed(3);
            X6      = Number(x6/data).toFixed(3);
            X7      = Number(x7/data).toFixed(3);
            X8      = Number(x8/data).toFixed(3);
            X9      = Number(x9/data).toFixed(3);
            X10     = Number(x10/data).toFixed(3);
            X11     = Number(x11/data).toFixed(3);
            X12     = Number(x12/data).toFixed(3);
            X13     = Number(x13/data).toFixed(3);
            Y_2         = Number(y_2 - (Math.pow(y,2)/data)).toFixed(3);
            console.log("Y_2=", Y_2)
            X1_2  = Number(x1_2 - (Math.pow(x1,2)/data)).toFixed(3);
            console.log("x1_2=", X1_2)
            X2_2        = Number(x2_2 - (Math.pow(x2,2)/data)).toFixed(3);
            console.log("x2_2=", X2_2)
            X3_2        = Number(x3_2 - (Math.pow(x3,2)/data)).toFixed(3);
            console.log("x3_2=", X3_2)
            X4_2        = Number(x4_2 - (Math.pow(x4,2)/data)).toFixed(3);
            console.log("x4_2=", X4_2)
            X5_2        = Number(x5_2 - (Math.pow(x5,2)/data)).toFixed(3);
            console.log("x5_2=", X5_2)
            X6_2        = Number(x6_2 - (Math.pow(x6,2)/data)).toFixed(3);
            console.log("x6_2=", X6_2)
            X7_2        = Number(x7_2 - (Math.pow(x7,2)/data)).toFixed(3);
            console.log("x7_2=", X7_2)
            X8_2        = Number(x8_2 - (Math.pow(x8,2)/data)).toFixed(3);
            console.log("x8_2=", X8_2)
            X9_2        = Number(x9_2 - (Math.pow(x9,2)/data)).toFixed(3);
            console.log("x9_2=", X9_2)
            X10_2       = Number(x10_2 - (Math.pow(x10,2)/data)).toFixed(3);
            console.log("x10_2=", X10_2)
            X11_2       = Number(x11_2 - (Math.pow(x11,2)/data)).toFixed(3);
            console.log("x11_2=", X11_2)
            X12_2       = Number(x12_2 - (Math.pow(x12,2)/data)).toFixed(3);
            console.log("x12_2=", X12_2)
            X13_2       = Number(x13_2 - (Math.pow(x13,2)/data)).toFixed(3);
            console.log("x13_2=", X13_2)
            
            console.log("===BATAS===")
            YX1         = Number(yx1 - ((x1 * y)/data)).toFixed(3);
            console.log("YX1=", YX1)
            YX2         = Number(yx2 - ((x2 * y)/data)).toFixed(3);
            console.log("YX2=", YX2)
            YX3         = Number(yx3 - ((x3 * y)/data)).toFixed(3);
            console.log("YX3=", YX3)
            YX4         = Number(yx4 - ((x4 * y)/data)).toFixed(3);
            console.log("YX4=", YX4)
            YX5         = Number(yx5 - ((x5 * y)/data)).toFixed(3);
            console.log("YX5=", YX5)
            YX6         = Number(yx6 - ((x6 * y)/data)).toFixed(3);
            console.log("YX6=", YX6)
            YX7         = Number(yx7 - ((x7 * y)/data)).toFixed(3);
            console.log("YX7=", YX7)
            YX8         = Number(yx8 - ((x8 * y)/data)).toFixed(3);
            console.log("YX8=", YX8)
            YX9         = Number(yx9 - ((x9 * y)/data)).toFixed(3);
            console.log("YX9=", YX9)
            YX10        = Number(yx10 - ((x10 * y)/data)).toFixed(3);
            console.log("YX10=", YX10)
            YX11        = Number(yx11 - ((x11 * y)/data)).toFixed(3);
            console.log("YX11=", YX11)
            YX12        = Number(yx12 - ((x12 * y)/data)).toFixed(3);
            console.log("YX12=", YX12)
            YX13        = Number(yx13 - ((x13 * y)/data)).toFixed(3);
            console.log("YX13=", YX13)
            
            X1X2        = Number(x1x2 - ((x1 * x2)/data)).toFixed(3);            
            X1X3        = Number(x1x3 - ((x1 * x3)/data)).toFixed(3);
            X1X4        = Number(x1x4 - ((x1 * x4)/data)).toFixed(3);
            X1X5        = Number(x1x5 - ((x1 * x5)/data)).toFixed(3);
            X1X6        = Number(x1x6 - ((x1 * x6)/data)).toFixed(3);
            X1X7        = Number(x1x7 - ((x1 * x7)/data)).toFixed(3);
            X1X8        = Number(x1x8 - ((x1 * x8)/data)).toFixed(3);
            X1X9        = Number(x1x9 - ((x1 * x9)/data)).toFixed(3);
            X1X10       = Number(x1x10 - ((x1 * x10)/data)).toFixed(3);
            X1X11       = Number(x1x11 - ((x1 * x11)/data)).toFixed(3);
            X1X12       = Number(x1x12 - ((x1 * x12)/data)).toFixed(3);
            X1X13       = Number(x1x13 - ((x1 * x13)/data)).toFixed(3); 

            X2X3        = Number(x2x3 - ((x2 * x3)/data)).toFixed(3);
            X2X4        = Number(x2x4 - ((x2 * x4)/data)).toFixed(3);
            X2X5        = Number(x2x5 - ((x2 * x5)/data)).toFixed(3);
            X2X6        = Number(x2x6 - ((x2 * x6)/data)).toFixed(3);
            X2X7        = Number(x2x7 - ((x2 * x7)/data)).toFixed(3);
            X2X8        = Number(x2x8 - ((x2 * x8)/data)).toFixed(3);
            X2X9        = Number(x2x9 - ((x2 * x9)/data)).toFixed(3);
            X2X10       = Number(x2x10 - ((x2 * x10)/data)).toFixed(3);
            X2X11       = Number(x2x11 - ((x2 * x11)/data)).toFixed(3);
            X2X12       = Number(x2x12 - ((x2 * x12)/data)).toFixed(3);
            X2X13       = Number(x2x13 - ((x2 * x13)/data)).toFixed(3);

            X3X4        = Number(x3x4 - ((x3 * x4)/data)).toFixed(3);            
            X3X5        = Number(x3x5 - ((x3 * x5)/data)).toFixed(3);
            X3X6        = Number(x3x6 - ((x3 * x6)/data)).toFixed(3);
            X3X7        = Number(x3x7 - ((x3 * x7)/data)).toFixed(3);
            X3X8        = Number(x3x8 - ((x3 * x8)/data)).toFixed(3);
            X3X9        = Number(x3x9 - ((x3 * x9)/data)).toFixed(3);
            X3X10       = Number(x3x10 - ((x3 * x10)/data)).toFixed(3);
            X3X11       = Number(x3x11 - ((x3 * x11)/data)).toFixed(3);
            X3X12       = Number(x3x12 - ((x3 * x12)/data)).toFixed(3);
            X3X13       = Number(x3x13 - ((x3 * x13)/data)).toFixed(3);

            X4X5        = Number(x4x5 - ((x4 * x5)/data)).toFixed(3);           
            X4X6        = Number(x4x6 - ((x4 * x6)/data)).toFixed(3);
            X4X7        = Number(x4x7 - ((x4 * x7)/data)).toFixed(3);
            X4X8        = Number(x4x8 - ((x4 * x8)/data)).toFixed(3);
            X4X9        = Number(x4x9 - ((x4 * x9)/data)).toFixed(3);
            X4X10       = Number(x4x10 - ((x4 * x10)/data)).toFixed(3);
            X4X11       = Number(x4x11 - ((x4 * x11)/data)).toFixed(3);
            X4X12       = Number(x4x12 - ((x4 * x12)/data)).toFixed(3);
            X4X13       = Number(x4x13 - ((x4 * x13)/data)).toFixed(3);

            X5X6        = Number(x5x6 - ((x5 * x6)/data)).toFixed(3);
            X5X7        = Number(x5x7 - ((x5 * x7)/data)).toFixed(3);
            X5X8        = Number(x5x8 - ((x5 * x8)/data)).toFixed(3);
            X5X9        = Number(x5x9 - ((x5 * x9)/data)).toFixed(3);
            X5X10       = Number(x5x10 - ((x5 * x10)/data)).toFixed(3);
            X5X11       = Number(x5x11 - ((x5 * x11)/data)).toFixed(3);
            X5X12       = Number(x5x12 - ((x5 * x12)/data)).toFixed(3);
            X5X13       = Number(x5x13 - ((x5 * x13)/data)).toFixed(3);

            X6X7        = Number(x6x7 - ((x6 * x7)/data)).toFixed(3);
            X6X8        = Number(x6x8 - ((x6 * x8)/data)).toFixed(3);
            X6X9        = Number(x6x9 - ((x6 * x9)/data)).toFixed(3);
            X6X10       = Number(x6x10 - ((x6 * x10)/data)).toFixed(3);
            X6X11       = Number(x6x11 - ((x6 * x11)/data)).toFixed(3);
            X6X12       = Number(x6x12 - ((x6 * x12)/data)).toFixed(3);
            X6X13       = Number(x6x13 - ((x6 * x13)/data)).toFixed(3);

            X7X8        = Number(x7x8 - ((x7 * x8)/data)).toFixed(3);
            X7X9        = Number(x7x9 - ((x7 * x9)/data)).toFixed(3);
            X7X10       = Number(x7x10 - ((x7 * x10)/data)).toFixed(3);
            X7X11       = Number(x7x11 - ((x7 * x11)/data)).toFixed(3);
            X7X12       = Number(x7x12 - ((x7 * x12)/data)).toFixed(3);
            X7X13       = Number(x7x13 - ((x7 * x13)/data)).toFixed(3);

            console.log("===BATAS===")
            X8X9        = Number(x8x9 - ((x8 * x9)/data)).toFixed(3);
            console.log("X8X9=", X8X9)
            X8X10       = Number(x8x10 - ((x8 * x10)/data)).toFixed(3);
            console.log("X8X10=", X8X10)
            X8X11       = Number(x8x11 - ((x8 * x11)/data)).toFixed(3);
            console.log("X8X11=", X8X11)
            X8X12       = Number(x8x12 - ((x8 * x12)/data)).toFixed(3);
            console.log("X8X12=", X8X12)
            X8X13       = Number(x8x13 - ((x8 * x13)/data)).toFixed(3);
            console.log("X8X13=", X8X13)

            console.log("===BATAS===")
            X9X10       = Number(x9x10 - ((x9 * x10)/data)).toFixed(3);
            console.log("X9X10=", X9X10)
            X9X11       = Number(x9x11 - ((x9 * x11)/data)).toFixed(3);
            console.log("X9X11=", X9X11)
            X9X12       = Number(x9x12 - ((x9 * x12)/data)).toFixed(3);
            console.log("X9X12=", X9X12)
            X9X13       = Number(x9x13 - ((x9 * x13)/data)).toFixed(3);
            console.log("X9X13=", X9X13)

            console.log("===BATAS===")
            X10X11       = Number(x10x11 - ((x10 * x11)/data)).toFixed(3);
            console.log("X10X11=", X10X11)
            X10X12       = Number(x10x12 - ((x10 * x12)/data)).toFixed(3);
            console.log("X10X12=", X10X12)
            X10X13       = Number(x10x13 - ((x10 * x13)/data)).toFixed(3);
            console.log("X10X13=", X10X13)

            console.log("===BATAS===")
            X11X12       = Number(x11x12 - ((x11 * x12)/data)).toFixed(3);
            console.log("X11X12=", X11X12)
            X11X13       = Number(x11x13 - ((x11 * x13)/data)).toFixed(3);
            console.log("X11X13=", X11X13)

            console.log("===BATAS===")
            X12X13       = Number(x12x13 - ((x12 * x13)/data)).toFixed(3);
            console.log("X12X13=", X12X13)

            //Apabila di interface dimulai dari persamaan 14 
            //persamaan 15 (1A) => mengeliminasi b13 dari persamaan(2) 
            const pers15_Y = Number(YX1/X1X13).toFixed(3),
            pers15_b1 = Number(X1_2/X1X13).toFixed(3),
            pers15_b2 = Number(X1X2/X1X13).toFixed(3),
            pers15_b3 = Number(X1X3/X1X13).toFixed(3),
            pers15_b4 = Number(X1X4/X1X13).toFixed(3),
            pers15_b5 = Number(X1X5/X1X13).toFixed(3),
            pers15_b6 = Number(X1X6/X1X13).toFixed(3),
            pers15_b7 = Number(X1X7/X1X13).toFixed(3),
            pers15_b8 = Number(X1X8/X1X13).toFixed(3),
            pers15_b9 = Number(X1X9/X1X13).toFixed(3),
            pers15_b10 = Number(X1X10/X1X13).toFixed(3),
            pers15_b11 = Number(X1X11/X1X13).toFixed(3),
            pers15_b12 = Number(X1X12/X1X13).toFixed(3),
            pers15_b13 = Number(X1X13/X1X13).toFixed(3);

            //persamaan 16 (2A) => mengeliminasi b13 dari persamaan(3)  
            const pers16_Y = Number(YX2/X2X13).toFixed(3),
            pers16_b1 = Number(X1X2/X2X13).toFixed(3),
            pers16_b2 = Number(X2_2/X2X13).toFixed(3),
            pers16_b3 = Number(X2X3/X2X13).toFixed(3),
            pers16_b4 = Number(X2X4/X2X13).toFixed(3),
            pers16_b5 = Number(X2X5/X2X13).toFixed(3),
            pers16_b6 = Number(X2X6/X2X13).toFixed(3),
            pers16_b7 = Number(X2X7/X2X13).toFixed(3),
            pers16_b8 = Number(X2X8/X2X13).toFixed(3),
            pers16_b9 = Number(X2X9/X2X13).toFixed(3),
            pers16_b10 = Number(X2X10/X2X13).toFixed(3),
            pers16_b11 = Number(X2X11/X2X13).toFixed(3),
            pers16_b12 = Number(X2X12/X2X13).toFixed(3),
            pers16_b13 = Number(X2X13/X2X13).toFixed(3);

            //persamaan 17 (3A) => mengeliminasi b13 dari persamaan(4)
            const pers17_Y = Number(YX3/X3X13).toFixed(3),
            pers17_b1 = Number(X1X3/X3X13).toFixed(3),
            pers17_b2 = Number(X2X3/X3X13).toFixed(3),
            pers17_b3 = Number(X3_2/X3X13).toFixed(3),
            pers17_b4 = Number(X3X4/X3X13).toFixed(3),
            pers17_b5 = Number(X3X5/X3X13).toFixed(3),
            pers17_b6 = Number(X3X6/X3X13).toFixed(3),
            pers17_b7 = Number(X3X7/X3X13).toFixed(3),
            pers17_b8 = Number(X3X8/X3X13).toFixed(3),
            pers17_b9 = Number(X3X9/X3X13).toFixed(3),
            pers17_b10 = Number(X3X10/X3X13).toFixed(3),
            pers17_b11 = Number(X3X11/X3X13).toFixed(3),
            pers17_b12 = Number(X3X12/X3X13).toFixed(3),
            pers17_b13 = Number(X3X13/X3X13).toFixed(3);
            

            //persamaan 18 (4A) => mengeliminasi b13 dari persamaan(5) 
            const pers18_Y = Number(YX4/X4X13).toFixed(3),
            pers18_b1 = Number(X1X4/X4X13).toFixed(3),
            pers18_b2 = Number(X2X4/X4X13).toFixed(3),
            pers18_b3 = Number(X3X4/X4X13).toFixed(3),
            pers18_b4 = Number(X4_2/X4X13).toFixed(3),
            pers18_b5 = Number(X4X5/X4X13).toFixed(3),
            pers18_b6 = Number(X4X6/X4X13).toFixed(3),
            pers18_b7 = Number(X4X7/X4X13).toFixed(3),
            pers18_b8 = Number(X4X8/X4X13).toFixed(3),
            pers18_b9 = Number(X4X9/X4X13).toFixed(3),
            pers18_b10 = Number(X4X10/X4X13).toFixed(3),
            pers18_b11 = Number(X4X11/X4X13).toFixed(3),
            pers18_b12 = Number(X4X12/X4X13).toFixed(3),
            pers18_b13 = Number(X4X13/X4X13).toFixed(3);

            //persamaan 19 (5A) => mengeliminasi b13 dari persamaan(6) 
            const pers19_Y = Number(YX5/X5X13).toFixed(3),
            pers19_b1 = Number(X1X5/X5X13).toFixed(3),
            pers19_b2 = Number(X2X5/X5X13).toFixed(3),
            pers19_b3 = Number(X3X5/X5X13).toFixed(3),
            pers19_b4 = Number(X4X5/X5X13).toFixed(3),
            pers19_b5 = Number(X5_2/X5X13).toFixed(3),
            pers19_b6 = Number(X5X6/X5X13).toFixed(3),
            pers19_b7 = Number(X5X7/X5X13).toFixed(3),
            pers19_b8 = Number(X5X8/X5X13).toFixed(3),
            pers19_b9 = Number(X5X9/X5X13).toFixed(3),
            pers19_b10 = Number(X5X10/X5X13).toFixed(3),
            pers19_b11 = Number(X5X11/X5X13).toFixed(3),
            pers19_b12 = Number(X5X12/X5X13).toFixed(3),
            pers19_b13 = Number(X5X13/X5X13).toFixed(3);
            
            //persamaan 20 (6A) => mengeliminasi b13 dari persamaan(7) 
            const pers20_Y = Number(YX6/X6X13).toFixed(3),
            pers20_b1 = Number(X1X6/X6X13).toFixed(3),
            pers20_b2 = Number(X2X6/X6X13).toFixed(3),
            pers20_b3 = Number(X3X6/X6X13).toFixed(3),
            pers20_b4 = Number(X4X6/X6X13).toFixed(3),
            pers20_b5 = Number(X5X6/X6X13).toFixed(3),
            pers20_b6 = Number(X6_2/X6X13).toFixed(3),
            pers20_b7 = Number(X6X7/X6X13).toFixed(3),
            pers20_b8 = Number(X6X8/X6X13).toFixed(3),
            pers20_b9 = Number(X6X9/X6X13).toFixed(3),
            pers20_b10 = Number(X6X10/X6X13).toFixed(3),
            pers20_b11 = Number(X6X11/X6X13).toFixed(3),
            pers20_b12 = Number(X6X12/X6X13).toFixed(3),
            pers20_b13 = Number(X6X13/X6X13).toFixed(3);

            //persamaan 21 (7A) => mengeliminasi b13 dari persamaan(8) 
            const pers21_Y = Number(YX7/X7X13).toFixed(3),
            pers21_b1 = Number(X1X7/X7X13).toFixed(3),
            pers21_b2 = Number(X2X7/X7X13).toFixed(3),
            pers21_b3 = Number(X3X7/X7X13).toFixed(3),
            pers21_b4 = Number(X4X7/X7X13).toFixed(3),
            pers21_b5 = Number(X5X7/X7X13).toFixed(3),
            pers21_b6 = Number(X6X7/X7X13).toFixed(3),
            pers21_b7 = Number(X7_2/X7X13).toFixed(3),
            pers21_b8 = Number(X7X8/X7X13).toFixed(3),
            pers21_b9 = Number(X7X9/X7X13).toFixed(3),
            pers21_b10 = Number(X7X10/X7X13).toFixed(3),
            pers21_b11 = Number(X7X11/X7X13).toFixed(3),
            pers21_b12 = Number(X7X12/X7X13).toFixed(3),
            pers21_b13 = Number(X7X13/X7X13).toFixed(3);

            //persamaan 22 (8A) => mengeliminasi b13 dari persamaan(9) 
            const pers22_Y = Number(YX8/X8X13).toFixed(3),
            pers22_b1 = Number(X1X8/X8X13).toFixed(3),
            pers22_b2 = Number(X2X8/X8X13).toFixed(3),
            pers22_b3 = Number(X3X8/X8X13).toFixed(3),
            pers22_b4 = Number(X4X8/X8X13).toFixed(3),
            pers22_b5 = Number(X5X8/X8X13).toFixed(3),
            pers22_b6 = Number(X6X8/X8X13).toFixed(3),
            pers22_b7 = Number(X7X8/X8X13).toFixed(3),
            pers22_b8 = Number(X8_2/X8X13).toFixed(3),
            pers22_b9 = Number(X8X9/X8X13).toFixed(3),
            pers22_b10 = Number(X8X10/X8X13).toFixed(3),
            pers22_b11 = Number(X8X11/X8X13).toFixed(3),
            pers22_b12 = Number(X8X12/X8X13).toFixed(3),
            pers22_b13 = Number(X8X13/X8X13).toFixed(3);

            //persamaan 23 (9A) => mengeliminasi b13 dari persamaan(10) 
            const pers23_Y = Number(YX9/X9X13).toFixed(3),
            pers23_b1 = Number(X1X9/X9X13).toFixed(3),
            pers23_b2 = Number(X2X9/X9X13).toFixed(3),
            pers23_b3 = Number(X3X9/X9X13).toFixed(3),
            pers23_b4 = Number(X4X9/X9X13).toFixed(3),
            pers23_b5 = Number(X5X9/X9X13).toFixed(3),
            pers23_b6 = Number(X6X9/X9X13).toFixed(3),
            pers23_b7 = Number(X7X9/X9X13).toFixed(3),
            pers23_b8 = Number(X8X9/X9X13).toFixed(3),
            pers23_b9 = Number(X9_2/X9X13).toFixed(3),
            pers23_b10 = Number(X9X10/X9X13).toFixed(3),
            pers23_b11 = Number(X9X11/X9X13).toFixed(3),
            pers23_b12 = Number(X9X12/X9X13).toFixed(3),
            pers23_b13 = Number(X9X13/X9X13).toFixed(3);

            //persamaan 24 (10A) => mengeliminasi b13 dari persamaan(11) 
            const pers24_Y = Number(YX10/X10X13).toFixed(3),
            pers24_b1 = Number(X1X10/X10X13).toFixed(3),
            pers24_b2 = Number(X2X10/X10X13).toFixed(3),
            pers24_b3 = Number(X3X10/X10X13).toFixed(3),
            pers24_b4 = Number(X4X10/X10X13).toFixed(3),
            pers24_b5 = Number(X5X10/X10X13).toFixed(3),
            pers24_b6 = Number(X6X10/X10X13).toFixed(3),
            pers24_b7 = Number(X7X10/X10X13).toFixed(3),
            pers24_b8 = Number(X8X10/X10X13).toFixed(3),
            pers24_b9 = Number(X9X10/X10X13).toFixed(3),
            pers24_b10 = Number(X10_2/X10X13).toFixed(3),
            pers24_b11 = Number(X10X11/X10X13).toFixed(3),
            pers24_b12 = Number(X10X12/X10X13).toFixed(3),
            pers24_b13 = Number(X10X13/X10X13).toFixed(3);

            //persamaan 25 (11A) => mengeliminasi b13 dari persamaan(12) 
            const pers25_Y = Number(YX11/X11X13).toFixed(3),
            pers25_b1 = Number(X1X11/X11X13).toFixed(3),
            pers25_b2 = Number(X2X11/X11X13).toFixed(3),
            pers25_b3 = Number(X3X11/X11X13).toFixed(3),
            pers25_b4 = Number(X4X11/X11X13).toFixed(3),
            pers25_b5 = Number(X5X11/X11X13).toFixed(3),
            pers25_b6 = Number(X6X11/X11X13).toFixed(3),
            pers25_b7 = Number(X7X11/X11X13).toFixed(3),
            pers25_b8 = Number(X8X11/X11X13).toFixed(3),
            pers25_b9 = Number(X9X11/X11X13).toFixed(3),
            pers25_b10 = Number(X10X11/X11X13).toFixed(3),
            pers25_b11 = Number(X11_2/X11X13).toFixed(3),
            pers25_b12 = Number(X11X12/X11X13).toFixed(3),
            pers25_b13 = Number(X11X13/X11X13).toFixed(3);

            //persamaan 26 (12A) => mengeliminasi b13 dari persamaan(13) 
            const pers26_Y = Number(YX12/X12X13).toFixed(3),
            pers26_b1 = Number(X1X12/X12X13).toFixed(3),
            pers26_b2 = Number(X2X12/X12X13).toFixed(3),
            pers26_b3 = Number(X3X12/X12X13).toFixed(3),
            pers26_b4 = Number(X4X12/X12X13).toFixed(3),
            pers26_b5 = Number(X5X12/X12X13).toFixed(3),
            pers26_b6 = Number(X6X12/X12X13).toFixed(3),
            pers26_b7 = Number(X7X12/X12X13).toFixed(3),
            pers26_b8 = Number(X8X12/X12X13).toFixed(3),
            pers26_b9 = Number(X9X12/X12X13).toFixed(3),
            pers26_b10 = Number(X10X12/X12X13).toFixed(3),
            pers26_b11 = Number(X11X12/X12X13).toFixed(3),
            pers26_b12 = Number(X12_2/X12X13).toFixed(3),
            pers26_b13 = Number(X12X13/X12X13).toFixed(3);

            //persamaan 27 (13A) => mengeliminasi b13 dari persamaan(14) 
            const pers27_Y = Number(YX13/X13_2).toFixed(3),
            pers27_b1 = Number(X1X13/X13_2).toFixed(3),
            pers27_b2 = Number(X2X13/X13_2).toFixed(3),
            pers27_b3 = Number(X3X13/X13_2).toFixed(3),
            pers27_b4 = Number(X4X13/X13_2).toFixed(3),
            pers27_b5 = Number(X5X13/X13_2).toFixed(3),
            pers27_b6 = Number(X6X13/X13_2).toFixed(3),
            pers27_b7 = Number(X7X13/X13_2).toFixed(3),
            pers27_b8 = Number(X8X13/X13_2).toFixed(3),
            pers27_b9 = Number(X9X13/X13_2).toFixed(3),
            pers27_b10 = Number(X10X13/X13_2).toFixed(3),
            pers27_b11 = Number(X11X13/X13_2).toFixed(3),
            pers27_b12 = Number(X12X13/X13_2).toFixed(3),
            pers27_b13 = Number(X13_2/X13_2).toFixed(3);
            
            //persamaan 28 (14) => mengeliminasi persamaan (15) dengan (16) <menghilangkan b13> //////////////////////
            const pers28_Y = Number(pers15_Y-pers16_Y).toFixed(3),
            pers28_b1 = Number(pers15_b1-pers16_b1).toFixed(3),
            pers28_b2 = Number(pers15_b2-pers16_b2).toFixed(3),
            pers28_b3 = Number(pers15_b3-pers16_b3).toFixed(3),
            pers28_b4 = Number(pers15_b4-pers16_b4).toFixed(3),
            pers28_b5 = Number(pers15_b5-pers16_b5).toFixed(3),
            pers28_b6 = Number(pers15_b6-pers16_b6).toFixed(3),
            pers28_b7 = Number(pers15_b7-pers16_b7).toFixed(3),
            pers28_b8 = Number(pers15_b8-pers16_b8).toFixed(3),
            pers28_b9 = Number(pers15_b9-pers16_b9).toFixed(3),
            pers28_b10 = Number(pers15_b10-pers16_b10).toFixed(3),
            pers28_b11 = Number(pers15_b11-pers16_b11).toFixed(3),
            pers28_b12 = Number(pers15_b12-pers16_b12).toFixed(3),
            pers28_b13 = (pers15_b13-pers16_b13);

            //persamaan 29 (15) => mengeliminasi persamaan (16) dengan  (17)
            const pers29_Y = Number(pers16_Y-pers17_Y).toFixed(3),
            pers29_b1 = Number(pers16_b1-pers17_b1).toFixed(3),
            pers29_b2 = Number(pers16_b2-pers17_b2).toFixed(3),
            pers29_b3 = Number(pers16_b3-pers17_b3).toFixed(3),
            pers29_b4 = Number(pers16_b4-pers17_b4).toFixed(3),
            pers29_b5 = Number(pers16_b5-pers17_b5).toFixed(3),
            pers29_b6 = Number(pers16_b6-pers17_b6).toFixed(3),
            pers29_b7 = Number(pers16_b7-pers17_b7).toFixed(3),
            pers29_b8 = Number(pers16_b8-pers17_b8).toFixed(3),
            pers29_b9 = Number(pers16_b9-pers17_b9).toFixed(3),
            pers29_b10 = Number(pers16_b10-pers17_b10).toFixed(3),
            pers29_b11 = Number(pers16_b11-pers17_b11).toFixed(3),
            pers29_b12 = Number(pers16_b12-pers17_b12).toFixed(3),
            pers29_b13 = (pers16_b13-pers17_b13);

            //persamaan 30 (16) => mengeliminasi persamaan (17) dengan (18)
            const pers30_Y = Number(pers17_Y-pers18_Y).toFixed(3),
            pers30_b1 = Number(pers17_b1-pers18_b1).toFixed(3),
            pers30_b2 = Number(pers17_b2-pers18_b2).toFixed(3),
            pers30_b3 = Number(pers17_b3-pers18_b3).toFixed(3),
            pers30_b4 = Number(pers17_b4-pers18_b4).toFixed(3),
            pers30_b5 = Number(pers17_b5-pers18_b5).toFixed(3),
            pers30_b6 = Number(pers17_b6-pers18_b6).toFixed(3),
            pers30_b7 = Number(pers17_b7-pers18_b7).toFixed(3),
            pers30_b8 = Number(pers17_b8-pers18_b8).toFixed(3),
            pers30_b9 = Number(pers17_b9-pers18_b9).toFixed(3),
            pers30_b10 = Number(pers17_b10-pers18_b10).toFixed(3),
            pers30_b11 = Number(pers17_b11-pers18_b11).toFixed(3),
            pers30_b12 = Number(pers17_b12-pers18_b12).toFixed(3),
            pers30_b13 = (pers17_b13-pers18_b13);

            //persamaan 31 (17) => mengeliminasi persamaan (18) dengan (19)
            const pers31_Y = Number(pers18_Y-pers19_Y).toFixed(3),
            pers31_b1 = Number(pers18_b1-pers19_b1).toFixed(3),
            pers31_b2 = Number(pers18_b2-pers19_b2).toFixed(3),
            pers31_b3 = Number(pers18_b3-pers19_b3).toFixed(3),
            pers31_b4 = Number(pers18_b4-pers19_b4).toFixed(3),
            pers31_b5 = Number(pers18_b5-pers19_b5).toFixed(3),
            pers31_b6 = Number(pers18_b6-pers19_b6).toFixed(3),
            pers31_b7 = Number(pers18_b7-pers19_b7).toFixed(3),
            pers31_b8 = Number(pers18_b8-pers19_b8).toFixed(3),
            pers31_b9 = Number(pers18_b9-pers19_b9).toFixed(3),
            pers31_b10 = Number(pers18_b10-pers19_b10).toFixed(3),
            pers31_b11 = Number(pers18_b11-pers19_b11).toFixed(3),
            pers31_b12 = Number(pers18_b12-pers19_b12).toFixed(3),
            pers31_b13 = (pers18_b13-pers19_b13);

            //persamaan 32 (18) => mengeliminasi persamaan (19) dengan (20)
            const pers32_Y = Number(pers19_Y-pers20_Y).toFixed(3),
            pers32_b1 = Number(pers19_b1-pers20_b1).toFixed(3),
            pers32_b2 = Number(pers19_b2-pers20_b2).toFixed(3),
            pers32_b3 = Number(pers19_b3-pers20_b3).toFixed(3),
            pers32_b4 = Number(pers19_b4-pers20_b4).toFixed(3),
            pers32_b5 = Number(pers19_b5-pers20_b5).toFixed(3),
            pers32_b6 = Number(pers19_b6-pers20_b6).toFixed(3),
            pers32_b7 = Number(pers19_b7-pers20_b7).toFixed(3),
            pers32_b8 = Number(pers19_b8-pers20_b8).toFixed(3),
            pers32_b9 = Number(pers19_b9-pers20_b9).toFixed(3),
            pers32_b10 = Number(pers19_b10-pers20_b10).toFixed(3),
            pers32_b11 = Number(pers19_b11-pers20_b11).toFixed(3),
            pers32_b12 = Number(pers19_b12-pers20_b12).toFixed(3),
            pers32_b13 = (pers19_b13-pers20_b13);

            //persamaan 33 (19) => mengeliminasi persamaan (20) dengan (21)
            const pers33_Y = Number(pers20_Y-pers21_Y).toFixed(3),
            pers33_b1 = Number(pers20_b1-pers21_b1).toFixed(3),
            pers33_b2 = Number(pers20_b2-pers21_b2).toFixed(3),
            pers33_b3 = Number(pers20_b3-pers21_b3).toFixed(3),
            pers33_b4 = Number(pers20_b4-pers21_b4).toFixed(3),
            pers33_b5 = Number(pers20_b5-pers21_b5).toFixed(3),
            pers33_b6 = Number(pers20_b6-pers21_b6).toFixed(3),
            pers33_b7 = Number(pers20_b7-pers21_b7).toFixed(3),
            pers33_b8 = Number(pers20_b8-pers21_b8).toFixed(3),
            pers33_b9 = Number(pers20_b9-pers21_b9).toFixed(3),
            pers33_b10 = Number(pers20_b10-pers21_b10).toFixed(3),
            pers33_b11 = Number(pers20_b11-pers21_b11).toFixed(3),
            pers33_b12 = Number(pers20_b12-pers21_b12).toFixed(3),
            pers33_b13 = (pers20_b13-pers21_b13);

            //persamaan 34 (20) => mengeliminasi persamaan (21) dengan (22)
            const pers34_Y = Number(pers21_Y-pers22_Y).toFixed(3),
            pers34_b1 = Number(pers21_b1-pers22_b1).toFixed(3),
            pers34_b2 = Number(pers21_b2-pers22_b2).toFixed(3),
            pers34_b3 = Number(pers21_b3-pers22_b3).toFixed(3),
            pers34_b4 = Number(pers21_b4-pers22_b4).toFixed(3),
            pers34_b5 = Number(pers21_b5-pers22_b5).toFixed(3),
            pers34_b6 = Number(pers21_b6-pers22_b6).toFixed(3),
            pers34_b7 = Number(pers21_b7-pers22_b7).toFixed(3),
            pers34_b8 = Number(pers21_b8-pers22_b8).toFixed(3),
            pers34_b9 = Number(pers21_b9-pers22_b9).toFixed(3),
            pers34_b10 = Number(pers21_b10-pers22_b10).toFixed(3),
            pers34_b11 = Number(pers21_b11-pers22_b11).toFixed(3),
            pers34_b12 = Number(pers21_b12-pers22_b12).toFixed(3),
            pers34_b13 = (pers21_b13-pers22_b13);

            //persamaan 35 (21) => mengeliminasi persamaan (22) dengan (23)
            const pers35_Y = Number(pers22_Y-pers23_Y).toFixed(3),
            pers35_b1 = Number(pers22_b1-pers23_b1).toFixed(3),
            pers35_b2 = Number(pers22_b2-pers23_b2).toFixed(3),
            pers35_b3 = Number(pers22_b3-pers23_b3).toFixed(3),
            pers35_b4 = Number(pers22_b4-pers23_b4).toFixed(3),
            pers35_b5 = Number(pers22_b5-pers23_b5).toFixed(3),
            pers35_b6 = Number(pers22_b6-pers23_b6).toFixed(3),
            pers35_b7 = Number(pers22_b7-pers23_b7).toFixed(3),
            pers35_b8 = Number(pers22_b8-pers23_b8).toFixed(3),
            pers35_b9 = Number(pers22_b9-pers23_b9).toFixed(3),
            pers35_b10 = Number(pers22_b10-pers23_b10).toFixed(3),
            pers35_b11 = Number(pers22_b11-pers23_b11).toFixed(3),
            pers35_b12 = Number(pers22_b12-pers23_b12).toFixed(3),
            pers35_b13 = (pers22_b13-pers23_b13);

            //persamaan 36 (22) => mengeliminasi persamaan (23) dengan (24)
            const pers36_Y = Number(pers23_Y-pers24_Y).toFixed(3),
            pers36_b1 = Number(pers23_b1-pers24_b1).toFixed(3),
            pers36_b2 = Number(pers23_b2-pers24_b2).toFixed(3),
            pers36_b3 = Number(pers23_b3-pers24_b3).toFixed(3),
            pers36_b4 = Number(pers23_b4-pers24_b4).toFixed(3),
            pers36_b5 = Number(pers23_b5-pers24_b5).toFixed(3),
            pers36_b6 = Number(pers23_b6-pers24_b6).toFixed(3),
            pers36_b7 = Number(pers23_b7-pers24_b7).toFixed(3),
            pers36_b8 = Number(pers23_b8-pers24_b8).toFixed(3),
            pers36_b9 = Number(pers23_b9-pers24_b9).toFixed(3),
            pers36_b10 = Number(pers23_b10-pers24_b10).toFixed(3),
            pers36_b11 = Number(pers23_b11-pers24_b11).toFixed(3),
            pers36_b12 = Number(pers23_b12-pers24_b12).toFixed(3),
            pers36_b13 = (pers23_b13-pers24_b13);

            //persamaan 37 (23) => mengeliminasi persamaan (24) dengan (25)
            const pers37_Y = Number(pers24_Y-pers25_Y).toFixed(3),
            pers37_b1 = Number(pers24_b1-pers25_b1).toFixed(3),
            pers37_b2 = Number(pers24_b2-pers25_b2).toFixed(3),
            pers37_b3 = Number(pers24_b3-pers25_b3).toFixed(3),
            pers37_b4 = Number(pers24_b4-pers25_b4).toFixed(3),
            pers37_b5 = Number(pers24_b5-pers25_b5).toFixed(3),
            pers37_b6 = Number(pers24_b6-pers25_b6).toFixed(3),
            pers37_b7 = Number(pers24_b7-pers25_b7).toFixed(3),
            pers37_b8 = Number(pers24_b8-pers25_b8).toFixed(3),
            pers37_b9 = Number(pers24_b9-pers25_b9).toFixed(3),
            pers37_b10 = Number(pers24_b10-pers25_b10).toFixed(3),
            pers37_b11 = Number(pers24_b11-pers25_b11).toFixed(3),
            pers37_b12 = Number(pers24_b12-pers25_b12).toFixed(3),
            pers37_b13 = (pers24_b13-pers25_b13);

            //persamaan 38 (24) => mengeliminasi persamaan (25) dengan (26)
            const pers38_Y = Number(pers25_Y-pers26_Y).toFixed(3),
            pers38_b1 = Number(pers25_b1-pers26_b1).toFixed(3),
            pers38_b2 = Number(pers25_b2-pers26_b2).toFixed(3),
            pers38_b3 = Number(pers25_b3-pers26_b3).toFixed(3),
            pers38_b4 = Number(pers25_b4-pers26_b4).toFixed(3),
            pers38_b5 = Number(pers25_b5-pers26_b5).toFixed(3),
            pers38_b6 = Number(pers25_b6-pers26_b6).toFixed(3),
            pers38_b7 = Number(pers25_b7-pers26_b7).toFixed(3),
            pers38_b8 = Number(pers25_b8-pers26_b8).toFixed(3),
            pers38_b9 = Number(pers25_b9-pers26_b9).toFixed(3),
            pers38_b10 = Number(pers25_b10-pers26_b10).toFixed(3),
            pers38_b11 = Number(pers25_b11-pers26_b11).toFixed(3),
            pers38_b12 = Number(pers25_b12-pers26_b12).toFixed(3),
            pers38_b13 = (pers25_b13-pers26_b13);

            //persamaan 39 (25) => mengeliminasi persamaan (26) dengan (27)
            const pers39_Y = Number(pers26_Y-pers27_Y).toFixed(3),
            pers39_b1 = Number(pers26_b1-pers27_b1).toFixed(3),
            pers39_b2 = Number(pers26_b2-pers27_b2).toFixed(3),
            pers39_b3 = Number(pers26_b3-pers27_b3).toFixed(3),
            pers39_b4 = Number(pers26_b4-pers27_b4).toFixed(3),
            pers39_b5 = Number(pers26_b5-pers27_b5).toFixed(3),
            pers39_b6 = Number(pers26_b6-pers27_b6).toFixed(3),
            pers39_b7 = Number(pers26_b7-pers27_b7).toFixed(3),
            pers39_b8 = Number(pers26_b8-pers27_b8).toFixed(3),
            pers39_b9 = Number(pers26_b9-pers27_b9).toFixed(3),
            pers39_b10 = Number(pers26_b10-pers27_b10).toFixed(3),
            pers39_b11 = Number(pers26_b11-pers27_b11).toFixed(3),
            pers39_b12 = Number(pers26_b12-pers27_b12).toFixed(3),
            pers39_b13 = (pers26_b13-pers27_b13);

            //persamaan 40 (14A) => mengeliminasi b12 dari persamaan (28) / (14) <<BATAS>> /////////////
            const pers40_Y = Number(pers28_Y/pers28_b12).toFixed(3),
            pers40_b1 = Number(pers28_b1/pers28_b12).toFixed(3),
            pers40_b2 = Number(pers28_b2/pers28_b12).toFixed(3),
            pers40_b3 = Number(pers28_b3/pers28_b12).toFixed(3),
            pers40_b4 = Number(pers28_b4/pers28_b12).toFixed(3),
            pers40_b5 = Number(pers28_b5/pers28_b12).toFixed(3),
            pers40_b6 = Number(pers28_b6/pers28_b12).toFixed(3),
            pers40_b7 = Number(pers28_b7/pers28_b12).toFixed(3),
            pers40_b8 = Number(pers28_b8/pers28_b12).toFixed(3),
            pers40_b9 = Number(pers28_b9/pers28_b12).toFixed(3),
            pers40_b10 = Number(pers28_b10/pers28_b12).toFixed(3),
            pers40_b11 = Number(pers28_b11/pers28_b12).toFixed(3),
            pers40_b12 = Number(pers28_b12/pers28_b12).toFixed(3);
            
            //persamaan 41 (15A) => mengeliminasi b12 dari persamaan (29)
            const pers41_Y = Number(pers29_Y/pers29_b12).toFixed(3),
            pers41_b1 = Number(pers29_b1/pers29_b12).toFixed(3),
            pers41_b2 = Number(pers29_b2/pers29_b12).toFixed(3),
            pers41_b3 = Number(pers29_b3/pers29_b12).toFixed(3),
            pers41_b4 = Number(pers29_b4/pers29_b12).toFixed(3),
            pers41_b5 = Number(pers29_b5/pers29_b12).toFixed(3),
            pers41_b6 = Number(pers29_b6/pers29_b12).toFixed(3),
            pers41_b7 = Number(pers29_b7/pers29_b12).toFixed(3),
            pers41_b8 = Number(pers29_b8/pers29_b12).toFixed(3),
            pers41_b9 = Number(pers29_b9/pers29_b12).toFixed(3),
            pers41_b10 = Number(pers29_b10/pers29_b12).toFixed(3),
            pers41_b11 = Number(pers29_b11/pers29_b12).toFixed(3),
            pers41_b12 = Number(pers29_b12/pers29_b12).toFixed(3);

            //persamaan 42 (16A)=> mengeliminasi b12 dari persamaan (30)
            const pers42_Y = Number(pers30_Y/pers30_b12).toFixed(3),
            pers42_b1 = Number(pers30_b1/pers30_b12).toFixed(3),
            pers42_b2 = Number(pers30_b2/pers30_b12).toFixed(3),
            pers42_b3 = Number(pers30_b3/pers30_b12).toFixed(3),
            pers42_b4 = Number(pers30_b4/pers30_b12).toFixed(3),
            pers42_b5 = Number(pers30_b5/pers30_b12).toFixed(3),
            pers42_b6 = Number(pers30_b6/pers30_b12).toFixed(3),
            pers42_b7 = Number(pers30_b7/pers30_b12).toFixed(3),
            pers42_b8 = Number(pers30_b8/pers30_b12).toFixed(3),
            pers42_b9 = Number(pers30_b9/pers30_b12).toFixed(3),
            pers42_b10 = Number(pers30_b10/pers30_b12).toFixed(3),
            pers42_b11 = Number(pers30_b11/pers30_b12).toFixed(3),
            pers42_b12 = Number(pers30_b12/pers30_b12).toFixed(3);

            //persamaan 43 (17A)=> mengeliminasi b12 dari persamaan (31)
            const pers43_Y = Number(pers31_Y/pers31_b12).toFixed(3),
            pers43_b1 = Number(pers31_b1/pers31_b12).toFixed(3),
            pers43_b2 = Number(pers31_b2/pers31_b12).toFixed(3),
            pers43_b3 = Number(pers31_b3/pers31_b12).toFixed(3),
            pers43_b4 = Number(pers31_b4/pers31_b12).toFixed(3),
            pers43_b5 = Number(pers31_b5/pers31_b12).toFixed(3),
            pers43_b6 = Number(pers31_b6/pers31_b12).toFixed(3),
            pers43_b7 = Number(pers31_b7/pers31_b12).toFixed(3),
            pers43_b8 = Number(pers31_b8/pers31_b12).toFixed(3),
            pers43_b9 = Number(pers31_b9/pers31_b12).toFixed(3),
            pers43_b10 = Number(pers31_b10/pers31_b12).toFixed(3),
            pers43_b11 = Number(pers31_b11/pers31_b12).toFixed(3),
            pers43_b12 = Number(pers31_b12/pers31_b12).toFixed(3);

            //persamaan 44 (18A)=> mengeliminasi b12 dari persamaan (32)
            const pers44_Y = Number(pers32_Y/pers32_b12).toFixed(3),
            pers44_b1 = Number(pers32_b1/pers32_b12).toFixed(3),
            pers44_b2 = Number(pers32_b2/pers32_b12).toFixed(3),
            pers44_b3 = Number(pers32_b3/pers32_b12).toFixed(3),
            pers44_b4 = Number(pers32_b4/pers32_b12).toFixed(3),
            pers44_b5 = Number(pers32_b5/pers32_b12).toFixed(3),
            pers44_b6 = Number(pers32_b6/pers32_b12).toFixed(3),
            pers44_b7 = Number(pers32_b7/pers32_b12).toFixed(3),
            pers44_b8 = Number(pers32_b8/pers32_b12).toFixed(3),
            pers44_b9 = Number(pers32_b9/pers32_b12).toFixed(3),
            pers44_b10 = Number(pers32_b10/pers32_b12).toFixed(3),
            pers44_b11 = Number(pers32_b11/pers32_b12).toFixed(3),
            pers44_b12 = Number(pers32_b12/pers32_b12).toFixed(3);

            //persamaan 45 (19A)=> mengeliminasi b12 dari persamaan (33)
            const pers45_Y = Number(pers33_Y/pers33_b12).toFixed(3),
            pers45_b1 = Number(pers33_b1/pers33_b12).toFixed(3),
            pers45_b2 = Number(pers33_b2/pers33_b12).toFixed(3),
            pers45_b3 = Number(pers33_b3/pers33_b12).toFixed(3),
            pers45_b4 = Number(pers33_b4/pers33_b12).toFixed(3),
            pers45_b5 = Number(pers33_b5/pers33_b12).toFixed(3),
            pers45_b6 = Number(pers33_b6/pers33_b12).toFixed(3),
            pers45_b7 = Number(pers33_b7/pers33_b12).toFixed(3),
            pers45_b8 = Number(pers33_b8/pers33_b12).toFixed(3),
            pers45_b9 = Number(pers33_b9/pers33_b12).toFixed(3),
            pers45_b10 = Number(pers33_b10/pers33_b12).toFixed(3),
            pers45_b11 = Number(pers33_b11/pers33_b12).toFixed(3),
            pers45_b12 = Number(pers33_b12/pers33_b12).toFixed(3);

            //persamaan 46 (20A)=> mengeliminasi b12 dari persamaan (34)
            const pers46_Y = Number(pers34_Y/pers34_b12).toFixed(3),
            pers46_b1 = Number(pers34_b1/pers34_b12).toFixed(3),
            pers46_b2 = Number(pers34_b2/pers34_b12).toFixed(3),
            pers46_b3 = Number(pers34_b3/pers34_b12).toFixed(3),
            pers46_b4 = Number(pers34_b4/pers34_b12).toFixed(3),
            pers46_b5 = Number(pers34_b5/pers34_b12).toFixed(3),
            pers46_b6 = Number(pers34_b6/pers34_b12).toFixed(3),
            pers46_b7 = Number(pers34_b7/pers34_b12).toFixed(3),
            pers46_b8 = Number(pers34_b8/pers34_b12).toFixed(3),
            pers46_b9 = Number(pers34_b9/pers34_b12).toFixed(3),
            pers46_b10 = Number(pers34_b10/pers34_b12).toFixed(3),
            pers46_b11 = Number(pers34_b11/pers34_b12).toFixed(3),
            pers46_b12 = Number(pers34_b12/pers34_b12).toFixed(3);;

            //persamaan 47 (21A)=> mengeliminasi b12 dari persamaan (35)
            const pers47_Y = Number(pers35_Y/pers35_b12).toFixed(3),
            pers47_b1 = Number(pers35_b1/pers35_b12).toFixed(3),
            pers47_b2 = Number(pers35_b2/pers35_b12).toFixed(3),
            pers47_b3 = Number(pers35_b3/pers35_b12).toFixed(3),
            pers47_b4 = Number(pers35_b4/pers35_b12).toFixed(3),
            pers47_b5 = Number(pers35_b5/pers35_b12).toFixed(3),
            pers47_b6 = Number(pers35_b6/pers35_b12).toFixed(3),
            pers47_b7 = Number(pers35_b7/pers35_b12).toFixed(3),
            pers47_b8 = Number(pers35_b8/pers35_b12).toFixed(3),
            pers47_b9 = Number(pers35_b9/pers35_b12).toFixed(3),
            pers47_b10 = Number(pers35_b10/pers35_b12).toFixed(3),
            pers47_b11 = Number(pers35_b11/pers35_b12).toFixed(3),
            pers47_b12 = Number(pers35_b12/pers35_b12).toFixed(3);

            //persamaan 48 (22A)=> mengeliminasi b12 dari persamaan (36)
            const pers48_Y = Number(pers36_Y/pers36_b12).toFixed(3),
            pers48_b1 = Number(pers36_b1/pers36_b12).toFixed(3),
            pers48_b2 = Number(pers36_b2/pers36_b12).toFixed(3),
            pers48_b3 = Number(pers36_b3/pers36_b12).toFixed(3),
            pers48_b4 = Number(pers36_b4/pers36_b12).toFixed(3),
            pers48_b5 = Number(pers36_b5/pers36_b12).toFixed(3),
            pers48_b6 = Number(pers36_b6/pers36_b12).toFixed(3),
            pers48_b7 = Number(pers36_b7/pers36_b12).toFixed(3),
            pers48_b8 = Number(pers36_b8/pers36_b12).toFixed(3),
            pers48_b9 = Number(pers36_b9/pers36_b12).toFixed(3),
            pers48_b10 = Number(pers36_b10/pers36_b12).toFixed(3),
            pers48_b11 = Number(pers36_b11/pers36_b12).toFixed(3),
            pers48_b12 = Number(pers36_b12/pers36_b12).toFixed(3);

            //persamaan 49 (23A)=> mengeliminasi b12 dari persamaan (37)
            const pers49_Y = Number(pers37_Y/pers37_b12).toFixed(3),
            pers49_b1 = Number(pers37_b1/pers37_b12).toFixed(3),
            pers49_b2 = Number(pers37_b2/pers37_b12).toFixed(3),
            pers49_b3 = Number(pers37_b3/pers37_b12).toFixed(3),
            pers49_b4 = Number(pers37_b4/pers37_b12).toFixed(3),
            pers49_b5 = Number(pers37_b5/pers37_b12).toFixed(3),
            pers49_b6 = Number(pers37_b6/pers37_b12).toFixed(3),
            pers49_b7 = Number(pers37_b7/pers37_b12).toFixed(3),
            pers49_b8 = Number(pers37_b8/pers37_b12).toFixed(3),
            pers49_b9 = Number(pers37_b9/pers37_b12).toFixed(3),
            pers49_b10 = Number(pers37_b10/pers37_b12).toFixed(3),
            pers49_b11 = Number(pers37_b11/pers37_b12).toFixed(3),
            pers49_b12 = Number(pers37_b12/pers37_b12).toFixed(3);

            //persamaan 50 (24A)=> mengeliminasi b12 dari persamaan (38)
            const pers50_Y = Number(pers38_Y/pers38_b12).toFixed(3),
            pers50_b1 = Number(pers38_b1/pers38_b12).toFixed(3),
            pers50_b2 = Number(pers38_b2/pers38_b12).toFixed(3),
            pers50_b3 = Number(pers38_b3/pers38_b12).toFixed(3),
            pers50_b4 = Number(pers38_b4/pers38_b12).toFixed(3),
            pers50_b5 = Number(pers38_b5/pers38_b12).toFixed(3),
            pers50_b6 = Number(pers38_b6/pers38_b12).toFixed(3),
            pers50_b7 = Number(pers38_b7/pers38_b12).toFixed(3),
            pers50_b8 = Number(pers38_b8/pers38_b12).toFixed(3),
            pers50_b9 = Number(pers38_b9/pers38_b12).toFixed(3),
            pers50_b10 = Number(pers38_b10/pers38_b12).toFixed(3),
            pers50_b11 = Number(pers38_b11/pers38_b12).toFixed(3),
            pers50_b12 = Number(pers38_b12/pers38_b12).toFixed(3);

            //persamaan 51 (25A) => mengeliminasi b12 dari persamaan (39)
            const pers51_Y = Number(pers39_Y/pers39_b12).toFixed(3),
            pers51_b1 = Number(pers39_b1/pers39_b12).toFixed(3),
            pers51_b2 = Number(pers39_b2/pers39_b12).toFixed(3),
            pers51_b3 = Number(pers39_b3/pers39_b12).toFixed(3),
            pers51_b4 = Number(pers39_b4/pers39_b12).toFixed(3),
            pers51_b5 = Number(pers39_b5/pers39_b12).toFixed(3),
            pers51_b6 = Number(pers39_b6/pers39_b12).toFixed(3),
            pers51_b7 = Number(pers39_b7/pers39_b12).toFixed(3),
            pers51_b8 = Number(pers39_b8/pers39_b12).toFixed(3),
            pers51_b9 = Number(pers39_b9/pers39_b12).toFixed(3),
            pers51_b10 = Number(pers39_b10/pers39_b12).toFixed(3),
            pers51_b11 = Number(pers39_b11/pers39_b12).toFixed(3),
            pers51_b12 = Number(pers39_b12/pers39_b12).toFixed(3);
    
            //persamaan 52 (26) => mengeliminasi persamaan (40) dengan (41) <menghilangkan b12> /////////
            const pers52_Y = Number(pers40_Y-pers41_Y).toFixed(3),
            pers52_b1 = Number(pers40_b1-pers41_b1).toFixed(3),
            pers52_b2 = Number(pers40_b2-pers41_b2).toFixed(3),
            pers52_b3 = Number(pers40_b3-pers41_b3).toFixed(3),
            pers52_b4 = Number(pers40_b4-pers41_b4).toFixed(3),
            pers52_b5 = Number(pers40_b5-pers41_b5).toFixed(3),
            pers52_b6 = Number(pers40_b6-pers41_b6).toFixed(3),
            pers52_b7 = Number(pers40_b7-pers41_b7).toFixed(3),
            pers52_b8 = Number(pers40_b8-pers41_b8).toFixed(3),
            pers52_b9 = Number(pers40_b9-pers41_b9).toFixed(3),
            pers52_b10 = Number(pers40_b10-pers41_b10).toFixed(3),
            pers52_b11 = Number(pers40_b11-pers41_b11).toFixed(3),
            pers52_b12 = (pers40_b12-pers41_b12);

            //persamaan 53 (27) => mengeliminasi persamaan (41) dengan (42) 
            const pers53_Y = Number(pers41_Y-pers42_Y).toFixed(3),
            pers53_b1 = Number(pers41_b1-pers42_b1).toFixed(3),
            pers53_b2 = Number(pers41_b2-pers42_b2).toFixed(3),
            pers53_b3 = Number(pers41_b3-pers42_b3).toFixed(3),
            pers53_b4 = Number(pers41_b4-pers42_b4).toFixed(3),
            pers53_b5 = Number(pers41_b5-pers42_b5).toFixed(3),
            pers53_b6 = Number(pers41_b6-pers42_b6).toFixed(3),
            pers53_b7 = Number(pers41_b7-pers42_b7).toFixed(3),
            pers53_b8 = Number(pers41_b8-pers42_b8).toFixed(3),
            pers53_b9 = Number(pers41_b9-pers42_b9).toFixed(3),
            pers53_b10 = Number(pers41_b10-pers42_b10).toFixed(3),
            pers53_b11 = Number(pers41_b11-pers42_b11).toFixed(3),
            pers53_b12 = (pers41_b12-pers42_b12);

            //persamaan 54 (28) => mengeliminasi persamaan (42) dengan (43) 
            const pers54_Y = Number(pers42_Y-pers43_Y).toFixed(3),
            pers54_b1 = Number(pers42_b1-pers43_b1).toFixed(3),
            pers54_b2 = Number(pers42_b2-pers43_b2).toFixed(3),
            pers54_b3 = Number(pers42_b3-pers43_b3).toFixed(3),
            pers54_b4 = Number(pers42_b4-pers43_b4).toFixed(3),
            pers54_b5 = Number(pers42_b5-pers43_b5).toFixed(3),
            pers54_b6 = Number(pers42_b6-pers43_b6).toFixed(3),
            pers54_b7 = Number(pers42_b7-pers43_b7).toFixed(3),
            pers54_b8 = Number(pers42_b8-pers43_b8).toFixed(3),
            pers54_b9 = Number(pers42_b9-pers43_b9).toFixed(3),
            pers54_b10 = Number(pers42_b10-pers43_b10).toFixed(3),
            pers54_b11 = Number(pers42_b11-pers43_b11).toFixed(3),
            pers54_b12 = (pers42_b12-pers43_b12);
            
            //persamaan 55 (29) => mengeliminasi persamaan (43) dengan (44) 
            const pers55_Y = Number(pers43_Y-pers44_Y).toFixed(3),
            pers55_b1 = Number(pers43_b1-pers44_b1).toFixed(3),
            pers55_b2 = Number(pers43_b2-pers44_b2).toFixed(3),
            pers55_b3 = Number(pers43_b3-pers44_b3).toFixed(3),
            pers55_b4 = Number(pers43_b4-pers44_b4).toFixed(3),
            pers55_b5 = Number(pers43_b5-pers44_b5).toFixed(3),
            pers55_b6 = Number(pers43_b6-pers44_b6).toFixed(3),
            pers55_b7 = Number(pers43_b7-pers44_b7).toFixed(3),
            pers55_b8 = Number(pers43_b8-pers44_b8).toFixed(3),
            pers55_b9 = Number(pers43_b9-pers44_b9).toFixed(3),
            pers55_b10 = Number(pers43_b10-pers44_b10).toFixed(3),
            pers55_b11 = Number(pers43_b11-pers44_b11).toFixed(3),
            pers55_b12 = (pers43_b12-pers44_b12);

            //persamaan 56 (30) => mengeliminasi persamaan (44) dengan (45) 
            const pers56_Y = Number(pers44_Y-pers45_Y).toFixed(3),
            pers56_b1 = Number(pers44_b1-pers45_b1).toFixed(3),
            pers56_b2 = Number(pers44_b2-pers45_b2).toFixed(3),
            pers56_b3 = Number(pers44_b3-pers45_b3).toFixed(3),
            pers56_b4 = Number(pers44_b4-pers45_b4).toFixed(3),
            pers56_b5 = Number(pers44_b5-pers45_b5).toFixed(3),
            pers56_b6 = Number(pers44_b6-pers45_b6).toFixed(3),
            pers56_b7 = Number(pers44_b7-pers45_b7).toFixed(3),
            pers56_b8 = Number(pers44_b8-pers45_b8).toFixed(3),
            pers56_b9 = Number(pers44_b9-pers45_b9).toFixed(3),
            pers56_b10 = Number(pers44_b10-pers45_b10).toFixed(3),
            pers56_b11 = Number(pers44_b11-pers45_b11).toFixed(3),
            pers56_b12 = (pers44_b12-pers45_b12);

            //persamaan 57 (31) => mengeliminasi persamaan (45) dengan (46) 
            const pers57_Y = Number(pers45_Y-pers46_Y).toFixed(3),
            pers57_b1 = Number(pers45_b1-pers46_b1).toFixed(3),
            pers57_b2 = Number(pers45_b2-pers46_b2).toFixed(3),
            pers57_b3 = Number(pers45_b3-pers46_b3).toFixed(3),
            pers57_b4 = Number(pers45_b4-pers46_b4).toFixed(3),
            pers57_b5 = Number(pers45_b5-pers46_b5).toFixed(3),
            pers57_b6 = Number(pers45_b6-pers46_b6).toFixed(3),
            pers57_b7 = Number(pers45_b7-pers46_b7).toFixed(3),
            pers57_b8 = Number(pers45_b8-pers46_b8).toFixed(3),
            pers57_b9 = Number(pers45_b9-pers46_b9).toFixed(3),
            pers57_b10 = Number(pers45_b10-pers46_b10).toFixed(3),
            pers57_b11 = Number(pers45_b11-pers46_b11).toFixed(3),
            pers57_b12 = (pers45_b12-pers46_b12);

            //persamaan 58 (32) => mengeliminasi persamaan (46) dengan (47) 
            const pers58_Y = Number(pers46_Y-pers47_Y).toFixed(3),
            pers58_b1 = Number(pers46_b1-pers47_b1).toFixed(3),
            pers58_b2 = Number(pers46_b2-pers47_b2).toFixed(3),
            pers58_b3 = Number(pers46_b3-pers47_b3).toFixed(3),
            pers58_b4 = Number(pers46_b4-pers47_b4).toFixed(3),
            pers58_b5 = Number(pers46_b5-pers47_b5).toFixed(3),
            pers58_b6 = Number(pers46_b6-pers47_b6).toFixed(3),
            pers58_b7 = Number(pers46_b7-pers47_b7).toFixed(3),
            pers58_b8 = Number(pers46_b8-pers47_b8).toFixed(3),
            pers58_b9 = Number(pers46_b9-pers47_b9).toFixed(3),
            pers58_b10 = Number(pers46_b10-pers47_b10).toFixed(3),
            pers58_b11 = Number(pers46_b11-pers47_b11).toFixed(3),
            pers58_b12 = (pers46_b12-pers47_b12);

            //persamaan 59 (33) => mengeliminasi persamaan (47) dengan (48) 
            const pers59_Y = Number(pers47_Y-pers48_Y).toFixed(3),
            pers59_b1 = Number(pers47_b1-pers48_b1).toFixed(3),
            pers59_b2 = Number(pers47_b2-pers48_b2).toFixed(3),
            pers59_b3 = Number(pers47_b3-pers48_b3).toFixed(3),
            pers59_b4 = Number(pers47_b4-pers48_b4).toFixed(3),
            pers59_b5 = Number(pers47_b5-pers48_b5).toFixed(3),
            pers59_b6 = Number(pers47_b6-pers48_b6).toFixed(3),
            pers59_b7 = Number(pers47_b7-pers48_b7).toFixed(3),
            pers59_b8 = Number(pers47_b8-pers48_b8).toFixed(3),
            pers59_b9 = Number(pers47_b9-pers48_b9).toFixed(3),
            pers59_b10 = Number(pers47_b10-pers48_b10).toFixed(3),
            pers59_b11 = Number(pers47_b11-pers48_b11).toFixed(3),
            pers59_b12 = (pers47_b12-pers48_b12);

            //persamaan 60 (34) => mengeliminasi persamaan (48) dengan (49) 
            const pers60_Y = Number(pers48_Y-pers49_Y).toFixed(3),
            pers60_b1 = Number(pers48_b1-pers49_b1).toFixed(3),
            pers60_b2 = Number(pers48_b2-pers49_b2).toFixed(3),
            pers60_b3 = Number(pers48_b3-pers49_b3).toFixed(3),
            pers60_b4 = Number(pers48_b4-pers49_b4).toFixed(3),
            pers60_b5 = Number(pers48_b5-pers49_b5).toFixed(3),
            pers60_b6 = Number(pers48_b6-pers49_b6).toFixed(3),
            pers60_b7 = Number(pers48_b7-pers49_b7).toFixed(3),
            pers60_b8 = Number(pers48_b8-pers49_b8).toFixed(3),
            pers60_b9 = Number(pers48_b9-pers49_b9).toFixed(3),
            pers60_b10 = Number(pers48_b10-pers49_b10).toFixed(3),
            pers60_b11 = Number(pers48_b11-pers49_b11).toFixed(3),
            pers60_b12 = (pers48_b12-pers49_b12);

            //persamaan 61 (35) => mengeliminasi persamaan (49) dengan (50) 
            const pers61_Y = Number(pers49_Y-pers50_Y).toFixed(3),
            pers61_b1 = Number(pers49_b1-pers50_b1).toFixed(3),
            pers61_b2 = Number(pers49_b2-pers50_b2).toFixed(3),
            pers61_b3 = Number(pers49_b3-pers50_b3).toFixed(3),
            pers61_b4 = Number(pers49_b4-pers50_b4).toFixed(3),
            pers61_b5 = Number(pers49_b5-pers50_b5).toFixed(3),
            pers61_b6 = Number(pers49_b6-pers50_b6).toFixed(3),
            pers61_b7 = Number(pers49_b7-pers50_b7).toFixed(3),
            pers61_b8 = Number(pers49_b8-pers50_b8).toFixed(3),
            pers61_b9 = Number(pers49_b9-pers50_b9).toFixed(3),
            pers61_b10 = Number(pers49_b10-pers50_b10).toFixed(3),
            pers61_b11 = Number(pers49_b11-pers50_b11).toFixed(3),
            pers61_b12 = (pers49_b12-pers50_b12);

            //persamaan 62 (36) => mengeliminasi persamaan (50) dengan (51) 
            const pers62_Y = Number(pers50_Y-pers51_Y).toFixed(3),
            pers62_b1 = Number(pers50_b1-pers51_b1).toFixed(3),
            pers62_b2 = Number(pers50_b2-pers51_b2).toFixed(3),
            pers62_b3 = Number(pers50_b3-pers51_b3).toFixed(3),
            pers62_b4 = Number(pers50_b4-pers51_b4).toFixed(3),
            pers62_b5 = Number(pers50_b5-pers51_b5).toFixed(3),
            pers62_b6 = Number(pers50_b6-pers51_b6).toFixed(3),
            pers62_b7 = Number(pers50_b7-pers51_b7).toFixed(3),
            pers62_b8 = Number(pers50_b8-pers51_b8).toFixed(3),
            pers62_b9 = Number(pers50_b9-pers51_b9).toFixed(3),
            pers62_b10 = Number(pers50_b10-pers51_b10).toFixed(3),
            pers62_b11 = Number(pers50_b11-pers51_b11).toFixed(3),
            pers62_b12 = (pers50_b12-pers51_b12);

            //persamaan 63 (26A) => mengeliminasi b11 dari persamaan (52) <<BATAS>> ///////////////////
            const pers63_Y = Number(pers52_Y/pers52_b11).toFixed(3),
            pers63_b1 = Number(pers52_b1/pers52_b11).toFixed(3),
            pers63_b2 = Number(pers52_b2/pers52_b11).toFixed(3),
            pers63_b3 = Number(pers52_b3/pers52_b11).toFixed(3),
            pers63_b4 = Number(pers52_b4/pers52_b11).toFixed(3),
            pers63_b5 = Number(pers52_b5/pers52_b11).toFixed(3),
            pers63_b6 = Number(pers52_b6/pers52_b11).toFixed(3),
            pers63_b7 = Number(pers52_b7/pers52_b11).toFixed(3),
            pers63_b8 = Number(pers52_b8/pers52_b11).toFixed(3),
            pers63_b9 = Number(pers52_b9/pers52_b11).toFixed(3),
            pers63_b10 = Number(pers52_b10/pers52_b11).toFixed(3),
            pers63_b11 = Number(pers52_b11/pers52_b11).toFixed(3);

            //persamaan 64 (27A) => mengeliminasi b11 dari persamaan (53) 
            const pers64_Y = Number(pers53_Y/pers53_b11).toFixed(3),
            pers64_b1 = Number(pers53_b1/pers53_b11).toFixed(3),
            pers64_b2 = Number(pers53_b2/pers53_b11).toFixed(3),
            pers64_b3 = Number(pers53_b3/pers53_b11).toFixed(3),
            pers64_b4 = Number(pers53_b4/pers53_b11).toFixed(3),
            pers64_b5 = Number(pers53_b5/pers53_b11).toFixed(3),
            pers64_b6 = Number(pers53_b6/pers53_b11).toFixed(3),
            pers64_b7 = Number(pers53_b7/pers53_b11).toFixed(3),
            pers64_b8 = Number(pers53_b8/pers53_b11).toFixed(3),
            pers64_b9 = Number(pers53_b9/pers53_b11).toFixed(3),
            pers64_b10 = Number(pers53_b10/pers53_b11).toFixed(3),
            pers64_b11 = Number(pers53_b11/pers53_b11).toFixed(3);

            //persamaan 65 (28A) => mengeliminasi b11 dari persamaan (54) 
            const pers65_Y = Number(pers54_Y/pers54_b11).toFixed(3),
            pers65_b1 = Number(pers54_b1/pers54_b11).toFixed(3),
            pers65_b2 = Number(pers54_b2/pers54_b11).toFixed(3),
            pers65_b3 = Number(pers54_b3/pers54_b11).toFixed(3),
            pers65_b4 = Number(pers54_b4/pers54_b11).toFixed(3),
            pers65_b5 = Number(pers54_b5/pers54_b11).toFixed(3),
            pers65_b6 = Number(pers54_b6/pers54_b11).toFixed(3),
            pers65_b7 = Number(pers54_b7/pers54_b11).toFixed(3),
            pers65_b8 = Number(pers54_b8/pers54_b11).toFixed(3),
            pers65_b9 = Number(pers54_b9/pers54_b11).toFixed(3),
            pers65_b10 = Number(pers54_b10/pers54_b11).toFixed(3),
            pers65_b11 = Number(pers54_b11/pers54_b11).toFixed(3);

            //persamaan 66 (29A) => mengeliminasi b11 dari persamaan (55) 
            const pers66_Y = Number(pers55_Y/pers55_b11).toFixed(3),
            pers66_b1 = Number(pers55_b1/pers55_b11).toFixed(3),
            pers66_b2 = Number(pers55_b2/pers55_b11).toFixed(3),
            pers66_b3 = Number(pers55_b3/pers55_b11).toFixed(3),
            pers66_b4 = Number(pers55_b4/pers55_b11).toFixed(3),
            pers66_b5 = Number(pers55_b5/pers55_b11).toFixed(3),
            pers66_b6 = Number(pers55_b6/pers55_b11).toFixed(3),
            pers66_b7 = Number(pers55_b7/pers55_b11).toFixed(3),
            pers66_b8 = Number(pers55_b8/pers55_b11).toFixed(3),
            pers66_b9 = Number(pers55_b9/pers55_b11).toFixed(3),
            pers66_b10 = Number(pers55_b10/pers55_b11).toFixed(3),
            pers66_b11 = Number(pers55_b11/pers55_b11).toFixed(3);

            //persamaan 67 (30A) => mengeliminasi b11 dari persamaan (56) 
            const pers67_Y = Number(pers56_Y/pers56_b11).toFixed(3),
            pers67_b1 = Number(pers56_b1/pers56_b11).toFixed(3),
            pers67_b2 = Number(pers56_b2/pers56_b11).toFixed(3),
            pers67_b3 = Number(pers56_b3/pers56_b11).toFixed(3),
            pers67_b4 = Number(pers56_b4/pers56_b11).toFixed(3),
            pers67_b5 = Number(pers56_b5/pers56_b11).toFixed(3),
            pers67_b6 = Number(pers56_b6/pers56_b11).toFixed(3),
            pers67_b7 = Number(pers56_b7/pers56_b11).toFixed(3),
            pers67_b8 = Number(pers56_b8/pers56_b11).toFixed(3),
            pers67_b9 = Number(pers56_b9/pers56_b11).toFixed(3),
            pers67_b10 = Number(pers56_b10/pers56_b11).toFixed(3),
            pers67_b11 = Number(pers56_b11/pers56_b11).toFixed(3);

            //persamaan 68 (31A) => mengeliminasi b11 dari persamaan (57) 
            const pers68_Y = Number(pers57_Y/pers57_b11).toFixed(3),
            pers68_b1 = Number(pers57_b1/pers57_b11).toFixed(3),
            pers68_b2 = Number(pers57_b2/pers57_b11).toFixed(3),
            pers68_b3 = Number(pers57_b3/pers57_b11).toFixed(3),
            pers68_b4 = Number(pers57_b4/pers57_b11).toFixed(3),
            pers68_b5 = Number(pers57_b5/pers57_b11).toFixed(3),
            pers68_b6 = Number(pers57_b6/pers57_b11).toFixed(3),
            pers68_b7 = Number(pers57_b7/pers57_b11).toFixed(3),
            pers68_b8 = Number(pers57_b8/pers57_b11).toFixed(3),
            pers68_b9 = Number(pers57_b9/pers57_b11).toFixed(3),
            pers68_b10 = Number(pers57_b10/pers57_b11).toFixed(3),
            pers68_b11 = Number(pers57_b11/pers57_b11).toFixed(3);

            //persamaan 69 (32A) => mengeliminasi b11 dari persamaan (58) 
            const pers69_Y = Number(pers58_Y/pers58_b11).toFixed(3),
            pers69_b1 = Number(pers58_b1/pers58_b11).toFixed(3),
            pers69_b2 = Number(pers58_b2/pers58_b11).toFixed(3),
            pers69_b3 = Number(pers58_b3/pers58_b11).toFixed(3),
            pers69_b4 = Number(pers58_b4/pers58_b11).toFixed(3),
            pers69_b5 = Number(pers58_b5/pers58_b11).toFixed(3),
            pers69_b6 = Number(pers58_b6/pers58_b11).toFixed(3),
            pers69_b7 = Number(pers58_b7/pers58_b11).toFixed(3),
            pers69_b8 = Number(pers58_b8/pers58_b11).toFixed(3),
            pers69_b9 = Number(pers58_b9/pers58_b11).toFixed(3),
            pers69_b10 = Number(pers58_b10/pers58_b11).toFixed(3),
            pers69_b11 = Number(pers58_b11/pers58_b11).toFixed(3);

            //persamaan 70 (33A) => mengeliminasi b11 dari persamaan (59) 
            const pers70_Y = Number(pers59_Y/pers59_b11).toFixed(3),
            pers70_b1 = Number(pers59_b1/pers59_b11).toFixed(3),
            pers70_b2 = Number(pers59_b2/pers59_b11).toFixed(3),
            pers70_b3 = Number(pers59_b3/pers59_b11).toFixed(3),
            pers70_b4 = Number(pers59_b4/pers59_b11).toFixed(3),
            pers70_b5 = Number(pers59_b5/pers59_b11).toFixed(3),
            pers70_b6 = Number(pers59_b6/pers59_b11).toFixed(3),
            pers70_b7 = Number(pers59_b7/pers59_b11).toFixed(3),
            pers70_b8 = Number(pers59_b8/pers59_b11).toFixed(3),
            pers70_b9 = Number(pers59_b9/pers59_b11).toFixed(3),
            pers70_b10 = Number(pers59_b10/pers59_b11).toFixed(3),
            pers70_b11 = Number(pers59_b11/pers59_b11).toFixed(3); //hasilnya 1

            //persamaan 71 (34A) => mengeliminasi b11 dari persamaan (60) 
            const pers71_Y = Number(pers60_Y/pers60_b11).toFixed(3),
            pers71_b1 = Number(pers60_b1/pers60_b11).toFixed(3),
            pers71_b2 = Number(pers60_b2/pers60_b11).toFixed(3),
            pers71_b3 = Number(pers60_b3/pers60_b11).toFixed(3),
            pers71_b4 = Number(pers60_b4/pers60_b11).toFixed(3),
            pers71_b5 = Number(pers60_b5/pers60_b11).toFixed(3),
            pers71_b6 = Number(pers60_b6/pers60_b11).toFixed(3),
            pers71_b7 = Number(pers60_b7/pers60_b11).toFixed(3),
            pers71_b8 = Number(pers60_b8/pers60_b11).toFixed(3),
            pers71_b9 = Number(pers60_b9/pers60_b11).toFixed(3),
            pers71_b10 = Number(pers60_b10/pers60_b11).toFixed(3),
            pers71_b11 = Number(pers60_b11/pers60_b11).toFixed(3); //hasilnya 1

            //persamaan 72 (35A) => mengeliminasi b11 dari persamaan (61) 
            const pers72_Y = Number(pers61_Y/pers61_b11).toFixed(3),
            pers72_b1 = Number(pers61_b1/pers61_b11).toFixed(3),
            pers72_b2 = Number(pers61_b2/pers61_b11).toFixed(3),
            pers72_b3 = Number(pers61_b3/pers61_b11).toFixed(3),
            pers72_b4 = Number(pers61_b4/pers61_b11).toFixed(3),
            pers72_b5 = Number(pers61_b5/pers61_b11).toFixed(3),
            pers72_b6 = Number(pers61_b6/pers61_b11).toFixed(3),
            pers72_b7 = Number(pers61_b7/pers61_b11).toFixed(3),
            pers72_b8 = Number(pers61_b8/pers61_b11).toFixed(3),
            pers72_b9 = Number(pers61_b9/pers61_b11).toFixed(3),
            pers72_b10 = Number(pers61_b10/pers61_b11).toFixed(3),
            pers72_b11 = Number(pers61_b11/pers61_b11).toFixed(3); //hasilnya 1

            //persamaan 73 (36A) => mengeliminasi b11 dari persamaan (62) 
            const pers73_Y = Number(pers62_Y/pers62_b11).toFixed(3),
            pers73_b1 = Number(pers62_b1/pers62_b11).toFixed(3),
            pers73_b2 = Number(pers62_b2/pers62_b11).toFixed(3),
            pers73_b3 = Number(pers62_b3/pers62_b11).toFixed(3),
            pers73_b4 = Number(pers62_b4/pers62_b11).toFixed(3),
            pers73_b5 = Number(pers62_b5/pers62_b11).toFixed(3),
            pers73_b6 = Number(pers62_b6/pers62_b11).toFixed(3),
            pers73_b7 = Number(pers62_b7/pers62_b11).toFixed(3),
            pers73_b8 = Number(pers62_b8/pers62_b11).toFixed(3),
            pers73_b9 = Number(pers62_b9/pers62_b11).toFixed(3),
            pers73_b10 = Number(pers62_b10/pers62_b11).toFixed(3),
            pers73_b11 = Number(pers62_b11/pers62_b11).toFixed(3); //hasilnya 1

            //persamaan 74 (37) => mengeliminasi persamaan (63) dengan (64) <Menghilangkan B11>//////////
            const pers74_Y = Number(pers63_Y-pers64_Y).toFixed(3),
            pers74_b1 = Number(pers63_b1-pers64_b1).toFixed(3),
            pers74_b2 = Number(pers63_b2-pers64_b2).toFixed(3),
            pers74_b3 = Number(pers63_b3-pers64_b3).toFixed(3),
            pers74_b4 = Number(pers63_b4-pers64_b4).toFixed(3),
            pers74_b5 = Number(pers63_b5-pers64_b5).toFixed(3),
            pers74_b6 = Number(pers63_b6-pers64_b6).toFixed(3),
            pers74_b7 = Number(pers63_b7-pers64_b7).toFixed(3),
            pers74_b8 = Number(pers63_b8-pers64_b8).toFixed(3),
            pers74_b9 = Number(pers63_b9-pers64_b9).toFixed(3),
            pers74_b10 = Number(pers63_b10-pers64_b10).toFixed(3),
            pers74_b11 = (pers63_b11-pers64_b11);       
            
            //persamaan 75 (38) => mengeliminasi persamaan (64) dengan (65) <Menghilangkan B11>
            const pers75_Y = Number(pers64_Y-pers65_Y).toFixed(3),
            pers75_b1 = Number(pers64_b1-pers65_b1).toFixed(3),
            pers75_b2 = Number(pers64_b2-pers65_b2).toFixed(3),
            pers75_b3 = Number(pers64_b3-pers65_b3).toFixed(3),
            pers75_b4 = Number(pers64_b4-pers65_b4).toFixed(3),
            pers75_b5 = Number(pers64_b5-pers65_b5).toFixed(3),
            pers75_b6 = Number(pers64_b6-pers65_b6).toFixed(3),
            pers75_b7 = Number(pers64_b7-pers65_b7).toFixed(3),
            pers75_b8 = Number(pers64_b8-pers65_b8).toFixed(3),
            pers75_b9 = Number(pers64_b9-pers65_b9).toFixed(3),
            pers75_b10 = Number(pers64_b10-pers65_b10).toFixed(3),
            pers75_b11 = (pers64_b11-pers65_b11);

            //persamaan 76 (39) => mengeliminasi persamaan (65) dengan (66) <Menghilangkan B11>
            const pers76_Y = Number(pers65_Y-pers66_Y).toFixed(3),
            pers76_b1 = Number(pers65_b1-pers66_b1).toFixed(3),
            pers76_b2 = Number(pers65_b2-pers66_b2).toFixed(3),
            pers76_b3 = Number(pers65_b3-pers66_b3).toFixed(3),
            pers76_b4 = Number(pers65_b4-pers66_b4).toFixed(3),
            pers76_b5 = Number(pers65_b5-pers66_b5).toFixed(3),
            pers76_b6 = Number(pers65_b6-pers66_b6).toFixed(3),
            pers76_b7 = Number(pers65_b7-pers66_b7).toFixed(3),
            pers76_b8 = Number(pers65_b8-pers66_b8).toFixed(3),
            pers76_b9 = Number(pers65_b9-pers66_b9).toFixed(3),
            pers76_b10 = Number(pers65_b10-pers66_b10).toFixed(3),
            pers76_b11 = (pers65_b11-pers66_b11);

            //persamaan 77 (40) => mengeliminasi persamaan (66) dengan (67) <Menghilangkan B11>
            const pers77_Y = Number(pers66_Y-pers67_Y).toFixed(3),
            pers77_b1 = Number(pers66_b1-pers67_b1).toFixed(3),
            pers77_b2 = Number(pers66_b2-pers67_b2).toFixed(3),
            pers77_b3 = Number(pers66_b3-pers67_b3).toFixed(3),
            pers77_b4 = Number(pers66_b4-pers67_b4).toFixed(3),
            pers77_b5 = Number(pers66_b5-pers67_b5).toFixed(3),
            pers77_b6 = Number(pers66_b6-pers67_b6).toFixed(3),
            pers77_b7 = Number(pers66_b7-pers67_b7).toFixed(3),
            pers77_b8 = Number(pers66_b8-pers67_b8).toFixed(3),
            pers77_b9 = Number(pers66_b9-pers67_b9).toFixed(3),
            pers77_b10 = Number(pers66_b10-pers67_b10).toFixed(3),
            pers77_b11 = (pers66_b11-pers67_b11);

            //persamaan 78 (41) => mengeliminasi persamaan (67) dengan (68) <Menghilangkan B11>
            const pers78_Y = Number(pers67_Y-pers68_Y).toFixed(3),
            pers78_b1 = Number(pers67_b1-pers68_b1).toFixed(3),
            pers78_b2 = Number(pers67_b2-pers68_b2).toFixed(3),
            pers78_b3 = Number(pers67_b3-pers68_b3).toFixed(3),
            pers78_b4 = Number(pers67_b4-pers68_b4).toFixed(3),
            pers78_b5 = Number(pers67_b5-pers68_b5).toFixed(3),
            pers78_b6 = Number(pers67_b6-pers68_b6).toFixed(3),
            pers78_b7 = Number(pers67_b7-pers68_b7).toFixed(3),
            pers78_b8 = Number(pers67_b8-pers68_b8).toFixed(3),
            pers78_b9 = Number(pers67_b9-pers68_b9).toFixed(3),
            pers78_b10 = Number(pers67_b10-pers68_b10).toFixed(3),
            pers78_b11 = (pers67_b11-pers68_b11);

            //persamaan 79 (42) => mengeliminasi persamaan (68) dengan (69) <Menghilangkan B11>
            const pers79_Y = Number(pers68_Y-pers69_Y).toFixed(3),
            pers79_b1 = Number(pers68_b1-pers69_b1).toFixed(3),
            pers79_b2 = Number(pers68_b2-pers69_b2).toFixed(3),
            pers79_b3 = Number(pers68_b3-pers69_b3).toFixed(3),
            pers79_b4 = Number(pers68_b4-pers69_b4).toFixed(3),
            pers79_b5 = Number(pers68_b5-pers69_b5).toFixed(3),
            pers79_b6 = Number(pers68_b6-pers69_b6).toFixed(3),
            pers79_b7 = Number(pers68_b7-pers69_b7).toFixed(3),
            pers79_b8 = Number(pers68_b8-pers69_b8).toFixed(3),
            pers79_b9 = Number(pers68_b9-pers69_b9).toFixed(3),
            pers79_b10 = Number(pers68_b10-pers69_b10).toFixed(3),
            pers79_b11 = (pers68_b11-pers69_b11);

            //persamaan 80 (43) => mengeliminasi persamaan (69) dengan (70) <Menghilangkan B11>
            const pers80_Y = Number(pers69_Y-pers70_Y).toFixed(3),
            pers80_b1 = Number(pers69_b1-pers70_b1).toFixed(3),
            pers80_b2 = Number(pers69_b2-pers70_b2).toFixed(3),
            pers80_b3 = Number(pers69_b3-pers70_b3).toFixed(3),
            pers80_b4 = Number(pers69_b4-pers70_b4).toFixed(3),
            pers80_b5 = Number(pers69_b5-pers70_b5).toFixed(3),
            pers80_b6 = Number(pers69_b6-pers70_b6).toFixed(3),
            pers80_b7 = Number(pers69_b7-pers70_b7).toFixed(3),
            pers80_b8 = Number(pers69_b8-pers70_b8).toFixed(3),
            pers80_b9 = Number(pers69_b9-pers70_b9).toFixed(3),
            pers80_b10 = Number(pers69_b10-pers70_b10).toFixed(3),
            pers80_b11 = (pers69_b11-pers70_b11);

            //persamaan 81 (44) => mengeliminasi persamaan (70) dengan (71) <Menghilangkan B11>
            const pers81_Y = Number(pers70_Y-pers71_Y).toFixed(3),
            pers81_b1 = Number(pers70_b1-pers71_b1).toFixed(3),
            pers81_b2 = Number(pers70_b2-pers71_b2).toFixed(3),
            pers81_b3 = Number(pers70_b3-pers71_b3).toFixed(3),
            pers81_b4 = Number(pers70_b4-pers71_b4).toFixed(3),
            pers81_b5 = Number(pers70_b5-pers71_b5).toFixed(3),
            pers81_b6 = Number(pers70_b6-pers71_b6).toFixed(3),
            pers81_b7 = Number(pers70_b7-pers71_b7).toFixed(3),
            pers81_b8 = Number(pers70_b8-pers71_b8).toFixed(3),
            pers81_b9 = Number(pers70_b9-pers71_b9).toFixed(3),
            pers81_b10 = Number(pers70_b10-pers71_b10).toFixed(3),
            pers81_b11 = (pers70_b11-pers71_b11);

            //persamaan 82 (45) => mengeliminasi persamaan (71) dengan (72) <Menghilangkan B11>
            const pers82_Y = Number(pers71_Y-pers72_Y).toFixed(3),
            pers82_b1 = Number(pers71_b1-pers72_b1).toFixed(3),
            pers82_b2 = Number(pers71_b2-pers72_b2).toFixed(3),
            pers82_b3 = Number(pers71_b3-pers72_b3).toFixed(3),
            pers82_b4 = Number(pers71_b4-pers72_b4).toFixed(3),
            pers82_b5 = Number(pers71_b5-pers72_b5).toFixed(3),
            pers82_b6 = Number(pers71_b6-pers72_b6).toFixed(3),
            pers82_b7 = Number(pers71_b7-pers72_b7).toFixed(3),
            pers82_b8 = Number(pers71_b8-pers72_b8).toFixed(3),
            pers82_b9 = Number(pers71_b9-pers72_b9).toFixed(3),
            pers82_b10 = Number(pers71_b10-pers72_b10).toFixed(3),
            pers82_b11 = (pers71_b11-pers72_b11);

            //persamaan 83 (46) => mengeliminasi persamaan (72) dengan (73) <Menghilangkan B11>
            const pers83_Y = Number(pers72_Y-pers73_Y).toFixed(3),
            pers83_b1 = Number(pers72_b1-pers73_b1).toFixed(3),
            pers83_b2 = Number(pers72_b2-pers73_b2).toFixed(3),
            pers83_b3 = Number(pers72_b3-pers73_b3).toFixed(3),
            pers83_b4 = Number(pers72_b4-pers73_b4).toFixed(3),
            pers83_b5 = Number(pers72_b5-pers73_b5).toFixed(3),
            pers83_b6 = Number(pers72_b6-pers73_b6).toFixed(3),
            pers83_b7 = Number(pers72_b7-pers73_b7).toFixed(3),
            pers83_b8 = Number(pers72_b8-pers73_b8).toFixed(3),
            pers83_b9 = Number(pers72_b9-pers73_b9).toFixed(3),
            pers83_b10 = Number(pers72_b10-pers73_b10).toFixed(3),
            pers83_b11 = (pers72_b11-pers73_b11);

            //persamaan 84 (37A) => mengeliminasi b10 dari persamaan (74) ///////////////////////////////
            const pers84_Y = Number(pers74_Y/pers74_b10).toFixed(3),
            pers84_b1 = Number(pers74_b1/pers74_b10).toFixed(3),
            pers84_b2 = Number(pers74_b2/pers74_b10).toFixed(3),
            pers84_b3 = Number(pers74_b3/pers74_b10).toFixed(3),
            pers84_b4 = Number(pers74_b4/pers74_b10).toFixed(3),
            pers84_b5 = Number(pers74_b5/pers74_b10).toFixed(3),
            pers84_b6 = Number(pers74_b6/pers74_b10).toFixed(3),
            pers84_b7 = Number(pers74_b7/pers74_b10).toFixed(3),
            pers84_b8 = Number(pers74_b8/pers74_b10).toFixed(3),
            pers84_b9 = Number(pers74_b9/pers74_b10).toFixed(3),
            pers84_b10 = Number(pers74_b10/pers74_b10).toFixed(3); //hasilnya 1 

            //persamaan 85 (38A) => mengeliminasi b10 dari persamaan (75) 
            const pers85_Y = Number(pers75_Y/pers75_b10).toFixed(3),
            pers85_b1 = Number(pers75_b1/pers75_b10).toFixed(3),
            pers85_b2 = Number(pers75_b2/pers75_b10).toFixed(3),
            pers85_b3 = Number(pers75_b3/pers75_b10).toFixed(3),
            pers85_b4 = Number(pers75_b4/pers75_b10).toFixed(3),
            pers85_b5 = Number(pers75_b5/pers75_b10).toFixed(3),
            pers85_b6 = Number(pers75_b6/pers75_b10).toFixed(3),
            pers85_b7 = Number(pers75_b7/pers75_b10).toFixed(3),
            pers85_b8 = Number(pers75_b8/pers75_b10).toFixed(3),
            pers85_b9 = Number(pers75_b9/pers75_b10).toFixed(3),
            pers85_b10 = Number(pers75_b10/pers75_b10).toFixed(3); //hasilnya 1

            //persamaan 86 (39A) => mengeliminasi b10 dari persamaan (76) 
            const pers86_Y = Number(pers76_Y/pers76_b10).toFixed(3),
            pers86_b1 = Number(pers76_b1/pers76_b10).toFixed(3),
            pers86_b2 = Number(pers76_b2/pers76_b10).toFixed(3),
            pers86_b3 = Number(pers76_b3/pers76_b10).toFixed(3),
            pers86_b4 = Number(pers76_b4/pers76_b10).toFixed(3),
            pers86_b5 = Number(pers76_b5/pers76_b10).toFixed(3),
            pers86_b6 = Number(pers76_b6/pers76_b10).toFixed(3),
            pers86_b7 = Number(pers76_b7/pers76_b10).toFixed(3),
            pers86_b8 = Number(pers76_b8/pers76_b10).toFixed(3),
            pers86_b9 = Number(pers76_b9/pers76_b10).toFixed(3),
            pers86_b10 = Number(pers76_b10/pers76_b10).toFixed(3); //hasilnya 1

            //persamaan 87 (40A) => mengeliminasi b10 dari persamaan (77) 
            const pers87_Y = Number(pers77_Y/pers77_b10).toFixed(3),
            pers87_b1 = Number(pers77_b1/pers77_b10).toFixed(3),
            pers87_b2 = Number(pers77_b2/pers77_b10).toFixed(3),
            pers87_b3 = Number(pers77_b3/pers77_b10).toFixed(3),
            pers87_b4 = Number(pers77_b4/pers77_b10).toFixed(3),
            pers87_b5 = Number(pers77_b5/pers77_b10).toFixed(3),
            pers87_b6 = Number(pers77_b6/pers77_b10).toFixed(3),
            pers87_b7 = Number(pers77_b7/pers77_b10).toFixed(3),
            pers87_b8 = Number(pers77_b8/pers77_b10).toFixed(3),
            pers87_b9 = Number(pers77_b9/pers77_b10).toFixed(3),
            pers87_b10 = Number(pers77_b10/pers77_b10).toFixed(3); //hasilnya 1

            //persamaan 88 (41A) => mengeliminasi b10 dari persamaan (78) 
            const pers88_Y = Number(pers78_Y/pers78_b10).toFixed(3),
            pers88_b1 = Number(pers78_b1/pers78_b10).toFixed(3),
            pers88_b2 = Number(pers78_b2/pers78_b10).toFixed(3),
            pers88_b3 = Number(pers78_b3/pers78_b10).toFixed(3),
            pers88_b4 = Number(pers78_b4/pers78_b10).toFixed(3),
            pers88_b5 = Number(pers78_b5/pers78_b10).toFixed(3),
            pers88_b6 = Number(pers78_b6/pers78_b10).toFixed(3),
            pers88_b7 = Number(pers78_b7/pers78_b10).toFixed(3),
            pers88_b8 = Number(pers78_b8/pers78_b10).toFixed(3),
            pers88_b9 = Number(pers78_b9/pers78_b10).toFixed(3),
            pers88_b10 = Number(pers78_b10/pers78_b10).toFixed(3); //hasilnya 1

            //persamaan 89 (42A) => mengeliminasi b10 dari persamaan (79) 
            const pers89_Y = Number(pers79_Y/pers79_b10).toFixed(3),
            pers89_b1 = Number(pers79_b1/pers79_b10).toFixed(3),
            pers89_b2 = Number(pers79_b2/pers79_b10).toFixed(3),
            pers89_b3 = Number(pers79_b3/pers79_b10).toFixed(3),
            pers89_b4 = Number(pers79_b4/pers79_b10).toFixed(3),
            pers89_b5 = Number(pers79_b5/pers79_b10).toFixed(3),
            pers89_b6 = Number(pers79_b6/pers79_b10).toFixed(3),
            pers89_b7 = Number(pers79_b7/pers79_b10).toFixed(3),
            pers89_b8 = Number(pers79_b8/pers79_b10).toFixed(3),
            pers89_b9 = Number(pers79_b9/pers79_b10).toFixed(3),
            pers89_b10 = Number(pers79_b10/pers79_b10).toFixed(3); //hasilnya 1

            //persamaan 90 (43A) => mengeliminasi b10 dari persamaan (80) 
            const pers90_Y = Number(pers80_Y/pers80_b10).toFixed(3),
            pers90_b1 = Number(pers80_b1/pers80_b10).toFixed(3),
            pers90_b2 = Number(pers80_b2/pers80_b10).toFixed(3),
            pers90_b3 = Number(pers80_b3/pers80_b10).toFixed(3),
            pers90_b4 = Number(pers80_b4/pers80_b10).toFixed(3),
            pers90_b5 = Number(pers80_b5/pers80_b10).toFixed(3),
            pers90_b6 = Number(pers80_b6/pers80_b10).toFixed(3),
            pers90_b7 = Number(pers80_b7/pers80_b10).toFixed(3),
            pers90_b8 = Number(pers80_b8/pers80_b10).toFixed(3),
            pers90_b9 = Number(pers80_b9/pers80_b10).toFixed(3),
            pers90_b10 = Number(pers80_b10/pers80_b10).toFixed(3); //hasilnya 1

            //persamaan 91 (44A) => mengeliminasi b10 dari persamaan (81) 
            const pers91_Y = Number(pers81_Y/pers81_b10).toFixed(3),
            pers91_b1 = Number(pers81_b1/pers81_b10).toFixed(3),
            pers91_b2 = Number(pers81_b2/pers81_b10).toFixed(3),
            pers91_b3 = Number(pers81_b3/pers81_b10).toFixed(3),
            pers91_b4 = Number(pers81_b4/pers81_b10).toFixed(3),
            pers91_b5 = Number(pers81_b5/pers81_b10).toFixed(3),
            pers91_b6 = Number(pers81_b6/pers81_b10).toFixed(3),
            pers91_b7 = Number(pers81_b7/pers81_b10).toFixed(3),
            pers91_b8 = Number(pers81_b8/pers81_b10).toFixed(3),
            pers91_b9 = Number(pers81_b9/pers81_b10).toFixed(3),
            pers91_b10 = Number(pers81_b10/pers81_b10).toFixed(3); //hasilnya 1

            //persamaan 92 (45A) => mengeliminasi b10 dari persamaan (82) 
            const pers92_Y = Number(pers82_Y/pers82_b10).toFixed(3),
            pers92_b1 = Number(pers82_b1/pers82_b10).toFixed(3),
            pers92_b2 = Number(pers82_b2/pers82_b10).toFixed(3),
            pers92_b3 = Number(pers82_b3/pers82_b10).toFixed(3),
            pers92_b4 = Number(pers82_b4/pers82_b10).toFixed(3),
            pers92_b5 = Number(pers82_b5/pers82_b10).toFixed(3),
            pers92_b6 = Number(pers82_b6/pers82_b10).toFixed(3),
            pers92_b7 = Number(pers82_b7/pers82_b10).toFixed(3),
            pers92_b8 = Number(pers82_b8/pers82_b10).toFixed(3),
            pers92_b9 = Number(pers82_b9/pers82_b10).toFixed(3),
            pers92_b10 = Number(pers82_b10/pers82_b10).toFixed(3); //hasilnya 1

            //persamaan 93 (46A) => mengeliminasi b10 dari persamaan (83) 
            const pers93_Y = Number(pers83_Y/pers83_b10).toFixed(3),
            pers93_b1 = Number(pers83_b1/pers83_b10).toFixed(3),
            pers93_b2 = Number(pers83_b2/pers83_b10).toFixed(3),
            pers93_b3 = Number(pers83_b3/pers83_b10).toFixed(3),
            pers93_b4 = Number(pers83_b4/pers83_b10).toFixed(3),
            pers93_b5 = Number(pers83_b5/pers83_b10).toFixed(3),
            pers93_b6 = Number(pers83_b6/pers83_b10).toFixed(3),
            pers93_b7 = Number(pers83_b7/pers83_b10).toFixed(3),
            pers93_b8 = Number(pers83_b8/pers83_b10).toFixed(3),
            pers93_b9 = Number(pers83_b9/pers83_b10).toFixed(3),
            pers93_b10 = Number(pers83_b10/pers83_b10).toFixed(3); //hasilnya 1
        
            //persamaan 94 (47) => mengeliminasi persamaan (84) dengan (85) <menghilangkan b10> ///////////////////////
            const pers94_Y = Number(pers84_Y-pers85_Y).toFixed(3),
            pers94_b1 = Number(pers84_b1-pers85_b1).toFixed(3),
            pers94_b2 = Number(pers84_b2-pers85_b2).toFixed(3),
            pers94_b3 = Number(pers84_b3-pers85_b3).toFixed(3),
            pers94_b4 = Number(pers84_b4-pers85_b4).toFixed(3),
            pers94_b5 = Number(pers84_b5-pers85_b5).toFixed(3),
            pers94_b6 = Number(pers84_b6-pers85_b6).toFixed(3),
            pers94_b7 = Number(pers84_b7-pers85_b7).toFixed(3),
            pers94_b8 = Number(pers84_b8-pers85_b8).toFixed(3),
            pers94_b9 = Number(pers84_b9-pers85_b9).toFixed(3),
            pers94_b10 = (pers84_b10-pers85_b10);

            //persamaan 95 (48) => mengeliminasi persamaan (85) dengan (86) <menghilangkan b10>
            const pers95_Y = Number(pers85_Y-pers86_Y).toFixed(3),
            pers95_b1 = Number(pers85_b1-pers86_b1).toFixed(3),
            pers95_b2 = Number(pers85_b2-pers86_b2).toFixed(3),
            pers95_b3 = Number(pers85_b3-pers86_b3).toFixed(3),
            pers95_b4 = Number(pers85_b4-pers86_b4).toFixed(3),
            pers95_b5 = Number(pers85_b5-pers86_b5).toFixed(3),
            pers95_b6 = Number(pers85_b6-pers86_b6).toFixed(3),
            pers95_b7 = Number(pers85_b7-pers86_b7).toFixed(3),
            pers95_b8 = Number(pers85_b8-pers86_b8).toFixed(3),
            pers95_b9 = Number(pers85_b9-pers86_b9).toFixed(3),
            pers95_b10 = (pers85_b10-pers86_b10);

            //persamaan 96 (49) => mengeliminasi persamaan (86) dengan (87) <menghilangkan b10>
            const pers96_Y = Number(pers86_Y-pers87_Y).toFixed(3),
            pers96_b1 = Number(pers86_b1-pers87_b1).toFixed(3),
            pers96_b2 = Number(pers86_b2-pers87_b2).toFixed(3),
            pers96_b3 = Number(pers86_b3-pers87_b3).toFixed(3),
            pers96_b4 = Number(pers86_b4-pers87_b4).toFixed(3),
            pers96_b5 = Number(pers86_b5-pers87_b5).toFixed(3),
            pers96_b6 = Number(pers86_b6-pers87_b6).toFixed(3),
            pers96_b7 = Number(pers86_b7-pers87_b7).toFixed(3),
            pers96_b8 = Number(pers86_b8-pers87_b8).toFixed(3),
            pers96_b9 = Number(pers86_b9-pers87_b9).toFixed(3),
            pers96_b10 = (pers86_b10-pers87_b10);

            //persamaan 97 (50) => mengeliminasi persamaan (87) dengan (88) <menghilangkan b10>
            const pers97_Y = Number(pers87_Y-pers88_Y).toFixed(3),
            pers97_b1 = Number(pers87_b1-pers88_b1).toFixed(3),
            pers97_b2 = Number(pers87_b2-pers88_b2).toFixed(3),
            pers97_b3 = Number(pers87_b3-pers88_b3).toFixed(3),
            pers97_b4 = Number(pers87_b4-pers88_b4).toFixed(3),
            pers97_b5 = Number(pers87_b5-pers88_b5).toFixed(3),
            pers97_b6 = Number(pers87_b6-pers88_b6).toFixed(3),
            pers97_b7 = Number(pers87_b7-pers88_b7).toFixed(3),
            pers97_b8 = Number(pers87_b8-pers88_b8).toFixed(3),
            pers97_b9 = Number(pers87_b9-pers88_b9).toFixed(3),
            pers97_b10 = (pers87_b10-pers88_b10);

            //persamaan 98 (51) => mengeliminasi persamaan (88) dengan (89) <menghilangkan b10>
            const pers98_Y = Number(pers88_Y-pers89_Y).toFixed(3),
            pers98_b1 = Number(pers88_b1-pers89_b1).toFixed(3),
            pers98_b2 = Number(pers88_b2-pers89_b2).toFixed(3),
            pers98_b3 = Number(pers88_b3-pers89_b3).toFixed(3),
            pers98_b4 = Number(pers88_b4-pers89_b4).toFixed(3),
            pers98_b5 = Number(pers88_b5-pers89_b5).toFixed(3),
            pers98_b6 = Number(pers88_b6-pers89_b6).toFixed(3),
            pers98_b7 = Number(pers88_b7-pers89_b7).toFixed(3),
            pers98_b8 = Number(pers88_b8-pers89_b8).toFixed(3),
            pers98_b9 = Number(pers88_b9-pers89_b9).toFixed(3),
            pers98_b10 = (pers88_b10-pers89_b10);

            //persamaan 99 (52) => mengeliminasi persamaan (89) dengan (90) <menghilangkan b10>
            const pers99_Y = Number(pers89_Y-pers90_Y).toFixed(3),
            pers99_b1 = Number(pers89_b1-pers90_b1).toFixed(3),
            pers99_b2 = Number(pers89_b2-pers90_b2).toFixed(3),
            pers99_b3 = Number(pers89_b3-pers90_b3).toFixed(3),
            pers99_b4 = Number(pers89_b4-pers90_b4).toFixed(3),
            pers99_b5 = Number(pers89_b5-pers90_b5).toFixed(3),
            pers99_b6 = Number(pers89_b6-pers90_b6).toFixed(3),
            pers99_b7 = Number(pers89_b7-pers90_b7).toFixed(3),
            pers99_b8 = Number(pers89_b8-pers90_b8).toFixed(3),
            pers99_b9 = Number(pers89_b9-pers90_b9).toFixed(3),
            pers99_b10 = (pers89_b10-pers90_b10);

            //persamaan 100 (53) => mengeliminasi persamaan (90) dengan (91) <menghilangkan b10>
            const pers100_Y = Number(pers90_Y-pers91_Y).toFixed(3),
            pers100_b1 = Number(pers90_b1-pers91_b1).toFixed(3),
            pers100_b2 = Number(pers90_b2-pers91_b2).toFixed(3),
            pers100_b3 = Number(pers90_b3-pers91_b3).toFixed(3),
            pers100_b4 = Number(pers90_b4-pers91_b4).toFixed(3),
            pers100_b5 = Number(pers90_b5-pers91_b5).toFixed(3),
            pers100_b6 = Number(pers90_b6-pers91_b6).toFixed(3),
            pers100_b7 = Number(pers90_b7-pers91_b7).toFixed(3),
            pers100_b8 = Number(pers90_b8-pers91_b8).toFixed(3),
            pers100_b9 = Number(pers90_b9-pers91_b9).toFixed(3),
            pers100_b10 = (pers90_b10-pers91_b10);

            //persamaan 101 (54) => mengeliminasi persamaan (91) dengan (92) <menghilangkan b10>
            const pers101_Y = Number(pers91_Y-pers92_Y).toFixed(3),
            pers101_b1 = Number(pers91_b1-pers92_b1).toFixed(3),
            pers101_b2 = Number(pers91_b2-pers92_b2).toFixed(3),
            pers101_b3 = Number(pers91_b3-pers92_b3).toFixed(3),
            pers101_b4 = Number(pers91_b4-pers92_b4).toFixed(3),
            pers101_b5 = Number(pers91_b5-pers92_b5).toFixed(3),
            pers101_b6 = Number(pers91_b6-pers92_b6).toFixed(3),
            pers101_b7 = Number(pers91_b7-pers92_b7).toFixed(3),
            pers101_b8 = Number(pers91_b8-pers92_b8).toFixed(3),
            pers101_b9 = Number(pers91_b9-pers92_b9).toFixed(3),
            pers101_b10 = (pers91_b10-pers92_b10);

            //persamaan 102 (55) => mengeliminasi persamaan (92) dengan (93) <menghilangkan b10>
            const pers102_Y = Number(pers92_Y-pers93_Y).toFixed(3),
            pers102_b1 = Number(pers92_b1-pers93_b1).toFixed(3),
            pers102_b2 = Number(pers92_b2-pers93_b2).toFixed(3),
            pers102_b3 = Number(pers92_b3-pers93_b3).toFixed(3),
            pers102_b4 = Number(pers92_b4-pers93_b4).toFixed(3),
            pers102_b5 = Number(pers92_b5-pers93_b5).toFixed(3),
            pers102_b6 = Number(pers92_b6-pers93_b6).toFixed(3),
            pers102_b7 = Number(pers92_b7-pers93_b7).toFixed(3),
            pers102_b8 = Number(pers92_b8-pers93_b8).toFixed(3),
            pers102_b9 = Number(pers92_b9-pers93_b9).toFixed(3),
            pers102_b10 = (pers92_b10-pers93_b10);

            //persamaan 103 (47A) => mengeliminasi b9 dari persamaan (94) /////////////////////////////
            const pers103_Y = Number(pers94_Y/pers94_b9).toFixed(3),
            pers103_b1 = Number(pers94_b1/pers94_b9).toFixed(3),
            pers103_b2 = Number(pers94_b2/pers94_b9).toFixed(3),
            pers103_b3 = Number(pers94_b3/pers94_b9).toFixed(3),
            pers103_b4 = Number(pers94_b4/pers94_b9).toFixed(3),
            pers103_b5 = Number(pers94_b5/pers94_b9).toFixed(3),
            pers103_b6 = Number(pers94_b6/pers94_b9).toFixed(3),
            pers103_b7 = Number(pers94_b7/pers94_b9).toFixed(3),
            pers103_b8 = Number(pers94_b8/pers94_b9).toFixed(3),
            pers103_b9 = Number(pers94_b9/pers94_b9).toFixed(3); //hasilnya 1 

            //persamaan 104 (48A) => mengeliminasi b9 dari persamaan (95)
            const pers104_Y = Number(pers95_Y/pers95_b9).toFixed(3),
            pers104_b1 = Number(pers95_b1/pers95_b9).toFixed(3),
            pers104_b2 = Number(pers95_b2/pers95_b9).toFixed(3),
            pers104_b3 = Number(pers95_b3/pers95_b9).toFixed(3),
            pers104_b4 = Number(pers95_b4/pers95_b9).toFixed(3),
            pers104_b5 = Number(pers95_b5/pers95_b9).toFixed(3),
            pers104_b6 = Number(pers95_b6/pers95_b9).toFixed(3),
            pers104_b7 = Number(pers95_b7/pers95_b9).toFixed(3),
            pers104_b8 = Number(pers95_b8/pers95_b9).toFixed(3),
            pers104_b9 = Number(pers95_b9/pers95_b9).toFixed(3); //hasilnya 1
            
            //persamaan 105 (49A) => mengeliminasi b9 dari persamaan (96)
            const pers105_Y = Number(pers96_Y/pers96_b9).toFixed(3),
            pers105_b1 = Number(pers96_b1/pers96_b9).toFixed(3),
            pers105_b2 = Number(pers96_b2/pers96_b9).toFixed(3),
            pers105_b3 = Number(pers96_b3/pers96_b9).toFixed(3),
            pers105_b4 = Number(pers96_b4/pers96_b9).toFixed(3),
            pers105_b5 = Number(pers96_b5/pers96_b9).toFixed(3),
            pers105_b6 = Number(pers96_b6/pers96_b9).toFixed(3),
            pers105_b7 = Number(pers96_b7/pers96_b9).toFixed(3),
            pers105_b8 = Number(pers96_b8/pers96_b9).toFixed(3),
            pers105_b9 = Number(pers96_b9/pers96_b9).toFixed(3); //hasilnya 1
            
            //persamaan 106 (50A) => mengeliminasi b9 dari persamaan (97)
            const pers106_Y = Number(pers97_Y/pers97_b9).toFixed(3),
            pers106_b1 = Number(pers97_b1/pers97_b9).toFixed(3),
            pers106_b2 = Number(pers97_b2/pers97_b9).toFixed(3),
            pers106_b3 = Number(pers97_b3/pers97_b9).toFixed(3),
            pers106_b4 = Number(pers97_b4/pers97_b9).toFixed(3),
            pers106_b5 = Number(pers97_b5/pers97_b9).toFixed(3),
            pers106_b6 = Number(pers97_b6/pers97_b9).toFixed(3),
            pers106_b7 = Number(pers97_b7/pers97_b9).toFixed(3),
            pers106_b8 = Number(pers97_b8/pers97_b9).toFixed(3),
            pers106_b9 = Number(pers97_b9/pers97_b9).toFixed(3); //hasilnya 1
            
            //persamaan 107 (51A) => mengeliminasi b9 dari persamaan (98)
            const pers107_Y = Number(pers98_Y/pers98_b9).toFixed(3),
            pers107_b1 = Number(pers98_b1/pers98_b9).toFixed(3),
            pers107_b2 = Number(pers98_b2/pers98_b9).toFixed(3),
            pers107_b3 = Number(pers98_b3/pers98_b9).toFixed(3),
            pers107_b4 = Number(pers98_b4/pers98_b9).toFixed(3),
            pers107_b5 = Number(pers98_b5/pers98_b9).toFixed(3),
            pers107_b6 = Number(pers98_b6/pers98_b9).toFixed(3),
            pers107_b7 = Number(pers98_b7/pers98_b9).toFixed(3),
            pers107_b8 = Number(pers98_b8/pers98_b9).toFixed(3),
            pers107_b9 = Number(pers98_b9/pers98_b9).toFixed(3); //hasilnya 1
            
            //persamaan 108 (52A) => mengeliminasi b9 dari persamaan (99)
            const pers108_Y = Number(pers99_Y/pers99_b9).toFixed(3),
            pers108_b1 = Number(pers99_b1/pers99_b9).toFixed(3),
            pers108_b2 = Number(pers99_b2/pers99_b9).toFixed(3),
            pers108_b3 = Number(pers99_b3/pers99_b9).toFixed(3),
            pers108_b4 = Number(pers99_b4/pers99_b9).toFixed(3),
            pers108_b5 = Number(pers99_b5/pers99_b9).toFixed(3),
            pers108_b6 = Number(pers99_b6/pers99_b9).toFixed(3),
            pers108_b7 = Number(pers99_b7/pers99_b9).toFixed(3),
            pers108_b8 = Number(pers99_b8/pers99_b9).toFixed(3),
            pers108_b9 = Number(pers99_b9/pers99_b9).toFixed(3); //hasilnya 1
            
            //persamaan 109 (53A) => mengeliminasi b9 dari persamaan (100)
            const pers109_Y = Number(pers100_Y/pers100_b9).toFixed(3),
            pers109_b1 = Number(pers100_b1/pers100_b9).toFixed(3),
            pers109_b2 = Number(pers100_b2/pers100_b9).toFixed(3),
            pers109_b3 = Number(pers100_b3/pers100_b9).toFixed(3),
            pers109_b4 = Number(pers100_b4/pers100_b9).toFixed(3),
            pers109_b5 = Number(pers100_b5/pers100_b9).toFixed(3),
            pers109_b6 = Number(pers100_b6/pers100_b9).toFixed(3),
            pers109_b7 = Number(pers100_b7/pers100_b9).toFixed(3),
            pers109_b8 = Number(pers100_b8/pers100_b9).toFixed(3),
            pers109_b9 = Number(pers100_b9/pers100_b9).toFixed(3); //hasilnya 1
            
            //persamaan 110 (54A) => mengeliminasi b9 dari persamaan (101)
            const pers110_Y = Number(pers101_Y/pers101_b9).toFixed(3),
            pers110_b1 = Number(pers101_b1/pers101_b9).toFixed(3),
            pers110_b2 = Number(pers101_b2/pers101_b9).toFixed(3),
            pers110_b3 = Number(pers101_b3/pers101_b9).toFixed(3),
            pers110_b4 = Number(pers101_b4/pers101_b9).toFixed(3),
            pers110_b5 = Number(pers101_b5/pers101_b9).toFixed(3),
            pers110_b6 = Number(pers101_b6/pers101_b9).toFixed(3),
            pers110_b7 = Number(pers101_b7/pers101_b9).toFixed(3),
            pers110_b8 = Number(pers101_b8/pers101_b9).toFixed(3),
            pers110_b9 = Number(pers101_b9/pers101_b9).toFixed(3); //hasilnya 1
            
            //persamaan 111 (55A) => mengeliminasi b9 dari persamaan (102)
            const pers111_Y = Number(pers102_Y/pers102_b9).toFixed(3),
            pers111_b1 = Number(pers102_b1/pers102_b9).toFixed(3),
            pers111_b2 = Number(pers102_b2/pers102_b9).toFixed(3),
            pers111_b3 = Number(pers102_b3/pers102_b9).toFixed(3),
            pers111_b4 = Number(pers102_b4/pers102_b9).toFixed(3),
            pers111_b5 = Number(pers102_b5/pers102_b9).toFixed(3),
            pers111_b6 = Number(pers102_b6/pers102_b9).toFixed(3),
            pers111_b7 = Number(pers102_b7/pers102_b9).toFixed(3),
            pers111_b8 = Number(pers102_b8/pers102_b9).toFixed(3),
            pers111_b9 = Number(pers102_b9/pers102_b9).toFixed(3); //hasilnya 1

            //persamaan 112 (56) => mengeliminasi persamaan (103) dengan (104)  <<menghilangkan b9>> //////////
            const pers112_Y = Number(pers103_Y-pers104_Y).toFixed(3),
            pers112_b1 = Number(pers103_b1-pers104_b1).toFixed(3),
            pers112_b2 = Number(pers103_b2-pers104_b2).toFixed(3),
            pers112_b3 = Number(pers103_b3-pers104_b3).toFixed(3),
            pers112_b4 = Number(pers103_b4-pers104_b4).toFixed(3),
            pers112_b5 = Number(pers103_b5-pers104_b5).toFixed(3),
            pers112_b6 = Number(pers103_b6-pers104_b6).toFixed(3),
            pers112_b7 = Number(pers103_b7-pers104_b7).toFixed(3),
            pers112_b8 = Number(pers103_b8-pers104_b8).toFixed(3),
            pers112_b9 = (pers103_b9-pers104_b9);

            //persamaan 113 (57) => mengeliminasi persamaan (104) dengan (105)
            const pers113_Y = Number(pers104_Y-pers105_Y).toFixed(3),
            pers113_b1 = Number(pers104_b1-pers105_b1).toFixed(3),
            pers113_b2 = Number(pers104_b2-pers105_b2).toFixed(3),
            pers113_b3 = Number(pers104_b3-pers105_b3).toFixed(3),
            pers113_b4 = Number(pers104_b4-pers105_b4).toFixed(3),
            pers113_b5 = Number(pers104_b5-pers105_b5).toFixed(3),
            pers113_b6 = Number(pers104_b6-pers105_b6).toFixed(3),
            pers113_b7 = Number(pers104_b7-pers105_b7).toFixed(3),
            pers113_b8 = Number(pers104_b8-pers105_b8).toFixed(3),
            pers113_b9 = (pers104_b9-pers105_b9);

            //persamaan 114 (58) => mengeliminasi persamaan (105) dengan (106)
            const pers114_Y = Number(pers105_Y-pers106_Y).toFixed(3),
            pers114_b1 = Number(pers105_b1-pers106_b1).toFixed(3),
            pers114_b2 = Number(pers105_b2-pers106_b2).toFixed(3),
            pers114_b3 = Number(pers105_b3-pers106_b3).toFixed(3),
            pers114_b4 = Number(pers105_b4-pers106_b4).toFixed(3),
            pers114_b5 = Number(pers105_b5-pers106_b5).toFixed(3),
            pers114_b6 = Number(pers105_b6-pers106_b6).toFixed(3),
            pers114_b7 = Number(pers105_b7-pers106_b7).toFixed(3),
            pers114_b8 = Number(pers105_b8-pers106_b8).toFixed(3),
            pers114_b9 = (pers105_b9-pers106_b9);

            //persamaan 115 (59) => mengeliminasi persamaan (106) dengan (107)
            const pers115_Y = Number(pers106_Y-pers107_Y).toFixed(3),
            pers115_b1 = Number(pers106_b1-pers107_b1).toFixed(3),
            pers115_b2 = Number(pers106_b2-pers107_b2).toFixed(3),
            pers115_b3 = Number(pers106_b3-pers107_b3).toFixed(3),
            pers115_b4 = Number(pers106_b4-pers107_b4).toFixed(3),
            pers115_b5 = Number(pers106_b5-pers107_b5).toFixed(3),
            pers115_b6 = Number(pers106_b6-pers107_b6).toFixed(3),
            pers115_b7 = Number(pers106_b7-pers107_b7).toFixed(3),
            pers115_b8 = Number(pers106_b8-pers107_b8).toFixed(3),
            pers115_b9 = (pers106_b9-pers107_b9);

            //persamaan 116 (60) => mengeliminasi persamaan (107) dengan (108)
            const pers116_Y = Number(pers107_Y-pers108_Y).toFixed(3),
            pers116_b1 = Number(pers107_b1-pers108_b1).toFixed(3),
            pers116_b2 = Number(pers107_b2-pers108_b2).toFixed(3),
            pers116_b3 = Number(pers107_b3-pers108_b3).toFixed(3),
            pers116_b4 = Number(pers107_b4-pers108_b4).toFixed(3),
            pers116_b5 = Number(pers107_b5-pers108_b5).toFixed(3),
            pers116_b6 = Number(pers107_b6-pers108_b6).toFixed(3),
            pers116_b7 = Number(pers107_b7-pers108_b7).toFixed(3),
            pers116_b8 = Number(pers107_b8-pers108_b8).toFixed(3),
            pers116_b9 = (pers107_b9-pers108_b9);

            //persamaan 117 (61) => mengeliminasi persamaan (108) dengan (109)
            const pers117_Y = Number(pers108_Y-pers109_Y).toFixed(3),
            pers117_b1 = Number(pers108_b1-pers109_b1).toFixed(3),
            pers117_b2 = Number(pers108_b2-pers109_b2).toFixed(3),
            pers117_b3 = Number(pers108_b3-pers109_b3).toFixed(3),
            pers117_b4 = Number(pers108_b4-pers109_b4).toFixed(3),
            pers117_b5 = Number(pers108_b5-pers109_b5).toFixed(3),
            pers117_b6 = Number(pers108_b6-pers109_b6).toFixed(3),
            pers117_b7 = Number(pers108_b7-pers109_b7).toFixed(3),
            pers117_b8 = Number(pers108_b8-pers109_b8).toFixed(3),
            pers117_b9 = (pers108_b9-pers109_b9);

            //persamaan 118 (62) => mengeliminasi persamaan (109) dengan (110)
            const pers118_Y = Number(pers109_Y-pers110_Y).toFixed(3),
            pers118_b1 = Number(pers109_b1-pers110_b1).toFixed(3),
            pers118_b2 = Number(pers109_b2-pers110_b2).toFixed(3),
            pers118_b3 = Number(pers109_b3-pers110_b3).toFixed(3),
            pers118_b4 = Number(pers109_b4-pers110_b4).toFixed(3),
            pers118_b5 = Number(pers109_b5-pers110_b5).toFixed(3),
            pers118_b6 = Number(pers109_b6-pers110_b6).toFixed(3),
            pers118_b7 = Number(pers109_b7-pers110_b7).toFixed(3),
            pers118_b8 = Number(pers109_b8-pers110_b8).toFixed(3),
            pers118_b9 = (pers109_b9-pers110_b9);

            //persamaan 119 (63) => mengeliminasi persamaan (110) dengan (111)
            const pers119_Y = Number(pers110_Y-pers111_Y).toFixed(3),
            pers119_b1 = Number(pers110_b1-pers111_b1).toFixed(3),
            pers119_b2 = Number(pers110_b2-pers111_b2).toFixed(3),
            pers119_b3 = Number(pers110_b3-pers111_b3).toFixed(3),
            pers119_b4 = Number(pers110_b4-pers111_b4).toFixed(3),
            pers119_b5 = Number(pers110_b5-pers111_b5).toFixed(3),
            pers119_b6 = Number(pers110_b6-pers111_b6).toFixed(3),
            pers119_b7 = Number(pers110_b7-pers111_b7).toFixed(3),
            pers119_b8 = Number(pers110_b8-pers111_b8).toFixed(3),
            pers119_b9 = (pers110_b9-pers111_b9);

            //persamaan 120 (56A) => mengeliminasi b8 dari persamaan (112) <<BATAS>> /////////////////////////
            const pers120_Y = Number(pers112_Y/pers112_b8).toFixed(3),
            pers120_b1 = Number(pers112_b1/pers112_b8).toFixed(3),
            pers120_b2 = Number(pers112_b2/pers112_b8).toFixed(3),
            pers120_b3 = Number(pers112_b3/pers112_b8).toFixed(3),
            pers120_b4 = Number(pers112_b4/pers112_b8).toFixed(3),
            pers120_b5 = Number(pers112_b5/pers112_b8).toFixed(3),
            pers120_b6 = Number(pers112_b6/pers112_b8).toFixed(3),
            pers120_b7 = Number(pers112_b7/pers112_b8).toFixed(3),
            pers120_b8 = Number(pers112_b8/pers112_b8).toFixed(3); //hasilnya 1 

            //persamaan 121 (57A) => mengeliminasi b8 dari persamaan (113) 
            const pers121_Y = Number(pers113_Y/pers113_b8).toFixed(3),
            pers121_b1 = Number(pers113_b1/pers113_b8).toFixed(3),
            pers121_b2 = Number(pers113_b2/pers113_b8).toFixed(3),
            pers121_b3 = Number(pers113_b3/pers113_b8).toFixed(3),
            pers121_b4 = Number(pers113_b4/pers113_b8).toFixed(3),
            pers121_b5 = Number(pers113_b5/pers113_b8).toFixed(3),
            pers121_b6 = Number(pers113_b6/pers113_b8).toFixed(3),
            pers121_b7 = Number(pers113_b7/pers113_b8).toFixed(3),
            pers121_b8 = Number(pers113_b8/pers113_b8).toFixed(3); //hasilnya 1 

            //persamaan 122 (58A) => mengeliminasi b8 dari persamaan (114) 
            const pers122_Y = Number(pers114_Y/pers114_b8).toFixed(3),
            pers122_b1 = Number(pers114_b1/pers114_b8).toFixed(3),
            pers122_b2 = Number(pers114_b2/pers114_b8).toFixed(3),
            pers122_b3 = Number(pers114_b3/pers114_b8).toFixed(3),
            pers122_b4 = Number(pers114_b4/pers114_b8).toFixed(3),
            pers122_b5 = Number(pers114_b5/pers114_b8).toFixed(3),
            pers122_b6 = Number(pers114_b6/pers114_b8).toFixed(3),
            pers122_b7 = Number(pers114_b7/pers114_b8).toFixed(3),
            pers122_b8 = Number(pers114_b8/pers114_b8).toFixed(3); //hasilnya 1

            //persamaan 123 (59A) => mengeliminasi b8 dari persamaan (115) 
            const pers123_Y = Number(pers115_Y/pers115_b8).toFixed(3),
            pers123_b1 = Number(pers115_b1/pers115_b8).toFixed(3),
            pers123_b2 = Number(pers115_b2/pers115_b8).toFixed(3),
            pers123_b3 = Number(pers115_b3/pers115_b8).toFixed(3),
            pers123_b4 = Number(pers115_b4/pers115_b8).toFixed(3),
            pers123_b5 = Number(pers115_b5/pers115_b8).toFixed(3),
            pers123_b6 = Number(pers115_b6/pers115_b8).toFixed(3),
            pers123_b7 = Number(pers115_b7/pers115_b8).toFixed(3),
            pers123_b8 = Number(pers115_b8/pers115_b8).toFixed(3); //hasilnya 1

            //persamaan 124 (60A) => mengeliminasi b8 dari persamaan (116) 
            const pers124_Y = Number(pers116_Y/pers116_b8).toFixed(3),
            pers124_b1 = Number(pers116_b1/pers116_b8).toFixed(3),
            pers124_b2 = Number(pers116_b2/pers116_b8).toFixed(3),
            pers124_b3 = Number(pers116_b3/pers116_b8).toFixed(3),
            pers124_b4 = Number(pers116_b4/pers116_b8).toFixed(3),
            pers124_b5 = Number(pers116_b5/pers116_b8).toFixed(3),
            pers124_b6 = Number(pers116_b6/pers116_b8).toFixed(3),
            pers124_b7 = Number(pers116_b7/pers116_b8).toFixed(3),
            pers124_b8 = Number(pers116_b8/pers116_b8).toFixed(3); //hasilnya 1

            //persamaan 125 (61A) => mengeliminasi b8 dari persamaan (117) 
            const pers125_Y = Number(pers117_Y/pers117_b8).toFixed(3),
            pers125_b1 = Number(pers117_b1/pers117_b8).toFixed(3),
            pers125_b2 = Number(pers117_b2/pers117_b8).toFixed(3),
            pers125_b3 = Number(pers117_b3/pers117_b8).toFixed(3),
            pers125_b4 = Number(pers117_b4/pers117_b8).toFixed(3),
            pers125_b5 = Number(pers117_b5/pers117_b8).toFixed(3),
            pers125_b6 = Number(pers117_b6/pers117_b8).toFixed(3),
            pers125_b7 = Number(pers117_b7/pers117_b8).toFixed(3),
            pers125_b8 = Number(pers117_b8/pers117_b8).toFixed(3); //hasilnya 1

            //persamaan 126 (62A) => mengeliminasi b8 dari persamaan (118) 
            const pers126_Y = Number(pers118_Y/pers118_b8).toFixed(3),
            pers126_b1 = Number(pers118_b1/pers118_b8).toFixed(3),
            pers126_b2 = Number(pers118_b2/pers118_b8).toFixed(3),
            pers126_b3 = Number(pers118_b3/pers118_b8).toFixed(3),
            pers126_b4 = Number(pers118_b4/pers118_b8).toFixed(3),
            pers126_b5 = Number(pers118_b5/pers118_b8).toFixed(3),
            pers126_b6 = Number(pers118_b6/pers118_b8).toFixed(3),
            pers126_b7 = Number(pers118_b7/pers118_b8).toFixed(3),
            pers126_b8 = Number(pers118_b8/pers118_b8).toFixed(3); //hasilnya 1

            //persamaan 127 (63A) => mengeliminasi b8 dari persamaan (119) 
            const pers127_Y = Number(pers119_Y/pers119_b8).toFixed(3),
            pers127_b1 = Number(pers119_b1/pers119_b8).toFixed(3),
            pers127_b2 = Number(pers119_b2/pers119_b8).toFixed(3),
            pers127_b3 = Number(pers119_b3/pers119_b8).toFixed(3),
            pers127_b4 = Number(pers119_b4/pers119_b8).toFixed(3),
            pers127_b5 = Number(pers119_b5/pers119_b8).toFixed(3),
            pers127_b6 = Number(pers119_b6/pers119_b8).toFixed(3),
            pers127_b7 = Number(pers119_b7/pers119_b8).toFixed(3),
            pers127_b8 = Number(pers119_b8/pers119_b8).toFixed(3); //hasilnya 1

            //persamaan 128 (64) => mengeliminasi persamaan (120) dengan (121) <<mengilangkan b8>> ///////////
            const pers128_Y = Number(pers120_Y-pers121_Y).toFixed(3),
            pers128_b1 = Number(pers120_b1-pers121_b1).toFixed(3),
            pers128_b2 = Number(pers120_b2-pers121_b2).toFixed(3),
            pers128_b3 = Number(pers120_b3-pers121_b3).toFixed(3),
            pers128_b4 = Number(pers120_b4-pers121_b4).toFixed(3),
            pers128_b5 = Number(pers120_b5-pers121_b5).toFixed(3),
            pers128_b6 = Number(pers120_b6-pers121_b6).toFixed(3),
            pers128_b7 = Number(pers120_b7-pers121_b7).toFixed(3),
            pers128_b8 = (pers120_b8-pers121_b8);

            //persamaan 129 (65) => mengeliminasi persamaan (121) dengan (122)
            const pers129_Y = Number(pers121_Y-pers122_Y).toFixed(3),
            pers129_b1 = Number(pers121_b1-pers122_b1).toFixed(3),
            pers129_b2 = Number(pers121_b2-pers122_b2).toFixed(3),
            pers129_b3 = Number(pers121_b3-pers122_b3).toFixed(3),
            pers129_b4 = Number(pers121_b4-pers122_b4).toFixed(3),
            pers129_b5 = Number(pers121_b5-pers122_b5).toFixed(3),
            pers129_b6 = Number(pers121_b6-pers122_b6).toFixed(3),
            pers129_b7 = Number(pers121_b7-pers122_b7).toFixed(3),
            pers129_b8 = (pers121_b8-pers122_b8);

            //persamaan 130 (66) => mengeliminasi persamaan (122) dengan (123)
            const pers130_Y = Number(pers122_Y-pers123_Y).toFixed(3),
            pers130_b1 = Number(pers122_b1-pers123_b1).toFixed(3),
            pers130_b2 = Number(pers122_b2-pers123_b2).toFixed(3),
            pers130_b3 = Number(pers122_b3-pers123_b3).toFixed(3),
            pers130_b4 = Number(pers122_b4-pers123_b4).toFixed(3),
            pers130_b5 = Number(pers122_b5-pers123_b5).toFixed(3),
            pers130_b6 = Number(pers122_b6-pers123_b6).toFixed(3),
            pers130_b7 = Number(pers122_b7-pers123_b7).toFixed(3),
            pers130_b8 = (pers122_b8-pers123_b8);

            //persamaan 131 (67) => mengeliminasi persamaan (123) dengan (124)
            const pers131_Y = Number(pers123_Y-pers124_Y).toFixed(3),
            pers131_b1 = Number(pers123_b1-pers124_b1).toFixed(3),
            pers131_b2 = Number(pers123_b2-pers124_b2).toFixed(3),
            pers131_b3 = Number(pers123_b3-pers124_b3).toFixed(3),
            pers131_b4 = Number(pers123_b4-pers124_b4).toFixed(3),
            pers131_b5 = Number(pers123_b5-pers124_b5).toFixed(3),
            pers131_b6 = Number(pers123_b6-pers124_b6).toFixed(3),
            pers131_b7 = Number(pers123_b7-pers124_b7).toFixed(3),
            pers131_b8 = (pers123_b8-pers124_b8);

            //persamaan 132 (68) => mengeliminasi persamaan (124) dengan (125)
            const pers132_Y = Number(pers124_Y-pers125_Y).toFixed(3),
            pers132_b1 = Number(pers124_b1-pers125_b1).toFixed(3),
            pers132_b2 = Number(pers124_b2-pers125_b2).toFixed(3),
            pers132_b3 = Number(pers124_b3-pers125_b3).toFixed(3),
            pers132_b4 = Number(pers124_b4-pers125_b4).toFixed(3),
            pers132_b5 = Number(pers124_b5-pers125_b5).toFixed(3),
            pers132_b6 = Number(pers124_b6-pers125_b6).toFixed(3),
            pers132_b7 = Number(pers124_b7-pers125_b7).toFixed(3),
            pers132_b8 = (pers124_b8-pers125_b8);

            //persamaan 133 (69) => mengeliminasi persamaan (125) dengan (126)
            const pers133_Y = Number(pers125_Y-pers126_Y).toFixed(3),
            pers133_b1 = Number(pers125_b1-pers126_b1).toFixed(3),
            pers133_b2 = Number(pers125_b2-pers126_b2).toFixed(3),
            pers133_b3 = Number(pers125_b3-pers126_b3).toFixed(3),
            pers133_b4 = Number(pers125_b4-pers126_b4).toFixed(3),
            pers133_b5 = Number(pers125_b5-pers126_b5).toFixed(3),
            pers133_b6 = Number(pers125_b6-pers126_b6).toFixed(3),
            pers133_b7 = Number(pers125_b7-pers126_b7).toFixed(3),
            pers133_b8 = (pers125_b8-pers126_b8);

            //persamaan 134 (70) => mengeliminasi persamaan (126) dengan (127)
            const pers134_Y = Number(pers126_Y-pers127_Y).toFixed(3),
            pers134_b1 = Number(pers126_b1-pers127_b1).toFixed(3),
            pers134_b2 = Number(pers126_b2-pers127_b2).toFixed(3),
            pers134_b3 = Number(pers126_b3-pers127_b3).toFixed(3),
            pers134_b4 = Number(pers126_b4-pers127_b4).toFixed(3),
            pers134_b5 = Number(pers126_b5-pers127_b5).toFixed(3),
            pers134_b6 = Number(pers126_b6-pers127_b6).toFixed(3),
            pers134_b7 = Number(pers126_b7-pers127_b7).toFixed(3),
            pers134_b8 = (pers126_b8-pers127_b8);

            //persamaan 135 (64A) => mengeliminasi b7 dari persamaan (128) /////////////////////////////////////////
            const pers135_Y = Number(pers128_Y/pers128_b7).toFixed(3),
            pers135_b1 = Number(pers128_b1/pers128_b7).toFixed(3),
            pers135_b2 = Number(pers128_b2/pers128_b7).toFixed(3),
            pers135_b3 = Number(pers128_b3/pers128_b7).toFixed(3),
            pers135_b4 = Number(pers128_b4/pers128_b7).toFixed(3),
            pers135_b5 = Number(pers128_b5/pers128_b7).toFixed(3),
            pers135_b6 = Number(pers128_b6/pers128_b7).toFixed(3),
            pers135_b7 = Number(pers128_b7/pers128_b7).toFixed(3); //hasilnya 1

            //persamaan 136 (65A) => mengeliminasi b7 dari persamaan (129) 
            const pers136_Y = Number(pers129_Y/pers129_b7).toFixed(3),
            pers136_b1 = Number(pers129_b1/pers129_b7).toFixed(3),
            pers136_b2 = Number(pers129_b2/pers129_b7).toFixed(3),
            pers136_b3 = Number(pers129_b3/pers129_b7).toFixed(3),
            pers136_b4 = Number(pers129_b4/pers129_b7).toFixed(3),
            pers136_b5 = Number(pers129_b5/pers129_b7).toFixed(3),
            pers136_b6 = Number(pers129_b6/pers129_b7).toFixed(3),
            pers136_b7 = Number(pers129_b7/pers129_b7).toFixed(3); //hasilnya 1

            //persamaan 137 (66A) => mengeliminasi b7 dari persamaan (130) 
            const pers137_Y = Number(pers130_Y/pers130_b7).toFixed(3),
            pers137_b1 = Number(pers130_b1/pers130_b7).toFixed(3),
            pers137_b2 = Number(pers130_b2/pers130_b7).toFixed(3),
            pers137_b3 = Number(pers130_b3/pers130_b7).toFixed(3),
            pers137_b4 = Number(pers130_b4/pers130_b7).toFixed(3),
            pers137_b5 = Number(pers130_b5/pers130_b7).toFixed(3),
            pers137_b6 = Number(pers130_b6/pers130_b7).toFixed(3),
            pers137_b7 = Number(pers130_b7/pers130_b7).toFixed(3); //hasilnya 1

            //persamaan 138 (67A) => mengeliminasi b7 dari persamaan (131) 
            const pers138_Y = Number(pers131_Y/pers131_b7).toFixed(3),
            pers138_b1 = Number(pers131_b1/pers131_b7).toFixed(3),
            pers138_b2 = Number(pers131_b2/pers131_b7).toFixed(3),
            pers138_b3 = Number(pers131_b3/pers131_b7).toFixed(3),
            pers138_b4 = Number(pers131_b4/pers131_b7).toFixed(3),
            pers138_b5 = Number(pers131_b5/pers131_b7).toFixed(3),
            pers138_b6 = Number(pers131_b6/pers131_b7).toFixed(3),
            pers138_b7 = Number(pers131_b7/pers131_b7).toFixed(3); //hasilnya 1

            //persamaan 139 (68A) => mengeliminasi b7 dari persamaan (132) 
            const pers139_Y = Number(pers132_Y/pers132_b7).toFixed(3),
            pers139_b1 = Number(pers132_b1/pers132_b7).toFixed(3),
            pers139_b2 = Number(pers132_b2/pers132_b7).toFixed(3),
            pers139_b3 = Number(pers132_b3/pers132_b7).toFixed(3),
            pers139_b4 = Number(pers132_b4/pers132_b7).toFixed(3),
            pers139_b5 = Number(pers132_b5/pers132_b7).toFixed(3),
            pers139_b6 = Number(pers132_b6/pers132_b7).toFixed(3),
            pers139_b7 = Number(pers132_b7/pers132_b7).toFixed(3); //hasilnya 1

            //persamaan 140 (69A) => mengeliminasi b7 dari persamaan (133) 
            const pers140_Y = Number(pers133_Y/pers133_b7).toFixed(3),
            pers140_b1 = Number(pers133_b1/pers133_b7).toFixed(3),
            pers140_b2 = Number(pers133_b2/pers133_b7).toFixed(3),
            pers140_b3 = Number(pers133_b3/pers133_b7).toFixed(3),
            pers140_b4 = Number(pers133_b4/pers133_b7).toFixed(3),
            pers140_b5 = Number(pers133_b5/pers133_b7).toFixed(3),
            pers140_b6 = Number(pers133_b6/pers133_b7).toFixed(3),
            pers140_b7 = Number(pers133_b7/pers133_b7).toFixed(3); //hasilnya 1

            //persamaan 141 (70A) => mengeliminasi b7 dari persamaan (134) 
            const pers141_Y = Number(pers134_Y/pers134_b7).toFixed(3),
            pers141_b1 = Number(pers134_b1/pers134_b7).toFixed(3),
            pers141_b2 = Number(pers134_b2/pers134_b7).toFixed(3),
            pers141_b3 = Number(pers134_b3/pers134_b7).toFixed(3),
            pers141_b4 = Number(pers134_b4/pers134_b7).toFixed(3),
            pers141_b5 = Number(pers134_b5/pers134_b7).toFixed(3),
            pers141_b6 = Number(pers134_b6/pers134_b7).toFixed(3),
            pers141_b7 = Number(pers134_b7/pers134_b7).toFixed(3); //hasilnya 1

            //persamaan 142 (71) => mengeliminasi persamaan (135) dengan (136) <<menghilangakn b7>> ///////////////////////////
            const pers142_Y = Number(pers135_Y-pers136_Y).toFixed(3),
            pers142_b1 = Number(pers135_b1-pers136_b1).toFixed(3),
            pers142_b2 = Number(pers135_b2-pers136_b2).toFixed(3),
            pers142_b3 = Number(pers135_b3-pers136_b3).toFixed(3),
            pers142_b4 = Number(pers135_b4-pers136_b4).toFixed(3),
            pers142_b5 = Number(pers135_b5-pers136_b5).toFixed(3),
            pers142_b6 = Number(pers135_b6-pers136_b6).toFixed(3),
            pers142_b7 = (pers135_b7-pers136_b7);

            //persamaan 143 (72) => mengeliminasi persamaan (136) dengan (137)
            const pers143_Y = Number(pers136_Y-pers137_Y).toFixed(3),
            pers143_b1 = Number(pers136_b1-pers137_b1).toFixed(3),
            pers143_b2 = Number(pers136_b2-pers137_b2).toFixed(3),
            pers143_b3 = Number(pers136_b3-pers137_b3).toFixed(3),
            pers143_b4 = Number(pers136_b4-pers137_b4).toFixed(3),
            pers143_b5 = Number(pers136_b5-pers137_b5).toFixed(3),
            pers143_b6 = Number(pers136_b6-pers137_b6).toFixed(3),
            pers143_b7 = (pers136_b7-pers137_b7);

            //persamaan 144 (73) => mengeliminasi persamaan (137) dengan (138)
            const pers144_Y = Number(pers137_Y-pers138_Y).toFixed(3),
            pers144_b1 = Number(pers137_b1-pers138_b1).toFixed(3),
            pers144_b2 = Number(pers137_b2-pers138_b2).toFixed(3),
            pers144_b3 = Number(pers137_b3-pers138_b3).toFixed(3),
            pers144_b4 = Number(pers137_b4-pers138_b4).toFixed(3),
            pers144_b5 = Number(pers137_b5-pers138_b5).toFixed(3),
            pers144_b6 = Number(pers137_b6-pers138_b6).toFixed(3),
            pers144_b7 = (pers137_b7-pers138_b7);

            //persamaan 145 (74) => mengeliminasi persamaan (138) dengan (139)
            const pers145_Y = Number(pers138_Y-pers139_Y).toFixed(3),
            pers145_b1 = Number(pers138_b1-pers139_b1).toFixed(3),
            pers145_b2 = Number(pers138_b2-pers139_b2).toFixed(3),
            pers145_b3 = Number(pers138_b3-pers139_b3).toFixed(3),
            pers145_b4 = Number(pers138_b4-pers139_b4).toFixed(3),
            pers145_b5 = Number(pers138_b5-pers139_b5).toFixed(3),
            pers145_b6 = Number(pers138_b6-pers139_b6).toFixed(3),
            pers145_b7 = (pers138_b7-pers139_b7);

            //persamaan 146 (75) => mengeliminasi persamaan (139) dengan (140)
            const pers146_Y = Number(pers139_Y-pers140_Y).toFixed(3),
            pers146_b1 = Number(pers139_b1-pers140_b1).toFixed(3),
            pers146_b2 = Number(pers139_b2-pers140_b2).toFixed(3),
            pers146_b3 = Number(pers139_b3-pers140_b3).toFixed(3),
            pers146_b4 = Number(pers139_b4-pers140_b4).toFixed(3),
            pers146_b5 = Number(pers139_b5-pers140_b5).toFixed(3),
            pers146_b6 = Number(pers139_b6-pers140_b6).toFixed(3),
            pers146_b7 = (pers139_b7-pers140_b7);

            //persamaan 147 (76) => mengeliminasi persamaan (140) dengan (141)
            const pers147_Y = Number(pers140_Y-pers141_Y).toFixed(3),
            pers147_b1 = Number(pers140_b1-pers141_b1).toFixed(3),
            pers147_b2 = Number(pers140_b2-pers141_b2).toFixed(3),
            pers147_b3 = Number(pers140_b3-pers141_b3).toFixed(3),
            pers147_b4 = Number(pers140_b4-pers141_b4).toFixed(3),
            pers147_b5 = Number(pers140_b5-pers141_b5).toFixed(3),
            pers147_b6 = Number(pers140_b6-pers141_b6).toFixed(3),
            pers147_b7 = (pers140_b7-pers141_b7);

            //persamaan 148 (71A) => mengeliminasi b6 dari persamaan (142) <<BATAS>> /////////////////////////////////////////
            const pers148_Y = Number(pers142_Y/pers142_b6).toFixed(3),
            pers148_b1 = Number(pers142_b1/pers142_b6).toFixed(3),
            pers148_b2 = Number(pers142_b2/pers142_b6).toFixed(3),
            pers148_b3 = Number(pers142_b3/pers142_b6).toFixed(3),
            pers148_b4 = Number(pers142_b4/pers142_b6).toFixed(3),
            pers148_b5 = Number(pers142_b5/pers142_b6).toFixed(3),
            pers148_b6 = Number(pers142_b6/pers142_b6).toFixed(3); //hasilnya 1

            //persamaan 149 (72A) => mengeliminasi b6 dari persamaan (143) 
            const pers149_Y = Number(pers143_Y/pers143_b6).toFixed(3),
            pers149_b1 = Number(pers143_b1/pers143_b6).toFixed(3),
            pers149_b2 = Number(pers143_b2/pers143_b6).toFixed(3),
            pers149_b3 = Number(pers143_b3/pers143_b6).toFixed(3),
            pers149_b4 = Number(pers143_b4/pers143_b6).toFixed(3),
            pers149_b5 = Number(pers143_b5/pers143_b6).toFixed(3),
            pers149_b6 = Number(pers143_b6/pers143_b6).toFixed(3); //hasilnya 1

            //persamaan 150 (73A) => mengeliminasi b6 dari persamaan (144) 
            const pers150_Y = Number(pers144_Y/pers144_b6).toFixed(3),
            pers150_b1 = Number(pers144_b1/pers144_b6).toFixed(3),
            pers150_b2 = Number(pers144_b2/pers144_b6).toFixed(3),
            pers150_b3 = Number(pers144_b3/pers144_b6).toFixed(3),
            pers150_b4 = Number(pers144_b4/pers144_b6).toFixed(3),
            pers150_b5 = Number(pers144_b5/pers144_b6).toFixed(3),
            pers150_b6 = Number(pers144_b6/pers144_b6).toFixed(3); //hasilnya 1

            //persamaan 151 (74A) => mengeliminasi b6 dari persamaan (145) 
            const pers151_Y = Number(pers145_Y/pers145_b6).toFixed(3),
            pers151_b1 = Number(pers145_b1/pers145_b6).toFixed(3),
            pers151_b2 = Number(pers145_b2/pers145_b6).toFixed(3),
            pers151_b3 = Number(pers145_b3/pers145_b6).toFixed(3),
            pers151_b4 = Number(pers145_b4/pers145_b6).toFixed(3),
            pers151_b5 = Number(pers145_b5/pers145_b6).toFixed(3),
            pers151_b6 = Number(pers145_b6/pers145_b6).toFixed(3); //hasilnya 1

            //persamaan 152 (75A) => mengeliminasi b6 dari persamaan (146) 
            const pers152_Y = Number(pers146_Y/pers146_b6).toFixed(3),
            pers152_b1 = Number(pers146_b1/pers146_b6).toFixed(3),
            pers152_b2 = Number(pers146_b2/pers146_b6).toFixed(3),
            pers152_b3 = Number(pers146_b3/pers146_b6).toFixed(3),
            pers152_b4 = Number(pers146_b4/pers146_b6).toFixed(3),
            pers152_b5 = Number(pers146_b5/pers146_b6).toFixed(3),
            pers152_b6 = Number(pers146_b6/pers146_b6).toFixed(3); //hasilnya 1

            //persamaan 153 (76A) => mengeliminasi b6 dari persamaan (147) 
            const pers153_Y = Number(pers147_Y/pers147_b6).toFixed(3),
            pers153_b1 = Number(pers147_b1/pers147_b6).toFixed(3),
            pers153_b2 = Number(pers147_b2/pers147_b6).toFixed(3),
            pers153_b3 = Number(pers147_b3/pers147_b6).toFixed(3),
            pers153_b4 = Number(pers147_b4/pers147_b6).toFixed(3),
            pers153_b5 = Number(pers147_b5/pers147_b6).toFixed(3),
            pers153_b6 = Number(pers147_b6/pers147_b6).toFixed(3); //hasilnya 1

            //persamaan 154 (77) => mengeliminasi persamaan (148) dengan (149) <<menghilangakn b6>> ///////////////////////////
            const pers154_Y = Number(pers148_Y-pers149_Y).toFixed(3),
            pers154_b1 = Number(pers148_b1-pers149_b1).toFixed(3),
            pers154_b2 = Number(pers148_b2-pers149_b2).toFixed(3),
            pers154_b3 = Number(pers148_b3-pers149_b3).toFixed(3),
            pers154_b4 = Number(pers148_b4-pers149_b4).toFixed(3),
            pers154_b5 = Number(pers148_b5-pers149_b5).toFixed(3),
            pers154_b6 = (pers148_b6-pers149_b6);

            //persamaan 155 (78) => mengeliminasi persamaan (149) dengan (150) 
            const pers155_Y = Number(pers149_Y-pers150_Y).toFixed(3),
            pers155_b1 = Number(pers149_b1-pers150_b1).toFixed(3),
            pers155_b2 = Number(pers149_b2-pers150_b2).toFixed(3),
            pers155_b3 = Number(pers149_b3-pers150_b3).toFixed(3),
            pers155_b4 = Number(pers149_b4-pers150_b4).toFixed(3),
            pers155_b5 = Number(pers149_b5-pers150_b5).toFixed(3),
            pers155_b6 = (pers149_b6-pers150_b6);

            //persamaan 156 (79) => mengeliminasi persamaan (150) dengan (151) 
            const pers156_Y = Number(pers150_Y-pers151_Y).toFixed(3),
            pers156_b1 = Number(pers150_b1-pers151_b1).toFixed(3),
            pers156_b2 = Number(pers150_b2-pers151_b2).toFixed(3),
            pers156_b3 = Number(pers150_b3-pers151_b3).toFixed(3),
            pers156_b4 = Number(pers150_b4-pers151_b4).toFixed(3),
            pers156_b5 = Number(pers150_b5-pers151_b5).toFixed(3),
            pers156_b6 = (pers150_b6-pers151_b6);

            //persamaan 157 (80) => mengeliminasi persamaan (151) dengan (152) 
            const pers157_Y = Number(pers151_Y-pers152_Y).toFixed(3),
            pers157_b1 = Number(pers151_b1-pers152_b1).toFixed(3),
            pers157_b2 = Number(pers151_b2-pers152_b2).toFixed(3),
            pers157_b3 = Number(pers151_b3-pers152_b3).toFixed(3),
            pers157_b4 = Number(pers151_b4-pers152_b4).toFixed(3),
            pers157_b5 = Number(pers151_b5-pers152_b5).toFixed(3),
            pers157_b6 = (pers151_b6-pers152_b6);

            //persamaan 158 (81) => mengeliminasi persamaan (152) dengan (153) 
            const pers158_Y = Number(pers152_Y-pers153_Y).toFixed(3),
            pers158_b1 = Number(pers152_b1-pers153_b1).toFixed(3),
            pers158_b2 = Number(pers152_b2-pers153_b2).toFixed(3),
            pers158_b3 = Number(pers152_b3-pers153_b3).toFixed(3),
            pers158_b4 = Number(pers152_b4-pers153_b4).toFixed(3),
            pers158_b5 = Number(pers152_b5-pers153_b5).toFixed(3),
            pers158_b6 = (pers152_b6-pers153_b6);

            //persamaan 159 (77A) => mengeliminasi b5 dari persamaan (154) <<BATAS>> /////////////////////////////////////////
            const pers159_Y = Number(pers154_Y/pers154_b5).toFixed(3),
            pers159_b1 = Number(pers154_b1/pers154_b5).toFixed(3),
            pers159_b2 = Number(pers154_b2/pers154_b5).toFixed(3),
            pers159_b3 = Number(pers154_b3/pers154_b5).toFixed(3),
            pers159_b4 = Number(pers154_b4/pers154_b5).toFixed(3),
            pers159_b5 = Number(pers154_b5/pers154_b5).toFixed(3); //hasilnya 1
            
            //persamaan 160 (78A) => mengeliminasi b5 dari persamaan (155)
            const pers160_Y = Number(pers155_Y/pers155_b5).toFixed(3),
            pers160_b1 = Number(pers155_b1/pers155_b5).toFixed(3),
            pers160_b2 = Number(pers155_b2/pers155_b5).toFixed(3),
            pers160_b3 = Number(pers155_b3/pers155_b5).toFixed(3),
            pers160_b4 = Number(pers155_b4/pers155_b5).toFixed(3),
            pers160_b5 = Number(pers155_b5/pers155_b5).toFixed(3); //hasilnya 1

            //persamaan 161 (79A) => mengeliminasi b5 dari persamaan (156)
            const pers161_Y = Number(pers156_Y/pers156_b5).toFixed(3),
            pers161_b1 = Number(pers156_b1/pers156_b5).toFixed(3),
            pers161_b2 = Number(pers156_b2/pers156_b5).toFixed(3),
            pers161_b3 = Number(pers156_b3/pers156_b5).toFixed(3),
            pers161_b4 = Number(pers156_b4/pers156_b5).toFixed(3),
            pers161_b5 = Number(pers156_b5/pers156_b5).toFixed(3); //hasilnya 1

            //persamaan 162 (80A) => mengeliminasi b5 dari persamaan (157)
            const pers162_Y = Number(pers157_Y/pers157_b5).toFixed(3),
            pers162_b1 = Number(pers157_b1/pers157_b5).toFixed(3),
            pers162_b2 = Number(pers157_b2/pers157_b5).toFixed(3),
            pers162_b3 = Number(pers157_b3/pers157_b5).toFixed(3),
            pers162_b4 = Number(pers157_b4/pers157_b5).toFixed(3),
            pers162_b5 = Number(pers157_b5/pers157_b5).toFixed(3); //hasilnya 1

            //persamaan 163 (81A) => mengeliminasi b5 dari persamaan (158)
            const pers163_Y = Number(pers158_Y/pers158_b5).toFixed(3),
            pers163_b1 = Number(pers158_b1/pers158_b5).toFixed(3),
            pers163_b2 = Number(pers158_b2/pers158_b5).toFixed(3),
            pers163_b3 = Number(pers158_b3/pers158_b5).toFixed(3),
            pers163_b4 = Number(pers158_b4/pers158_b5).toFixed(3),
            pers163_b5 = Number(pers158_b5/pers158_b5).toFixed(3); //hasilnya 1

            //persamaan 164 (82) => mengeliminasi persamaan (159) dengan (160) <<menghilangkan b5>> ////////////////////////////
            const pers164_Y = Number(pers159_Y-pers160_Y).toFixed(3),
            pers164_b1 = Number(pers159_b1-pers160_b1).toFixed(3),
            pers164_b2 = Number(pers159_b2-pers160_b2).toFixed(3),
            pers164_b3 = Number(pers159_b3-pers160_b3).toFixed(3),
            pers164_b4 = Number(pers159_b4-pers160_b4).toFixed(3),
            pers164_b5 = (pers159_b5-pers160_b5);

            //persamaan 165 (83) => mengeliminasi persamaan (160) dengan (161)
            const pers165_Y = Number(pers160_Y-pers161_Y).toFixed(3),
            pers165_b1 = Number(pers160_b1-pers161_b1).toFixed(3),
            pers165_b2 = Number(pers160_b2-pers161_b2).toFixed(3),
            pers165_b3 = Number(pers160_b3-pers161_b3).toFixed(3),
            pers165_b4 = Number(pers160_b4-pers161_b4).toFixed(3),
            pers165_b5 = (pers160_b5-pers161_b5);

            //persamaan 166 (84) => mengeliminasi persamaan (161) dengan (162)
            const pers166_Y = Number(pers161_Y-pers162_Y).toFixed(3),
            pers166_b1 = Number(pers161_b1-pers162_b1).toFixed(3),
            pers166_b2 = Number(pers161_b2-pers162_b2).toFixed(3),
            pers166_b3 = Number(pers161_b3-pers162_b3).toFixed(3),
            pers166_b4 = Number(pers161_b4-pers162_b4).toFixed(3),
            pers166_b5 = (pers161_b5-pers162_b5);

            //persamaan 167 (85) => mengeliminasi persamaan (162) dengan (163)
            const pers167_Y = Number(pers162_Y-pers163_Y).toFixed(3),
            pers167_b1 = Number(pers162_b1-pers163_b1).toFixed(3),
            pers167_b2 = Number(pers162_b2-pers163_b2).toFixed(3),
            pers167_b3 = Number(pers162_b3-pers163_b3).toFixed(3),
            pers167_b4 = Number(pers162_b4-pers163_b4).toFixed(3),
            pers167_b5 = (pers162_b5-pers163_b5);

            //persamaan 168 (82A) => mengeliminasi b4 dari persamaan (164) <<BATAS>> /////////////////////////////////////////
            const pers168_Y = Number(pers164_Y/pers164_b4).toFixed(3),
            pers168_b1 = Number(pers164_b1/pers164_b4).toFixed(3),
            pers168_b2 = Number(pers164_b2/pers164_b4).toFixed(3),
            pers168_b3 = Number(pers164_b3/pers164_b4).toFixed(3),
            pers168_b4 = Number(pers164_b4/pers164_b4).toFixed(3); //hasilnya 1

            //persamaan 169 (83A) => mengeliminasi b4 dari persamaan (165) 
            const pers169_Y = Number(pers165_Y/pers165_b4).toFixed(3),
            pers169_b1 = Number(pers165_b1/pers165_b4).toFixed(3),
            pers169_b2 = Number(pers165_b2/pers165_b4).toFixed(3),
            pers169_b3 = Number(pers165_b3/pers165_b4).toFixed(3),
            pers169_b4 = Number(pers165_b4/pers165_b4).toFixed(3); //hasilnya 1

            //persamaan 170 (84A) => mengeliminasi b4 dari persamaan (166) 
            const pers170_Y = Number(pers166_Y/pers166_b4).toFixed(3),
            pers170_b1 = Number(pers166_b1/pers166_b4).toFixed(3),
            pers170_b2 = Number(pers166_b2/pers166_b4).toFixed(3),
            pers170_b3 = Number(pers166_b3/pers166_b4).toFixed(3),
            pers170_b4 = Number(pers166_b4/pers166_b4).toFixed(3); //hasilnya 1

            //persamaan 171 (85A) => mengeliminasi b4 dari persamaan (167) 
            const pers171_Y = Number(pers167_Y/pers167_b4).toFixed(3),
            pers171_b1 = Number(pers167_b1/pers167_b4).toFixed(3),
            pers171_b2 = Number(pers167_b2/pers167_b4).toFixed(3),
            pers171_b3 = Number(pers167_b3/pers167_b4).toFixed(3),
            pers171_b4 = Number(pers167_b4/pers167_b4).toFixed(3); //hasilnya 1

            //persamaan 172 (86) => mengeliminasi persamaan (168) dengan (169) <<menghilangakn b4>> /////////////////////////
            const pers172_Y = Number(pers168_Y-pers169_Y).toFixed(3),
            pers172_b1 = Number(pers168_b1-pers169_b1).toFixed(3),
            pers172_b2 = Number(pers168_b2-pers169_b2).toFixed(3),
            pers172_b3 = Number(pers168_b3-pers169_b3).toFixed(3),
            pers172_b4 = (pers168_b4-pers169_b4);

            //persamaan 173 (87) => mengeliminasi persamaan (169) dengan (170) 
            const pers173_Y = Number(pers169_Y-pers170_Y).toFixed(3),
            pers173_b1 = Number(pers169_b1-pers170_b1).toFixed(3),
            pers173_b2 = Number(pers169_b2-pers170_b2).toFixed(3),
            pers173_b3 = Number(pers169_b3-pers170_b3).toFixed(3),
            pers173_b4 = (pers169_b4-pers170_b4);

            //persamaan 174 (88) => mengeliminasi persamaan (170) dengan (171) 
            const pers174_Y = Number(pers170_Y-pers171_Y).toFixed(3),
            pers174_b1 = Number(pers170_b1-pers171_b1).toFixed(3),
            pers174_b2 = Number(pers170_b2-pers171_b2).toFixed(3),
            pers174_b3 = Number(pers170_b3-pers171_b3).toFixed(3),
            pers174_b4 = (pers170_b4-pers171_b4);

            //persamaan 175 (86A) => mengeliminasi b3 dari persamaan (172) <<BATAS>> ///////////////////////////////////////
            const pers175_Y = Number(pers172_Y/pers172_b3).toFixed(3),
            pers175_b1 = Number(pers172_b1/pers172_b3).toFixed(3),
            pers175_b2 = Number(pers172_b2/pers172_b3).toFixed(3),
            pers175_b3 = Number(pers172_b3/pers172_b3).toFixed(3); //hasilnya 1

            //persamaan 176 (87A) => mengeliminasi b3 dari persamaan (173)
            const pers176_Y = Number(pers173_Y/pers173_b3).toFixed(3),
            pers176_b1 = Number(pers173_b1/pers173_b3).toFixed(3),
            pers176_b2 = Number(pers173_b2/pers173_b3).toFixed(3),
            pers176_b3 = Number(pers173_b3/pers173_b3).toFixed(3); //hasilnya 1

            //persamaan 177 (88A) => mengeliminasi b3 dari persamaan (174)
            const pers177_Y = Number(pers174_Y/pers174_b3).toFixed(3),
            pers177_b1 = Number(pers174_b1/pers174_b3).toFixed(3),
            pers177_b2 = Number(pers174_b2/pers174_b3).toFixed(3),
            pers177_b3 = Number(pers174_b3/pers174_b3).toFixed(3); //hasilnya 1

            //persamaan 178 (89) => mengeliminasi persamaan (175) dengan (176) <<menghilangkan b3>> ////////////////////////
            const pers178_Y = Number(pers175_Y-pers176_Y).toFixed(3),
            pers178_b1 = Number(pers175_b1-pers176_b1).toFixed(3),
            pers178_b2 = Number(pers175_b2-pers176_b2).toFixed(3),
            pers178_b3 = (pers175_b3-pers176_b3);

            //persamaan 179 (90) => mengeliminasi persamaan (176) dengan (177)
            const pers179_Y = Number(pers176_Y-pers177_Y).toFixed(3),
            pers179_b1 = Number(pers176_b1-pers177_b1).toFixed(3),
            pers179_b2 = Number(pers176_b2-pers177_b2).toFixed(3),
            pers179_b3 = (pers176_b3-pers177_b3);

            //persamaan 180 (89A) => mengeliminasi b2 dari persamaan (178) <<BATAS>> /////////////////////////////////////////
            const pers180_Y = Number(pers178_Y/pers178_b2).toFixed(3),
            pers180_b1 = Number(pers178_b1/pers178_b2).toFixed(3),
            pers180_b2 = Number(pers178_b2/pers178_b2).toFixed(3); //hasilnya 1

            //persamaan 181 (90A) => mengeliminasi b2 dari persamaan (179) 
            const pers181_Y = Number(pers179_Y/pers179_b2).toFixed(3),
            pers181_b1 = Number(pers179_b1/pers179_b2).toFixed(3),
            pers181_b2 = Number(pers179_b2/pers179_b2).toFixed(3); //hasilnya 1

            //mencari b1 => mengeliminasi persamaan (180) dengan (181) <<menghilangkan b2>> //////////////////////////////////
            const pers182_Y = Number(pers180_Y-pers181_Y).toFixed(3),
            pers182_b1 = Number(pers180_b1-pers181_b1).toFixed(3),
            pers182_b2 = (pers180_b2-pers181_b2),
            b1 = Number(pers182_Y/pers182_b1).toFixed(3);

            //mencari b2 => mensubtitusikan b1 ke persamaan (180)
            const b2 = Number(pers181_Y - (pers181_b1 * b1)).toFixed(3);

            //mencari b3 => mensubtitusikan b1 & b2 ke persamaan (175)
            const b3 = Number(pers177_Y - ((pers177_b1 * b1) + (pers177_b2 * b2))).toFixed(3);

            //mencari b4 => mensubtitusikan b1, b2 & b3 ke persamaan (168)
            const b4 = Number(pers171_Y - ((pers171_b1* b1) + (pers171_b2 * b2) + (pers171_b3 * b3))).toFixed(3);

            //mencari b5 => mensubtitusikan b1, b2, b3 & b4 ke persamaan (159)
            const b5 = Number(pers163_Y - ((pers163_b1 * b1) + (pers163_b2* b2) + (pers163_b3 * b3) + (pers163_b4 * b4))).toFixed(3);
            
            //mencari b6 => mensubtitusikan b1, b2, b3, b4 & b5 ke persamaan (148)
            const b6 = Number(pers153_Y - ((pers153_b1 * b1) + (pers153_b2 * b2) + (pers153_b3 * b3) + (pers153_b4 * b4) + (pers153_b5 * b5))).toFixed(3);

            //mencari b7 => mensubtitusikan b1, b2, b3, b4, b5 & b6 ke persamaan (135)
            const b7 = Number(pers141_Y - ((pers141_b1 * b1) + (pers141_b2 * b2) + (pers141_b3 * b3) + (pers141_b4 * b4) + (pers141_b5 * b5) + (pers141_b6 * b6))).toFixed(3);

            //mencari b8 => mensubtitusikan b1, b2, b3, b4, b5, b6 & b7 ke persamaan (120)
            const b8 = Number(pers127_Y - ((pers127_b1 * b1) + (pers127_b2 * b2) + (pers127_b3 * b3) + (pers127_b4 * b4) + (pers127_b5 * b5) + (pers127_b6 * b6) + (pers127_b7 * b7))).toFixed(3); 

            //mencari b9 => mensubtitusikan b1, b2, b3, b4, b5, b6, b7 & b8 ke persamaan (103)
            const b9 = Number(pers111_Y - ((pers111_b1 * b1) + (pers111_b2 * b2) + (pers111_b3 * b3) + (pers111_b4 * b4) + (pers111_b5 * b5) + (pers111_b6 * b6) + (pers111_b7 * b7) + (pers111_b8 * b8))).toFixed(3);

            // mencari b10 => mensubtitusikan b1,b2,b3,b4,b5,b6,b7,b8 & b9 ke persamaan (84)
            const b10 = Number(pers93_Y - ((pers93_b1*b1) + (pers93_b2*b2) + (pers93_b3*b3) + (pers93_b4*b4) + (pers93_b5*b5) + (pers93_b6*b6) + (pers93_b7*b7) + (pers93_b8*b8) + (pers93_b9*b9))).toFixed(3);

            // mencari b11 => mensubtitusikan b1,b2,b3,b4,b5,b6,b7,b8,b9 & b10 ke persamaan (63)
            const b11 = Number(pers73_Y - ((pers73_b1*b1) + (pers73_b2*b2) + (pers73_b3*b3) + (pers73_b4*b4) + (pers73_b5*b5) + (pers73_b6*b6) + (pers73_b7*b7) + (pers73_b8*b8) + (pers73_b9*b9) + (pers73_b10*b10))).toFixed(3);

            // mencari b12 => mensubtitusikan b1,b2,b3,b4,b5,b6,b7,b8,b9,b10 & b11 ke persamaan (40)
            const b12 = Number(pers51_Y - ((pers51_b1*b1) + (pers51_b2*b2) + (pers51_b3*b3) + (pers51_b4*b4) + (pers51_b5*b5) + (pers51_b6*b6) + (pers51_b7*b7) + (pers51_b8*b8) + (pers51_b9*b9) + (pers51_b10*b10) + (pers51_b11*b11))).toFixed(3);

            // mencari b13 => mensubtitusikan b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11 & b12 ke persamaan (15)
            const b13 = Number(pers27_Y - ((pers27_b1*b1) + (pers27_b2*b2) + (pers27_b3*b3) + (pers27_b4*b4) + (pers27_b5*b5) + (pers27_b6*b6) + (pers27_b7*b7) + (pers27_b8*b8) + (pers27_b9*b9) + (pers27_b10*b10) + (pers27_b11*b11) + (pers27_b12*b12))).toFixed(3);

            // mencari b0 => mensubtitusikan b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12 & b13 ke persamaan (1)
            const b0 = Number(Y - (X1 * b1) - (X2 * b2) - (X3 * b3) - (X4 * b4) - (X5 * b5) - (X6 * b6) - (X7 * b7) - (X8 * b8) - (X9 * b9) - (X10 * b10) - (X11 * b11) - (X12 * b12) - (X13 * b13)).toFixed(3);

            //PERSAMAAN SIMULTAN /NORMAL
            //Isi persamaan 1
            $('#s1').val(YX1 + " = " + X1_2 + " B1 + " + X1X2 + " B2 + " + X1X3 + " B3 + " + X1X4 + " B4 + " + X1X5 + " B5 + " + X1X6 + " B6 + " + X1X7 + " B7 + " + X1X8 + " B8 + " + X1X9 + " B9 + " + X1X10 + " B10 + " + X1X11 + " B11 + " + X1X12 + " B12 + " + X1X13 + " B13");

            //Isi persamaan 2
            $('#s2').val(YX2 + " = " + X1X2 + " B1 + " + X2_2 + " B2 + " + X2X3 + " B3 + " + X2X4 + " B4 + " + X2X5 + " B5 + " + X2X6 + " B6 + " + X2X7 + " B7 + " + X2X8 + " B8 + " + X2X9 + " B9 + " + X2X10 + " B10 + " + X2X11 + " B11 + " + X2X12 + " B12 + " + X2X13 + " B13");

            //Isi persamaan 3
            $('#s3').val(YX3 + " = " + X1X3 + " B1 + " + X2X3 + " B2 + " + X3_2 + " B3 + "+ X3X4 + " B4 + "+ X3X5 + " B5 + " + X3X6 + " B6 + " + X3X7 + " B7 + " + X3X8 + " B8 + " + X3X9 + " B9 + " + X3X10 + " B10 + " + X3X11 + " B11 + " + X3X12 + " B12 + " + X3X13 + " B13");

            //Isi persamaan 4
            $('#s4').val(YX4 + " = " + X1X4 + " B1 + " + X2X4 + " B2 + " + X3X4 + " B3 + " + X4_2 + " B4 + "+ X4X5 + " B5 + " + X4X6 + " B6 + " + X4X7 + " B7 + " + X4X8 + " B8 + " + X4X9 + " B9 + " + X4X10 + " B10 + " + X4X11 + " B11 + " + X4X12 + " B12 + " + X4X13 + " B13");

            //Isi persamaan 5
            $('#s5').val(YX5 + " = " + X1X5 + " B1 + " + X2X5 + " B2 + " + X3X5 + " B3 + "+ X4X5 + " B4 + "+ X5_2 + " B5 + " + X5X6 + " B6 + " + X5X7 + " B7 + " + X5X8 + " B8 + " + X5X9 + " B9 + " + X5X10 + " B10 + " + X5X11 + " B11 + " + X5X12 + " B12 + " + X5X13 + " B13");

            //Isi persamaan 6
            $('#s6').val(YX6 + " = " + X1X6 + " B1 + " + X2X6 + " B2 + " + X3X6 + " B3 + " + X4X6 + " B4 + " + X5X6 + " B5 + " + X6_2 + " B6 + " + X6X7 + " B7 + " + X6X8 + " B8 + " + X6X9 + " B9 + " + X6X10 + " B10 + " + X6X11 + " B11 + " + X6X12 + " B12 + " + X6X13 + " B13");

            //Isi persamaan 7
            $('#s7').val(YX7 + " = " + X1X7 + " B1 + " + X2X7 + " B2 + " + X3X7 + " B3 + " + X4X7 + " B4 + " + X5X7 + " B5 + " + X6X7 + " B6 + " + X7_2 + " B7 + " + X7X8 + " B8 + " + X7X9 + " B9 + " + X7X10 + " B10 + " + X7X11 + " B11 + " + X7X12 + " B12 + " + X7X13 + " B13");

            //Isi persamaan 8
            $('#s8').val(YX8 + " = " + X1X8 + " B1 + " + X2X8 + " B2 + " + X3X8 + " B3 + " + X4X8 + " B4 + " + X5X8 + " B5 + " + X6X8 + " B6 + " + X7X8 + " B7 + " + X8_2 + " B8 + " + X8X9 + " B9 + " + X8X10 + " B10 + " + X8X11 + " B11 + " + X8X12 + " B12 + " + X8X13 + " B13");

            //Isi persamaan 9
            $('#s9').val(YX9 + " = " + X1X9 + " B1 + " + X2X9 + " B2 + " + X3X9 + " B3 + " + X4X9 + " B4 + " + X5X9 + " B5 + " + X6X9 + " B6 + " + X7X9 + " B7 + " + X8X9 + " B8 + " + X9_2 + " B9 + " + X9X10 + " B10 + " + X9X11 + " B11 + " + X9X12 + " B12 + " + X9X13 + " B13");

            //Isi persamaan 10
            $('#s10').val(YX10 + " = " + X1X10 + " B1 + " + X2X10 + " B2 + " + X3X10 + " B3 + " + X4X10 + " B4 + " + X5X10 + " B5 + " + X6X10 + " B6 + " + X7X10 + " B7 + " + X8X10 + " B8 + " + X9X10 + " B9 + " + X10_2 + " B10 + " + X10X11 + " B11 + " + X10X12 + " B12 + " + X10X13 + " B13");

            //Isi persamaan 11
            $('#s11').val(YX11 + " = " + X1X11 + " B1 + " + X2X11 + " B2 + " + X3X11 + " B3 + " + X4X11 + " B4 + " + X5X11 + " B5 + " + X6X11 + " B6 + " + X7X11 + " B7 + " + X8X11 + " B8 + " + X9X11 + " B9 + " + X10X11 + " B10 + " + X11_2 + " B11 + " + X11X12 + " B12 + " + X11X13 + " B13");

            //Isi persamaan 12
            $('#s12').val(YX12 + " = " + X1X12 + " B1 + " + X2X12 + " B2 + " + X3X12 + " B3 + " + X4X12 + " B4 + " + X5X12 + " B5 + " + X6X12 + " B6 + " + X7X12 + " B7 + " + X8X12 + " B8 + " + X9X12 + " B9 + " + X10X12 + " B10 + " + X11X12 + " B11 + " + X12_2 + " B12 + " + X12X13 + " B13");

            //Isi persamaan 13
            $('#s13').val(YX13 + " = " + X1X13 + " B1 + " + X2X13 + " B2 + " + X3X13 + " B3 + " + X4X13 + " B4 + " + X5X13 + " B5 + " + X6X13 + " B6 + " + X7X13 + " B7 + " + X8X13 + " B8 + " + X9X13 + " B9 + " + X10X13 + " B10 + " + X11X13 + " B11 + " + X12X13 + " B13 + " + X13_2 + " B13");
            
            //Isi persamaan 28 => Menghilangkan B13
            $('#s14').val(pers28_Y + " = " + pers28_b1 + " B1 + " + pers28_b2 + " B2 + " + pers28_b3 + " B3 + " + pers28_b4 + " B4 + " + pers28_b5 + " B5 + " + pers28_b6 + " B6 + " + pers28_b7 + " B7 + " + pers28_b8 + " B8 + " + pers28_b9  + " B9 + " + pers28_b10  + " B10 + " + pers28_b11  + " B11 + " + pers28_b12 + " B12 + " + pers28_b13  + " B13");

            //Isi persamaan 29 => Menghilangkan B13
            $('#s15').val(pers29_Y + " = " + pers29_b1 + " B1 + " + pers29_b2 + " B2 + " + pers29_b3 + " B3 + " + pers29_b4 + " B4 + " + pers29_b5 + " B5 + " + pers29_b6 + " B6 + " + pers29_b7 + " B7 + " + pers29_b8 + " B8 + " + pers29_b9  + " B9 + " + pers29_b10  + " B10 + " + pers29_b11 + " B11 + " + pers29_b12 + " B12 + " + pers29_b13  + " B13");

            //Isi persamaan 30 => Menghilangkan B13
            $('#s16').val(pers31_Y + " = " + pers31_b1 + " B1 + " + pers31_b2 + " B2 + " + pers31_b3 + " B3 + " + pers31_b4 + " B4 + " + pers31_b5 + " B5 + " + pers31_b6 + " B6 + " + pers31_b7  + " B7 + " + pers31_b8  + " B8 + " + pers31_b9  + " B9 + " + pers31_b10  + " B10 + " + pers31_b11  + " B11 + " + pers31_b12 + " B12 + " + pers31_b13  + " B13");

            //Isi persamaan 31 => Menghilangkan B13
            $('#s17').val(pers31_Y + " = " + pers31_b1 + " B1 + " + pers31_b2 + " B2 + " + pers31_b3 + " B3 + " + pers31_b4 + " B4 + " + pers31_b5 + " B5 + " + pers31_b6 + " B6 + " + pers31_b7  + " B7 + " + pers31_b8  + " B8 + " + pers31_b9  + " B9 + " + pers31_b10  + " B10 + " + pers31_b11  + " B11 + " + pers31_b12 + " B12 + " + pers31_b13  + " B13");

            //Isi persamaan 32 => Menghilangkan B13
            $('#s18').val(pers32_Y + " = " + pers32_b1 + " B1 + " + pers32_b2 + " B2 + " + pers32_b3 + " B3 + " + pers32_b4 + " B4 + " + pers32_b5 + " B5 + " + pers32_b6 + " B6 + " + pers32_b7  + " B7 + " + pers32_b8  + " B8 + " + pers32_b9  + " B9 + " + pers32_b10  + " B10 + " + pers32_b11  + " B11 + " + pers32_b12 + " B12 + " + pers32_b13  + " B13");

            //Isi persamaan 33 => Menghilangkan B13
            $('#s19').val(pers33_Y + " = " + pers33_b1 + " B1 + " + pers33_b2 + " B2 + " + pers33_b3 + " B3 + " + pers33_b4 + " B4 + " + pers33_b5 + " B5 + " + pers33_b6 + " B6 + " + pers33_b7  + " B7 + " + pers33_b8  + " B8 + " + pers33_b9  + " B9 + " + pers33_b10  + " B10 + " + pers33_b11  + " B11 + " + pers33_b12 + " B12 + " + pers33_b13  + " B13");

            //Isi persamaan 34 => Menghilangkan B13
            $('#s20').val(pers35_Y + " = " + pers35_b1 + " B1 + " + pers35_b2 + " B2 + " + pers35_b3 + " B3 + " + pers35_b4 + " B4 + " + pers35_b5 + " B5 + " + pers35_b6 + " B6 + " + pers35_b7  + " B7 + " + pers35_b8  + " B8 + " + pers35_b9  + " B9 + " + pers35_b10  + " B10 + " + pers35_b11  + " B11 + " + pers35_b12 + " B12 + " + pers35_b13  + " B13");

            //Isi persamaan 35 => Menghilangkan B13
            $('#s21').val(pers35_Y + " = " + pers35_b1 + " B1 + " + pers35_b2 + " B2 + " + pers35_b3 + " B3 + " + pers35_b4 + " B4 + " + pers35_b5 + " B5 + " + pers35_b6 + " B6 + " + pers35_b7  + " B7 + " + pers35_b8  + " B8 + " + pers35_b9  + " B9 + " + pers35_b10  + " B10 + " + pers35_b11  + " B11 + " + pers35_b12 + " B12 + " + pers35_b13  + " B13");

            //Isi persamaan 36 => Menghilangkan B13
            $('#s22').val(pers36_Y + " = " + pers36_b1 + " B1 + " + pers36_b2 + " B2 + " + pers36_b3 + " B3 + " + pers36_b4 + " B4 + " + pers36_b5 + " B5 + " + pers36_b6 + " B6 + " + pers36_b7  + " B7 + " + pers36_b8  + " B8 + " + pers36_b9  + " B9 + " + pers36_b10  + " B10 + " + pers36_b11  + " B11 + " + pers36_b12 + " B12 + " + pers36_b13  + " B13");

            //Isi persamaan 37 => Menghilangkan B13
            $('#s23').val(pers37_Y + " = " + pers37_b1 + " B1 + " + pers37_b2 + " B2 + " + pers37_b3 + " B3 + " + pers37_b4 + " B4 + " + pers37_b5 + " B5 + " + pers37_b6 + " B6 + " + pers37_b7  + " B7 + " + pers37_b8  + " B8 + " + pers37_b9  + " B9 + " + pers37_b10  + " B10 + " + pers37_b11  + " B11 + " + pers37_b12 + " B12 + " + pers37_b13  + " B13");

            //Isi persamaan 38 => Menghilangkan B13
            $('#s24').val(pers38_Y + " = " + pers38_b1 + " B1 + " + pers38_b2 + " B2 + " + pers38_b3 + " B3 + " + pers38_b4 + " B4 + " + pers38_b5 + " B5 + " + pers38_b6 + " B6" + pers38_b7  + " B7 + " + pers38_b8  + " B8 + " + pers38_b9  + " B9 + " + pers38_b10  + " B10 + " + pers38_b11  + " B11 + " + pers38_b12 + " B12 + " + pers38_b13  + " B13");

            //Isi persamaan 39 => Menghilangkan B13
            $('#s25').val(pers39_Y + " = " + pers39_b1 + " B1 + " + pers39_b2 + " B2 + " + pers39_b3 + " B3 + " + pers39_b4 + " B4 + " + pers39_b5 + " B5 + " + pers39_b6 + " B6 +" + pers39_b7  + " B7 + " + pers39_b8  + " B8 + " + pers39_b9  + " B9 + " + pers39_b10  + " B10 + " + pers39_b11  + " B11 + " + pers39_b12 + " B12 + " + pers39_b13  + " B13");

            //BATAS/////////////////////////////////////////////
            //Isi persamaan(52) => Menghilangkan B12
            $('#s26').val(pers52_Y + " = " + pers52_b1 + " B1 + " + pers52_b2 + " B2 + " + pers52_b3 + " B3 + " + pers52_b4 + " B4 + " + pers52_b5 + " B5 + " + pers52_b6 + " B6 +" + pers52_b7  + " B7 + " + pers52_b8  + " B8 + " + pers52_b9  + " B9 + " + pers52_b10  + " B10 + " + pers52_b11  + " B11 + " + pers52_b12 + " B12");
            //Isi persamaan(53) => Menghilangkan B12
            $('#s27').val(pers53_Y + " = " + pers53_b1 + " B1 + " + pers53_b2 + " B2 + " + pers53_b3 + " B3 + " + pers53_b4 + " B4 + " + pers53_b5 + " B5 + " + pers53_b6 + " B6 +" + pers53_b7  + " B7 + " + pers53_b8  + " B8 + " + pers53_b9  + " B9 + " + pers53_b10  + " B10 + " + pers53_b11  + " B11 + " + pers53_b12 + " B12");
            //Isi persamaan(54) => Menghilangkan B12
            $('#s28').val(pers54_Y + " = " + pers54_b1 + " B1 + " + pers54_b2 + " B2 + " + pers54_b3 + " B3 + " + pers54_b4 + " B4 + " + pers54_b5 + " B5 + " + pers54_b6 + " B6 +" + pers54_b7  + " B7 + " + pers54_b8  + " B8 + " + pers54_b9  + " B9 + " + pers54_b10  + " B10 + " + pers54_b11  + " B11 + " + pers54_b12 + " B12");
            //Isi persamaan(55) => Menghilangkan B12
            $('#s29').val(pers55_Y + " = " + pers55_b1 + " B1 + " + pers55_b2 + " B2 + " + pers55_b3 + " B3 + " + pers55_b4 + " B4 + " + pers55_b5 + " B5 + " + pers55_b6 + " B6 +" + pers55_b7  + " B7 + " + pers55_b8  + " B8 + " + pers55_b9  + " B9 + " + pers55_b10  + " B10 + " + pers55_b11  + " B11 + " + pers55_b12 + " B12");
            //Isi persamaan(56) => Menghilangkan B12
            $('#s30').val(pers56_Y + " = " + pers56_b1 + " B1 + " + pers56_b2 + " B2 + " + pers56_b3 + " B3 + " + pers56_b4 + " B4 + " + pers56_b5 + " B5 + " + pers56_b6 + " B6 +" + pers56_b7  + " B7 + " + pers56_b8  + " B8 + " + pers56_b9  + " B9 + " + pers56_b10  + " B10 + " + pers56_b11  + " B11 + " + pers56_b12 + " B12");
            //Isi persamaan(57) => Menghilangkan B12
            $('#s31').val(pers57_Y + " = " + pers57_b1 + " B1 + " + pers57_b2 + " B2 + " + pers57_b3 + " B3 + " + pers57_b4 + " B4 + " + pers57_b5 + " B5 + " + pers57_b6 + " B6 +" + pers57_b7  + " B7 + " + pers57_b8  + " B8 + " + pers57_b9  + " B9 + " + pers57_b10  + " B10 + " + pers57_b11  + " B11 + " + pers57_b12 + " B12");
            //Isi persamaan(58) => Menghilangkan B12
            $('#s32').val(pers58_Y + " = " + pers58_b1 + " B1 + " + pers58_b2 + " B2 + " + pers58_b3 + " B3 + " + pers58_b4 + " B4 + " + pers58_b5 + " B5 + " + pers58_b6 + " B6 +" + pers58_b7  + " B7 + " + pers58_b8  + " B8 + " + pers58_b9  + " B9 + " + pers58_b10  + " B10 + " + pers58_b11  + " B11 + " + pers58_b12 + " B12");
            //Isi persamaan(59) => Menghilangkan B12
            $('#s33').val(pers59_Y + " = " + pers59_b1 + " B1 + " + pers59_b2 + " B2 + " + pers59_b3 + " B3 + " + pers59_b4 + " B4 + " + pers59_b5 + " B5 + " + pers59_b6 + " B6 +" + pers59_b7  + " B7 + " + pers59_b8  + " B8 + " + pers59_b9  + " B9 + " + pers59_b10  + " B10 + " + pers59_b11  + " B11 + " + pers59_b12 + " B12");
            //Isi persamaan(60) => Menghilangkan B12
            $('#s34').val(pers60_Y + " = " + pers60_b1 + " B1 + " + pers60_b2 + " B2 + " + pers60_b3 + " B3 + " + pers60_b4 + " B4 + " + pers60_b5 + " B5 + " + pers60_b6 + " B6 +" + pers60_b7  + " B7 + " + pers60_b8  + " B8 + " + pers60_b9  + " B9 + " + pers60_b10  + " B10 + " + pers60_b11  + " B11 + " + pers60_b12 + " B12");
            //Isi persamaan(61) => Menghilangkan B12
            $('#s35').val(pers61_Y + " = " + pers61_b1 + " B1 + " + pers61_b2 + " B2 + " + pers61_b3 + " B3 + " + pers61_b4 + " B4 + " + pers61_b5 + " B5 + " + pers61_b6 + " B6 +" + pers61_b7  + " B7 + " + pers61_b8  + " B8 + " + pers61_b9  + " B9 + " + pers61_b10  + " B10 + " + pers61_b11  + " B11 + " + pers61_b12 + " B12");
            //Isi persamaan(62) => Menghilangkan B12
            $('#s36').val(pers62_Y + " = " + pers62_b1 + " B1 + " + pers62_b2 + " B2 + " + pers62_b3 + " B3 + " + pers62_b4 + " B4 + " + pers62_b5 + " B5 + " + pers62_b6 + " B6 +" + pers62_b7  + " B7 + " + pers62_b8  + " B8 + " + pers62_b9  + " B9 + " + pers62_b10  + " B10 + " + pers62_b11  + " B11 + " + pers62_b12 + " B12");

            //BATAS/////////////////////////////////////////////
            //Isi persamaan(74) => Menghilangkan b11
            $('#s37').val(pers74_Y + " = " + pers74_b1 + " B1 + " + pers74_b2 + " B2 + " + pers74_b3 + " B3 + " + pers74_b4 + " B4 + " + pers74_b5 + " B5 + " + pers74_b6 + " B6 +" + pers74_b7  + " B7 + " + pers74_b8  + " B8 + " + pers74_b9  + " B9 + " + pers74_b10  + " B10 + " + pers74_b11  + " B11");
            //Isi persamaan(75) => Menghilangkan b11
            $('#s38').val(pers75_Y + " = " + pers75_b1 + " B1 + " + pers75_b2 + " B2 + " + pers75_b3 + " B3 + " + pers75_b4 + " B4 + " + pers75_b5 + " B5 + " + pers75_b6 + " B6 +" + pers75_b7  + " B7 + " + pers75_b8  + " B8 + " + pers75_b9  + " B9 + " + pers75_b10  + " B10 + " + pers75_b11  + " B11");
            //Isi persamaan(76) => Menghilangkan b11
            $('#s39').val(pers76_Y + " = " + pers76_b1 + " B1 + " + pers76_b2 + " B2 + " + pers76_b3 + " B3 + " + pers76_b4 + " B4 + " + pers76_b5 + " B5 + " + pers76_b6 + " B6 +" + pers76_b7  + " B7 + " + pers76_b8  + " B8 + " + pers76_b9  + " B9 + " + pers76_b10  + " B10 + " + pers76_b11  + " B11");
            //Isi persamaan(77) => Menghilangkan b11
            $('#s40').val(pers77_Y + " = " + pers77_b1 + " B1 + " + pers77_b2 + " B2 + " + pers77_b3 + " B3 + " + pers77_b4 + " B4 + " + pers77_b5 + " B5 + " + pers77_b6 + " B6 +" + pers77_b7  + " B7 + " + pers77_b8  + " B8 + " + pers77_b9  + " B9 + " + pers77_b10  + " B10 + " + pers77_b11  + " B11");
            //Isi persamaan(78) => Menghilangkan b11
            $('#s41').val(pers78_Y + " = " + pers78_b1 + " B1 + " + pers78_b2 + " B2 + " + pers78_b3 + " B3 + " + pers78_b4 + " B4 + " + pers78_b5 + " B5 + " + pers78_b6 + " B6 +" + pers78_b7  + " B7 + " + pers78_b8  + " B8 + " + pers78_b9  + " B9 + " + pers78_b10  + " B10 + " + pers78_b11  + " B11");
            //Isi persamaan(79) => Menghilangkan b11
            $('#s42').val(pers79_Y + " = " + pers79_b1 + " B1 + " + pers79_b2 + " B2 + " + pers79_b3 + " B3 + " + pers79_b4 + " B4 + " + pers79_b5 + " B5 + " + pers79_b6 + " B6 +" + pers79_b7  + " B7 + " + pers79_b8  + " B8 + " + pers79_b9  + " B9 + " + pers79_b10  + " B10 + " + pers79_b11  + " B11");
            //Isi persamaan(80) => Menghilangkan b11
            $('#s43').val(pers80_Y + " = " + pers80_b1 + " B1 + " + pers80_b2 + " B2 + " + pers80_b3 + " B3 + " + pers80_b4 + " B4 + " + pers80_b5 + " B5 + " + pers80_b6 + " B6 +" + pers80_b7  + " B7 + " + pers80_b8  + " B8 + " + pers80_b9  + " B9 + " + pers80_b10  + " B10 + " + pers80_b11  + " B11");
            //Isi persamaan(81) => Menghilangkan b11
            $('#s44').val(pers81_Y + " = " + pers81_b1 + " B1 + " + pers81_b2 + " B2 + " + pers81_b3 + " B3 + " + pers81_b4 + " B4 + " + pers81_b5 + " B5 + " + pers81_b6 + " B6 +" + pers81_b7  + " B7 + " + pers81_b8  + " B8 + " + pers81_b9  + " B9 + " + pers81_b10  + " B10 + " + pers81_b11  + " B11");
            //Isi persamaan(82) => Menghilangkan b11
            $('#s45').val(pers82_Y + " = " + pers82_b1 + " B1 + " + pers82_b2 + " B2 + " + pers82_b3 + " B3 + " + pers82_b4 + " B4 + " + pers82_b5 + " B5 + " + pers82_b6 + " B6 +" + pers82_b7  + " B7 + " + pers82_b8  + " B8 + " + pers82_b9  + " B9 + " + pers82_b10  + " B10 + " + pers82_b11  + " B11");
            //Isi persamaan(83) => Menghilangkan b11
            $('#s46').val(pers83_Y + " = " + pers83_b1 + " B1 + " + pers83_b2 + " B2 + " + pers83_b3 + " B3 + " + pers83_b4 + " B4 + " + pers83_b5 + " B5 + " + pers83_b6 + " B6 +" + pers83_b7  + " B7 + " + pers83_b8  + " B8 + " + pers83_b9  + " B9 + " + pers83_b10  + " B10 + " + pers83_b11  + " B11");

            //BATAS/////////////////////////////////////////////
            //isi persamaan(94) => Menghilangkan b10 
            $('#s47').val(pers94_Y + " = " + pers94_b1 + " B1 + " + pers94_b2 + " B2 + " + pers94_b3 + " B3 + " + pers94_b4 + " B4 + " + pers94_b5 + " B5 + " + pers94_b6 + " B6 +" + pers94_b7  + " B7 + " + pers94_b8  + " B8 + " + pers94_b9  + " B9 + " + pers94_b10  + " B10");
            //isi persamaan(95) => Menghilangkan b10 
            $('#s48').val(pers95_Y + " = " + pers95_b1 + " B1 + " + pers95_b2 + " B2 + " + pers95_b3 + " B3 + " + pers95_b4 + " B4 + " + pers95_b5 + " B5 + " + pers95_b6 + " B6 +" + pers95_b7  + " B7 + " + pers95_b8  + " B8 + " + pers95_b9  + " B9 + " + pers95_b10  + " B10");
            //isi persamaan(96) => Menghilangkan b10 
            $('#s49').val(pers96_Y + " = " + pers96_b1 + " B1 + " + pers96_b2 + " B2 + " + pers96_b3 + " B3 + " + pers96_b4 + " B4 + " + pers96_b5 + " B5 + " + pers96_b6 + " B6 +" + pers96_b7  + " B7 + " + pers96_b8  + " B8 + " + pers96_b9  + " B9 + " + pers96_b10  + " B10");
            //isi persamaan(97) => Menghilangkan b10 
            $('#s50').val(pers97_Y + " = " + pers97_b1 + " B1 + " + pers97_b2 + " B2 + " + pers97_b3 + " B3 + " + pers97_b4 + " B4 + " + pers97_b5 + " B5 + " + pers97_b6 + " B6 +" + pers97_b7  + " B7 + " + pers97_b8  + " B8 + " + pers97_b9  + " B9 + " + pers97_b10  + " B10");
            //isi persamaan(98) => Menghilangkan b10 
            $('#s51').val(pers98_Y + " = " + pers98_b1 + " B1 + " + pers98_b2 + " B2 + " + pers98_b3 + " B3 + " + pers98_b4 + " B4 + " + pers98_b5 + " B5 + " + pers98_b6 + " B6 +" + pers98_b7  + " B7 + " + pers98_b8  + " B8 + " + pers98_b9  + " B9 + " + pers98_b10  + " B10");
            //isi persamaan(99) => Menghilangkan b10 
            $('#s52').val(pers99_Y + " = " + pers99_b1 + " B1 + " + pers99_b2 + " B2 + " + pers99_b3 + " B3 + " + pers99_b4 + " B4 + " + pers99_b5 + " B5 + " + pers99_b6 + " B6 +" + pers99_b7  + " B7 + " + pers99_b8  + " B8 + " + pers99_b9  + " B9 + " + pers99_b10  + " B10");
            //isi persamaan(100) => Menghilangkan b10 
            $('#s53').val(pers100_Y + " = " + pers100_b1 + " B1 + " + pers100_b2 + " B2 + " + pers100_b3 + " B3 + " + pers100_b4 + " B4 + " + pers100_b5 + " B5 + " + pers100_b6 + " B6 +" + pers100_b7  + " B7 + " + pers100_b8  + " B8 + " + pers100_b9  + " B9 + " + pers100_b10  + " B10");
            //isi persamaan(101) => Menghilangkan b10 
            $('#s54').val(pers101_Y + " = " + pers101_b1 + " B1 + " + pers101_b2 + " B2 + " + pers101_b3 + " B3 + " + pers101_b4 + " B4 + " + pers101_b5 + " B5 + " + pers101_b6 + " B6 +" + pers101_b7  + " B7 + " + pers101_b8  + " B8 + " + pers101_b9  + " B9 + " + pers101_b10  + " B10");
            //isi persamaan(102) => Menghilangkan b10 
            $('#s55').val(pers102_Y + " = " + pers102_b1 + " B1 + " + pers102_b2 + " B2 + " + pers102_b3 + " B3 + " + pers102_b4 + " B4 + " + pers102_b5 + " B5 + " + pers102_b6 + " B6 +" + pers102_b7  + " B7 + " + pers102_b8  + " B8 + " + pers102_b9  + " B9 + " + pers102_b10  + " B10");

            //BATAS/////////////////////////////////////////////
            //isi persamaan(112) => Menghilangkan b9
            $('#s56').val(pers112_Y + " = " + pers112_b1 + " B1 + " + pers112_b2 + " B2 + " + pers112_b3 + " B3 + " + pers112_b4 + " B4 + " + pers112_b5 + " B5 + " + pers112_b6 + " B6 +" + pers112_b7  + " B7 + " + pers112_b8  + " B8 + " + pers112_b9  + " B9");
            //isi persamaan(112) => Menghilangkan b9
            $('#s57').val(pers113_Y + " = " + pers113_b1 + " B1 + " + pers113_b2 + " B2 + " + pers113_b3 + " B3 + " + pers113_b4 + " B4 + " + pers113_b5 + " B5 + " + pers113_b6 + " B6 +" + pers113_b7  + " B7 + " + pers113_b8  + " B8 + " + pers113_b9  + " B9");
            //isi persamaan(114) => Menghilangkan b9
            $('#s58').val(pers114_Y + " = " + pers114_b1 + " B1 + " + pers114_b2 + " B2 + " + pers114_b3 + " B3 + " + pers114_b4 + " B4 + " + pers114_b5 + " B5 + " + pers114_b6 + " B6 +" + pers114_b7  + " B7 + " + pers114_b8  + " B8 + " + pers114_b9  + " B9");
            //isi persamaan(115) => Menghilangkan b9
            $('#s59').val(pers115_Y + " = " + pers115_b1 + " B1 + " + pers115_b2 + " B2 + " + pers115_b3 + " B3 + " + pers115_b4 + " B4 + " + pers115_b5 + " B5 + " + pers115_b6 + " B6 +" + pers115_b7  + " B7 + " + pers115_b8  + " B8 + " + pers115_b9  + " B9");
            //isi persamaan(116) => Menghilangkan b9
            $('#s60').val(pers116_Y + " = " + pers116_b1 + " B1 + " + pers116_b2 + " B2 + " + pers116_b3 + " B3 + " + pers116_b4 + " B4 + " + pers116_b5 + " B5 + " + pers116_b6 + " B6 +" + pers116_b7  + " B7 + " + pers116_b8  + " B8 + " + pers116_b9  + " B9");
            //isi persamaan(117) => Menghilangkan b9
            $('#s61').val(pers117_Y + " = " + pers117_b1 + " B1 + " + pers117_b2 + " B2 + " + pers117_b3 + " B3 + " + pers117_b4 + " B4 + " + pers117_b5 + " B5 + " + pers117_b6 + " B6 +" + pers117_b7  + " B7 + " + pers117_b8  + " B8 + " + pers117_b9  + " B9");
            //isi persamaan(118) => Menghilangkan b9
            $('#s62').val(pers118_Y + " = " + pers118_b1 + " B1 + " + pers118_b2 + " B2 + " + pers118_b3 + " B3 + " + pers118_b4 + " B4 + " + pers118_b5 + " B5 + " + pers118_b6 + " B6 +" + pers118_b7  + " B7 + " + pers118_b8  + " B8 + " + pers118_b9  + " B9");
            //isi persamaan(119) => Menghilangkan b9
            $('#s63').val(pers119_Y + " = " + pers119_b1 + " B1 + " + pers119_b2 + " B2 + " + pers119_b3 + " B3 + " + pers119_b4 + " B4 + " + pers119_b5 + " B5 + " + pers119_b6 + " B6 +" + pers119_b7  + " B7 + " + pers119_b8  + " B8 + " + pers119_b9  + " B9");

            //BATAS/////////////////////////////////////////////
            //isi persamaan(128) => Menghilangkan b8
            $('#s64').val(pers128_Y + " = " + pers128_b1 + " B1 + " + pers128_b2 + " B2 + " + pers128_b3 + " B3 + " + pers128_b4 + " B4 + " + pers128_b5 + " B5 + " + pers128_b6 + " B6 +" + pers128_b7  + " B7 + " + pers128_b8  + " B8");
            //isi persamaan(129) => Menghilangkan b8
            $('#s65').val(pers129_Y + " = " + pers129_b1 + " B1 + " + pers129_b2 + " B2 + " + pers129_b3 + " B3 + " + pers129_b4 + " B4 + " + pers129_b5 + " B5 + " + pers129_b6 + " B6 +" + pers129_b7  + " B7 + " + pers129_b8  + " B8");
            //isi persamaan(130) => Menghilangkan b8
            $('#s66').val(pers130_Y + " = " + pers130_b1 + " B1 + " + pers130_b2 + " B2 + " + pers130_b3 + " B3 + " + pers130_b4 + " B4 + " + pers130_b5 + " B5 + " + pers130_b6 + " B6 +" + pers130_b7  + " B7 + " + pers130_b8  + " B8");
            //isi persamaan(131) => Menghilangkan b8
            $('#s67').val(pers131_Y + " = " + pers131_b1 + " B1 + " + pers131_b2 + " B2 + " + pers131_b3 + " B3 + " + pers131_b4 + " B4 + " + pers131_b5 + " B5 + " + pers131_b6 + " B6 +" + pers131_b7  + " B7 + " + pers131_b8  + " B8");
            //isi persamaan(132) => Menghilangkan b8
            $('#s68').val(pers132_Y + " = " + pers132_b1 + " B1 + " + pers132_b2 + " B2 + " + pers132_b3 + " B3 + " + pers132_b4 + " B4 + " + pers132_b5 + " B5 + " + pers132_b6 + " B6 +" + pers132_b7  + " B7 + " + pers132_b8  + " B8");
            //isi persamaan(133) => Menghilangkan b8
            $('#s69').val(pers133_Y + " = " + pers133_b1 + " B1 + " + pers133_b2 + " B2 + " + pers133_b3 + " B3 + " + pers133_b4 + " B4 + " + pers133_b5 + " B5 + " + pers133_b6 + " B6 +" + pers133_b7  + " B7 + " + pers133_b8  + " B8");
            //isi persamaan(134) => Menghilangkan b8
            $('#s70').val(pers134_Y + " = " + pers134_b1 + " B1 + " + pers134_b2 + " B2 + " + pers134_b3 + " B3 + " + pers134_b4 + " B4 + " + pers134_b5 + " B5 + " + pers134_b6 + " B6 +" + pers134_b7  + " B7 + " + pers134_b8  + " B8");

            //BATAS/////////////////////////////////////////////
            //isi persamaan(142) => Menghilangkan b7
            $('#s71').val(pers142_Y + " = " + pers142_b1 + " B1 + " + pers142_b2 + " B2 + " + pers142_b3 + " B3 + " + pers142_b4 + " B4 + " + pers142_b5 + " B5 + " + pers142_b6 + " B6 +" + pers142_b7  + " B7");
            //isi persamaan(143) => Menghilangkan b7
            $('#s72').val(pers143_Y + " = " + pers143_b1 + " B1 + " + pers143_b2 + " B2 + " + pers143_b3 + " B3 + " + pers143_b4 + " B4 + " + pers143_b5 + " B5 + " + pers143_b6 + " B6 +" + pers143_b7  + " B7");
            //isi persamaan(144) => Menghilangkan b7
            $('#s73').val(pers144_Y + " = " + pers144_b1 + " B1 + " + pers144_b2 + " B2 + " + pers144_b3 + " B3 + " + pers144_b4 + " B4 + " + pers144_b5 + " B5 + " + pers144_b6 + " B6 +" + pers144_b7  + " B7");
            //isi persamaan(145) => Menghilangkan b7
            $('#s74').val(pers145_Y + " = " + pers145_b1 + " B1 + " + pers145_b2 + " B2 + " + pers145_b3 + " B3 + " + pers145_b4 + " B4 + " + pers145_b5 + " B5 + " + pers145_b6 + " B6 +" + pers145_b7  + " B7");
            //isi persamaan(146) => Menghilangkan b7
            $('#s75').val(pers146_Y + " = " + pers146_b1 + " B1 + " + pers146_b2 + " B2 + " + pers146_b3 + " B3 + " + pers146_b4 + " B4 + " + pers146_b5 + " B5 + " + pers146_b6 + " B6 +" + pers146_b7  + " B7");
            //isi persamaan(147) => Menghilangkan b7
            $('#s76').val(pers147_Y + " = " + pers147_b1 + " B1 + " + pers147_b2 + " B2 + " + pers147_b3 + " B3 + " + pers147_b4 + " B4 + " + pers147_b5 + " B5 + " + pers147_b6 + " B6 +" + pers147_b7  + " B7");

            //BATAS/////////////////////////////////////////////
            //isi persamaan(154) => Menghilangkan b6
            $('#s77').val(pers154_Y + " = " + pers154_b1 + " B1 + " + pers154_b2 + " B2 + " + pers154_b3 + " B3 + " + pers154_b4 + " B4 + " + pers154_b5 + " B5 + " + pers154_b6 + " B6");
            //isi persamaan(155) => Menghilangkan b6
            $('#s78').val(pers155_Y + " = " + pers155_b1 + " B1 + " + pers155_b2 + " B2 + " + pers155_b3 + " B3 + " + pers155_b4 + " B4 + " + pers155_b5 + " B5 + " + pers155_b6 + " B6");
            //isi persamaan(156) => Menghilangkan b6
            $('#s79').val(pers156_Y + " = " + pers156_b1 + " B1 + " + pers156_b2 + " B2 + " + pers156_b3 + " B3 + " + pers156_b4 + " B4 + " + pers156_b5 + " B5 + " + pers156_b6 + " B6");
            //isi persamaan(157) => Menghilangkan b6
            $('#s80').val(pers157_Y + " = " + pers157_b1 + " B1 + " + pers157_b2 + " B2 + " + pers157_b3 + " B3 + " + pers157_b4 + " B4 + " + pers157_b5 + " B5 + " + pers157_b6 + " B6");
            //isi persamaan(158) => Menghilangkan b6
            $('#s81').val(pers158_Y + " = " + pers158_b1 + " B1 + " + pers158_b2 + " B2 + " + pers158_b3 + " B3 + " + pers158_b4 + " B4 + " + pers158_b5 + " B5 + " + pers158_b6 + " B6");

            //BATAS/////////////////////////////////////////////
            //isi persamaan(164) => Menghilangkan b5
            $('#s82').val(pers164_Y + " = " + pers164_b1 + " B1 + " + pers164_b2 + " B2 + " + pers164_b3 + " B3 + " + pers164_b4 + " B4 + " + pers164_b5 + " B5");
            //isi persamaan(165) => Menghilangkan b5
            $('#s83').val(pers165_Y + " = " + pers165_b1 + " B1 + " + pers165_b2 + " B2 + " + pers165_b3 + " B3 + " + pers165_b4 + " B4 + " + pers165_b5 + " B5");
            //isi persamaan(164) => Menghilangkan b5
            $('#s84').val(pers166_Y + " = " + pers166_b1 + " B1 + " + pers166_b2 + " B2 + " + pers166_b3 + " B3 + " + pers166_b4 + " B4 + " + pers166_b5 + " B5");
            //isi persamaan(164) => Menghilangkan b5
            $('#s85').val(pers167_Y + " = " + pers167_b1 + " B1 + " + pers167_b2 + " B2 + " + pers167_b3 + " B3 + " + pers167_b4 + " B4 + " + pers167_b5 + " B5");

            //BATAS/////////////////////////////////////////////
            //isi persamaan(172) => Menghilangkan b4
            $('#s86').val(pers172_Y + " = " + pers172_b1 + " B1 + " + pers172_b2 + " B2 + " + pers172_b3 + " B3 + " + pers172_b4 + " B4");
            //isi persamaan(173) => Menghilangkan b4
            $('#s87').val(pers173_Y + " = " + pers173_b1 + " B1 + " + pers173_b2 + " B2 + " + pers173_b3 + " B3 + " + pers173_b4 + " B4");
            //isi persamaan(174) => Menghilangkan b4
            $('#s88').val(pers174_Y + " = " + pers174_b1 + " B1 + " + pers174_b2 + " B2 + " + pers174_b3 + " B3 + " + pers174_b4 + " B4");


            //BATAS/////////////////////////////////////////////
            //isi persamaan(178) => Menghilangkan b3
            $('#s89').val(pers178_Y + " = " + pers178_b1 + " B1 + " + pers178_b2 + " B2 + " + pers178_b3 + " B3");
            //isi persamaan(179) => Menghilangkan b3
            $('#s90').val(pers179_Y + " = " + pers179_b1 + " B1 + " + pers179_b2 + " B2 + " + pers179_b3 + " B3");

            //BATAS/////////////////////////////////////////////
            //isi persamaan(182) => Menghilangkan b2
            $('#s91').val(pers182_Y + " = " + pers182_b1 + " B1 + " + pers182_b2 + " B2");


            //===================================B T S============================================
            // $('#s1A').val(pers15_Y + " = " + pers15_b1 + " B1 + " + pers15_b2 + " B2 + " + pers15_b3 + " B3 + " + pers15_b4 + " B4 + " + pers15_b5 + " B5 + " + pers15_b6 + " B6 + " + pers15_b7 + " B7 + " + pers15_b8 + " B8 + " + pers15_b9  + " B9 + " + pers15_b10  + " B10 + " + pers15_b11  + " B11 + " + pers15_b12 + " B12 + " + pers15_b13  + " B13");

            // $('#s2A').val(pers16_Y + " = " + pers16_b1 + " B1 + " + pers16_b2 + " B2 + " + pers16_b3 + " B3 + " + pers16_b4 + " B4 + " + pers16_b5 + " B5 + " + pers16_b6 + " B6 + " + pers16_b7 + " B7 + " + pers16_b8 + " B8 + " + pers16_b9  + " B9 + " + pers16_b10  + " B10 + " + pers16_b11  + " B11 + " + pers16_b12 + " B12 + " + pers16_b13  + " B13");

            // $('#s3A').val(pers17_Y + " = " + pers17_b1 + " B1 + " + pers17_b2 + " B2 + " + pers17_b3 + " B3 + " + pers17_b4 + " B4 + " + pers17_b5 + " B5 + " + pers17_b6 + " B6 + " + pers17_b7 + " B7 + " + pers17_b8 + " B8 + " + pers17_b9  + " B9 + " + pers17_b10  + " B10 + " + pers17_b11  + " B11 + " + pers17_b12 + " B12 + " + pers17_b13  + " B13");

            // $('#s4A').val(pers18_Y + " = " + pers18_b1 + " B1 + " + pers18_b2 + " B2 + " + pers18_b3 + " B3 + " + pers18_b4 + " B4 + " + pers18_b5 + " B5 + " + pers18_b6 + " B6 + " + pers18_b7 + " B7 + " + pers18_b8 + " B8 + " + pers18_b9  + " B9 + " + pers18_b10  + " B10 + " + pers18_b11  + " B11 + " + pers18_b12 + " B12 + " + pers18_b13  + " B13");

            // $('#s5A').val(pers19_Y + " = " + pers19_b1 + " B1 + " + pers19_b2 + " B2 + " + pers19_b3 + " B3 + " + pers19_b4 + " B4 + " + pers19_b5 + " B5 + " + pers19_b6 + " B6 + " + pers19_b7 + " B7 + " + pers19_b8 + " B8 + " + pers19_b9  + " B9 + " + pers19_b10  + " B10 + " + pers19_b11  + " B11 + " + pers19_b12 + " B12 + " + pers19_b13  + " B13");

            // $('#s6A').val(pers20_Y + " = " + pers20_b1 + " B1 + " + pers20_b2 + " B2 + " + pers20_b3 + " B3 + " + pers20_b4 + " B4 + " + pers20_b5 + " B5 + " + pers20_b6 + " B6 + " + pers20_b7 + " B7 + " + pers20_b8 + " B8 + " + pers20_b9  + " B9 + " + pers20_b10  + " B10 + " + pers20_b11  + " B11 + " + pers20_b12 + " B12 + " + pers20_b13  + " B13");

            // $('#s7A').val(pers21_Y + " = " + pers21_b1 + " B1 + " + pers21_b2 + " B2 + " + pers21_b3 + " B3 + " + pers21_b4 + " B4 + " + pers21_b5 + " B5 + " + pers21_b6 + " B6 + " + pers21_b7 + " B7 + " + pers21_b8 + " B8 + " + pers21_b9  + " B9 + " + pers21_b10  + " B10 + " + pers21_b11  + " B11 + " + pers21_b12 + " B12 + " + pers21_b13  + " B13");

            // $('#s8A').val(pers22_Y + " = " + pers22_b1 + " B1 + " + pers22_b2 + " B2 + " + pers22_b3 + " B3 + " + pers22_b4 + " B4 + " + pers22_b5 + " B5 + " + pers22_b6 + " B6 + " + pers22_b7 + " B7 + " + pers22_b8 + " B8 + " + pers22_b9  + " B9 + " + pers22_b10  + " B10 + " + pers22_b11  + " B11 + " + pers22_b12 + " B12 + " + pers22_b13  + " B13");

            // $('#s9A').val(pers23_Y + " = " + pers23_b1 + " B1 + " + pers23_b2 + " B2 + " + pers23_b3 + " B3 + " + pers23_b4 + " B4 + " + pers23_b5 + " B5 + " + pers23_b6 + " B6 + " + pers23_b7 + " B7 + " + pers23_b8 + " B8 + " + pers23_b9  + " B9 + " + pers23_b10  + " B10 + " + pers23_b11  + " B11 + " + pers23_b12 + " B12 + " + pers23_b13  + " B13");

            // $('#s10A').val(pers24_Y + " = " + pers24_b1 + " B1 + " + pers24_b2 + " B2 + " + pers24_b3 + " B3 + " + pers24_b4 + " B4 + " + pers24_b5 + " B5 + " + pers24_b6 + " B6 + " + pers24_b7 + " B7 + " + pers24_b8 + " B8 + " + pers24_b9  + " B9 + " + pers24_b10  + " B10 + " + pers24_b11  + " B11 + " + pers24_b12 + " B12 + " + pers24_b13  + " B13");

            // $('#s11A').val(pers25_Y + " = " + pers25_b1 + " B1 + " + pers25_b2 + " B2 + " + pers25_b3 + " B3 + " + pers25_b4 + " B4 + " + pers25_b5 + " B5 + " + pers25_b6 + " B6 + " + pers25_b7 + " B7 + " + pers25_b8 + " B8 + " + pers25_b9  + " B9 + " + pers25_b10  + " B10 + " + pers25_b11  + " B11 + " + pers25_b12 + " B12 + " + pers25_b13  + " B13");

            // $('#s12A').val(pers26_Y + " = " + pers26_b1 + " B1 + " + pers26_b2 + " B2 + " + pers26_b3 + " B3 + " + pers26_b4 + " B4 + " + pers26_b5 + " B5 + " + pers26_b6 + " B6 + " + pers26_b7 + " B7 + " + pers26_b8 + " B8 + " + pers26_b9  + " B9 + " + pers26_b10  + " B10 + " + pers26_b11  + " B11 + " + pers26_b12 + " B12 + " + pers26_b13  + " B13");

            // $('#s13A').val(pers27_Y + " = " + pers27_b1 + " B1 + " + pers27_b2 + " B2 + " + pers27_b3 + " B3 + " + pers27_b4 + " B4 + " + pers27_b5 + " B5 + " + pers27_b6 + " B6 + " + pers27_b7 + " B7 + " + pers27_b8 + " B8 + " + pers27_b9  + " B9 + " + pers27_b10  + " B10 + " + pers27_b11  + " B11 + " + pers27_b12 + " B12 + " + pers27_b13  + " B13");

            $('#s13A').val(pers27_Y + " = " + pers27_b1 + " B1 + " + pers27_b2 + " B2 + " + pers27_b3 + " B3 + " + pers27_b4 + " B4 + " + pers27_b5 + " B5 + " + pers27_b6 + " B6 + " + pers27_b7 + " B7 + " + pers27_b8 + " B8 + " + pers27_b9  + " B9 + " + pers27_b10  + " B10 + " + pers27_b11  + " B11 + " + pers27_b12 + " B12 + " + pers27_b13  + " B13");
            $('#s12A').val(pers51_Y + " = " + pers51_b1 + " B1 + " + pers51_b2 + " B2 + " + pers51_b3 + " B3 + " + pers51_b4 + " B4 + " + pers51_b5 + " B5 + " + pers51_b6 + " B6 + " + pers51_b7 + " B7 + " + pers51_b8 + " B8 + " + pers51_b9  + " B9 + " + pers51_b10  + " B10 + " + pers51_b11  + " B11 + " + pers51_b12 + " B12 ");
            $('#s11A').val(pers73_Y + " = " + pers73_b1 + " B1 + " + pers73_b2 + " B2 + " + pers73_b3 + " B3 + " + pers73_b4 + " B4 + " + pers73_b5 + " B5 + " + pers73_b6 + " B6 + " + pers73_b7 + " B7 + " + pers73_b8 + " B8 + " + pers73_b9  + " B9 + " + pers73_b10  + " B10 + " + pers73_b11  + " B11");
            $('#s10A').val(pers93_Y + " = " + pers93_b1 + " B1 + " + pers93_b2 + " B2 + " + pers93_b3 + " B3 + " + pers93_b4 + " B4 + " + pers93_b5 + " B5 + " + pers93_b6 + " B6 + " + pers93_b7 + " B7 + " + pers93_b8 + " B8 + " + pers93_b9  + " B9 + " + pers93_b10  + " B10");
            $('#s9A').val(pers111_Y + " = " + pers111_b1 + " B1 + " + pers111_b2 + " B2 + " + pers111_b3 + " B3 + " + pers111_b4 + " B4 + " + pers111_b5 + " B5 + " + pers111_b6 + " B6 + " + pers111_b7 + " B7 + " + pers111_b8 + " B8 + " + pers111_b9  + " B9");
            $('#s8A').val(pers127_Y + " = " + pers127_b1 + " B1 + " + pers127_b2 + " B2 + " + pers127_b3 + " B3 + " + pers127_b4 + " B4 + " + pers127_b5 + " B5 + " + pers127_b6 + " B6 + " + pers127_b7 + " B7 + " + pers127_b8 + " B8");
            $('#s7A').val(pers141_Y + " = " + pers141_b1 + " B1 + " + pers141_b2 + " B2 + " + pers141_b3 + " B3 + " + pers141_b4 + " B4 + " + pers141_b5 + " B5 + " + pers141_b6 + " B6 + " + pers141_b7 + " B7");
            $('#s6A').val(pers153_Y + " = " + pers153_b1 + " B1 + " + pers153_b2 + " B2 + " + pers153_b3 + " B3 + " + pers153_b4 + " B4 + " + pers153_b5 + " B5 + " + pers153_b6 + " B6");
            $('#s5A').val(pers163_Y + " = " + pers163_b1 + " B1 + " + pers163_b2 + " B2 + " + pers163_b3 + " B3 + " + pers163_b4 + " B4 + " + pers163_b5 + " B5");
            $('#s4A').val(pers171_Y + " = " + pers171_b1 + " B1 + " + pers171_b2 + " B2 + " + pers171_b3 + " B3 + " + pers171_b4 + " B4");
            $('#s3A').val(pers177_Y + " = " + pers177_b1 + " B1 + " + pers177_b2 + " B2 + " + pers177_b3 + " B3");
            $('#s2A').val(pers181_Y + " = " + pers181_b1 + " B1 + " + pers181_b2 + " B2");
            //===============================R E A D O N L Y=======================================

            $('#b13').val(b13); //Isi b13
            $('#b12').val(b12); //Isi b12
            $('#b11').val(b11); //Isi b11
            $('#b10').val(b10); //Isi b10
            $('#b9').val(b9); //Isi b9
            $('#b8').val(b8); //Isi b8
            $('#b7').val(b7); //Isi b7
            $('#b6').val(b6); //Isi b6
            $('#b5').val(b5); //Isi b5
            $('#b4').val(b4); //Isi b4
            $('#b3').val(b3); //Isi b3
            $('#b2').val(b2); //Isi b2
            $('#b1').val(b1); //Isi b1
            $('#b0').val(b0); //Isi b0

            $('#regresi').val("Y = " + b0 + " + (" + b1 + ") X1 + (" + b2 + ") X2 + (" + b3 + ") X3 + (" + b4 + ") X4 + (" + b5 + ") X5 + (" + b6 + ") X6 + (" + b7 + ") X7 + (" + b8 + ") X8 + (" + b9 + ") X9 + (" + b10 + ") X10 + (" + b11 + ") X11 + (" + b12 + ") X12 + (" + b13 + ") X13");
        });

        //save data
        $('#simpan_rumus').click(function(){
            console.log("simpan")
          $('form').submit();
        });
    </script>
@endsection
