<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $perhitungan->id !!}</p>
</div>

<!-- B0 Field -->
<div class="form-group">
    {!! Form::label('b0', 'B0:') !!}
    <p>{!! $perhitungan->b0 !!}</p>
</div>

<!-- B1 Field -->
<div class="form-group">
    {!! Form::label('b1', 'B1:') !!}
    <p>{!! $perhitungan->b1 !!}</p>
</div>

<!-- B2 Field -->
<div class="form-group">
    {!! Form::label('b2', 'B2:') !!}
    <p>{!! $perhitungan->b2 !!}</p>
</div>

<!-- B3 Field -->
<div class="form-group">
    {!! Form::label('b3', 'B3:') !!}
    <p>{!! $perhitungan->b3 !!}</p>
</div>

<!-- B4 Field -->
<div class="form-group">
    {!! Form::label('b4', 'B4:') !!}
    <p>{!! $perhitungan->b4 !!}</p>
</div>

<!-- B5 Field -->
<div class="form-group">
    {!! Form::label('b5', 'B5:') !!}
    <p>{!! $perhitungan->b5 !!}</p>
</div>

<!-- B6 Field -->
<div class="form-group">
    {!! Form::label('b6', 'B6:') !!}
    <p>{!! $perhitungan->b6 !!}</p>
</div>

<!-- B7 Field -->
<div class="form-group">
    {!! Form::label('b7', 'B7:') !!}
    <p>{!! $perhitungan->b7 !!}</p>
</div>

<!-- B8 Field -->
<div class="form-group">
    {!! Form::label('b8', 'B8:') !!}
    <p>{!! $perhitungan->b8 !!}</p>
</div>

<!-- B9 Field -->
<div class="form-group">
    {!! Form::label('b9', 'B9:') !!}
    <p>{!! $perhitungan->b9 !!}</p>
</div>

<!-- B10 Field -->
<div class="form-group">
    {!! Form::label('b10', 'B10:') !!}
    <p>{!! $perhitungan->b10 !!}</p>
</div>

<!-- B11 Field -->
<div class="form-group">
    {!! Form::label('b11', 'B11:') !!}
    <p>{!! $perhitungan->b11 !!}</p>
</div>

<!-- B12 Field -->
<div class="form-group">
    {!! Form::label('b12', 'B12:') !!}
    <p>{!! $perhitungan->b12 !!}</p>
</div>

<!-- B13 Field -->
<div class="form-group">
    {!! Form::label('b13', 'B13:') !!}
    <p>{!! $perhitungan->b13 !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $perhitungan->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $perhitungan->updated_at !!}</p>
</div>

