<div class="table-responsive-sm">
    <table class="table table-striped" id="kondisiSistemRems-table">
        <thead>
            <th>Nama Kondisi</th>
        <!-- <th>Nilai</th> -->
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($kondisiSistemRems as $kondisiSistemRem)
            <tr>
                <td>{!! $kondisiSistemRem->nama_kondisi !!}</td>
            <!-- <td>{!! $kondisiSistemRem->nilai !!}</td> -->
                <td>
                    {!! Form::open(['route' => ['kondisiSistemRems.destroy', $kondisiSistemRem->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('kondisiSistemRems.show', [$kondisiSistemRem->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('kondisiSistemRems.edit', [$kondisiSistemRem->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>