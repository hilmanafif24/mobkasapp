<!-- Nama Kondisi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_kondisi', 'Nama Kondisi:') !!}
    {!! Form::text('nama_kondisi', null, ['class' => 'form-control']) !!}
</div>

<!-- Nilai Field -->
<div class="form-group col-sm-6">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    {!! Form::text('keterangan', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('kondisiSistemRems.index') !!}" class="btn btn-default">Cancel</a>
</div>
