<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $tipeModel->id !!}</p>
</div>

<!-- Nama Tipe M Field -->
<div class="form-group">
    {!! Form::label('nama_tipe', 'Nama Tipe:') !!}
    <p>{!! $tipeModel->nama_tipe !!}</p>
</div>

<!-- Id Model M Field -->
<div class="form-group">
    {!! Form::label('model_merek_id', 'Model:') !!}
    <p>{!! $tipeModel->model_merek_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $tipeModel->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $tipeModel->updated_at !!}</p>
</div>

