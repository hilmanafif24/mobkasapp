<div class="table-responsive-sm">
    <table class="table table-striped" id="tipeModels-table">
        <thead>
            <th>Nama Tipe</th>
        <th>Model</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($tipeModels as $tipeModel)
            <tr>
                <td>{!! $tipeModel->nama_tipe !!}</td>
            <td>{!! $tipeModel->get_model->nama_model !!}</td>
                <td>
                    {!! Form::open(['route' => ['tipeModels.destroy', $tipeModel->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('tipeModels.show', [$tipeModel->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('tipeModels.edit', [$tipeModel->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>