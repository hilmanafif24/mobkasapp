<!-- Nama Tipe M Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_tipe', 'Nama Tipe:') !!}
    {!! Form::text('nama_tipe', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Model M Field -->
<div class="form-group col-sm-6">
	<select class="form-control" id="modelSelect" name="model_merek_id">
    @foreach($models as $data)                              
      <option value="{{ $data->id }}">{{ $data->nama_model }}</option>
    @endforeach
	</select>
	<!-- {!! Form::label('model_merek', 'Model:') !!}
    {!! Form::select('model_merek_id', $models, null, ['class' => 'form-control']) !!} -->
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('tipeModels.index') !!}" class="btn btn-default">Cancel</a>
</div>
