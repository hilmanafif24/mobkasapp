<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<title>Mobkas App</title>
		<meta name="description" content="Sistem Estimasi Harga Jual Mobil Bekas">
		<meta name="keywords" content="mobkas app, mobil bekas">
    
    
    <!-- <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,700" rel="stylesheet"> -->

		<link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap.css') }}">
	    <link rel="stylesheet" href="{{ asset('css/animate.css')}}">
	    <link rel="stylesheet" href="{{ asset('fonts/ionicons/css/ionicons.min.css')}}">
	    
	    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
	    
	    <link rel="stylesheet" href="{{ asset('fonts/flaticon/font/flaticon.css')}}">

	    <link rel="stylesheet" href="{{ asset('fonts/fontawesome/css/font-awesome.min.css')}}">
	    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css')}}">
	    <link rel="stylesheet" href="{{ asset('css/select2.css')}}">
	    

	    <link rel="stylesheet" href="{{ asset('css/helpers.css')}}">
	    <link rel="stylesheet" href="{{ asset('css/style.css')}}">

      <style type="text/css">
        #result_mesin { background-color: #F0FFED; border: 0px solid #215800; padding: 0px; width: auto; margin-bottom: 0px; margin-top: -20px; font-size: 14px;}
        #result_rem { background-color: #F0FFED; border: 0px solid #215800; padding: 0px; width: auto; margin-bottom: 0px; margin-top: -20px; font-size: 14px;}
        #result_kemudi { background-color: #F0FFED; border: 0px solid #215800; padding: 0px; width: auto; margin-bottom: 0px; margin-top: -20px; font-size: 14px;}
        #result_suspensi { background-color: #F0FFED; border: 0px solid #215800; padding: 0px; width: auto; margin-bottom: 0px; margin-top: -20px; font-size: 14px;}
        #result_eksterior { background-color: #F0FFED; border: 0px solid #215800; padding: 0px; width: auto; margin-bottom: 0px; margin-top: -20px; font-size: 14px;}
        #result_interior { background-color: #F0FFED; border: 0px solid #215800; padding: 0px; width: auto; margin-bottom: 0px; margin-top: -20px; font-size: 14px;}
        #result_dokumen { background-color: #F0FFED; border: 0px solid #215800; padding: 0px; width: auto; margin-bottom: 0px; margin-top: -20px; font-size: 14px;}
      </style>
      <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->

	</head>

	<body>
  

    <nav class="navbar navbar-expand-lg navbar-dark probootstrap_navbar" id="probootstrap-navbar">
      <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">Mobkas App</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#probootstrap-menu" aria-controls="probootstrap-menu" aria-expanded="false" aria-label="Toggle navigation">
          <span><i class="ion-navicon"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="probootstrap-menu">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a class="nav-link" href="{{ route('home.index') }}">Home</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('dashboard') }}">Dashboard</a></li>
            <li class="nav-item"><a class="nav-link" href="services.html">About</a></li>
            <!-- <li class="nav-item"><a class="nav-link" href="travel.html">Help</a></li> -->
            <li class="nav-item"><a class="nav-link" href="contact.html">Contact</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- END nav -->
    

    <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('{{ asset('images/bg_1.jpg') }}');" data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container">
        <div class="row align-items-center">

          <div class="col-md" >
            <h2 class="heading mb-1 display-4 font-light probootstrap-animate probootstrap-cover-h2">Sistem Estimasi Harga Jual <br>Mobil Bekas</h2>
            <p class="lead mb-5 probootstrap-animate probootstrap-cover-p">Sebagai Penelitian Tugas Akhir <a href="#" target="_blank">Hilman Afif</a> NIM <a href="#" target="_blank">1147050074</a></p>
            <p class="probootstrap-animate">
              <a href="onepage.html" role="button" class="btn btn-primary p-3 mr-3 pl-5 pr-5 text-uppercase d-lg-inline d-md-inline d-sm-block d-block mb-3">Detail Info</a> 
            </p>
          </div> 

          <div class="col-md probootstrap-animate">          
            <!-- <form action="{{ route('home.proses_estimasi') }}" method="POST" class="probootstrap-form"> -->
              {!! Form::open(['route' => 'home.proses_estimasi', 'class' => 'probootstrap-form']) !!} 
              <div class="form-group">
                <!-- Kolom 1 Baris 1-->
                <div class="row mb-1">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single13">Merek</label>
                      <div class="probootstrap_select-wrap">
                        <label for="id_label_single13" style="width: 100%;">
                          {!! Form::select('merek_id',[''=>'--- Pilih Merek ---']+$mereks,null,['class'=>'js-example-basic-single js-states form-control','style'=>'width: 100%'])!!}
                         <!--  <input type="text" name="id_merek" class="form-control"> -->
                        </label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single5">Model</label>
                        <div class="probootstrap_select-wrap">
                          <label for="id_label_single5" style="width: 100%;">
                            {!! Form::select('model_merek_id',[''=>'--- Pilih Model ---'],null,['class'=>'js-example-basic-single js-states form-control','style'=>'width: 100%']) !!}
                          </label>
                        </div>
                    </div>
                  </div>

                </div>
                <!-- END row -->

                <!-- Kolom 1 Baris 2-->
                <div class="row mb-1">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single5">Tipe Model</label>
                        <div class="probootstrap_select-wrap">
                          <label for="id_label_single5" style="width: 100%;">
                            {!! Form::select('tipe_model_id',[''=>'--- Pilih Tipe ---'],null,['class'=>'js-example-basic-single js-states form-control','style'=>'width: 100%']) !!}
                          </label>
                        </div>
                    </div>
                  </div>

                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single7">Warna</label>
                        <div class="probootstrap_select-wrap">
                          <label for="id_label_single14" style="width: 100%;">
                            <select class="js-example-basic-single js-states form-control" id="id_label_single14" style="width: 100%;" name="warna_id" required="required">
                              @foreach($warnas as $data) 
                                <option value="{{ $data->id }}">{{ $data->nama_warna }}</option>
                              @endforeach
                            </select>
                          </label>
                        </div>
                    </div>
                  </div>

                  
                </div>
                <!-- END row -->

                <!-- Kolom 1 Baris 3-->
                <div class="row mb-1">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single9">Tahun</label>
                      <div class="probootstrap_select-wrap">
                        <label for="id_label_single9" style="width: 100%;">
                          <select class="js-example-basic-single js-states form-control" id="id_label_single9" style="width: 100%;" name="tahun" required="required">
                            <option value="2019">2019</option>
                            <option value="2018">2018</option>
                            <option value="2017">2017</option>
                            <option value="2016">2016</option>
                            <option value="2015">2015</option>
                            <option value="2014">2014</option>
                          </select>
                        </label>
                      </div>
                    </div>
                  </div>

                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single11">Tipe Transmisi</label>
                        <select class="js-example-basic-single js-states form-control" id="id_label_single7" style="width: 100%;" name="transmisi_id" required="required">
                          @foreach($transmisis as $data) 
                            <option value="{{ $data->id }}">{{ $data->nama_transmisi }}</option>
                          @endforeach
                        </select>
                    </div>
                  </div>                 
                </div>
                <!-- END row -->

                <!-- Kolom 1 Baris 4-->
                <div class="row mb-1"> 
                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single">Kondisi Mesin</label>
                        <div class="input-group mb-3">
                          <input id="isiMesin" type="text" class="form-control" placeholder="Pilih Kondisi" name="mesin_id" readonly>
                          
                          <div class="input-group-append">
                            <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#mesinModal">Pilih</button>
                          </div>
                        </div>
                      <div id="result_mesin"></div>
                    </div>
                  </div>

                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single2">Kondisi Sistem Rem</label>
                        <div class="input-group mb-3">
                          <input type="text" class="form-control" placeholder="Pilih Kondisi" id="isiRem" name="sis_rem_id" readonly>
                          
                          <div class="input-group-append">
                            <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#remModal">Pilih</button>
                          </div>
                        </div>
                        <div id="result_rem"></div>
                    </div>
                  </div>                 
                </div>
                
                <!-- Kolom 1 Baris 5 -->
                <div class="row mb-1">                  
                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single6">Kondisi Kemudi</label>
                          <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Pilih Kondisi" id="isiKemudi" name="kemudi_id" readonly>
                            
                            <div class="input-group-append">
                              <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#kemudiModal">Pilih</button>
                            </div>
                          </div>
                          <div id="result_kemudi"></div>
                    </div>
                  </div>

                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single6">Kondisi Suspensi</label>
                          <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Pilih Kondisi" id="isiSuspensi" name="suspensi_id" readonly>
                            
                            <div class="input-group-append">
                              <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#suspensiModal">Pilih</button>
                            </div>
                          </div>
                          <div id="result_suspensi"></div>
                    </div>
                  </div> 
                </div>
                <!-- END row -->

                <!-- Kolom 1 baris 6-->
                <div class="row mb-1">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single8">Kondisi Eksterior</label>
                      <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Pilih Kondisi" id="isiEksterior" name="eksterior_id" readonly>
                        
                        <div class="input-group-append">
                          <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#eksteriorModal">Pilih</button>
                        </div>
                      </div>
                      <div id="result_eksterior"></div>
                    </div>
                    </div>

                    <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single10">Kondisi Interior</label>
                        <div class="input-group mb-3">
                          <input type="text" class="form-control" placeholder="Pilih Kondisi" id="isiInterior" name="interior_id" readonly>
                          
                          <div class="input-group-append">
                            <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#interiorModal">Pilih</button>
                          </div>
                        </div>
                        <div id="result_interior"></div>
                    </div>
                  </div>                                
                </div>
                <!-- END row -->

                <!-- Kolom 1 Baris Terakhir -->
                <div class="row mb-1">
                  <div class="col-md">
                    <div class="form-group">
                      <label for="id_label_single12">Kondisi Dokumen</label>
                        <div class="input-group mb-3">
                          <input type="text" class="form-control" placeholder="Pilih Kondisi" id="isiDokumen" name="dokumen_id" readonly>
                          
                          <div class="input-group-append">
                            <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#dokumenModal">Pilih</button>
                          </div>
                        </div>
                      <div id="result_dokumen"></div>
                    </div>
                  </div>

                  <div class="col-md" style="padding-top: 34px">
                    <input type="submit" value="Estimasikan" class="btn btn-primary btn-block">
                  </div>
                </div>
              </div>

            </div>
            <!-- END row -->
            {!! Form::close() !!}
            <!-- </form> -->
            
          </div>
        </div>
      </div>
    
    </section>
    <!-- END section -->
    

    <section class="probootstrap_section" id="section-feature-testimonial">
      <div class="container">
        <div class="row justify-content-center mb-1s">
          <div class="col-md-12 text-center mb-5 probootstrap-animate">
            <h2 class="display-4 border-bottom probootstrap-section-heading">Implementasi Algoritma Regresi Linear Berganda Dalam Menentukan Estimasi Harga Jual Mobil Bekas</h2>
            <blockquote class="">
              <p class="lead mb-4"><em>Dengan melihat sifat-sifat kecenderungan harga jual mobil bekas dipengaruhi berbagai paremeter maka dapat disimpulkan bahwa harga jual mobil bekas dapat diestimasikan menggunakan data mining. Estimasi tersebut dapat dilakukan menggunakan metode regresi linear berganda karena terdapat variabel dependent dan lebih dari satu variabel independent. </em></p>
              <p class="probootstrap-author">
                <a href="https://uicookies.com/" target="_blank">
                  <img src="{{ asset('images/person_3.jpg') }}" alt="Free Template by uicookies.com" class="rounded-circle">
                  <span class="probootstrap-name">Hilman Afif</span>
                  <span class="probootstrap-title">Developer</span>
                </a>
              </p>
            </blockquote>
          </div>
        </div>   
      </div>
    </section>
    <!-- END section -->
    <section class="probootstrap_section">
      <div class="container">
        <div class="row text-center mb-5 probootstrap-animate">
          <div class="col-md-12">
            <h2 class="display-4 border-bottom probootstrap-section-heading">Layanan Aplikasi</h2>
          </div>
        </div>
      </div>
    </section>
        

    <section class="probootstrap-section-half d-md-flex" id="section-about">
      <div class="probootstrap-image probootstrap-animate" data-animate-effect="fadeIn" style="background-image: url('{{ asset('images/img_2.jpg')}}')"></div>
      <div class="probootstrap-text">
        <div class="probootstrap-inner probootstrap-animate" data-animate-effect="fadeInRight">
          <h2 class="heading mb-4">Manajemen Dataset</h2>
          <p>Pengelola memiliki halaman backend manajemen data. Melalui halaman teresbut seorang pengelola dapat memperbarui dan memanipulasi data..</p>
          <p>Data utama pada sistem adalah dataset mobil bekas yang telah dilakukan proses CRISP-DM. Dataset tersebut berelasi dengan data refernsional yang menerangkan lebih lengkap tetang sebuah data item.</p>
          <p><a href="#" class="btn btn-primary">Baca Selengkapnya</a></p>
        </div>
      </div>
    </section>


    <section class="probootstrap-section-half d-md-flex">
      <div class="probootstrap-image order-2 probootstrap-animate" data-animate-effect="fadeIn" style="background-image: url('{{ asset('images/img_3.jpg')}}')"></div>
      <div class="probootstrap-text order-1">
        <div class="probootstrap-inner probootstrap-animate" data-animate-effect="fadeInLeft">
          <h2 class="heading mb-4">Perhitungan Persamaan</h2>
          <p>Pengelola dapat melakukan perhitungan untuk mencari persamaan regresi linear berganda. Perhitungan dilakukan apabila terdapat pembaruan data.</p>
          <p>Perhitungan persamaan menggunakan operasi eliminasi dalam sistem telah teruji ketepatannya dalam mencari nilai koefisien dan konstanta terbaik.</p>
          <p><a href="#" class="btn btn-primary">Baca Selengkapnya</a></p>
        </div>
      </div>
    </section>
    <!-- END section -->

    <section class="probootstrap_section">
      <div class="container">
        <div class="row text-center mb-5 probootstrap-animate">
          <div class="col-md-12">
            <h2 class="display-4 border-bottom probootstrap-section-heading">Variabel Independent</h2>
          </div>
        </div>
        
        <div class="row probootstrap-animate">
          <div class="col-md-12">
            <div class="owl-carousel js-owl-carousel">
              <a class="probootstrap-slide" href="#">
                <span class="flaticon-teatro-de-la-caridad"></span>
                <em>Merek</em>
              </a>
              <a class="probootstrap-slide" href="#">
                <span class="flaticon-royal-museum-of-the-armed-forces"></span>
                <em>Model</em>
              </a>
              <a class="probootstrap-slide" href="#">
                <span class="flaticon-parthenon"></span>
                <em>Tipe</em>
              </a>
              <a class="probootstrap-slide" href="#">
                <span class="flaticon-marina-bay-sands"></span>
                <em>Warna</em>
              </a>
              <a class="probootstrap-slide" href="#">
                <span class="flaticon-samarra-minaret"></span>
                <em>Tahun</em>
              </a>
              <a class="probootstrap-slide" href="#">
                <span class="flaticon-chiang-kai-shek-memorial"></span>
                <em>Tipe Transmisi</em>
              </a>
              <a class="probootstrap-slide" href="#">
                <span class="flaticon-heuvelse-kerk-tilburg"></span>
                <em>Kondisi Mesin</em>
              </a>
              <a class="probootstrap-slide" href="#">
                <span class="flaticon-cathedral-of-cordoba"></span>
                <em>Kondisi Sistem Rem</em>
              </a>
              <a class="probootstrap-slide" href="#">
                <span class="flaticon-london-bridge"></span>
                <em>Kondisi Kemudi</em>
              </a>
              <a class="probootstrap-slide" href="#">
                <span class="flaticon-taj-mahal"></span>
                <em>Kondisi Suspensi</em>
              </a>
              <a class="probootstrap-slide" href="#">
                <span class="flaticon-leaning-tower-of-pisa"></span>
                <em>Kondisi Eksterior</em>
              </a>
              <a class="probootstrap-slide" href="#">
                <span class="flaticon-burj-al-arab"></span>
                <em>Kondisi Interior</em>
              </a>
              <a class="probootstrap-slide" href="#">
                <span class="flaticon-gate-of-india"></span>
                <em>Kondisi Dokumen</em>
              </a>
                           
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

    <!-- <section class="probootstrap_section bg-light">
      <div class="container">
        <div class="row text-center mb-5 probootstrap-animate">
          <div class="col-md-12">
            <h2 class="display-4 border-bottom probootstrap-section-heading">More Services</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">

            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url('{{ asset('images/img_1.jpg')}}')">
              </div>
              <div class="media-body">
                <h5 class="mb-3">01. Service Title Here</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
              </div>
            </div>

            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url('{{ asset('images/img_2.jpg')}}')">
              </div>
              <div class="media-body">
                <h5 class="mb-3">02. Service Title Here</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
              </div>
            </div>

          </div>
          <div class="col-md-6">
            
            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url('{{ asset('images/img_4.jpg')}}')">
              </div>
              <div class="media-body">
                <h5 class="mb-3">03. Service Title Here</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
              </div>
            </div>

            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url('{{ asset('images/img_5.jpg')}}')">
              </div>
              <div class="media-body">
                <h5 class="mb-3">04. Service Title Here</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section> -->
    <!-- END section -->


    <!--  <section class="probootstrap_section" id="section-feature-testimonial">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-12 text-center mb-5 probootstrap-animate">
            <h2 class="display-4 border-bottom probootstrap-section-heading">Testimonial</h2>
            <blockquote class="">
              <p class="lead mb-4"><em>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</em></p>
              <p class="probootstrap-author">
                <a href="https://uicookies.com/" target="_blank">
                  <img src="{{ asset('images/person_1.jpg')}}" alt="Free Template by uicookies.com" class="rounded-circle">
                  <span class="probootstrap-name">James Smith</span>
                  <span class="probootstrap-title">Chief Executive Officer</span>
                </a>
              </p>
            </blockquote>

          </div>
        </div>
        
      </div>
    </section> -->
    <!-- END section -->

    <!-- <section class="probootstrap_section bg-light">
      <div class="container">
        <div class="row text-center mb-5 probootstrap-animate">
          <div class="col-md-12">
            <h2 class="display-4 border-bottom probootstrap-section-heading">News</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">

            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url('{{ asset('images/img_1.jpg') }}')">
              </div>
              <div class="media-body">
                <span class="text-uppercase">January 1st 2018</span>
                <h5 class="mb-3">Travel To United States Without Visa</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                <p><a href="#">Read More</a></p>
              </div>
            </div>

            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url('{{ asset('images/img_2.jpg')}}')">
              </div>
              <div class="media-body">
                <span class="text-uppercase">January 1st 2018</span>
                <h5 class="mb-3">Travel To United States Without Visa</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                <p><a href="#">Read More</a></p>
              </div>
            </div>

          </div>
          <div class="col-md-6">
            
            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url('{{ asset('images/img_4.jpg')}}')">
              </div>
              <div class="media-body">
                <span class="text-uppercase">January 1st 2018</span>
                <h5 class="mb-3">Travel To United States Without Visa</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                <p><a href="#">Read More</a></p>
              </div>
            </div>

            <div class="media probootstrap-media d-flex align-items-stretch mb-4 probootstrap-animate">
              <div class="probootstrap-media-image" style="background-image: url('{{ asset('images/img_5.jpg')}}')">
              </div>
              <div class="media-body">
                <span class="text-uppercase">January 1st 2018</span>
                <h5 class="mb-3">Travel To United States Without Visa</h5>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                <p><a href="#">Read More</a></p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section> -->
    <!-- END section -->

    
    <!-- END section -->

    <footer class="probootstrap_section probootstrap-border-top">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-3">
            <h3 class="probootstrap_font-18 mb-3">Quick Links</h3>
            <ul class="list-unstyled">
              <li><a href="#" target="_blank">Teknik Informatika</a></li>
              <li><a href="#" target="_blank">UIN SGD Bandung</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <h3 class="probootstrap_font-18 mb-3">Kontak</h3>
            <ul class="list-unstyled">
              <li><a href="https://free-template.co" target="_blank">Email</a></li>
              <li><a href="https://free-template.co" target="_blank">Facebook</a></li>
              <li><a href="https://free-template.co" target="_blank">Instagram</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <h3 class="probootstrap_font-18 mb-3">Source Code</h3>
            <ul class="list-unstyled">
              <li><a href="https://free-template.co" target="_blank">Bitbucket</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <h3 class="probootstrap_font-18 mb-3">Quick Links</h3>
            <ul class="list-unstyled">
              <li><a href="https://free-template.co" target="_blank">Home</a></li>
              <li><a href="https://free-template.co" target="_blank">About</a></li>
              <li><a href="https://free-template.co" target="_blank">Services</a></li>
              <li><a href="https://free-template.co" target="_blank">Contact</a></li>
            </ul>
          </div>
        </div>
        <div class="row pt-5">
          <div class="col-md-12 text-center">
            <p class="probootstrap_font-14">&copy; 2019. All Rights Reserved. <br> Designed &amp; Developed by <a href="#" target="_blank">Hilman Afif</a></p>
            <p class="probootstrap_font-14">Demo Images: <a href="https://unsplash.com/" target="_blank">Unsplash</a></p>
          </div>
        </div>
      </div>
    </footer>

   
    <!-- Modal Kondisi Mesin-->
    <div class="modal fade" id="mesinModal" tabindex="-1" role="dialog" aria-labelledby="mesinModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="mesinModalLabel">Kondisi Mesin</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="mesinForm">
            {{ csrf_field() }}
            <div class="modal-body">
            
              <div class="row">
                <div class="col-md">
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[1]" checked>  Tenaga mesin maksimal</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[2]" checked>  Tidak ada bahan bakar atau oli yang bocor</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[3]" checked>  Mesin mudah dinyalakan</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[4]" checked>  Suara mesin halus (tidak ada suara-suara menggelitik dan detonasi)</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[5]" checked>  Putaran mesin normal</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[6]" checked>  Mesin bekerja normal pada waktu dipercepat</label>                  
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[7]" checked>  AC dingin</label><br>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[8]" checked>  Temperatur normal</label><br>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[9]" checked>  Sistem pelumasan  penuh dan tidak kotor</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[10]" checked>  Warna asap normal (Tidak berwarna putih atau hitam) serta ujung knalpot bersih</label>
                  <label for="round" class="mr-5"><input type="checkbox" id="row11" checked>  Perpindahan gigi berlangsung cepat</label>
                  <label for="round" class="mr-5"><input type="checkbox" id="row12" checked>  Tidak ada lampu indikator menyala</label>
                  <label for="round" class="mr-5"><input type="checkbox" id="row13" checked>  Getaran mesin wajar</label>
                  <label for="round" class="mr-5"><input type="checkbox" id="row14" checked>  Kepala silinder tidak melengkung</label>
                  <label for="round" class="mr-5"><input type="checkbox" id="row15" checked>  Timing belt tersambung dengan baik</label><br>
                  <label for="round" class="mr-5"><input type="checkbox" id="row16" checked>  Waterpump normal</label><br>
                  <label for="round" class="mr-5"><input type="checkbox" id="row17" checked>  Tidak ada knocking di mesin turbocharger</label>
                  <label for="round" class="mr-5"><input type="checkbox" id="row18" checked>  Mesin tidak mengeluarkan bau</label>
                  <label for="round" class="mr-5"><input type="checkbox" id="row19" checked>  Dapat menanjak dengan baik</label>
                  <label for="round" class="mr-5"><input type="checkbox" id="row20" checked disabled>  Bukan bekas terendam banjir</label>
                </div>
                <!--  <div class="custom-control custom-checkbox mb-3">
                   <input type="checkbox" class="custom-control-input" id="customCheck1" name="example1">
                   <label class="custom-control-label" for="customCheck1">Option 1</label>
                 </div> -->
              </div>   
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
        </div>
      </div>
    </div>
    
    <!-- Modal Sistem Rem -->
    <div class="modal fade" id="remModal" tabindex="-1" role="dialog" aria-labelledby="remModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="remModalLabel">Kondisi Sistem Rem</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="remForm">
            {{ csrf_field() }}
            <div class="modal-body">
            
              <div class="row">
                <div class="col-md">
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[1]" checked>  Pedal rem terasa padat tidak spongy (agak loss)</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[2]" checked>  Tekanan pedal rem penuh </label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[3]" checked>  Rem tidak membanting ke samping</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[4]" checked>  Pedal rem tidak keras</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[5]" checked>  Rem tidak macet pada satu roda</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[6]" checked>  Semua roda normal</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[6]" checked>  Rem tidak meluncur (blong)</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[6]" checked>  Tidak mengeluarkan bunyi-bunyi tidak wajar</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[6]" checked>  Tidak terjadi getaran tidak wajar</label>
                </div>
              </div>   
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
        </div>
      </div>
    </div>

    <!-- Modal Kemudi -->
    <div class="modal fade" id="kemudiModal" tabindex="-1" role="dialog" aria-labelledby="kemudiModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="kemudiModalLabel">Kondisi Kemudi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="kemudiForm">
            {{ csrf_field() }}
            <div class="modal-body">           
              <div class="row">
                <div class="col-md">
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[1]" checked>  Kemudi tidak berat</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[2]" checked>  Kemudi tidak menarik kesatu sisi</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[3]" checked>  Kemudi tidak bergetar</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[4]" checked>  Goncangan jalan tidak terasa pada kemudi</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[5]" checked>  Kemudi presisi (lurus)</label>
                </div>
              </div>   
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
        </div>
      </div>
    </div>

    <!-- Modal Suspensi -->
    <div class="modal fade" id="suspensiModal" tabindex="-1" role="dialog" aria-labelledby="remModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="remModalLabel">Kondisi Suspensi</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="suspensiForm">
            {{ csrf_field() }}
            <div class="modal-body">
            
              <div class="row">
                <div class="col-md">
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[1]" checked>  Kondisi fisik shockbreaker baik</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[2]" checked>  Pegas peredam kejut bebas kebocoran minyak pada rumahnya</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[3]" checked>  Dudukan dan bantalan karet pegas peredam kejut dalam keadaan baik</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[3]" checked>  Tidak terdapat bunyi abnormal</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[3]" checked>  Stabilisator dalam keadaan baik</label>
                </div>
              </div>   
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
        </div>
      </div>
    </div>

    <!-- Modal Eksterior -->
    <div class="modal fade" id="eksteriorModal" tabindex="-1" role="dialog" aria-labelledby="eksteriorModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="eksteriorModalLabel">Kondisi Eksterior</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="eksteriorForm">
            {{ csrf_field() }}
            <div class="modal-body">
            
              <div class="row">
                <div class="col-md">
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[1]" checked>  Tidak ada tanda bekas banjir</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[2]" checked>  Tidak ada tanda bekas terbakar</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[3]" checked>  Tidak ada tanda kecelakaan besar</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[4]" checked>  Panel body baik</label><br>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[5]" checked>  Bumper baik</label><br>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[6]" checked>  Pintu, Kap depan, Decklid, dan Tailgate baik</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[7]" checked>  Grille, Trim dan Rak di atap baik</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[8]" checked>  Kaca, Wiper Blade dan Spion baik</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[8]" checked>  Lampu Eksterior baik</label><br>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[8]" checked>  Ketebalan ban normal</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[8]" checked>  Velg dalam keadaan baik</label>
                </div>
              </div>   
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
        </div>
      </div>
    </div>

    <!-- Modal Interior -->
    <div class="modal fade" id="interiorModal" tabindex="-1" role="dialog" aria-labelledby="interiorModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="interiorModalLabel">Kondisi Interior</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="interiorForm">
            {{ csrf_field() }}
            <div class="modal-body">
            
              <div class="row">
                <div class="col-md">
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[1]" checked>  Airbag dan Sabuk Pengaman baik</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[2]" checked>  Sistem Audio dan Alarm baik</label><br>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[3]" checked>  Sistem AC baik</label><br>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[4]" checked>  Filter cabin baik</label><br>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[5]" checked>  Fitur interior baik</label><br>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[6]" checked>  Karpet dasar baik</label><br>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[7]" checked>  Karpet lantai baik</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[8]" checked>  Trim pintu dan panel pintu baik</label><br>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[9]" checked>  Jok Kursi baik</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[10]" checked>  Sunroof / Moonroof / Convertible Top baik (jika tersedia)</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[11]" checked>  Sistem jendela dan pintu baik</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[12]" checked>  Kondisi ruang bagasi baik</label><br>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[13]" checked>  Ban serep baik</label>
                </div>
              </div>   
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
        </div>
      </div>
    </div>

      <!-- Modal Dokumen -->
    <div class="modal fade" id="dokumenModal" tabindex="-1" role="dialog" aria-labelledby="dokumenModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="eksteriorModalLabel">Kondisi Dokumen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="dokumenForm">
            {{ csrf_field() }}
            <div class="modal-body">
            
              <div class="row">
                <div class="col-md">
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[1]" checked>  Berkas BPKB asli (nama asli pemiliknya)</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[2]" checked>  Berkas STNK asli</label><br>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[3]" checked>  Lembar pajak tersedia</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[4]" checked>  Faktur pembelian tersedia</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[5]" checked>  Form A (bagi mobil CBU) tersedia</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[6]" checked>  Service record / perawatan berkala tersedia</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[7]" checked>  Nomor rangka sesuai di dokumen</label>
                  <label for="round" class="mr-5"><input type="checkbox" name="check_list[7]" checked>  Nomor Mesin sesuai di dokumen</label>
                </div>
              </div>   
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
        </div>
      </div>
    </div>
    
    <script src="{{ asset('js/jquery.min.js')}}"></script>

    <script type="text/javascript">
      $(document).ready(function() {
            console.log("mulai");
      });

      $("select[name='merek_id']").change(function(){
          console.log('menampilkan model')
          var merek_id = $(this).val();
          var token = $("input[name='_token']").val();
          $.ajax({
              url: "<?php echo route('select-ajax') ?>",
              method: 'POST',
              data: {merek_id:merek_id, _token:token},
              success: function(data) {
                $("select[name='model_merek_id']").html('');
                $("select[name='model_merek_id']").html(data.options);
              }
          });
      });     
    </script>

    <script type="text/javascript">
      $("select[name='model_merek_id']").change(function(){
          console.log('menampilkan tipe')
          var model_merek_id = $(this).val();
          var token2 = $("input[name='_token']").val();
          $.ajax({
              url: "<?php echo route('select-ajax_2') ?>",
              method: 'POST',
              data: {model_merek_id:model_merek_id, _token:token2},
              success: function(data) {
                $("select[name='tipe_model_id']").html('');
                $("select[name='tipe_model_id']").html(data.options);
              }
          });
      });

      $(document).ready(function(){

        //Aksi simpan kondisi mesin
        $('#mesinForm').submit(function( event ) {
            console.log('kondisi mesin terpilih')
            event.preventDefault();
            var selected = new Array();
            //var token3 = $("input[name='_token']").val();
            // var row1 = $("input[name='row1]").val();
            $.ajax({
                url: "<?php echo route('select-mesin') ?>",
                type: 'POST',
                data: $('#mesinForm').serialize(),
                // data: $('myForm').serialize(),  Remember that you need to have your csrf token included
                // data: $('ajaxSubmit').serialize(),
                // dataType: 'html',
                success: function(response){
                  var dataM = "3";
                  var dataM2 = "2";
                  var dataM3 = "1";
                  var data = "Bagus";
                  var data2 = "Cukup";
                  var data3 = "Kurang";
                  // console.log(response)
                  // var values = $("input[name='check_list[]']").map(function(){
                  //   return $(this).val();
                  // }).get(); 
                  $('#mesinForm input[type=checkbox]:checked').each(function(){
                    selected.push(this.value);
                  })

                  if(selected.length > 0){
                    if (selected.length >= 20){
                      alert("Selected values:"+selected.length)
                      $('#isiMesin').val(dataM);
                      $('#result_mesin').html(data);
                    }else if(selected.length >= 19){
                      alert("Selected values:"+selected.length)
                      $('#isiMesin').val(dataM2);
                      $('#result_mesin').html(data2);
                    }else{
                      alert("Selected values:"+selected.length)
                      $('#isiMesin').val(dataM3);
                      $('#result_mesin').html(data3);
                    }
                    
                  }else{
                    alert("Anda belum memilih");
                  }
                  $('#mesinModal').modal('hide')
                  // alert("Data Selected");
                    // $("Bagus").html(data.input);
                    // $(".result").html(data);
                },
                error: function( _response ){
                    console.log(error)
                    alert("Data Not Selected");
                }
            });
            // return false;
        });
      });

      //Aksi simpan kondisi sistem rem
      $(document).ready(function(){
        $('#remForm').submit(function( event ) {
            console.log('submit kondisi rem')
            event.preventDefault();
            var selected = new Array();
            $.ajax({
                url: "<?php echo route('select-mesin') ?>",
                type: 'POST',
                data: $('#remForm').serialize(),
                success: function(response){
                  var dataR = "3";
                  var dataR2 = "2";
                  var dataR3 = "1";
                  var data = "Bagus";
                  var data2 = "Cukup";
                  var data3 = "Kurang";
                  $('#remForm input[type=checkbox]:checked').each(function(){
                    selected.push(this.value);
                  })
                  if(selected.length >0){
                    if (selected.length >= 9){
                      alert("Selected values:"+selected.length)
                      $('#isiRem').val(dataR);
                      $('#result_rem').html(data);
                    }else if(selected.length >= 8){
                      alert("Selected values:"+selected.length)
                      $('#isiRem').val(dataR2);
                      $('#result_rem').html(data2);
                    }else{
                      alert("Selected values:"+selected.length)
                      $('#isiRem').val(dataR3);
                      $('#result_rem').html(data3);
                    }   
                  }else{
                    alert("Anda belum memilih");
                  }
                  $('#remModal').modal('hide')
                },
                error: function(response){
                    console.log(error)
                    alert("Data Not Selected");
                }
            });
        });
      });

      //Aksi simpan kondisi kemudi
      $(document).ready(function(){
        $('#kemudiForm').submit(function( event ) {
            console.log('submit kondisi kemudi')
            event.preventDefault();
            var selected = new Array();
            $.ajax({
                url: "<?php echo route('select-mesin') ?>",
                type: 'POST',
                data: $('#kemudiForm').serialize(),
                success: function(response){
                  var dataK = "3";
                  var dataK2 = "2";
                  var dataK3 = "1";
                  var data = "Bagus";
                  var data2 = "Cukup";
                  var data3 = "Kurang";
                  $('#kemudiForm input[type=checkbox]:checked').each(function(){
                    selected.push(this.value);
                  })
                  if(selected.length > 0){
                    if (selected.length >= 5){
                      alert("Selected values:"+selected.length)
                      $('#isiKemudi').val(dataK);
                      $('#result_kemudi').html(data);
                    }else if(selected.length >= 4){
                      alert("Selected values:"+selected.length)
                      $('#isiKemudi').val(dataK2);
                      $('#result_kemudi').html(data2);
                    }else{
                      alert("Selected values:"+selected.length)
                      $('#isiKemudi').val(dataK3);
                      $('#result_kemudi').html(data3);
                    }   
                  }else{
                    alert("Anda belum memilih");
                  }
                  $('#kemudiModal').modal('hide')
                },
                error: function(response){
                    console.log(error)
                    alert("Data Not Selected");
                }
            });
        });
      });

      //Aksi simpan kondisi suspensi
      $(document).ready(function(){
        $('#suspensiForm').submit(function( event ) {
            console.log('submit kondisi suspensi')
            event.preventDefault();
            var selected = new Array();
            $.ajax({
                url: "<?php echo route('select-mesin') ?>",
                type: 'POST',
                data: $('#suspensiForm').serialize(),
                success: function(response){
                  var dataS = "3";
                  var dataS2 = "2";
                  var dataS3 = "1";
                  var data = "Bagus";
                  var data2 = "Cukup";
                  var data3 = "Kurang";
                  $('#suspensiForm input[type=checkbox]:checked').each(function(){
                    selected.push(this.value);
                  })
                  if(selected.length > 0){
                    if (selected.length >= 5){
                      alert("Selected values:"+selected.length)
                      $('#isiSuspensi').val(dataS);
                      $('#result_suspensi').html(data);
                    }else if(selected.length >= 4){
                      alert("Selected values:"+selected.length)
                      $('#isiSuspensi').val(dataS2);
                      $('#result_suspensi').html(data2);
                    }else{
                      alert("Selected values:"+selected.length)
                      $('#isiSuspensi').val(dataS3);
                      $('#result_suspensi').html(data3);
                    }   
                  }else{
                    alert("Anda belum memilih");
                  }
                  $('#suspensiModal').modal('hide')
                },
                error: function(response){
                    console.log(error)
                    alert("Data Not Selected");
                }
            });
        });
      });

      //Aksi simpan kondisi eksterior
      $(document).ready(function(){
        $('#eksteriorForm').submit(function( event ) {
            console.log('submit kondisi eksterior')
            event.preventDefault();
            var selected = new Array();
            $.ajax({
                url: "<?php echo route('select-mesin') ?>",
                type: 'POST',
                data: $('#eksteriorForm').serialize(),
                success: function(response){
                  var dataE = "3";
                  var dataE2 = "2";
                  var dataE3 = "1";
                  var data = "Bagus";
                  var data2 = "Cukup";
                  var data3 = "Kurang";
                  $('#eksteriorForm input[type=checkbox]:checked').each(function(){
                    selected.push(this.value);
                  })
                  if(selected.length > 0){
                    if (selected.length >= 11){
                      alert("Selected values:"+selected.length)
                      $('#isiEksterior').val(dataE);
                      $('#result_eksterior').html(data);
                    }else if(selected.length >= 10){
                      alert("Selected values:"+selected.length)
                      $('#isiEksterior').val(dataE2);
                      $('#result_eksterior').html(data2);
                    }else{
                      alert("Selected values:"+selected.length)
                      $('#isiEksterior').val(dataE3);
                      $('#result_eksterior').html(data3);
                    }   
                  }else{
                    alert("Anda belum memilih");
                  }
                  $('#eksteriorModal').modal('hide')
                },
                error: function(response){
                    console.log(error)
                    alert("Data Not Selected");
                }
            });
        });
      });

      //Aksi simpan kondisi interior
      $(document).ready(function(){
        $('#interiorForm').submit(function( event ) {
            console.log('submit kondisi interior')
            event.preventDefault();
            var selected = new Array();
            $.ajax({
                url: "<?php echo route('select-mesin') ?>",
                type: 'POST',
                data: $('#interiorForm').serialize(),
                success: function(response){
                  var dataI = "3";
                  var dataI2 = "2";
                  var dataI3 = "1";
                  var data = "Bagus";
                  var data2 = "Cukup";
                  var data3 = "Kurang";
                  $('#interiorForm input[type=checkbox]:checked').each(function(){
                    selected.push(this.value);
                  })
                  if(selected.length > 0){
                    if (selected.length >= 13){
                      alert("Selected values:"+selected.length)
                      $('#isiInterior').val(dataI);
                      $('#result_interior').html(data);
                    }else if(selected.length >= 12){
                      alert("Selected values:"+selected.length)
                      $('#isiInterior').val(dataI2);
                      $('#result_interior').html(data2);
                    }else{
                      alert("Selected values:"+selected.length)
                      $('#isiInterior').val(dataI3);
                      $('#result_interior').html(data3);
                    }   
                  }else{
                    alert("Anda belum memilih");
                  }
                  $('#interiorModal').modal('hide')
                },
                error: function(response){
                    console.log(error)
                    alert("Data Not Selected");
                }
            });
        });
      });

      //Aksi simpan kondisi dokumen
      $(document).ready(function(){
        $('#dokumenForm').submit(function( event ) {
            console.log('submit kondisi dokumen')
            event.preventDefault();
            var selected = new Array();
            $.ajax({
                url: "<?php echo route('select-mesin') ?>",
                type: 'POST',
                data: $('#dokumenForm').serialize(),
                success: function(response){
                  var dataD = "2";
                  var dataD2 = "1";
                  var data_1 = "Lengkap";
                  var data_2 = "Tidak Lengkap";
                  $('#dokumenForm input[type=checkbox]:checked').each(function(){
                    selected.push(this.value);
                  })
                  if(selected.length > 0){
                    if (selected.length >= 8){
                      alert("Selected values:"+selected.length)
                      $('#isiDokumen').val(dataD);
                      $('#result_dokumen').html(data_1);
                    }else{
                      alert("Selected values:"+selected.length)
                      $('#isiDokumen').val(dataD2);
                      $('#result_dokumen').html(data_2);
                    }   
                  }else{
                    alert("Anda belum memilih");
                  }
                  $('#dokumenModal').modal('hide')
                },
                error: function(response){
                    console.log(error)
                    alert("Data Not Selected");
                }
            });
        });
      });
    </script>
    
    <script src="{{ asset('js/popper.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/owl.carousel.min.js')}}"></script>

    <script src="{{ asset('js/bootstrap-datepicker.js')}}"></script>
    <script src="{{ asset('js/jquery.waypoints.min.js')}}"></script>
    <script src="{{ asset('js/jquery.easing.1.3.js')}}"></script>

    <script src="{{ asset('js/select2.min.js')}}"></script>

    <script src="{{ asset('js/main.js')}}"></script>
    <script src="{{ asset('js/copyright.js') }}"></script>
	</body>
</html>