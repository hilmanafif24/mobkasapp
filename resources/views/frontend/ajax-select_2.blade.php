<option>--- Pilih Tipe ---</option>
@if(!empty($tipes))
  @foreach($tipes as $key => $value)
    <option value="{{ $key }}">{{ $value }}</option>
  @endforeach
@endif