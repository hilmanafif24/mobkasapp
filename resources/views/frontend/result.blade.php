<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<title>MobkasApp: Hasil Estimasi</title>
		<meta name="description" content="Sistem Estimasi Harga Jual Mobil Bekas">
		<meta name="keywords" content="mobkas app, mobil bekas">
    
    
    <!-- <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,700" rel="stylesheet"> -->

		<link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap.css') }}">
	    <link rel="stylesheet" href="{{ asset('css/animate.css')}}">
	    <link rel="stylesheet" href="{{ asset('fonts/ionicons/css/ionicons.min.css')}}">
	    
	    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
	    
	    <link rel="stylesheet" href="{{ asset('fonts/flaticon/font/flaticon.css')}}">

	    <link rel="stylesheet" href="{{ asset('fonts/fontawesome/css/font-awesome.min.css')}}">
	    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css')}}">
	    <link rel="stylesheet" href="{{ asset('css/select2.css')}}">
	    

	    <link rel="stylesheet" href="{{ asset('css/helpers.css')}}">
	    <link rel="stylesheet" href="{{ asset('css/style.css')}}">

	</head>

	<body>
  

    <nav class="navbar navbar-expand-lg navbar-dark probootstrap_navbar" id="probootstrap-navbar">
      <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">Mobkas App</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#probootstrap-menu" aria-controls="probootstrap-menu" aria-expanded="false" aria-label="Toggle navigation">
          <span><i class="ion-navicon"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="probootstrap-menu">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a class="nav-link" href="{{ route('home.index') }}">Home</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ url('dashboard') }}">Dashboard</a></li>
            <li class="nav-item"><a class="nav-link" href="services.html">About</a></li>
            <li class="nav-item"><a class="nav-link" href="travel.html">Help</a></li>
            <li class="nav-item"><a class="nav-link" href="contact.html">Contact</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- END nav -->
    

    <section class="probootstrap-cover overflow-hidden relative"  style="background-image: url('{{ asset('images/bg_1.jpg') }}');" data-stellar-background-ratio="0.5"  id="section-home">
      <div class="overlay"></div>
      <div class="container">

        <div class="row align-items-center">
        	<div class="col-md probootstrap-animate">
            	<div class="jumbotron bg-light text-dark text-center">
	            	<h3 class="teks-gelap-3">Hasil Estimasi</h3>
	            	<p class="teks-gelap-1">
	            		<span style="padding: 4px;">{{ $mereks }}</span>
	            		<span style="padding: 4px;">{{ $models }}</span>
	            		<span style="padding: 4px;">{{ $tipes }}</span>
	            		<span style="padding: 4px;">{{ $tahun }}</span>
	            	</p>

                <h1 class="text-dark">Rp. {{ number_format($hargas*1000000, 0, ",",".") }}</h1>
                
	            	<!-- <p class="teks-gelap-1">Harga Terendah</p>
	            	<h2 class="text-dark">Rp. 200.000.000<span> -</span></h2> -->
	            	<!-- <p class="teks-gelap-1">Harga</p> 
                -->

            	</div>
          	</div>

	        <div class="col-md">
	            <h4 class="heading mb-2 display-7 font-light probootstrap-animate probootstrap-cover-h2">Perlu Diperhatikan</h4>
	            <div class="col-md probootstrap-animate">
		            <ul class="fa-ul text-light">
						<li><i class="fa-li fa fa-check-circle fa-2x"></i>Harga yang tertera berdasarkan kondisi mobil yang bagus bebas banjir dan tabrakan</li>
						<li><i class="fa-li fa fa-check-circle fa-2x"></i>Harga tersebut hanya sebagai referensi, selanjutnya dapat ditentukan sendiri oleh pengguna</li>
						<li><i class="fa-li fa fa-check-circle fa-2x"></i>Disarankan bagi pengguna (penjual dan showroom) untuk melakukan inspeksi ke teknisi ahli</li>
						<li><i class="fa-li fa fa-check-circle fa-2x"></i>Data mobil yan digunakan sebagai data traning telah melalui proses filtering</li>
					</ul>
				</div>
	        </div> 
          
        </div>
      </div>
    
    </section>
    <!-- END section -->
    

    <footer class="probootstrap_section probootstrap-border-top">
      <div class="container">
        <!--
        <div class="row mb-5">
          <div class="col-md-3">
            <h3 class="probootstrap_font-18 mb-3">Quick Links</h3>
            <ul class="list-unstyled">
              <li><a href="https://free-template.co" target="_blank">Home</a></li>
              <li><a href="https://free-template.co" target="_blank">About</a></li>
              <li><a href="https://free-template.co" target="_blank">Services</a></li>
              <li><a href="https://free-template.co" target="_blank">Contact</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <h3 class="probootstrap_font-18 mb-3">Quick Links</h3>
            <ul class="list-unstyled">
              <li><a href="https://free-template.co" target="_blank">Home</a></li>
              <li><a href="https://free-template.co" target="_blank">About</a></li>
              <li><a href="https://free-template.co" target="_blank">Services</a></li>
              <li><a href="https://free-template.co" target="_blank">Contact</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <h3 class="probootstrap_font-18 mb-3">Quick Links</h3>
            <ul class="list-unstyled">
              <li><a href="https://free-template.co" target="_blank">Home</a></li>
              <li><a href="https://free-template.co" target="_blank">About</a></li>
              <li><a href="https://free-template.co" target="_blank">Services</a></li>
              <li><a href="https://free-template.co" target="_blank">Contact</a></li>
            </ul>
          </div>
          <div class="col-md-3">
            <h3 class="probootstrap_font-18 mb-3">Quick Links</h3>
            <ul class="list-unstyled">
              <li><a href="https://free-template.co" target="_blank">Home</a></li>
              <li><a href="https://free-template.co" target="_blank">About</a></li>
              <li><a href="https://free-template.co" target="_blank">Services</a></li>
              <li><a href="https://free-template.co" target="_blank">Contact</a></li>
            </ul>
          </div>
        </div> -->
        <div class="row pt-1">
          <div class="col-md-12 text-center">
            <p class="probootstrap_font-14">&copy; 2019. All Rights Reserved. <br> Designed &amp; Developed by Hilman Afif<small>
            <!-- <p class="probootstrap_font-14">Demo Images: <a href="https://unsplash.com/" target="_blank">Unsplash</a></p> -->
          </div>
        </div>
      </div>
    </footer>

    
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    
    <script src="{{ asset('js/popper.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/owl.carousel.min.js')}}"></script>

    <script src="{{ asset('js/bootstrap-datepicker.js')}}"></script>
    <script src="{{ asset('js/jquery.waypoints.min.js')}}"></script>
    <script src="{{ asset('js/jquery.easing.1.3.js')}}"></script>

    <script src="{{ asset('js/select2.min.js')}}"></script>

    <script src="{{ asset('js/main.js')}}"></script>
	</body>
</html>