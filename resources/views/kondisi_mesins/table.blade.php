<div class="table-responsive-sm">
    <table class="table table-striped" id="kondisiMesins-table">
        <thead>
            <th>Nama Kondisi</th>
        <!-- <th>Nilai</th> -->
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($kondisiMesins as $kondisiMesin)
            <tr>
                <td>{!! $kondisiMesin->nama_kondisi !!}</td>
            <!-- <td>{!! $kondisiMesin->nilai !!}</td> -->
                <td>
                    {!! Form::open(['route' => ['kondisiMesins.destroy', $kondisiMesin->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('kondisiMesins.show', [$kondisiMesin->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('kondisiMesins.edit', [$kondisiMesin->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>