@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('kondisiMesins.index') !!}">Kondisi Mesin</a>
          </li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             @include('coreui-templates::common.errors')
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Edit Kondisi Mesin</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($kondisiMesin, ['route' => ['kondisiMesins.update', $kondisiMesin->id], 'method' => 'patch']) !!}

                              @include('kondisi_mesins.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection