<div class="table-responsive-sm">
    <table class="table table-striped" id="kondisiKemudis-table">
        <thead>
            <th>Nama Kondisi</th>
        <!-- <th>Nilai</th> -->
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($kondisiKemudis as $kondisiKemudi)
            <tr>
                <td>{!! $kondisiKemudi->nama_kondisi !!}</td>
            <!-- <td>{!! $kondisiKemudi->nilai !!}</td> -->
                <td>
                    {!! Form::open(['route' => ['kondisiKemudis.destroy', $kondisiKemudi->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('kondisiKemudis.show', [$kondisiKemudi->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('kondisiKemudis.edit', [$kondisiKemudi->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>