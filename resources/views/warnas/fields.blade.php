<!-- Nama Warna Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_warna', 'Nama Warna:') !!}
    {!! Form::text('nama_warna', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('warnas.index') !!}" class="btn btn-default">Cancel</a>
</div>
