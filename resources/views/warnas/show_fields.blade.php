<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $warna->id !!}</p>
</div>

<!-- Nama Warna Field -->
<div class="form-group">
    {!! Form::label('nama_warna', 'Nama Warna:') !!}
    <p>{!! $warna->nama_warna !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $warna->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $warna->updated_at !!}</p>
</div>

