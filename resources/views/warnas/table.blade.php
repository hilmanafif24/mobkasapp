<div class="table-responsive-sm">
    <table class="table table-striped" id="warnas-table">
        <thead>
            <th>Nama Warna</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($warnas as $warna)
            <tr>
                <td>{!! $warna->nama_warna !!}</td>
                <td>
                    {!! Form::open(['route' => ['warnas.destroy', $warna->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('warnas.show', [$warna->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('warnas.edit', [$warna->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>