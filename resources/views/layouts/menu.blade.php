<li class="nav-item {{ Request::is('dashboard*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('dashboard.index') !!}">
        <i class="nav-icon fa fa-tachometer" aria-hidden="true"></i>
        <span>Dashboard</span>
    </a>
</li>

<li class="nav-item {{ Request::is('home*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('home.index') !!}">
        <i class="nav-icon fa fa-arrow-left" aria-hidden="true"></i>
        <span>Back To Home</span>
    </a>
</li>

<li class="nav-title">Data Utama</li>

<li class="nav-item {{ Request::is('mobilBekas*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('mobilBekas.index') !!}">
        <i class="nav-icon fa fa-car" aria-hidden="true"></i>
        <span>Mobil Bekas</span>
    </a>
    <a class="nav-link" href="{!! route('perhitungans.index') !!}">
        <i class="nav-icon fa fa-money" aria-hidden="true"></i>
        <span>Perhitungan Regresi</span>
    </a>
    <a class="nav-link" href="{!! route('laporans.index') !!}">
        <i class="nav-icon fa fa-book" aria-hidden="true"></i>
        <span>Laporan</span>
    </a>
</li>

<li class="nav-title">Kelola Referensi</li>

<li class="nav-item {{ Request::is('mereks*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('mereks.index') !!}">
        <i class="nav-icon fa fa-briefcase" aria-hidden="true"></i>
        <span>Merek</span>
    </a>
</li>
<li class="nav-item {{ Request::is('modelMobkas*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('modelMereks.index') !!}">
        <i class="nav-icon fa fa-flag" aria-hidden="true"></i>
        <span>Model</span>
    </a>
</li>
<li class="nav-item {{ Request::is('tipeModels*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('tipeModels.index') !!}">
        <i class="nav-icon fa fa-flag" aria-hidden="true"></i>
        <span>Tipe</span>
    </a>
</li>
<li class="nav-item {{ Request::is('warnas*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('warnas.index') !!}">
        <i class="nav-icon fa fa-paint-brush" aria-hidden="true"></i>
        <span>Warna</span>
    </a>
</li>
<li class="nav-item {{ Request::is('tipeTransmisis*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('tipeTransmisis.index') !!}">
        <i class="nav-icon fa fa-road"></i>
        <span>Tipe Transmisi</span>
    </a>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#">
      <i class="nav-icon fa fa-cogs"></i> Kondisi
    </a>
    <ul class="nav-dropdown-items">

        <li class="nav-item {{ Request::is('kondisiMesins*') ? 'active' : '' }}">
            <a class="nav-link" href="{!! route('kondisiMesins.index') !!}">
                <i class="nav-icon fa fa-cog" aria-hidden="true"></i>
                <span>Kondisi Mesin</span>
            </a>
        </li>

        <li class="nav-item {{ Request::is('kondisiSistemRems*') ? 'active' : '' }}">
            <a class="nav-link" href="{!! route('kondisiSistemRems.index') !!}">
                <i class="nav-icon fa fa-wrench" aria-hidden="true"></i>
                <span>Kondisi Sistem Rem</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('kondisiKemudis*') ? 'active' : '' }}">
            <a class="nav-link" href="{!! route('kondisiKemudis.index') !!}">
                <i class="nav-icon fa fa-support" aria-hidden="true"></i>
                <span>Kondisi Kemudi</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('kondisiSuspensis*') ? 'active' : '' }}">
            <a class="nav-link" href="{!! route('kondisiSuspensis.index') !!}">
                <i class="nav-icon fa fa-microchip" aria-hidden="true"></i>
                <span>Kondisi Suspensi</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('kondisiEksteriors*') ? 'active' : '' }}">
            <a class="nav-link" href="{!! route('kondisiEksteriors.index') !!}">
                <i class="nav-icon fa fa-image" aria-hidden="true"></i>
                <span>Kondisi Eksterior</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('kondisiInteriors*') ? 'active' : '' }}">
            <a class="nav-link" href="{!! route('kondisiInteriors.index') !!}">
                <i class="nav-icon fa fa-image" aria-hidden="true"></i>
                <span>Kondisi Interior</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('kondisiDokumens*') ? 'active' : '' }}">
            <a class="nav-link" href="{!! route('kondisiDokumens.index') !!}">
                <i class="nav-icon fa fa-newspaper-o" aria-hidden="true"></i>
                <span>Kondisi Dokumen</span>
            </a>
        </li>

    </ul>
</li>




