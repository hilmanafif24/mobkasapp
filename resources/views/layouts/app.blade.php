<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{config('app.name')}}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 4.1.1 -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap-datepicker-417.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/coreui.min.css')}}">
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="{{ asset('css/coreui-icons.min.css') }}"> -->
    <!-- <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"> -->
    <!-- <link href="{{ asset('css/simple-line-icons.css')}}" rel="stylesheet"> -->
    <link rel="stylesheet" href="{{ asset('fonts/ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{ asset('fonts/fontawesome/css/font-awesome.min.css')}}">
    <!-- <link rel="stylesheet" href="{{ asset('css/flag-icon.min.css') }}"> -->
    <style type="text/css">
         #gagal_hitung { background-color: #fee2e1; border: 0px solid #215800; padding: 0px; width: auto; margin-bottom: 0px; font-size: 12px;}
    </style>
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand text-success" href="{{ route('dashboard.index') }}"> Mobkas App
        <!-- <img class="navbar-brand-full" src="" width="30" height="30"
             alt="Infyom Logo">
        <img class="navbar-brand-minimized" src="" width="30"
             height="30" alt="Infyom Logo"> -->
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="nav navbar-nav ml-auto">
        <!-- <li class="nav-item d-md-down-none">
            <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Aktivitas Baru">
                <a class="nav-link" href="#">
                    <i class="fa fa-bell" aria-hidden="true"></i>
                    <span class="badge badge-pill badge-danger" >1</span>
                </a>
            </span>
        </li> -->
        <li class="nav-item dropdown">
            <a class="nav-link" style="margin-right: 10px" data-toggle="dropdown" href="#" role="button"
               aria-haspopup="true" aria-expanded="false">
                {!! Auth::user()->name !!}
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <!--<div class="dropdown-header text-center">
                    <strong>Account</strong>
                </div>
                 <a class="dropdown-item" href="#">
                    <i class="fa fa-envelope-o"></i> Messages
                    <span class="badge badge-success">42</span>
                </a> -->
                <div class="dropdown-header text-center">
                    <strong>Settings</strong>
                </div>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-user"></i> Profile</a>
                <a class="dropdown-item" href="#">
                    <i class="fa fa-wrench"></i> Settings</a>
                <div class="dropdown-divider"></div>
                <!-- <a class="dropdown-item" href="#">
                    <i class="fa fa-shield"></i> Lock Account</a> -->
                <a class="dropdown-item" href="{!! url('/logout') !!}" class="btn btn-default btn-flat"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="fa fa-lock"></i>Logout
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </li>
    </ul>
</header>

<div class="app-body">
    @include('layouts.sidebar')
    <main class="main">
        @yield('content')
    </main>
</div>
<footer class="app-footer">
    <div>
        <a href="#" class="text-success">Mobkas App </a>
        <span>&copy; 2019 Hilman Afif.</span>
    </div>
    <div class="ml-auto">
        <span>Powered by</span>
        <a href="https://coreui.io">CoreUI</a>
    </div>
</footer>
</body>
<!-- jQuery 3.1.1 -->
<!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script> -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/coreui.min.js') }}"></script>
<script src="{{ asset('js/copyright.js') }}"></script>
<!-- <script src="https://unpkg.com/@coreui/coreui/dist/js/coreui.min.js"></script> -->
@yield('scripts')

</html>
